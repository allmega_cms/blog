/**
 * Read or get Video(file) and play this in Modal
 */

import {Events} from './Events'

export default class Player
{ 
    #videoBtn = '.allmega-play-video'
    #fileBtn = '.allmega-play-file'
    #type = 'video/mp4'
    #autoplay = true
    #controls = true
    #height = '100%'
    #width = '100%'
    #size = 'lg'
    #source

    constructor()
    {
        if ($(this.#videoBtn).length || $(this.#fileBtn).length) this.#registerEvents()
    }

    #registerEvents()
    {
        let self = this
        $(document).on('click', this.#fileBtn, function (event) {self.#showFile(event, $(this))})
        $(document).on('click', this.#videoBtn, function (event) {self.#show(event, $(this))})
        $(document).on(Events.APP_PLAYER_FILE_SHOW, (event) => {this.#showFile(event.elem)})
        $(document).on(Events.APP_PLAYER_VIDEO_SHOW, (event) => {this.#show(event.elem)})
    }

    #setData (elem)
    {
        this.#source = elem.data('src')
        this.#type = elem.data('type') || this.#type
        this.#size = elem.data('size') || this.#size
        this.#width = elem.data('width') || this.#width
        this.#height = elem.data('height') || this.#height
        this.#autoplay = elem.data('autoplay') || this.#autoplay
        this.#controls = elem.data('controls') || this.#controls
    }

    #show(event, elem = null)
    {
        if (elem) this.#setData(elem)

        let source = $('<source>').attr({'src': this.#source, 'type': this.#type})
        let video = $('<video>')
            .attr({
                'controls': this.#controls,
                'autoplay': this.#autoplay,
                'height': this.#height,
                'width': this.#width
            }).append(source)

        let modalData = {size: this.#size, content: video}
        $(document).trigger({type: Events.APP_MODAL_SHOW, modalData})
    }

    #showFile(event, link)
    {
        event.preventDefault()

        $(document).trigger(Events.APP_LOADER_SHOW)

        let url  = link.attr('href')
        let size = link.data('size')

        $.get(url, (response) => {
            $(document).trigger(Events.APP_LOADER_HIDE)
            this.#size = response.size || size || this.#size
            this.#source = response.content
            this.#show(event)
        })
    }
}