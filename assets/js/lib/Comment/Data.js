export default class Data
{
    #entityName
    #entityId
    #parentId
    #message

    getEntityName()
    {
        return this.#entityName
    }

    setEntityName(name)
    {
        this.#entityName = name
        return this
    }

    getEntityId()
    {
        return this.#entityId
    }

    setEntityId(id)
    {
        this.#entityId = id
        return this
    }

    getParentId()
    {
        return this.#parentId
    }

    setParentId(id)
    {
        this.#parentId = id
        return this
    }

    getMessage()
    {
        return this.#message
    }

    setMessage(message)
    {
        this.#message = message
        return this
    }

    getData()
    {
        return {
            entityName: this.#entityName,
            entityId: this.#entityId,
            parentId: this.#parentId,
            message: this.#message,
        }
    }
}