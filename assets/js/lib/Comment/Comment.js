import {Events} from '../Events'
import Data from './Data'

export default class Comment
{
    #sliderArrowClass = '.allmega-slider-arrow'
    #sliderContent = '.allmega-slider-content'
    #commentCancelId = '#allmega-cancel-comment'
    #commentNewId = 'allmega-add-new-comment'
    #commentListId = '#allmega-comments-root'
    #commentDataId = '#allmega-comment-data'
    #allmegaReplyClass = 'a.allmega-reply'
    #newCommentId = '#allmega-new-comment'
    #sliderBodyId = '.allmega-slider-body'
    #allmegaBtn = 'button.allmega-btn'
    #loaderImg = '.allmega-loader-img'
    #fas = 'fa-solid fa-angle-'
    #commentData
    #form
    #url

    constructor()
    {
        let elem = $(this.#commentDataId)
        if (elem.length) {
            $(this.#commentListId).find(this.#sliderBodyId).slideDown()
            this.#setCommentData(elem).#registerEvents()
            this.#url = elem.data('url')
        }
    }

    #setCommentData(elem)
    {
        this.#commentData = new Data()
            .setEntityName(elem.data('entityname'))
            .setEntityId(elem.data('entityid'))

        return this
    }

    #registerEvents()
    {
        let self = this
        let newCommentReplyIds = '#' + this.#commentNewId + ', ' + this.#allmegaReplyClass

        $(document).on('click', newCommentReplyIds, function (event) {self.showForm(event, $(this))})
        $(document).on('click', this.#commentCancelId, () => this.#removeForm())
        $(document).on('click', this.#allmegaBtn, () => this.#send())
    }

    #createForm()
    { 
        this.#removeForm()
        this.#form = $(this.#newCommentId).clone()
    }

    #removeForm()
    {
        if (this.#form) this.#form.remove()
        this.#commentData.setMessage('')
    }

    showForm(event, elem)
    {
        event.preventDefault()
        this.#createForm()

        if ($(elem).attr('id') == this.#commentNewId) {
            this.#form.prependTo(this.#commentListId)
            this.#commentData.setParentId('')
        } else {
            let parentComment = $(elem).parent().parent().parent()
            this.#commentData.setParentId(parentComment.attr('id'))
            parentComment.after(this.#form)
        }

        this.#form.find('.alert').hide()
        this.#form.show()

        let icon = this.#form.find(this.#sliderArrowClass).first().children()
        let arrow = icon.attr('class') == this.#fas + 'up' ? 'down' : 'up'
        icon.attr('class', this.#fas + arrow)
    }

    #send()
    {
        let message = this.#form.find('textarea').val()
        if (!message) return

        $(document).trigger(Events.APP_LOADER_SHOW)
        this.#commentData.setMessage(message)

        this.#form.find('button').hide().next().show()
        this.#form.find('.alert').text('').hide()

        $.post(this.#url, this.#commentData.getData(), (data) => {
            if (data.error) {
                this.#form.find('.alert').text(data.error).show()
                this.#form.find('button').show().next().hide()
            } else {
                this.#formToComment()
            }
            $(document).trigger(Events.APP_LOADER_HIDE)
        })
    }

    #formToComment()
    {
        this.#form.find(this.#sliderContent).text(this.#commentData.getMessage())
        this.#form.find(this.#commentCancelId).remove()
        this.#form.find(this.#loaderImg).remove()
        this.#form.find('button').remove()
        this.#form.removeAttr('id')
        this.#form = null
    }
}