export default class DateExtender
{
    constructor()
    {
        this
            .#addGetLocalFormattedDate()
            .#addGetInputFormattedDate()
            .#addWeekNumberExt()
            .#addDatesLineExt()
            .#addDatesEqual()
            .#addFormatDate()
            .#addDaysExt()
    }

    #addWeekNumberExt()
    {
        if (!Date.prototype.getWeekNumber) {
            Date.prototype.getWeekNumber = function() {
                 let d = new Date(Date.UTC(this.getFullYear(), this.getMonth(), this.getDate()))
                  let dayNum = d.getUTCDay() || 7
                  d.setUTCDate(d.getUTCDate() + 4 - dayNum)
                  let yearStart = new Date(Date.UTC(d.getUTCFullYear(),0,1))
                  return Math.ceil((((d - yearStart) / 86400000) + 1)/7)
            }
        }
        return this
    }

    #addDaysExt()
    {
        if (!Date.prototype.addDays) {
            Date.prototype.addDays = function(days) {
                let date = new Date(this.valueOf())
                date.setDate(date.getDate() + days)
                return date
            }
        }
        return this
    }

    #addFormatDate()
    {
        if (!Date.prototype.formatDate) {
            Date.prototype.formatDate = function (full = true, options = {}, locale = 'de-DE') {
                let defaultOptions = {dateStyle: 'short', timeStyle: 'short'}
                options = Object.assign(defaultOptions, options) 
                let date = this.toLocaleString(locale, defaultOptions)
                return full ? date : date.slice(0, 8)
            }
        }
        return this
    }

    #addGetLocalFormattedDate()
    {
        if (!Date.prototype.getLocalFormattedDate) {
            Date.prototype.getLocalFormattedDate = function (options = {}) {
                let defaultOptions = {
                    weekday: 'long',
                    year: 'numeric',
                    month: 'long',
                    day: 'numeric',
                }
                
                let locale = 'de-DE'
                Object.assign(defaultOptions, options)
                return this.toLocaleDateString(locale, defaultOptions)
            }
        }
        return this
    }

    #addGetInputFormattedDate()
    {
        if (!Date.prototype.getInputFormattedDate) {
            Date.prototype.getInputFormattedDate = function () {
                return this.toISOString().slice(0, 10)
            }
        }
        return this
    }

    #addDatesLineExt()
	{
        if (!Date.prototype.getDatesLine) {
            Date.prototype.getDatesLine = function(date, full = true) {
                let begin = this.formatDate(full)
                let finish = date ? date.formatDate(full) : begin

                if (full) {
                    if (this.formatDate(false) === date.formatDate(false)) {
                        let phrase = this.getLocalFormattedDate()
                        let timeStyle = {timeStyle: 'short'}
                        let locale = 'de-DE'
                        let beginTime = this.toLocaleTimeString(locale, timeStyle)
                        let finishTime = date.toLocaleTimeString(locale, timeStyle)
                        return phrase + ' ' + beginTime + ' - ' + finishTime
                    }
                }
                return begin == finish ? begin : begin + ' - ' + finish
            }
        }
        return this
	}

    #addDatesEqual()
    {
        if (!Date.prototype.isEqual) {
            Date.prototype.isEqual = function(date) {
                return this.formatDate(false) === date.formatDate(false)
            }
        }
        return this
    }
}