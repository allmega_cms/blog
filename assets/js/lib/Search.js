import {Events} from "./Events"

String.prototype.render = function (parameters) {
    return this.replace(/(% (\w+) %)/g, (match, pattern, name) => parameters[name])
}

export default class Search
{
    #resultsClass = '.allmega-search-results'
    #contentClass = '.allmega-search-content'
    #inputClass = '.allmega-search-field'
    #iconClass = '.allmega-search-icon'
    #formClass = '.allmega-search-form'
    #emptyResultsMessage
    #currentForm

    constructor()
    {
        if ($(this.#iconClass).length) this.#registerEvents()
    }

    #registerEvents()
    {
        let self = this
        $(document).on('submit', this.#formClass + ' > form', function (event) {self.search(event, $(this))})
        $(document).on('click', this.#iconClass, function (event) {self.showSearch(event, $(this))})
        $(document).on('hidden.bs.modal', () => this.#currentForm = null)
    }

    #showModal(modalData)
    {
        $(document).trigger({type: Events.APP_MODAL_SHOW, modalData})
    }

    showSearch(event, elem)
    {
        event.preventDefault()

        this.#currentForm = elem.parent().find(this.#contentClass).clone()
        this.#emptyResultsMessage = this.#currentForm.removeClass('d-none').data('empty-results-message')
        this.#showEmptyResults().#showModal({size: elem.data('size'), content: this.#currentForm})
    }

    #showEmptyResults()
    {
        let p = $('<p>').addClass('text-muted').text(this.#emptyResultsMessage)
        this.#currentForm.find(this.#resultsClass).html(p)
        return this
    }

    search(event, elem)
    {
        event.preventDefault()

        let form = elem.find(this.#inputClass).closest('form')
        let data = form.serializeArray()
        let url = form.attr('action')

        $(document).trigger(Events.APP_LOADER_SHOW)

        $.getJSON(url, data, (response) => {
            $(document).trigger(Events.APP_LOADER_HIDE)
            if (response.data.error) {
                let p = $('<p>').addClass('text-danger').text(response.data.error)
                elem.closest(this.#contentClass).find(this.#resultsClass).html(p)
            } else {
                this.#showResults(response.data)
                $(document).trigger(Events.APP_INIT_TOOLTIP)
            }
        })
    }

    #showResults(data)
    {
        if (!data.items.length) {
            this.#showEmptyResults()
            return
        }

        let resultsList = $(this.#resultsClass).empty()
        if (data.title) {
            let title = $('<p>').addClass('fw-bold').text(data.title)
            resultsList.append(title)
        }
        data.items.forEach((item) => resultsList.append(data.template.render(item)))
    }
}