export default class Slider
{
    #showInfoBtnId = '.allmega-show-infobtn'
    #folderMenu = 'allmega-folder-menu'
    #title = '.allmega-slider-title'
    #arrow = 'allmega-slider-arrow'
    #body = '.allmega-slider-body'
    #listId = '.allmega-root-list'
    #infoBtn = '.allmega-info-btn'
    #infoBox = '.allmega-info-box'
    #infoBtnShowed = true
    #firstOpen = false

    constructor()
    {
        this
            .#openFirstElementList()
            .#showEnabledInfoIcons()
            .#registerEvents()
    }

    setFirstOpen(open)
    {
        this.#firstOpen = open
        return this
    }

    #openFirstElementList()
    {
        let elems = $(this.#listId)
        if (elems.length && this.#firstOpen) {
            for(let elem of elems) {
                let li = elem.children().first()

                let icon = li.find('.' + this.#arrow).children().first()
                this.#setIcon(icon)

                let infoBtn = li.find(this.#infoBtn).first()
                if (!this.#infoBtnShowed && infoBtn) this.#showInfoBtn(infoBtn)

                li.find(this.#body).first().slideToggle()
            }
        }
        return this
    }

    #showEnabledInfoIcons()
    {
        let infoBtns = $(this.#showInfoBtnId)

        if (infoBtns.length) {
            for (let elem of infoBtns) elem.find(this.#infoBtn).show()
        }

        if (this.#infoBtnShowed) $(this.#infoBtn).show()
        return this
    }

    #registerEvents()
    {
        let self = this
        $(document).on('click', this.#title + ', .' + this.#arrow, function (event) {self.handle(event, $(this))})
        $(document).on('click', this.#infoBtn, function (event) {self.showInfos(event, $(this))})
    }

    #setIcon(icon, type = 'angle-')
    {
        let fas = 'fa-solid fa-' + type
        switch (type) {
            case 'angle-':
                fas += icon.attr('class') == fas + 'up' ? 'down' : 'up'
                break
            case 'folder':
                fas += icon.attr('class') == fas ? '-open' : ''
                break
            default:
                console.log(fas + ' was not found!')
        }

        icon.attr('class', fas)
        return this
    }

    #showInfoBtn(elem)
    {
        let infoBtn = elem.parent().parent().find(this.#infoBtn)
        if (infoBtn) infoBtn.toggle()
    }

    showInfos(event, elem)
    {
        let area = elem.parent().find(this.#infoBox).slideToggle()
        window.setTimeout(() => area.slideToggle(), 5000)
    }

    handle(event, elem)
    {
        let type = elem.hasClass(this.#folderMenu) ? 'folder' : 'angle-'
        let icon = elem.hasClass(this.#folderMenu) || elem.hasClass(this.#arrow) ? elem.children() : elem.prev('.' + this.#arrow).children()
        
        !this.#infoBtnShowed ? this.#setIcon(icon, type).#showInfoBtn(elem) : this.#setIcon(icon, type)

        elem.hasClass(this.#folderMenu) ?
            elem.parent().find('ul').first().slideToggle() : 
            elem.parent().parent().next().slideToggle()
    }
}