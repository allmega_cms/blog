export class Events
{
    static APP_FORM_REGISTER_SUBMIT = 'app.form.register.submit'
    static APP_LINK_BEFORE_REDIRECT = 'app.link.before.redirect'
    static APP_FORM_DELETE_BEFORE = 'app.form.delete.before'
    static APP_FORM_SEND_BEFORE = 'app.form.send.before'
    static APP_PLAYER_VIDEO_SHOW = 'app.player.video.show'
    static APP_PLAYER_FILE_SHOW = 'app.player.file.show'
    static APP_LINK_BEFORE_LOAD = 'app.link.before.load'
    static APP_INIT_FLATPICKER = 'app.init.flatpicker'
    static APP_CONTENT_LOADED = 'app.content.loaded'
    static APP_INIT_TOOLTIP = 'app.init.tooltip'
    static APP_LOADER_HIDE = 'app.loader.hide'
    static APP_LOADER_SHOW = 'app.loader.show'
    static APP_MODAL_SHOW = 'app.modal.show'
}