export default class PopupBox
{
    #popupBtn = '.allmega-popup-btn'
    #popupBox = '.allmega-popup-box'

    constructor()
    {
        if ($(this.#popupBtn).length) this.#registerEvents()
    }

    #registerEvents() {
        let self = this
        $(document).on('click', this.#popupBtn, function (event) {self.#handleBox(event, $(this))})
    }

    #handleBox(event, elem) {
        elem.next().toggle()
    }
}