export default class SlideMenu
{
    #navAreaCloseId = '#allmega-slide-bars-close'
    #navAreaId = '#allmega-slide-navarea'
    #barsId = '#allmega-slide-bars'
    #menuWidth = 350

    constructor()
    {
        let elem = $(this.#barsId)
        if (elem.length) this.#registerEvents()
    }

    #registerEvents()
    {
        $(document).on('click', this.#navAreaCloseId, () => this.#hideMenu())
        $(document).on('click', this.#barsId, () => this.#handleMenu())
    }

    #handleMenu()
    {
        let nav = $(this.#navAreaId)
        nav.width() == 0 ? nav.width(this.#menuWidth + 'px') : nav.width(0)
    }

    #hideMenu()
    {
        $(this.#navAreaId).width(0)
    }
}