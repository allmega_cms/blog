import {German} from 'flatpickr/dist/l10n/de.js'
import flatpickr from 'flatpickr'
import {Events} from './Events'

export default class Picker
{
    #dateTimePicker =  '.allmega-datetime-picker'
    #datePicker = '.allmega-date-picker'
    #options
    
    constructor()
    {
        this.#registerEvents()
    }

    #registerEvents()
    {
        $(document).on(Events.APP_INIT_FLATPICKER, (event) => this.#init(event))
    }

    #init(event)
    {
        this.#options = {}
        Object.assign(this.#options, this.#getDefaultOptions(), event?.options)
        this.#initDatePicker(event).#initDatetimePicker(event)
    }

    #getDefaultOptions()
    {
        return {date: null, datePicker: true, datetimePicker: true}
    }

    #initDatePicker()
    {
        if (this.#options.datePicker && $(this.#datePicker).length) {
            flatpickr(this.#datePicker, this.#getDateDefaultOptions())
        }
        return this
    }

    #initDatetimePicker()
    {
        if (this.#options.datetimePicker && $(this.#dateTimePicker).length) {
            let options = Object.assign(this.#getDateDefaultOptions(), this.#getTimeDefaultOptions())
            let fp = flatpickr(this.#dateTimePicker, options)
            this.#setInputs(fp)
        }
    }

    #getDateDefaultOptions()
    {
        return {
            locale: German,
            altInput: true,
            allowInput: true,
            weekNumbers: true,
            altFormat: 'd.m.Y',
            dateFormat: "Y-m-d",
            disableMobile: true,
        }
    }

    #getTimeDefaultOptions()
    {
        return {
            altFormat: 'd.m.Y, H:i',
            dateFormat: 'Y-m-d H:i',
            enableTime: true,
            time_24hr: true,
        }
    }

    #setInputs(fp)
    {
        let date = this.#options.date

        if (date && fp && fp.length == 4) {
            fp.forEach((item, index) => {
                let hours = index > 0 ? 16 : 10
                date.setHours(hours)
                date.setMinutes(0)
                item.setDate(date)
            })
        }
    }
}