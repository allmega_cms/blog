import {Events} from './Events'

export default class SelectSearch
{
    #searchClass = '.allmega-select-search'
    #loadedRegistered = false
    #keyupRegistered = false

    constructor()
    {
        this.#registerEvents()
    }

    #registerEvents()
    {
        if (!this.#keyupRegistered && $(this.#searchClass).length) {
            let self = this
            $(document).on('keyup', this.#searchClass, function (event) {self.#handle($(this))})
            this.#keyupRegistered = true
        }
        
        if (!this.#loadedRegistered) {
            $(document).on(Events.APP_CONTENT_LOADED, () => this.#registerEvents())
            this.#loadedRegistered = true
        }
    }

    #handle(elem)
    {
        let items = elem.parent().next()
        if (items.length) {
            if (elem.val().length > 2) {
                $.each(items[0], (i, item) => {
                    if (!$(item).text().includes(elem.val())) $(item).hide()
                })
            } else if (elem.val().length < 1) {
                $.each(items[0], (i, item) => $(item).show())
            }
        }
    }
}