import {Events} from "./Events"

export default class Menupoint
{
    #namesInputId = '#allmega-data-names'
    #sourceId = '#menu_point_type'
    #fieldId = '#menu_point_'
    #menupointTypes = []
    #categorySelectId
    #routeInputId
    #selectedId
    #categoryId
    #routeId
    #route

    constructor()
    {
        this.#setData().#registerEvents().#setSelectedId()
    }

    #registerEvents()
    {
        $(document).on('change', this.#sourceId, () => this.#setSelectedId())
        $(document).on(Events.APP_CONTENT_LOADED, () => this.#setSelectedId())
        return this
    }

    #setData()
    {
        let names = $(this.#namesInputId)

        if (names.length) {

            for (let name of names.data('names').split(',')) {
                let selector = '#allmega-data-' + name
                this.#menupointTypes[this.#fieldId + name] = $(selector).data(name + 'Id')
            }

            this.#categoryId =  this.#menupointTypes[this.#fieldId + 'category']
            this.#routeId = this.#menupointTypes[this.#fieldId + 'route']
            this.#categorySelectId = this.#fieldId + 'categoriesTypes'
            this.#routeInputId = this.#fieldId + 'route'
        }
        return this
    }

    #setSelectedId() {
        this.#selectedId = $(this.#sourceId).val()
        this.#setData().#setRoute().#showSelected()
    }

    #setRoute() {
        let value = $(this.#fieldId + 'route').val()
        this.#route = value ? value : ''
        if (!value) $(this.#routeInputId).val(this.#route)
        return this
    }

    #showSelected() {
        switch (this.#selectedId) {
            case this.#routeId:
                this.#showRouteInput()
                break
            case this.#categoryId:
                this.#showCategorySelect()
                break
            default:
                this.#hideAll()
        }
    }

    #showRouteInput() {
        $(this.#categorySelectId).parent().hide()
        $(this.#routeInputId).parent().parent().show()
    }

    #showCategorySelect() {
        $(this.#routeInputId).parent().parent().hide()
        $(this.#categorySelectId).parent().show()
    }

    #hideAll() {
        $(this.#routeInputId).parent().parent().hide()
        $(this.#categorySelectId).parent().hide()
    }
}