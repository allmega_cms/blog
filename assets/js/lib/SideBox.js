export default class SideBox
{
    #endBox = 'allmega-side-box-end'
    #startBox = 'allmega-side-box-start'
    #content = '.allmega-side-box-content'
    #label = '.allmega-side-box-label'
    #boxClass = '.allmega-side-box'
    #boxes

    constructor()
    {
        this.#boxes = $(this.#boxClass)
        if (this.#boxes.length) this.#init().#registerEvents()
    }

    #init()
    {
        $.each(this.#boxes, (i,box) => this.#position(box))
        return this
    }

    #registerEvents()
    {
        let self = this
        $(document).on('click', this.#label, function () {self.handle($(this))})
    }

    handle(elem)
    {
        this.#position(elem.parent())
    }

    #getBoxParams(box)
    {
        let item = $(box)

        let labelWidth = item.find(this.#label).outerWidth().toFixed(2)
        let boxWidth = item.outerWidth().toFixed(2)
        let position = boxWidth - labelWidth

        let isStart = item.hasClass(this.#startBox)
        let css = isStart ? item.css('left') : item.css('right')
        let flag = parseInt(css) == 0

        return {position, isStart, flag}
    }

    #position(box)
    {
        let params = this.#getBoxParams(box)
        let direction = params.isStart ? 'left' : 'right'
        let value = params.flag ? -params.position : 0
        $(box).css(direction, value + 'px')
    }
}