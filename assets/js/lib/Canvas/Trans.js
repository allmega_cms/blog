export default class Trans
{
    #thickness = 'Marker thickness'
    #color = 'Marker color'
    #tool = 'Tools'

    getThickness()
    {
        return this.#thickness
    }

    setThickness(thickness)
    {
        this.#thickness = thickness
        return this
    }

    getColor()
    {
        return this.#color
    }

    setColor(color)
    {
        this.#color = color
        return this
    }

    getTool()
    {
        return this.#tool
    }

    setTool(tool)
    {
        this.#tool = tool
        return this
    }
}