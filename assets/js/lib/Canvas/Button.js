export default class Button
{
    #label = 'Delete all marks'

    getLabel()
    {
        return this.#label
    }

    setLabel(label)
    {
        this.#label = label
        return this
    }
}