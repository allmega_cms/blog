import Toolbar from './Toolbar'
import Events from '../Events'

export default class Canvas
{
    // internal options
    #clearRect = [0, 0, 0, 0]
    #clearCircle = [0, 0, 0]
    #clickDrag = []
    #paint = false
    #clickX = []
    #clickY = []
    #startX = 0
    #startY = 0
    #cssPrefix
	#toolbars
    #context
    #canvas
    #color

    #colorClass = 'canvas-color'
    #activeColorClass = 'canvas-color-active'
    #thicknessClass = 'canvas-thickness'
    #activeThicknessClass = 'canvas-thickness-active'
    #activeShapeClass = 'canvas-shape-active'
    #shapeClass = 'canvas-shape'

    // changeable defaults
    #toolsFilepath = '/images/canvas-tools/'
    #id = 'example-canvas'
    #drawTool = 'path'
    #shape = 'round'
    #tool = 'path'
    #lineWidth = 5
    #toolbar
    #image
    #url

    constructor (options = null)
    {
        this.#setOptions(options)

        this.#toolbars = $('<div>')
            .addClass(this.#cssPrefix + 'toolbars')
            .attr('style', 'text-align: left;')

        this.#addColors().#addSizes().#addTools().#addButtons()
        $('#' + this.#id).parent().append(this.#toolbars)
        this.#setCanvas().#registerEvents()
    }

    #setOptions(options)
    {
        if (!options) return
        this.#toolbar = options.getToolbar() ?? new Toolbar()
        this.#color = this.#toolbar.getColors()[0]
        this.#id = options.getId() ?? this.#id
        this.#cssPrefix = this.#id + '-'
        this.#image = options.getImage()
        this.#url = options.getUrl()
    }

    getId()
    {
        return this.#id
    }

    #registerEvents ()
    {
        this.#canvas.addEventListener('mouseleave', this.#mouseleave.bind(this), false)
        this.#canvas.addEventListener('mousedown', this.#mousedown.bind(this), false)
        this.#canvas.addEventListener('mousemove', this.#mousemove.bind(this), false)
        this.#canvas.addEventListener('mouseup', this.#mouseup.bind(this), false)
        this.#canvas.addEventListener('resize', this.#resize.bind(this), false)

        let self = this
        $(document).on('click', '#' + this.#cssPrefix + 'clear', () => this.clearCanvas())
        $(document).on('click', '#' + this.#cssPrefix + 'save', () => this.saveBoard())

        let activeColorClass = this.#activeColorClass
        $(document).on('click', `.${this.#id}-colors .${this.#colorClass}`, function () {
            let color = self.modifyClasses($(this), activeColorClass)
            self.setContextColor(color)
        })

        let activeThicknessClass = this.#activeThicknessClass
        $(document).on('click', `.${this.#id}-sizes .${this.#thicknessClass}`, function () {
            let size = self.modifyClasses($(this), activeThicknessClass)
            self.setLineSize(size)
        })

        let activeShapeClass = this.#activeShapeClass
        $(document).on('click', `.${this.#id}-tools .${this.#shapeClass}`, function () {
            let tool = self.modifyClasses($(this), activeShapeClass)
            self.setTool(tool)
        })
    }

    modifyClasses(row, activeClass)
    {
        let value = row.data('value')
        let elems = row.parent().children()
        $.each(elems, (i,elem) => $(elem).removeClass(activeClass))
        row.addClass(activeClass)
        return value
    }

    setContextColor (color)
    {
        this.#context.strokeStyle = color
        this.#context.fillStyle = color
        this.#color = color
        return this
    }

    setLineSize(lineWidth = 5)
    {
        this.#context.lineWidth = lineWidth
        return this
    }

    setTool(tool)
    {
        this.#drawTool = tool
        return this
    }

    #addClick (position, dragging = false)
    {
        this.#clickX.push(position.x)
        this.#clickY.push(position.y)
        this.#clickDrag.push(dragging)
    }

    #redraw ()
    {
        let context = this.#context

        let oldGlobalCompositeOperation = context.globalCompositeOperation
        let oldStrokeStyle = context.strokeStyle
        let oldLineWidth = context.lineWidth

        if (this.#drawTool == 'eraser') {
            context.globalCompositeOperation = 'destination-out'
            context.strokeStyle = 'rgba(0,0,0,1)'
            context.lineWidth = 20
        }

        for (let i = 0; i < this.#clickX.length; i++) {
            context.beginPath()
            this.#clickDrag[i] && i ?
                context.moveTo(this.#clickX[i - 1], this.#clickY[i - 1]) : 
                context.moveTo(this.#clickX[i] - 1, this.#clickY[i])

            context.lineTo(this.#clickX[i], this.#clickY[i])
            context.closePath()
            context.stroke()
        }

        if (this.#drawTool == 'eraser') {
            context.globalCompositeOperation = oldGlobalCompositeOperation
            context.strokeStyle = oldStrokeStyle
            context.lineWidth = oldLineWidth
        }
    }

    /** add the drawing colors for toolbars */
	#addColors ()
	{
		if (this.#toolbar.getColors().length) {

            let label = $('<span>').addClass('fw-bold me-2').text(this.#toolbar.getTrans().getColor())
			let line = $('<div>').addClass(this.#cssPrefix + 'colors pb-2 border-bottom')

			let colorsDesc = this.#toolbar.getColorsDesc().length
            let colors = $('<span>')

            for (let i = 0; i < this.#toolbar.getColors().length; i++) {
                
                let color = this.#toolbar.getColors()[i]
                let colordesc = colorsDesc ? this.#toolbar.getColorsDesc()[i] : color

                let activeClass = color == this.#color ? ' ' + this.#activeColorClass : ''
				let symbol = $('<i>')
					.addClass('physio-dot ' + this.#colorClass + activeClass)
					.attr({
						'style': 'cursor: pointer; background-color: ' + color,
                        'data-bs-toggle': 'tooltip',
                        'title': colordesc,
						'data-value': color
					})

                colors.append(symbol)
            }

			line.append(label).append(colors)
            this.#toolbars.append(line)
        }
		return this
	}

    /** add the drawing line sizes for toolbars */
	#addSizes ()
	{
		if (this.#toolbar.getSizes().length) {

            let label = $('<span>').addClass('fw-bold me-2').text(this.#toolbar.getTrans().getThickness())
			let line = $('<div>').addClass(this.#cssPrefix + 'sizes py-2 border-bottom')
            let sizes = $('<span>')

            for (let i = 0; i < this.#toolbar.getSizes().length; i++) {
                
                let size = this.#toolbar.getSizes()[i]
                let activeClass = size == this.#lineWidth ? ' ' + this.#activeThicknessClass : ''

                let img = $('<img>')
					.addClass(this.#thicknessClass + activeClass)
					.attr({
						'src': this.#toolsFilepath + 'tool-line-' + size + '.png',
						'style': 'margin-left: 15px; cursor: pointer;',
                        'data-bs-toggle': 'tooltip',
                        'title': size + ' Px',
						'data-value': size,
						'height': 25
					})

                sizes.append(img)
            }
            
			line.append(label).append(sizes)
			this.#toolbars.append(line)
        }
		return this
	}

    /** add the drawing tools for toolbars */
	#addTools ()
	{
		if (this.#toolbar.getTools().length) {
            
            let label = $('<span>').addClass('fw-bold me-2').text(this.#toolbar.getTrans().getTool())
			let line = $('<div>').addClass(this.#cssPrefix + 'tools py-2 border-bottom')
            let tools = $('<span>')

            for (let i = 0; i < this.#toolbar.getTools().length; i++) {

                let tool = this.#toolbar.getTools()[i]
                let activeClass = tool == this.#tool ? ' ' + this.#activeShapeClass : ''

                let img = $('<img>')
					.addClass(this.#shapeClass + activeClass)
					.attr({
						'src': this.#toolsFilepath + 'tool-' + tool + '.png',
						'style': 'margin-left: 15px; cursor: pointer;',
                        'data-bs-toggle': 'tooltip',
						'data-value': tool,
                        'title': tool,
						'height': 25
					})

                tools.append(img)
            }

			line.append(label).append(tools)
            this.#toolbars.append(line)
        }
		return this
	}

    /** add the buttons for clear and save the canvas as image */
	#addButtons ()
	{
		if (this.#toolbar.getClearButton() || this.#toolbar.saveButton()) {

			let line = $('<div>').addClass(this.#cssPrefix + 'buttons pt-3')

            if (this.#toolbar.getClearButton()) {
                let clearBtn = $('<button>')
                    .text(this.#toolbar.getClearButton().getLabel())
					.attr('id', this.#cssPrefix + 'clear')
					.addClass('btn btn-sm btn-warning')
				
				line.append(clearBtn)
            }

            if (this.#toolbar.getSaveButton()) {
                let saveBtn = $('<button>')
                    .text(this.#toolbar.getSaveButton().getLabel())
					.addClass('btn btn-sm btn-primary ms-2')
					.attr('id', this.#cssPrefix + 'save')
				
				line.append(saveBtn)
            }
            this.#toolbars.append(line)
        }
		return this
	}

    /** set the canvas area and draw image, if exists */
	#setCanvas ()
	{
		this.#canvas = document.getElementById(this.#id)
        if (this.#canvas.getContext) {
            this.#context = this.#canvas.getContext('2d')
            this.#context.lineWidth = this.#lineWidth
            this.#context.strokeStyle = this.#color
            this.#context.lineJoin = this.#shape

            // if options[image] is passed, load image from given url and draw it over canvas
            if (this.#image) {
                let img = new Image()
                img.onload = () => this.#context.drawImage(img, 0, 0)
                img.src = this.#image
            }
        }
		return this
	}

    /** blank the entire canvas */
    clearCanvas ()
    {
        this.#context.clearRect(0, 0, this.#canvas.width, this.#canvas.height)
        this.#context.lineWidth = this.#lineWidth
        this.#context.lineJoin = this.#shape
        this.setContextColor(this.#color)
        this.#clickDrag = []
        this.#clickX = []
        this.#clickY = []
    }

    #getMousePosition(event)
    {
        let rect = event.target.getBoundingClientRect()
        let x = event.clientX - rect.left
        let y = event.clientY - rect.top
        return {x, y, rect}
    }

    #resize (event)
    {
        let rect = event.target.getBoundingClientRect()
        this.#canvas.width = rect.width
        this.#canvas.height = rect.height
    }

    #mouseleave (event)
    {
        this.#paint = false
    }

    #mouseup (event)
    {
        this.#clearRect = [0, 0, 0, 0]
        this.#clearCircle = [0, 0, 0]
        this.#paint = false
        this.#clickDrag = []
        this.#clickX = []
        this.#clickY = []
    }

    #mousedown (event)
    {
        let position = this.#getMousePosition(event)

        this.#startX = position.x
        this.#startY = position.y
        this.#paint = true

        if (this.#drawTool == 'path' || this.#drawTool == 'eraser') {
            this.#addClick(position)
            this.#redraw()
        }
    }
    
    #mousemove (event)
    {
        if (this.#paint) {
            // clear any rectangles that should be cleared
            this.#handleContext().setContextColor(this.#color)

            // draw different shapes
            let position = this.#getMousePosition(event)
            switch (this.#drawTool) {
                case 'rectangle':
                case 'filledrectangle': this.#drawRectangle(position); break
                case 'circle':
                case 'filledcircle': this.#drawCircle(position); break
                default: this.#addClick(position, true)
            }
            this.#redraw()
        }
    }

    #handleContext ()
    {
        let context = this.#context

        context.clearRect(this.#clearRect[0], this.#clearRect[1], this.#clearRect[2], this.#clearRect[3])

        // clear any circles that have to be cleared
        // set color to white but remember old color
        context.strokeStyle = context.fillStyle = '#ffffff'
        context.beginPath()

        context.arc(this.#clearCircle[0], this.#clearCircle[1], this.#clearCircle[2], 0, Math.PI * 2)
        context.closePath()
        context.stroke()
        context.fill()

        return this
    }

    #drawRectangle(position)
    {
        let width = position.x - this.#startX
        let height = position.y - this.#startY
		
        this.#clearRect = [this.#startX, this.#startY, width, height]
        this.#drawTool == 'rectangle' ? 
            this.#context.strokeRect(this.#startX, this.#startY, width, height) : 
            this.#context.fillRect(this.#startX, this.#startY, width, height)
    }

    #drawCircle (position)
    {
        let height = Math.abs(position.y - this.#startY)
        let width = Math.abs(position.x - this.#startX)
        let radius = height > width ? height : width
							            
        this.#clearCircle = [this.#startX, this.#startY, radius]
        this.#context.beginPath()

        this.#context.arc(this.#startX, this.#startY, radius, 0, Math.PI * 2)
        this.#context.closePath()

        this.#drawTool == 'circle' ? this.#context.stroke() : this.#context.fill()
    }

    saveBoard ()
    {
        if (!this.#url) return

        $(document).trigger(Events.APP_LOADER_SHOW)

        let imageData = this.#canvas.toDataURL('image/png')
        let data = {'image_data': encodeURIComponent(imageData)}

        $.post(this.#url, data, (response) => {
            $(document).trigger(Events.APP_LOADER_HIDE)
            this.#showModal(response)
        })
    }

    #showModal(modalData)
    {
        $(document).trigger({type: Events.APP_MODAL_SHOW, modalData})
    }
}