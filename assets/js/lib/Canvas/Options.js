import Toolbar from "./Toolbar"

export class Options
{
    #color = '#999'
    #toolbar
    #image
    #url
    #id

    constructor()
    {
        this.#toolbar = new Toolbar()
    }

    getColor()
    {
        return this.#color
    }

    setColor(color)
    {
        this.#color = color
        return this
    }

    getToolbar()
    {
        return this.#toolbar
    }

    setToolbar(toolbar)
    {
        this.#toolbar = toolbar
        return this
    }

    getImage()
    {
        return this.#image
    }

    setImage(image)
    {
        this.#image = image
        return this
    }

    getUrl()
    {
        return this.#url
    }

    setUrl(url)
    {
        this.#url = url
        return this
    }

    getId()
    {
        return this.#id
    }

    setId(id)
    {
        this.#id = id
        return this
    }
}