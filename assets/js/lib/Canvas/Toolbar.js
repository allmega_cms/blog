import Button from './Button'
import Trans from './Trans'

export default class Toolbar
{
    #tools = ['circle', 'filledcircle', 'filledrectangle', 'path', 'rectangle', 'eraser']
    #colors = ['#ca1111', '#f0f21f', '#235ee3']
    #sizes = [2, 5]
    #clearButton
    #saveButton
    #colorsdesc
    #trans

    constructor()
    {
        this.#saveButton = new Button('Save')
        this.#clearButton = new Button()
        this.#trans = new Trans()
    }

    getTools()
    {
        return this.#tools
    }

    setTools(tools)
    {
        this.#tools = tools
        return this
    }

    getSizes()
    {
        return this.#sizes
    }

    setSizes(sizes)
    {
        this.#sizes = sizes
        return this
    }

    getColorsDesc()
    {
        return this.#colorsdesc
    }

    setColorsDesc(color)
    {
        this.#colorsdesc = color
        return this
    }

    getClearButton()
    {
        return this.#clearButton
    }

    setClearButton(clearButton)
    {
        this.#clearButton = clearButton
        return this
    }

    getSaveButton()
    {
        return this.#saveButton
    }

    setSaveButton(saveButton)
    {
        this.#saveButton = saveButton
        return this
    }

    getColors()
    {
        return this.#colors
    }

    setColors(colors)
    {
        this.#colors = colors
        return this
    }

    getTrans()
    {
        return this.#trans
    }

    setTrans(trans)
    {
        this.#trans = trans
        return this
    }
}