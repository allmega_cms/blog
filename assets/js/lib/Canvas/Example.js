import {Options as CanvasOptions} from './Canvas/Options'
import Canvas from './Canvas/Canvas'

export default class Bodycharts
{
    #bodychartCanvas = '.physio-bodychart-canvas'
    #commentColorsId = '#physio-comment-colors'
    #options
    
    constructor()
    {
        let bodycharts = $(this.#bodychartCanvas)
        if (bodycharts.length) {
            this.#setOptions()
            $.each(bodycharts, (i,elem) => {
                this.#options.setImage($(elem).data('image'))
                this.#options.setUrl($(elem).data('url'))
                this.#options.setId($(elem).attr('id'))
                new Canvas(this.#options)
            })
        }
    }

    #setOptions ()
    {
        let elem = $(this.#commentColorsId)
        let colors = elem.data('colors').split(',')
        let colorsdesc = elem.data('colorsdesc').split(',')

        this.#options = new CanvasOptions()
        let toolbar = this.#options.getToolbar()
        
        toolbar.setColors(colors).setColorsDesc(colorsdesc)
        toolbar.getClearButton().setLabel('Alle Markierungen löschen')
        toolbar.getSaveButton().setLabel('Speichern')
        toolbar.getTrans()
            .setThickness('Dicke der Markierung')
            .setColor('Farbe der Markierung')
            .setTool('Werkzeug')
    }
}