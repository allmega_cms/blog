import {Events} from './Events'

export default class ImagePreview
{
    #carouselId = '#allmegaArticleCarousel'
    #preview = '.allmega-file-preview'
    #gallery = '.allmega-show-gallery'

    constructor()
    {
        let elem = $(this.#preview)
        if (elem.length) this.#registerEvents()
    }

    #registerEvents()
    {
        let self = this
        $(document).on('click', this.#preview, function (event) {self.show(event, $(this))})
        $(document).on('click', this.#gallery, function (event) {self.showGallery(event, $(this))})
        $(document).on('show.bs.modal', () => $(this.#carouselId).carousel())
    }

    #showModal(modalData)
    {
        $(document).trigger({type: Events.APP_MODAL_SHOW, modalData})
    }

    show(event, elem)
    {
        event.preventDefault()

        let size = elem.data('size')
        let image = elem.closest('.allmega-mediafile-area').children('img').attr('src')
        let img = $('<img>').attr('src', image).addClass('w-100')

        this.#showModal({'size': size, 'content': img})
    }

    showGallery(event, elem)
    {
        event.preventDefault()

        let size = elem.data('size')
        let url = elem.attr('href')

        $(document).trigger(Events.APP_LOADER_SHOW)

        $.get(url, (response) => {
            $(document).trigger(Events.APP_LOADER_HIDE)
            if (response.content) {
                if (size) response.size = size
                this.#showModal(response)
            }
        })
    }
}