import SelectSearch from './SelectSearch'
import ImagePreview from './ImagePreview'
import DateExtender from './DateExtender'
import Comment from './Comment/Comment'
import Menupoint from './Menupoint'
import PopupBox from './PopupBox'
import {Events} from './Events'
import SideBox from './SideBox'
import Header from './Header'
import Picker from './Picker'
import Player from './Player'
import Search from './Search'
import Loader from './Loader'
import Slider from './Slider'
import Box from './Modal/Box'

export default class App
{
    #flashMessageId = '.allmega-flash-message'
    #tooltips = '[data-bs-toggle="tooltip"]'
    #scrollBtnId = '#allmega-scroll-btn'
    #flashBoxId = '#allmega-flash-area'
    #loaderId = '#allmega-loader'
    #showTime = 2000

    constructor()
    {
        this.#init().#registerEvents().#handleFlashMessage()
    }

    setShowTime(time)
    {
        this.#showTime = time
    }

    #init()
    {
        new DateExtender()
        new SideBox()
        new Header()
        new Box()
        new Slider()
        new Loader()
        new Player()
        new Comment()
        new Menupoint()
        new SelectSearch()
        new ImagePreview()
        new PopupBox()
        new Search()
        new Picker()
        return this
    }

    #registerEvents()
    {
        // Scroll to top, show|hide scroll button
        $(document).on('click', this.#scrollBtnId, () => $('body,html').animate({scrollTop: 0}, 200))
        $(document).on('show.bs.modal', () => $(document).trigger(Events.APP_INIT_TOOLTIP))

        $(document).on(Events.APP_LOADER_HIDE, () => $(this.#loaderId).hide())
        $(document).on(Events.APP_LOADER_SHOW, () => {
            let height = $(window).height()
            let top = $(document).scrollTop()
            $(this.#loaderId).css({height: height + 'px', top: top + 'px'}).show()
        })

        let scrollBtn = $(this.#scrollBtnId)
        $(window).on('scroll', function () {
            $(this).scrollTop() > 5 ? scrollBtn.fadeIn() : scrollBtn.fadeOut()
        })

        // Init tooltips
        $(document).on(Events.APP_INIT_TOOLTIP, (event) => {
            $.each($('.form-control'), function () {
                let placeholder = $(this).attr('placeholder')
                $(this).attr({title: placeholder, 'data-bs-toggle': 'tooltip'})
            })
            $(this.#tooltips).tooltip()
        })

        $(document).trigger(Events.APP_INIT_TOOLTIP).trigger(Events.APP_INIT_FLATPICKER)
        return this
    }

    #handleFlashMessage()
    {
        let alertBox = $(this.#flashBoxId)
        if (alertBox.length) {
            if (alertBox.find(this.#flashMessageId).length) {
                alertBox.show()
                window.setTimeout(() => alertBox.hide(), this.#showTime)
            }
        }
    }
}