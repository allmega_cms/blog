export default class Header
{
    #id = '#allmega-header'
    #elem

    constructor()
    {
        this.#elem = $(this.#id)
        if (this.#elem.length) this.#registerEvents()
    }

    #registerEvents()
    {
        $(window).on('scroll', () => this.#doStyle())
    }

    #doStyle()
    {
        window.scrollY > 5 ?
            this.#elem.addClass('allmega-fixed-header') :
            this.#elem.removeClass('allmega-fixed-header')
    }
}