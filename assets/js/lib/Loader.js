import Data from './Modal/Data'
import Box from './Modal/Box'
import {Events} from './Events'

export default class Loader
{
    #fileLoadIds = '#media_file_file, #file_load_file'
    #progressBarClass = 'allmega-progress-bar'
    #confirmForm = 'form[data-confirmation]'
    #confirmLink = 'a[data-confirmation]'
    #confirmBtn = '#allmega-btn-confirm'
    #dataIdPrefix = '#allmega-confirm-'
    #sendForm = 'form[data-send]'
    #loadLink = 'a[data-load]'
    #ajax = true
    #modalData
    #progress
    #form

    constructor()
    {
        this.#registerEvents().#registerSubmitEvents()
    }

    setAjax(ajax)
    {
        this.#ajax = ajax
    }

    setForm(form)
    {
        this.#form = form
        return this
    }

    #registerEvents()
    {
        let self = this

        $(document).on('click', this.#confirmLink, function(event) {self.handle(event, $(this))})

        if (this.#ajax) {
            $(document).on('click', this.#loadLink, function (event) {self.loadFromLink(event, $(this))})
            $(document).on(Events.APP_FORM_REGISTER_SUBMIT, (event) => this.#registerSubmitEvents(event))
            $(document).on('change', this.#fileLoadIds, function () {
                $(this).next('.custom-file-label').html($(this).val())
            })
        }
        return this
    }

    #registerSubmitEvents(event = null)
    {
        let self = this

        $(document).off('submit', this.#sendForm).on('submit', this.#sendForm, function (event) {
            self.send(event, $(this))
        })

        $(document).off('submit', this.#confirmForm).on('submit', this.#confirmForm, function (event) {
            self.setForm($(this).clone()).handle(event, $(this), 'form')
        })
    }

    #buildModalData(item, itemType)
    {
        let flag = itemType == 'link'
        let elem = flag ? $(this.#dataIdPrefix + item.data('type')) : item.next()

        if (elem && elem.length) {

            let icon = flag ? 'circle-check' : 'trash'
            let type = flag ? 'success' : 'danger'

            this.#modalData = new Data()
                .setContent(elem.data('message'))
                .setTitle(elem.data('title'))
                .setShowBtns(true)
            
            this.#modalData.getCancel().setLabel(elem.data('cancel'))

            this.#modalData
                .getCertify()
                .setLabel(elem.data('certify'))
                .setType(type)
                .setIcon(icon)
        }
        return this
    }

    #showModal(modalData)
    {
        $(document).trigger({type: Events.APP_MODAL_SHOW, modalData})
    }

    loadFromLink(event, link)
    {
        event.preventDefault()
        
        $(document).trigger(Events.APP_LOADER_SHOW)
        $(document).trigger(Events.APP_LINK_BEFORE_LOAD)

        let url  = link.attr('href')
        let size = link.data('size')

        $.get(url, (response) => {
            $(document).trigger(Events.APP_LOADER_HIDE)
            if (size) response.size = size
            this.#showModal(response)
            $(document)
                .trigger(Events.APP_FORM_REGISTER_SUBMIT)
                .trigger({type: Events.APP_CONTENT_LOADED, options: response})
                .trigger({type: Events.APP_INIT_FLATPICKER, options: response})
        })
    }

    send(event, form)
    {
        // if this is multipat/form-data, use sendFile() function
        let attr = form.attr('enctype');
        if (typeof attr !== 'undefined' && attr !== false) {
            this.#sendFile(event, form)
            return
        }

        event.preventDefault()

        $(document).trigger(Events.APP_LOADER_SHOW)
        $(document).trigger({type: Events.APP_FORM_SEND_BEFORE, options: {form}})

        let url = form.attr('action')

        $.post(url, form.serialize(), (response) => {
            $(document).trigger(Events.APP_LOADER_HIDE)
            this.#showModal(response)
            $(document)
                .trigger({type: Events.APP_CONTENT_LOADED, options: response})
                .trigger({type: Events.APP_INIT_FLATPICKER, options: response})
        })
    }

    #setProgressBar()
    {
        if (!this.#progress) this.#progress = $('<div>').addClass(this.#progressBarClass)
    }

    #sendFile(event, form)
    {
        event.preventDefault()

        this.#setProgressBar()
        $(Box.getId()).find('.modal-body').append(this.#progress)

        let elem = document.querySelector(this.#sendForm)
        let data = new FormData(elem)
        let url = form.attr('action')
        let progress = this.#progress

        $.ajax({
            processData: false,
            contentType: false,
            dataType: 'json',
            type: 'POST',
            data: data,
            url: url,
            success: (response) => this.#showModal(response),
            xhr: () => {
                var xhr = $.ajaxSettings.xhr()
                xhr.upload.onprogress = function(state) {
                    let width = state.loaded / state.total * 100
                    progress.width(width + '%')
                }
                return xhr 
            },
        })
    }

    handle(event, elem, type = 'link')
    {
        event.preventDefault()

        this.#buildModalData(elem, type)
        let confirm = $(Box.getId())

        if (!confirm.data('confirmed')) {
            event.preventDefault()
            confirm.off('click', this.#confirmBtn).on('click', this.#confirmBtn, () => {

                confirm.data('confirmed', true)
                $(document).trigger(Events.APP_LOADER_SHOW)

                if (event.type == 'click') {
                    let url = elem.attr('href')
                    $(document).trigger({type: Events.APP_LINK_BEFORE_REDIRECT, options: {link: elem}})
                    $(window).attr('location', url)
                } else {
                    $('body').append(this.#form)
                    $(document)
                        .trigger({type: Events.APP_FORM_DELETE_BEFORE, options: {form: this.#form}})
                        .off('submit', this.#confirmForm)

                    this.#form.trigger('submit')
                }
            })
            this.#showModal(this.#modalData)
        }
    }
}