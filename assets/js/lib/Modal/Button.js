export default class Button
{
    #label = 'cancel'
    #type = 'default'
    #icon = 'ban'

    constructor(data = null)
    {
        if (data) {
            this.#label = data?.label ?? this.#label
            this.#type = data?.type ?? this.#type
            this.#icon = data?.icon ?? this.#icon
        }
    }

    getLabel()
    {
        return this.#label
    }

    setLabel(label)
    {
        this.#label = label
        return this
    }

    getType()
    {
        return this.#type
    }

    setType(type)
    {
        this.#type = type
        return this
    }

    getIcon()
    {
        return this.#icon
    }

    setIcon(icon)
    {
        this.#icon = icon
        return this
    }
}