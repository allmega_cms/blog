import {Modal} from 'bootstrap'
import {Events} from '../Events'
import Data from './Data'

export default class Box
{
    #id = '#allmega-modal-box'
    #reloadTime = 3000
    #modalData
    #box

    constructor()
    {
        let modal = document.querySelector(this.#id)
        this.#box = new Modal(modal)
        this.#registerEvents(modal)
    }

    static getId()
    {
        return '#allmega-modal-box'
    }

    setReloadTime(time)
    {
        this.#reloadTime = time
        return this
    }
    
    #registerEvents(modal)
    {
        modal.addEventListener('hidden.bs.modal', (event) => this.#build(event))
        $(document).on(Events.APP_MODAL_SHOW, (event) => this.#build(event).#show())
    }

    #build(event)
    {
        let modalData = event?.modalData

        this.#modalData = modalData && modalData instanceof Data ? modalData : new Data(modalData)

        let btns = '', modal = $(this.#id)

        this.#setSize(modal, this.#modalData.getSize())
        
        if (this.#modalData.getShowBtns()) {
            btns = $('<div>')
                .addClass('allmega-modal-footer-btns')
                .append(this.#buildButton(this.#modalData.getCancel(), 'cancel', ' me-3'))
                .append(this.#buildButton(this.#modalData.getCertify(), 'confirm'))
        }

        modal.find('.modal-title').html(this.#modalData.getTitle())
        modal.find('.modal-body').html(this.#modalData.getContent())
        modal.find('.modal-footer').html(btns)

        return this
    }

    #setSize(modal, size)
    {
        let area = modal.find('.modal-dialog')

        if (!size) {
            area.attr('class').split(/\s+/).forEach((item) => {
                if (item !== 'modal-dialog') area.removeClass(item)
            })
            return this
        }

        area.addClass('modal-' + size)
        return this
    }

    #buildButton(btn, id, cssClasses = '')
    {
        let btnIcon = $('<i>').attr({'class': 'fa-solid fa-' + btn.getIcon(), 'aria-hidden': true})

        return $('<button>')
            .attr({
                'class': 'btn btn-sm btn-' + btn.getType() + cssClasses,
                'data-bs-dismiss': 'modal',
                'id': 'allmega-btn-' + id,
                'type': 'button',
            })
            .append(btnIcon)
            .append(btn.getLabel())
    }

    #show()
    {
        this.#box.show()
        if (this.#modalData.getReload()) setTimeout(() => location.reload(), this.#reloadTime)
    }
}