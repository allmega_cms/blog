import Button from './Button'

export default class Data
{
    #showBtns = false
    #reload = false
    #content = ''
    #title = ''
    #size = ''
    #certify
    #cancel

    constructor(data = null)
    {
        this.#certify = data?.certify ?? new Button(data)
        this.#cancel = data?.cancel ?? new Button(data)

        if (data) {
            this.#showBtns = data?.showBtns ?? this.#showBtns
            this.#content = data?.content ?? this.#content
            this.#reload = data?.reload ?? this.#reload
            this.#title = data?.title ?? this.#title
            this.#size = data?.size ?? this.#size
        }
    }

    getCertify()
    {
        return this.#certify
    }

    getCancel()
    {
        return this.#cancel
    }

    getShowBtns()
    {
        return this.#showBtns
    }

    setShowBtns(show)
    {
        this.#showBtns = show
        return this
    }

    getReload()
    {
        return this.#reload
    }

    setReload(reload)
    {
        this.#reload = reload
        return this
    }

    getContent()
    {
        return this.#content
    }

    setContent(content)
    {
        this.#content = content
        return this
    }

    getTitle()
    {
        return this.#title
    }

    setTitle(title)
    {
        this.#title = title
        return this
    }

    getSize()
    {
        return this.#size
    }

    setSize(size)
    {
        this.#size = size
        return this
    }
}