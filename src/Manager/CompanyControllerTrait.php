<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Manager;

use Allmega\BlogBundle\{Data, Events};
use Allmega\BlogBundle\Entity\Company;
use Allmega\BlogBundle\Form\CompanyType;
use Allmega\BlogBundle\Controller\CompanyController;
use Allmega\BlogBundle\Utils\Params\BaseControllerParams;
use Symfony\Component\HttpFoundation\Response;

trait CompanyControllerTrait
{
    private function save(): Response
    {
        $company = $this->companyRepo->findFirst();
        $formParams = $this->buildFormParams($company);

        $params = (new BaseControllerParams())->init(
            formParams: $formParams,
            entity: $company,
            domain: Data::DOMAIN,
            eventName: Events::COMPANY_UPDATED,
            formType: CompanyType::class,
            routeName: CompanyController::ROUTE_NAME,
            templatesPath: CompanyController::ROUTE_TEMPLATE_PATH
        );
        return $this->handle($params);
    }

    private function buildFormParams(Company $company): array
    {
        return [
            'showDelete' => false,
            'item' => $company,
            'link' => [
                'route' => CompanyController::ROUTE_NAME . 'show',
                'title' => 'company.back'
            ],
        ];
    }
}