<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Manager;

use Allmega\BlogBundle\Entity\MenuPoint;
use Allmega\BlogBundle\Repository\MenuPointRepository;
use Symfony\Component\String\Slugger\AsciiSlugger;
use Doctrine\ORM\EntityManagerInterface;

class MenupointHandler
{
	private MenuPoint $menupoint;

	public function __construct(
        private readonly MenuPointRepository $menuPointRepo,
        private readonly EntityManagerInterface $em) {}

	public function handle(MenuPoint $menuPoint, bool $delete = false): void
	{
		$delete ? $this->delete($menuPoint) : $this->modify($menuPoint);
	    $this->em->flush();
	}

    private function delete(MenuPoint $menuPoint): void
    {
        $this->menuPointRepo->setParentToNull($menuPoint->getId());
        $menuPoint->getCategoriesTypes()->clear();
        $menuPoint->getGroups()->clear();
        $this->em->remove($menuPoint);
    }

    private function modify(MenuPoint $menuPoint): void
    {
        $slug = (new AsciiSlugger())->slug($menuPoint->getTitle());
        $menuPoint->setSlug($slug);
        $this->em->persist($menuPoint);

        $this->menupoint = $menuPoint;
        $this->synchronizeChildGroups($menuPoint);
        $this->em->persist($this->menupoint);
        $this->synchronizeParentGroups($menuPoint);

        $this->synchronizeChildState($menuPoint);
        $this->synchronizeParentState($menuPoint);
    }

    /**
     * Assign all groups of each child menu item to the current menu item
     */
    private function synchronizeChildGroups(MenuPoint $menuPoint): void
    {
        if ($menuPoint->getGroups()->count()) {
            $childs = $this->menuPointRepo->findChilds($menuPoint->getId());
            foreach ($childs as $child) {
                foreach ($child->getGroups() as $group) $this->menupoint->addGroup($group);
                $this->synchronizeChildGroups($child);
            }
        }
    }

    /**
     * Assign all groups of the current menu item to each parent menu item
     */
    private function synchronizeParentGroups(MenuPoint $menuPoint): void
    {
        $parentMenuPoint = $menuPoint->getParent();
        if ($parentMenuPoint != NULL) {

            $groups = $menuPoint->getGroups();
            $parentGroups = $parentMenuPoint->getGroups();

            if ($groups->count() && $parentGroups->count()) {
                foreach ($groups as $group) {
                    if (!$parentGroups->contains($group)) {
                        $parentMenuPoint->addGroup($group);
                        $this->em->persist($parentMenuPoint);
                    }
                    $this->synchronizeParentGroups($parentMenuPoint);
                }
            } else {
                $parentMenuPoint->getGroups()->clear();
                $this->em->persist($parentMenuPoint);
                $this->synchronizeParentGroups($parentMenuPoint);
            }
        }
    }

    /**
     * If the current menu item is disabled, each child menu item must also be disabled
     */
    private function synchronizeChildState(MenuPoint $menuPoint): void
    {
        if (!$menuPoint->isActive()) {
            $rows = $this->menuPointRepo->findChilds($menuPoint->getId());
            foreach ($rows as $row) {
                $row->setActive($menuPoint->isActive());
                $this->em->persist($row);
                $this->synchronizeChildState($row);
            }
        }
    }

    /**
     * If the current menu item is activated, each parent menu item must also be activated
     */
    private function synchronizeParentState(MenuPoint $menuPoint): void
    {
        if ($menuPoint->isActive()) {
            $parentMenuPoint = $menuPoint->getParent();
            if ($parentMenuPoint != NULL) {
                $parentMenuPoint->setActive($menuPoint->isActive());
                $this->em->persist($parentMenuPoint);
                $this->synchronizeParentState($parentMenuPoint);
            }
        }
    }
}