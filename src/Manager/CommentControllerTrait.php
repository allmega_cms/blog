<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Manager;

use Allmega\BlogBundle\Utils\Helper;
use Allmega\BlogBundle\{Data, Events};
use Allmega\BlogBundle\Form\CommentType;
use Allmega\BlogBundle\Entity\{Comment,Item};
use Allmega\BlogBundle\Controller\CommentController;
use Allmega\BlogBundle\Utils\Params\BaseControllerParams;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\HttpFoundation\Response;

trait CommentControllerTrait
{
    public function __construct(private readonly EntityManagerInterface $entityManager) {}

    private function addComment(): Response
    {
        if (!$this->isXmlRequest) return $this->json(['content' => 'Who are you?']);

        sleep(3);

        extract($this->getRequest()->request->all());
        // [$entityName, $entityId, $parentId, $message]
        
        $item = $this->itemRepo->findOneBy(['entityname' => $entityName]);
        if (!$item) {
            $message = $this->translate('comment.create.error.item', [], Data::DOMAIN);
            return $this->json(['error' => $message]);
        }

        $content = Helper::removeTags($message);
        if (!$this->checkContentLength($content)) {
            $message = $this->translate('comment.create.error.text', [], Data::DOMAIN);
            return $this->json(['error' => $message]);
        }

        $parent = $parentId ? $this->commentRepo->find($parentId) : null;
        $comment = $this->createComment($item, $parent, $entityId, $content);
        $this->dispatchCommentEvent($comment, Events::COMMENT_CREATED);

        $message = $this->translate('comment.create.success', [], Data::DOMAIN);
        return $this->json(['success' => $message]);
    }

    private function checkContentLength(string $content): bool
    {
        return $content && strlen($content) > 3 && strlen($content) < 3000;
    }

    private function createComment(Item $item, ?Comment $parent, string $entityId, string $content): Comment
    {
        $active = !$item->getExamine();
        $comment = (new Comment())
            ->setAuthor($this->getUser())
            ->setEntityid($entityId)
            ->setContent($content)
            ->setActive($active)
            ->setParent($parent)
            ->setItem($item);

        $em = $this->getEntityManager();
        $em->persist($comment);
        $em->flush();

        return $comment;
    }

    private function dispatchCommentEvent(Comment $comment, string $eventName): void
    {
        $event = new GenericEvent($comment, ['isXmlRequest' => $this->isXmlRequest, 'domain' => Data::DOMAIN]);
        $this->services
            ->getService('eventDispatcher')
            ->dispatch($event, $eventName);
    }

    private function deleteComment(Comment $comment): Response
    {
        extract($this->getParameterForRoute($comment));

        $em = $this->getEntityManager();
        $em->getRepository(Comment::class)->changeChildParentId($comment);

        $em->remove($comment);
        $em->flush();
        $this->dispatchCommentEvent($comment, Events::COMMENT_DELETED);

        return $this->redirectToRoute($route, [$prop => $value], Response::HTTP_SEE_OTHER);
    }

    private function getParameterForRoute(Comment $comment): array
    {
        $route = $comment->getItem()->getRoute();
        $prop = $comment->getItem()->getProp();
        $value = $comment->getEntityid();

        if ($prop != 'id') {
            $entity = $this
                ->getEntityManager()
                ->getRepository($comment->getItem()->getEntityname())
                ->find($value);

            $method = 'get' . ucfirst($prop);
            if ($entity && method_exists($entity, $method)) {
                $value = $entity->$method();
            }
        }
        return ['route' => $route, 'prop' => $prop, 'value' => $value];
    }

    private function save(Comment $comment, string $place): Response
    {
        $showDelete = $place == 'manage' && $this->getUser()->hasRole(Data::COMMENT_MODERATOR_ROLE);
        $params = (new BaseControllerParams())->init(
            formParams: ['showDelete' => $showDelete, 'item' => $comment],
            entity: $comment,
            domain: Data::DOMAIN,
            eventName: Events::COMMENT_UPDATED,
            formType: CommentType::class,
            routeName: CommentController::ROUTE_NAME,
            templatesPath: CommentController::ROUTE_TEMPLATE_PATH
        );
        return $this->handle($params);
    }
}
