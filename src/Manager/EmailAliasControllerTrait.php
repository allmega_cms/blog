<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Manager;

use Allmega\BlogBundle\{Data, Events};
use Allmega\BlogBundle\Entity\EmailAlias;
use Allmega\BlogBundle\Form\EmailAliasType;
use Allmega\BlogBundle\Controller\EmailAliasController;
use Allmega\BlogBundle\Utils\Params\BaseControllerParams;
use Symfony\Component\HttpFoundation\Response;

trait EmailAliasControllerTrait
{
    protected function preExecution(): static
    {
        $user = $this->getUser();
        switch ($this->params->getAction()) {
            case $this->add:
                $this->params
                    ->getEntity()
                    ->setCreator($user)
                    ->setEditor($user);
                break;
            case $this->edit:
                $this->params
                    ->getEntity()
                    ->setEditor($user)
                    ->setUpdated();
                break;
            case $this->delete:
                $this->params->getEntity()->getUsers()->clear();
                break;
            default:
        }
        return $this;
    }

    private function save(EmailAlias $emailalias = null, array $arguments = []): Response
    {
        $eventName = $emailalias ? Events::EMAILALIAS_UPDATED : Events::EMAILALIAS_CREATED;
        $emailalias = $emailalias ?? new EmailAlias();

        $params = (new BaseControllerParams())->init(
            arguments: $arguments,
            formParams: ['item' => $emailalias],
            entity: $emailalias,
            domain: Data::DOMAIN,
            eventName: $eventName,
            formType: EmailAliasType::class,
            routeName: EmailAliasController::ROUTE_NAME,
            templatesPath: EmailAliasController::ROUTE_TEMPLATE_PATH
        );
        return $this->handle($params);
    }
}
