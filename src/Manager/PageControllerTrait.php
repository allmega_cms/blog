<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Manager;

use Allmega\BlogBundle\Entity\Page;
use Allmega\BlogBundle\Form\PageType;
use Allmega\BlogBundle\{Data, Events};
use Allmega\MediaBundle\Events as MediaEvents;
use Allmega\BlogBundle\Controller\PageController;
use Allmega\BlogBundle\Controller\DefaultController;
use Allmega\BlogBundle\Utils\Params\BaseControllerParams;
use Symfony\Component\HttpFoundation\Response;

trait PageControllerTrait
{
    protected function preExecution(): static
    {
        switch ($this->params->getAction()) {
            case $this->edit:
            case $this->add:
                $this->handlePage();
                break;
            case $this->delete:
                $this->params
                    ->getEntity()
                    ->getMenuPoints()
                    ->clear();
                break;
            default:
        }
        return $this;
    }

    protected function postExecution(): static
    {
        switch ($this->params->getAction()) {
            case $this->edit:
            case $this->add:
                $mediaDir = $this->params->getEntity()->getMediaDir();
                $this->saveFiles($mediaDir, $this->getFormFiles());
                break;
            case $this->delete:
                $this->deletePageFiles();
                break;
            default:
        }
        return $this;
    }

    protected function setRedirect(): static
    {
        switch ($this->params->getAction()) {
            case $this->state:
            case $this->edit:
            case $this->add:
                $this->params
                    ->setRouteName($this->params->getRouteShort(), $this->show)
                    ->addRouteParams(['slug' => $this->params->getEntity()->getSlug()]);
                break;
            default:
        }
        return $this;
    }

    private function handlePage(): void
    {
        $title = $this->params->getEntity()->getTitle();
        $slug = $this->services->getService('slugger')->slug($title);
        $this->params->getEntity()->setSlug($slug)->setUpdated();
    }

    private function deletePageFiles(): void
    {
        $this->params
            ->addArguments(['folder' => $this->params->getEntity()->getMediaDir()])
            ->setEventName(MediaEvents::DELETE_FILES);

        $this->dispatchEvent();
    }

    private function getStartParams(): array
    {
        return ['start' => ['route' => DefaultController::ROUTE_NAME]];
    }

    private function save(Page $page = null, array $arguments = []): Response
    {
        $eventName = $page ? Events::PAGE_UPDATED : Events::PAGE_CREATED;
        $page = $page ?? new Page();

        $params = (new BaseControllerParams())->init(
            arguments: $arguments,
            formParams: ['item' => $page],
            entity: $page,
            domain: Data::DOMAIN,
            eventName: $eventName,
            formType: PageType::class,
            routeName: PageController::ROUTE_NAME,
            templatesPath: PageController::ROUTE_TEMPLATE_PATH
        );
        return $this->handle($params);
    }
}