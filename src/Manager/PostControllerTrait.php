<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Manager;

use Allmega\BlogBundle\Entity\Post;
use Allmega\BlogBundle\Form\PostType;
use Allmega\BlogBundle\{Data, Events};
use Allmega\MediaBundle\Events as MediaEvents;
use Allmega\BlogBundle\Controller\PostController;
use Allmega\BlogBundle\Controller\DefaultController;
use Allmega\BlogBundle\Utils\Params\BaseControllerParams;
use Symfony\Component\HttpFoundation\Response;

trait PostControllerTrait
{
    protected function preExecution(): static
    {
        switch ($this->params->getAction()) {
            case $this->edit:
            case $this->add:
                $this->handlePost();
                break;
            case $this->delete:
                $this->clearPost();
                break;
            default:
        }
        return $this;
    }

    protected function postExecution(): static
    {
        switch ($this->params->getAction()) {
            case $this->edit:
            case $this->add:
                $mediaDir = $this->params->getEntity()->getMediaDir();
                $this->saveFiles($mediaDir, $this->getFormFiles());
                break;
            case $this->delete:
                $this->deleteCommentsAndFiles();
                break;
            default:
        }
        return $this;
    }

    protected function setRedirect(): static
    {
        switch ($this->params->getAction()) {
            case $this->state:
            case $this->edit:
            case $this->add:
                $this->params
                    ->setRouteName($this->params->getRouteShort(), $this->show)
                    ->addRouteParams(['slug' => $this->params->getEntity()->getSlug()]);
                break;
            default:
        }
        return $this;
    }

    private function handlePost(): void
    {
        $post = $this->params->getEntity();
        if ($this->params->getAction() == $this->add) {
            $post->addAuthor($this->getUser());
        }

        $slug = $this->services->getService('slugger')->slug($post->getTitle());
        $post->setSlug($slug)->setUpdated();
    }

    private function clearPost(): void
    {
        $post = $this->params->getEntity();
        $post->getMenuPoints()->clear();
        $post->getAuthors()->clear();
        $post->getTags()->clear();
    }

    private function deleteCommentsAndFiles(): void
    {
        $ids = [$this->params->getEntity()->getId()];
        $this->params
            ->setEventName(Events::DELETE_COMMENTS)
            ->addArguments(['ids' => $ids]);

        $this->dispatchEvent();

        $this->params
            ->addArguments(['folder' => $this->params->getEntity()->getMediaDir()])
            ->setEventName(MediaEvents::DELETE_FILES);

        $this->dispatchEvent();
    }

    private function getStartParams(): array
    {
        return ['start' => ['route' => DefaultController::ROUTE_NAME]];
    }

    private function save(Post $post = null, array $arguments = []): Response
    {
        $eventName = $post ? Events::POST_UPDATED : Events::POST_CREATED;
        $post = $post ?? new Post();

        $params = (new BaseControllerParams())->init(
            arguments: $arguments,
            formParams: ['item' => $post],
            entity: $post,
            domain: Data::DOMAIN,
            eventName: $eventName,
            formType: PostType::class,
            routeName: PostController::ROUTE_NAME,
            templatesPath: PostController::ROUTE_TEMPLATE_PATH
        );
        return $this->handle($params);
    }
}
