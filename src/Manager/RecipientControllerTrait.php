<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Manager;

use Allmega\BlogBundle\{Data, Events};
use Allmega\BlogBundle\Entity\Recipient;
use Allmega\BlogBundle\Form\RecipientType;
use Allmega\BlogBundle\Controller\RecipientController;
use Allmega\BlogBundle\Utils\Params\BaseControllerParams;
use Symfony\Component\HttpFoundation\Response;

trait RecipientControllerTrait
{

    private function save(Recipient $recipient = null, array $arguments = []): Response
    {
        $eventName = $recipient ? Events::RECIPIENT_UPDATED : Events::RECIPIENT_CREATED;
        $recipient = $recipient ?? new Recipient();

        $params = (new BaseControllerParams())->init(
            arguments: $arguments,
            formParams: ['item' => $recipient],
            entity: $recipient,
            domain: Data::DOMAIN,
            eventName: $eventName,
            formType: RecipientType::class,
            routeName: RecipientController::ROUTE_NAME,
            templatesPath: RecipientController::ROUTE_TEMPLATE_PATH
        );
        return $this->handle($params);
    }
}