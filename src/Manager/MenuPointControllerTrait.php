<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Manager;

use Allmega\BlogBundle\{Data, Events};
use Allmega\BlogBundle\Entity\MenuPoint;
use Allmega\BlogBundle\Form\MenuPointType;
use Allmega\BlogBundle\Controller\MenuPointController;
use Allmega\BlogBundle\Utils\Params\BaseControllerParams;
use Symfony\Component\HttpFoundation\Response;

trait MenuPointControllerTrait
{
    protected function preExecution(): static
    {
        $menupoint = $this->params->getEntity();
        switch ($this->params->getAction()) {
            case $this->edit:
            case $this->add:
                $this->menupointHandler->handle($menupoint);
                break;
            case $this->state:
                $this->checkState($menupoint);
                break;
            case $this->delete:
                $this->checkDelete($menupoint);
                break;
            default:
        }
        return $this;
    }

    protected function postExecution(): static
    {
        switch ($this->params->getAction()) {
            case $this->state:
                $this->menupointHandler->handle($this->params->getEntity());
                break;
            default:
        }
        return $this;
    }

    private function checkState(MenuPoint $menupoint): void
    {
        if ($menupoint->isSys() && !$menupoint->canBeDeactivated()) {
            $this->params->setEventName(Events::MENUPOINT_STATE_CHANGE_FAILED);
            $this->done = false;
        }
    }

    private function checkDelete(MenuPoint $menupoint): void
    {
        $menupoint->isSys() ?
            $this->params->setEventName(Events::MENUPOINT_DELETE_FAILED) :
            $this->menupointHandler->handle($menupoint, true);

        $this->done = false;
    }

    private function renderCategoriesTypes(MenuPoint $menupoint): string
    {
        $content = '';
        foreach ($menupoint->getCategoriesTypes() as $categoryType) {
            $controllerExists = class_exists($categoryType->getController());
            $entityExists = class_exists($categoryType->getEntityname());

            if ($entityExists && $controllerExists) {
                $items = $this
                    ->getEntityManager()
                    ->getRepository($categoryType->getEntityname())
                    ->findByMenupoint($menupoint);

                $package = ucfirst($categoryType->getPackage());
                $domain = 'Allmega' . $package . 'Bundle';
                $params = $this->getTemplateParams($categoryType->getController(), $domain, ['items' => $items]);

                $template = '@Allmega' . $package . '/' . $categoryType->getTemplate();
                $content .= $this->renderView($template, ['params' => $params]);
            }
        }
        return $content;
    }

    private function buildMenuPoints(): array
    {
        $menupoints = $this->menupointRepo->findWithoutGroups();
        $user = $this->getUser();

        if ($user) {
            $rows = $this->menupointRepo->findByUserGroups($user);
            $menupoints = array_merge($menupoints, $rows);
        }

        usort($menupoints, fn($a, $b) => $a->getPrio() - $b->getPrio());

        $this->markCurrentMenuPoint($menupoints);
        return $menupoints;
    }

	private function markCurrentMenuPoint(array &$menupoints): void
	{
		$mp = $this->getRequest()->attributes->get('menupoint');
		$path = $this->getRequest()->getPathInfo();
		$selected = false;
		$found = false;

		foreach ($menupoints as $menupoint) {
			$mpType = $menupoint->getType()->getShortname();
			switch ($mpType) {
                case Data::MENUPOINT_TYPE_ROUTE:
					$route = $menupoint->getRoute();
					if (!$found && $path === $route || $path === $route . '/') $found = true;
					break;
				case Data::MENUPOINT_TYPE_CATEGORY:
                    $route = $this
                        ->getUrlGenerator()
                        ->generate(MenuPointController::ROUTE_NAME . 'show', ['slug' => $menupoint->getSlug()]);
					$menupoint->setRoute($route);
					if (!$found && $mp && $mp->getSlug() === $menupoint->getSlug()) $found = true;
                    break;
                default:
			}

            if (!$selected && $found) {
                $menupoint->setSelected(true);
				$selected = true;
            }
		}
	}

    private function save(MenuPoint $menupoint = null, array $arguments = []): Response
    {
        $formParams = $this->buildFormParams($menupoint);

        $eventName = $menupoint ? Events::MENUPOINT_UPDATED : Events::MENUPOINT_CREATED;
        $menupoint = $menupoint ?? new MenuPoint();

        $params = (new BaseControllerParams())->init( 
            arguments: $arguments,
            formParams: $formParams,
            entity: $menupoint,
            domain: Data::DOMAIN,
            eventName: $eventName,
            formType: MenuPointType::class,
            routeName: MenuPointController::ROUTE_NAME,
            templatesPath: MenuPointController::ROUTE_TEMPLATE_PATH
        );
        return $this->handle($params);
    }

    private function buildFormParams(?MenuPoint $menupoint): array
    {
        $template = MenuPointController::ROUTE_TEMPLATE_PATH . 'inc/_menupoint_types.html.twig';
        return [
            'item' => $menupoint,
            'blocks' => [
                'modify' => [
                    'bottom' => $this->renderView($template, [
                            'menupointTypes' => $this->getMenupointTypes()
                        ]
                    )
                ]
            ]
        ];
    }

    private function getMenupointTypes(): array
    {
        $rows = $this->menupointTypeRepo->findAll();
        $data = [];

        foreach ($rows as $row) {
            $shortname = $row->getShortname();

            $offset = strpos($shortname, '.') + 1;
            $shortname = substr($shortname, $offset, strlen($shortname));

            $data['rows'][] = ['shortname' => $shortname, 'id' => $row->getId()];
            $data['names'][] = $shortname;
        }
        return $data;
    }
}