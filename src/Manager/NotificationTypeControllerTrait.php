<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Manager;

use Allmega\BlogBundle\{Data, Events};
use Allmega\BlogBundle\Entity\NotificationType;
use Allmega\BlogBundle\Form\NotificationTypeType;
use Allmega\BlogBundle\Controller\NotificationTypeController;
use Allmega\BlogBundle\Utils\Params\BaseControllerParams;
use Symfony\Component\HttpFoundation\Response;

trait NotificationTypeControllerTrait
{
    private function save(NotificationType $notificationtype = null, array $arguments = []): Response
    {
        $formParams = ['item' => $notificationtype, 'showDelete' => false];

        $eventName = $notificationtype ? Events::NOTIFICATIONTYPE_UPDATED : Events::NOTIFICATIONTYPE_CREATED;
        $notificationtype = $notificationtype ?? new NotificationType();

        $params = (new BaseControllerParams())->init(
            arguments: $arguments,
            formParams: $formParams,
            entity: $notificationtype,
            domain: Data::DOMAIN,
            eventName: $eventName,
            formType: NotificationTypeType::class,
            routeName: NotificationTypeController::ROUTE_NAME,
            templatesPath: NotificationTypeController::ROUTE_TEMPLATE_PATH
        );
        return $this->handle($params);
    }
}