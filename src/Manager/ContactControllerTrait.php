<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Manager;

use Allmega\BlogBundle\{Data, Events};
use Allmega\BlogBundle\Controller\ContactController;
use Allmega\BlogBundle\Entity\{Contact, Notification};
use Allmega\BlogBundle\Form\{AnswerType, ContactType};
use Allmega\BlogBundle\Utils\Params\BaseControllerParams;
use Symfony\Component\HttpFoundation\Response;
use DateTime;

trait ContactControllerTrait
{
    protected function preExecution(): static
    { 
        switch ($this->params->getAction()) {
            case $this->add:
                if ($this->validateMail) $this->checkContactSender();
                break;
            case $this->edit:
                $this->params
                    ->getEntity()
                    ->setAnswered(true)
                    ->setReadedAt(new DateTime());
                break;
            case $this->state:
                $this->params
                    ->getEntity()
                    ->setReadedAt(new DateTime());
                break;
            default:
        }
        return $this;
    }

    protected function postExecution(): static
    {
        switch ($this->params->getAction()) {
            case $this->edit:
                $this->sendMail();
                break;
            default:
        }
        return $this;
    }

    protected function setRedirect(): static
    {
        switch ($this->params->getAction()) {
            case $this->add:
                $this->params->setRouteName($this->params->getRouteShort(), $this->add);
                break;
            default:
        }
        return $this;
    }

    private function checkContactSender(): void
    {
        $sender = $this->params->getEntity()->getEmail();
        $result = $this->validateEmails([$sender]);
            
        if (!$result[$sender]) {
            $this->params->addErrorMessage('label.invalid_email');
            $this->cancel();
        }
    }

    private function sendMail(): void
    {
        $contact = $this->params->getEntity();
        $params = ['content' => $contact->getMessage()];

        $notification = (new Notification())
            ->setReceivers([$contact->getEmail()])
            ->setSubject($contact->getSubject())
            ->setParams($params);

        $this->services
            ->getService('bus')
            ->dispatch($notification);
    }

    private function save(Contact $contact = null, array $arguments = []): Response
    {
        $formPath = $contact ? null : ContactController::ROUTE_TEMPLATE_PATH;
        $formParams = $this->buildFormParams($contact);
        $hideLinks = !$contact;

        $eventName = $contact ? Events::CONTACT_ANSWERED : Events::CONTACT_INQUIRY;
        $formType = $contact ? AnswerType::class : ContactType::class;
        $contact = $contact ?? new Contact();
    
        $params = (new BaseControllerParams())->init(
            arguments: $arguments,
            formParams: $formParams,
            hideLinks: $hideLinks,
            entity: $contact,
            domain: Data::DOMAIN,
            eventName: $eventName,
            formPath: $formPath,
            formType: $formType,
            routeName: ContactController::ROUTE_NAME,
            templatesPath: ContactController::ROUTE_TEMPLATE_PATH
        );
        return $this->handle($params);
    }

    private function buildFormParams(?Contact $contact): array
    {
        $label = $this->translate('action.send.message', [], Data::DOMAIN);
        if ($contact) {
            $template = ContactController::ROUTE_TEMPLATE_PATH . 'inc/_old_message.html.twig';
            $content = $this->renderView($template, [
                'params' => ['message' => $contact->getMessage(), 'domain' => Data::DOMAIN]
            ]);
            $contact->setSubject('RE: ' . $contact->getSubject())->setMessage($content);
            $formParams = [
                'title' => ['content' => 'contact.answer'],
                'form' => [
                    'params' => ['id' => $contact->getId()],
                    'path' => ContactController::ROUTE_NAME . 'edit',
                    'icon' => 'paper-plane',
                    'label' => $label,
                ],
                'item' => $contact
            ];
        } else {
            $formParams = [
                'form' => [
                    'path' => ContactController::ROUTE_NAME . 'add',
                    'icon' => 'paper-plane',
                    'label' => $label,
                    'load' => false,
                    'params' => [],
                ],
            ];
        }
        return $formParams;
    }
}
