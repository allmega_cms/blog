<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Manager;

use Allmega\BlogBundle\Entity\Post;
use Allmega\BlogBundle\Data;

trait PostControllerTestTrait
{
    private function createPost(): Post
    {
        $author = $this->findUserByRole(Data::POST_AUTHOR_ROLE);
        $post = Post::build(authors: [$author]);
        $this->em->persist($post);
        $this->em->flush();
        return $post;
    }
}