<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Manager;

use Allmega\BlogBundle\Entity\Job;
use Allmega\BlogBundle\Form\JobType;
use Allmega\BlogBundle\{Data, Events};
use Allmega\BlogBundle\Controller\JobController;
use Allmega\BlogBundle\Utils\Params\BaseControllerParams;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Response;

trait JobControllerTrait
{
    protected function preExecution(): static
    {
        switch ($this->params->getAction()) {
            case $this->add:
                if (!$this->validateMail) $this->saveFile();
                else {
                    $this->checkJobSender() ? $this->saveFile() : $this->cancel();
                }
                break;
            default:
        }
        return $this;
    }
    
    protected function postExecution(): static
    {
        switch ($this->params->getAction()) {
            case $this->delete:
                $this->removeFile();
                break;
            default:
        }
        return $this;
    }

    protected function setRedirect(): static
    {
        switch ($this->params->getAction()) {
            case $this->add:
                $this->params->setRouteName($this->params->getRouteShort(), $this->show);
                break;
            default:
        }
        return $this;
    }

    private function checkJobSender(): bool
    {
        $sender = $this->params->getEntity()->getEmail();
        $result = $this->validateEmails([$sender]);
            
        if (!$result[$sender]) {
            $this->params->addErrorMessage('label.invalid_email');
            return false;
        }
        return true;
    }

    private function saveFile(): void
    {
        $file = $this->form->get('documents')->getData();
        if ($file) {
            $filename = $this->buildFilename($file);
            $file->move($this->config->getDocumentsDir(), $filename);
            $this->params->getEntity()->setDocuments($filename);
        }
    }

    private function buildFilename(UploadedFile $file): string
    {
        $originalFilename = pathinfo($file->getClientOriginalName(), PATHINFO_FILENAME);
        $safeFilename = $this->services->getService('slugger')->slug($originalFilename);
        return $safeFilename . '-' . uniqid() . '.' . $file->guessExtension();
    }

    private function removeFile(): void
    {
        $document = $this->params->getEntity()->getDocuments();
        if ($document) {
            $filename = $this->config->getDocumentsDir() . $document;
            if ($this->filesystem->exists($filename)) {
                $this->filesystem->remove($filename);
            }
        }
    }

    private function save(): Response
    {
        $params = (new BaseControllerParams())->init(
            arguments: ['folder' => $this->config->getDocumentsDir()],
            formParams: $this->buildFormParams(),
            entity: new Job(),
            domain: Data::DOMAIN,
            eventName: Events::JOB_INQUIRY,
            formType: JobType::class,
            routeName: JobController::ROUTE_NAME,
            templatesPath: JobController::ROUTE_TEMPLATE_PATH
        );
        return $this->handle($params);
    }

    private function buildFormParams(): array
    {
        $label = $this->translate('action.send.message', [], Data::DOMAIN);
        return [
            'link' => [
                'route' => JobController::ROUTE_NAME . 'show',
                'title' => 'action.back'
            ],
            'form' => [
                'path' => JobController::ROUTE_NAME . 'add',
                'icon' => 'paper-plane',
                'label' => $label,
                'params' => [],
            ]
        ];
    }
}