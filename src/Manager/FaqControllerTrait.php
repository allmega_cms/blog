<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Manager;

use Allmega\BlogBundle\Entity\Faq;
use Allmega\BlogBundle\Form\FaqType;
use Allmega\BlogBundle\{Data, Events};
use Allmega\BlogBundle\Controller\FaqController;
use Allmega\BlogBundle\Utils\Params\BaseControllerParams;
use Symfony\Component\HttpFoundation\Response;

trait FaqControllerTrait
{
    protected function preExecution(): static
    {
        $user = $this->getUser();
        switch ($this->params->getAction()) {
            case $this->add:
                $this->params
                    ->getEntity()
                    ->setCreator($user)
                    ->setEditor($user);
                break;
            case $this->edit:
                $this->params
                    ->getEntity()
                    ->setEditor($user)
                    ->setUpdated();
                break;
            case $this->delete:
                $this->params
                    ->getEntity()
                    ->getMenuPoints()
                    ->clear();
                break;
            default:
        }
        return $this;
    }

    private function save(Faq $faq = null, array $arguments = []): Response
    {
        $eventName = $faq ? Events::FAQ_UPDATED : Events::FAQ_CREATED;
        $faq = $faq ?? new Faq();

        $params = (new BaseControllerParams())->init(
            arguments: $arguments,
            formParams: ['item' => $faq],
            entity: $faq,
            domain: Data::DOMAIN,
            eventName: $eventName,
            formType: FaqType::class,
            routeName: FaqController::ROUTE_NAME,
            templatesPath: FaqController::ROUTE_TEMPLATE_PATH
        );
        return $this->handle($params);
    }
}
