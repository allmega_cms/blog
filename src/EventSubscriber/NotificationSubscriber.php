<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\EventSubscriber;

use Allmega\BlogBundle\{Data, Events};
use Allmega\BlogBundle\Entity\{Post, Comment};
use Allmega\BlogBundle\Utils\Params\NotificationParams;
use Symfony\Component\EventDispatcher\{EventSubscriberInterface, GenericEvent};
use Symfony\Component\Messenger\Exception\ExceptionInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Doctrine\ORM\EntityManagerInterface;

readonly class NotificationSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private UrlGeneratorInterface  $urlGenerator,
        private EntityManagerInterface $em,
        private MessageBusInterface    $bus) {}

    public static function getSubscribedEvents(): array
    {
        return [
            Events::JOB_INQUIRY  => 'onJobInquiry',
            Events::CONTACT_INQUIRY => 'onContactInquiry',
            Events::COMMENT_CREATED => 'onCommentCreated',
            Events::COMMENT_ENABLED => 'onCommentEnabled',
        ];
    }

    /**
     * @throws ExceptionInterface
     */
    public function onJobInquiry(GenericEvent $event): void
    {
        $job = $event->getSubject();
        $folder = $event->getArgument('folder');

        $attachments = $job->getDocuments() ? [$folder . $job->getDocuments()] : [];

        $search  = ['%sender%', '%email%', '%phone%', '%contact%', '%message%'];
        $replace = [
            $job->getSender(),
            $job->getEmail(),
            $job->getPhone(),
            $job->getContact(),
            $job->getMessage(),
        ];

        $params = new NotificationParams(Data::NOTIFICATION_JOB, [], $search, $replace, $attachments);
        $this->bus->dispatch($params);
    }

    /**
     * @throws ExceptionInterface
     */
    public function onContactInquiry(GenericEvent $event): void
    {
        $contact = $event->getSubject();
        $search  = ['%sender%', '%email%', '%phone%', '%subject%', '%message%'];
        $replace = [
            $contact->getSender(),
            $contact->getEmail(),
            $contact->getPhone(),
            $contact->getSubject(),
            $contact->getMessage(),
        ];

        $params = new NotificationParams(Data::NOTIFICATION_CONTACT, [], $search, $replace);
        $this->bus->dispatch($params);
    }

    /**
     * @throws ExceptionInterface
     */
    public function onCommentCreated(GenericEvent $event): void
    {
        $comment = $event->getSubject();
        if ($comment->getItem()->getEntityname() == 'Post') {
            $this->sendByPost($comment, Data::NOTIFICATION_COMMENT_NEW);
        }
    }

    /**
     * @throws ExceptionInterface
     */
    public function onCommentEnabled(GenericEvent $event): void
    {
        $comment = $event->getSubject();
        if ($comment->getItem()->getEntityname() == 'Post') {
            $this->sendByPost($comment, Data::NOTIFICATION_COMMENT_ENABLED);
        }
    }

    /**
     * @throws ExceptionInterface
     */
    private function sendByPost(Comment $comment, string $subject): void
    {
        $post = $this->em->getRepository(Post::class)->find($comment->getEntityid());

        $urlParams = ['slug' => $post->getSlug(),'_fragment' => 'answer_'.$comment->getId()];
        $link = $this->urlGenerator->generate('allmega_blog_post_show', $urlParams, UrlGeneratorInterface::ABSOLUTE_URL);

        $search  = ['%title%', '%link%'];
        $replace = [$post->getTitle(), $link];

        $params = new NotificationParams($subject, [], $search, $replace);
        foreach ($post->getAuthors() as $author) $params->addReceiver($author->getEmail());
        $this->bus->dispatch($params);
    }
}