<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\EventSubscriber;

use Allmega\AuthBundle\Entity\User;
use Allmega\BlogBundle\{Data, Events};
use Allmega\BlogBundle\Utils\FlashBag;
use Allmega\AuthBundle\Events as AuthEvents;
use Allmega\BlogBundle\Entity\{Comment, EmailAlias, Faq, Tag};
use Symfony\Component\EventDispatcher\{EventSubscriberInterface, GenericEvent};
use Doctrine\ORM\EntityManagerInterface;

readonly class DeletionSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private EntityManagerInterface $em,
        private FlashBag $flashbag) {}

    public static function getSubscribedEvents(): array
    {
        return [
            Events::MENUPOINT_DELETED => 'handleMenupointRelations',
            AuthEvents::USER_DELETE => 'handleUserRelations',
            Events::DELETE_COMMENTS => 'deleteComments',
            Events::TAG_DELETED => 'handleTagRelations',
        ];
    }

    public function handleMenupointRelations(GenericEvent $event, string $eventName): void
    {
        $menupoint = $event->getSubject();
        foreach ($menupoint->getCategoriesTypes() as $categoryType) {
            $controllerExists = class_exists($categoryType->getController());
            $entityExists = class_exists($categoryType->getEntityname());

            if ($entityExists && $controllerExists) {
                $items = $this->em
                    ->getRepository($categoryType->getEntityname())
                    ->findByMenupoint($menupoint);

                foreach ($items as $item) {
                    $item->removeMenuPoint($menupoint);
                    $this->em->persist($item);
                }
            }
        }
        $this->em->flush();
    }

    public function deleteComments(GenericEvent $event, string $eventName): void
    {
        if ($event->hasArgument('ids')) {
            $ids = $event->getArgument('ids');
            $this->em->getRepository(Comment::class)->deleteByIds($ids);
            
            $isXmlRequest = $event->getArgument('isXmlRequest');
            if (!$isXmlRequest) $this->flashbag->add('success', $eventName, Data::DOMAIN);
        }
    }

    public function handleUserRelations(GenericEvent $event, string $eventName): void
    {
        $userRepo = $this->em->getRepository(User::class);
        $user = $event->getSubject();
        
        $userRepo
            ->updateField(EmailAlias::class, 'creator', $user)
            ->updateField(EmailAlias::class, 'editor', $user)
            ->updateField(Comment::class, 'author', $user)
            ->updateField(Faq::class, 'creator', $user)
            ->updateField(Faq::class, 'editor', $user);

        $props = ['email_alias', 'post'];
        foreach ($props as $prop) {
            $table = 'allmega_blog__' . $prop . '_user';
            $userRepo->deleteUserRelations($table, $user);
        }
    }

    public function handleTagRelations(GenericEvent $event, string $eventName): void
    {
        $this->em
            ->getRepository(Tag::class)
            ->deleteTagRelations('allmega_blog__post_tag', $event);
    }
}