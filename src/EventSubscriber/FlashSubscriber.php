<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\EventSubscriber;

use Allmega\BlogBundle\Events;
use Allmega\BlogBundle\Model\FlashesTrait;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class FlashSubscriber implements EventSubscriberInterface
{
    use FlashesTrait;

    public static function getSubscribedEvents(): array
    {
        return [
            Events::STARTPAGE_CHANGED => 'addSuccessFlash',
            Events::COMPANY_UPDATED => 'addSuccessFlash',
            Events::TAG_DELETED => 'addSuccessFlash',
            Events::JOB_INQUIRY => 'addSuccessFlash',
            Events::JOB_DELETED => 'addSuccessFlash',
            Events::CONTACT_INQUIRY => 'addSuccessFlash',
            Events::CONTACT_ANSWERED => 'addSuccessFlash',
            Events::CONTACT_DELETED => 'addSuccessFlash',
            Events::CONTACT_STATE_CHANGED => 'addSuccessFlash',
            Events::PAGE_CREATED => 'addSuccessFlash',
            Events::PAGE_UPDATED => 'addSuccessFlash',
            Events::PAGE_DELETED => 'addSuccessFlash',
            Events::PAGE_STATE_CHANGED => 'addSuccessFlash',
            Events::POST_CREATED => 'addSuccessFlash',
            Events::POST_UPDATED => 'addSuccessFlash',
            Events::POST_DELETED => 'addSuccessFlash',
            Events::POST_STATE_CHANGED => 'addSuccessFlash',
            Events::COMMENT_ENABLED => 'addSuccessFlash',
            Events::COMMENT_UPDATED => 'addSuccessFlash',
            Events::COMMENT_DELETED => 'addSuccessFlash',
            Events::FAQ_CREATED => 'addSuccessFlash',
            Events::FAQ_UPDATED => 'addSuccessFlash',
            Events::FAQ_DELETED => 'addSuccessFlash',
            Events::FAQ_STATE_CHANGED => 'addSuccessFlash',
            Events::MENUPOINT_CREATED => 'addSuccessFlash',
            Events::MENUPOINT_UPDATED => 'addSuccessFlash',
            Events::MENUPOINT_DELETED => 'addSuccessFlash',
            Events::MENUPOINT_DELETE_FAILED => 'addWarningFlash',
            Events::MENUPOINT_STATE_CHANGED => 'addSuccessFlash',
            Events::MENUPOINT_STATE_CHANGE_FAILED => 'addWarningFlash',
            Events::RECIPIENT_CREATED => 'addSuccessFlash',
            Events::RECIPIENT_UPDATED => 'addSuccessFlash',
            Events::RECIPIENT_DELETED => 'addSuccessFlash',
            Events::RECIPIENT_STATE_CHANGED => 'addSuccessFlash',
            Events::NOTIFICATIONTYPE_UPDATED => 'addSuccessFlash',
            Events::NOTIFICATIONTYPE_STATE_CHANGED => 'addSuccessFlash',
            Events::EMAILALIAS_CREATED => 'addSuccessFlash',
            Events::EMAILALIAS_UPDATED => 'addSuccessFlash',
            Events::EMAILALIAS_DELETED => 'addSuccessFlash',
            Events::EMAILALIAS_STATE_CHANGED => 'addSuccessFlash',
        ];
    }
}