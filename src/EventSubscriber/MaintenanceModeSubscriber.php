<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright Allmega 
 * @package   Auth Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\EventSubscriber;

use Allmega\BlogBundle\Controller\DefaultController;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\KernelEvents;

readonly class MaintenanceModeSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private UrlGeneratorInterface $urlGenerator,
        private int $maintenanceMode) {}

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::REQUEST => 'onRequest',
        ];
    }

    public function onRequest(RequestEvent $event): void
    {
        $maintenanceRoute = DefaultController::ROUTE_NAME . 'maintenance';
        $currentRoute = $event->getRequest()->get('_route');
        $eqRoute = $currentRoute === $maintenanceRoute;

        if ($this->maintenanceMode != 0) {
            if ($eqRoute) return;
            $route = $this->urlGenerator->generate($maintenanceRoute);
            $event->setResponse(new RedirectResponse($route));
        } elseif ($eqRoute) {
            $event->setResponse(new RedirectResponse('/'));
        }
    }
}