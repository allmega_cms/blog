<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Model;

use Allmega\AuthBundle\Entity\User;
use DateTimeInterface;

interface ItemInfoInterface
{
	public function getCreated(): ?DateTimeInterface;
    public function getUpdated(): ?DateTimeInterface;
	public function getCreator(): ?User;
    public function getEditor(): ?User;
}