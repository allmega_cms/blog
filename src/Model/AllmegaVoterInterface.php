<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Model;

use Allmega\AuthBundle\Entity\User;

interface AllmegaVoterInterface
{
	public function isGranted(string $attribute, mixed $subject = null, ?User $user = null): bool;
	public function isSubjectValid(mixed $subject): bool;
}