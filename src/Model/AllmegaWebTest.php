<?php

/**
 * This file is part of the Allmega Announcement Bundle package.
 *
 * @copyright Allmega 
 * @package   Announcement Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Model;

use Allmega\BlogBundle\Entity\Item;
use Allmega\AuthBundle\Entity\{Group, User};
use Allmega\BlogBundle\Repository\ItemRepository;
use Allmega\BlogBundle\Utils\Params\WebTestParams;
use Allmega\AuthBundle\Controller\SecurityController;
use Allmega\AuthBundle\Repository\{GroupRepository, UserRepository};
use Symfony\Component\VarExporter\Exception\ClassNotFoundException;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Component\Routing\RouterInterface;
use Symfony\Component\DomCrawler\Crawler;
use Doctrine\ORM\EntityManagerInterface;

abstract class AllmegaWebTest extends WebTestCase
{
    protected ?KernelBrowser $client = null;
    protected EntityManagerInterface $em;
    protected RouterInterface $router;
    protected WebTestParams $params;
    protected ?User $user = null;
    protected Crawler $crawler;
    
    protected string $dashboard = 'dashboard';
    protected string $delete = 'delete';
    protected string $search = 'search';
    protected string $index = 'index';
    protected string $state = 'state';
    protected string $show = 'show';
    protected string $edit = 'edit';
    protected string $icon = 'icon';
    protected string $add = 'add';

    public function setUp(): void
    {
        if (!$this->client) {
            $this->client = self::createClient();
            $this->em = self::getContainer()->get('doctrine')->getManager();
            $this->router = self::getContainer()->get('router');
            $this->params = new WebTestParams($this->router);
            $this->setProps();
        }
    }

    /**
     * @throws ClassNotFoundException
     */
    protected function runTests(
        string $routeName,
        string $role = null,
        bool $create = false,
        bool $testRedirect = true,
        string $fnRouteParams = null,
        array $routeParams = [],
        array $optParams = [],
        bool $xmlHttp = false,
        string $method = 'GET'): void
    {
        $this->params->init(
            testRedirect: $testRedirect,
            fnRouteParams: $fnRouteParams,
            routeName: $routeName,
            routeParams: $routeParams,
            method: $method,
            optParams: $optParams,
            xmlHttp: $xmlHttp,
            create: $create
        );
        $this->handleParams()->runWebTest();

        $this->params->setRole($role)->setTestRedirect(false);
        $this->runWebTest();
    }

    /**
     * @throws ClassNotFoundException
     */
    protected function runModifyTests(
        string $fnRouteParams = null,
        string $routeName = null,
        string $role = null,
        bool $delete = false,
        bool $testRedirect = true,
        array $redirectParams = [],
        int $redirectCode = 0,
        string $redirectName = null,
        string $redirectRoute = null,
        string $fnRedirectParams = null): void
    {
        if (!$redirectRoute) {
            $name = $delete ? $this->index : $this->show;
            $redirectName = $redirectName ?? $name;
        }

        $name = $delete ? $this->delete : $this->state;
        $routeName = $routeName ?? $name;
        $method = $delete ? 'DELETE' : 'GET';

        $this->params->init(
            fnRedirectParams: $fnRedirectParams,
            redirectRoute: $redirectRoute,
            redirectName: $redirectName,
            redirectParams: $redirectParams,
            testRedirect: $testRedirect,
            fnRouteParams: $fnRouteParams,
            routeName: $routeName,
            method: $method,
            redirectCode: $redirectCode,
            create: true,
        );
        $this->handleParams()->runWebTest();

        $this->params->setRole($role)->setTestRedirect(false);
        $this->runWebTest();
    }

    /**
     * @throws ClassNotFoundException
     */
    protected function runPublicTest(
        string $routeName,
        string $fnRouteParams = null,
        bool $create = false): void
    {
        $this->params->init(
            testRedirect: false,
            fnRouteParams: $fnRouteParams,
            routeName: $routeName,
            create: $create
        );
        $this->handleParams()->runWebTest();
    }

    /**
     * @throws ClassNotFoundException
     */
    protected function runWebTest(bool $doRequest = true): void
    {
        if ($doRequest) $this->doRequest();

        if ($this->params->getTestRedirect()) {
            $url = $this->router->generate($this->getLoginRoute());
            $this->assertResponseRedirects($url, 302);
        } else {
            $this->params->getRedirectUrl() ?
                $this->assertRouteRedirect() :
                $this->assertResponseIsSuccessful();
        }
    }

    /**
     * @throws ClassNotFoundException
     */
    protected function doRequest(): void
    {
        $this->setAndLoginUser();
        $method = $this->params->isXmlHttp() ? 'xmlHttpRequest' : 'request';

        $this->crawler = $this->client->$method(
            $this->params->getMethod(),
            $this->params->getRouteUrl(),
            $this->params->getOptParams(),
            $this->params->getFiles()
        );
    }

    protected function handleParams(): static
    {
        $this
            ->createEntity()
            ->setRouteName()
            ->addRouteParams()
            ->setRedirectName()
            ->addRedirectParams();

        $this->params->setRouteUrl()->setRedirectUrl();
        return $this;
    }

    protected function createEntity(): static
    {
        if ($this->params->getCreate() && !$this->params->getEntity()) {
            $this->create();
        }
        return $this;
    }

    protected function setRouteName(): static
    {
        $routeName = $this->params->getRouteName();
        if ($routeName) $this->params->setRouteName($this->getRouteName($routeName));
        return $this;
    }

    protected function addRouteParams(): static
    {
        if (!$this->params->getRouteParams()) {
            $method = $this->params->getFnRouteParams();
            if ($method && method_exists($this, $method)) {
                $this->params->addRouteParams($this->$method());
            } else {
                $this->params->addRouteParams($this->getIdAsRouteParams());
            }
        }
        return $this;
    }

    protected function setRedirectName(): static
    {
        $redirectName = $this->params->getRedirectName();
        if ($redirectName) $this->params->setRedirectName($this->getRouteName($redirectName));
        return $this;
    }

    protected function addRedirectParams(): static
    {
        if (!$this->params->getRedirectParams()) {
            $method = $this->params->getFnRedirectParams();
            if ($method && method_exists($this, $method)) {
                $this->params->addRedirectParams($this->$method());
            } else {
                $this->params->addRouteParams($this->getIdAsRouteParams());
            }
        }
        return $this;
    }

    protected function getIdAsRouteParams(): array
    {
        $entity = $this->params->getEntity();
        return $entity ? ['id' => $entity->getId()] : [];
    }

    protected function getSlugAsRouteParams(): array
    {
        $entity = $this->params->getEntity();
        return $entity ? ['slug' => $entity->getSlug()] : [];
    }

    /**
     * @throws ClassNotFoundException
     */
    protected function setAndLoginUser(): static
    {
        $this->user = $this->params->getRole() ? $this->findUserByRole($this->params->getRole()) : null;
        if ($this->params->getLogin() && $this->user) {
            $this->client->loginUser($this->user);
        }
        return $this;
    }

    protected function assertRouteRedirect(): void
    {
        $this->assertResponseRedirects($this->params->getRedirectUrl(), $this->params->getRedirectCode());
    }

    /**
     * @throws ClassNotFoundException
     */
    protected function getService(string $class): mixed
    {
        if (!class_exists($class)) throw new ClassNotFoundException($class);
        return self::getContainer()->get($class);
    }

    /**
     * @throws ClassNotFoundException
     */
    protected function findItemByName(string $entityname): Item
    {
        return $this->getService(ItemRepository::class)->findOneBy(['entityname' => $entityname]);
    }

    /**
     * @throws ClassNotFoundException
     */
    protected function findUserByRole(string $role): ?User
    {
        return $this->getService(UserRepository::class)->findOneByRole($role);
    }

    /**
     * @throws ClassNotFoundException
     */
    protected function findGroupByShortname(string $shortname): Group
    {
        return $this->getService(GroupRepository::class)->findOneBy(['shortname' => $shortname]);
    }

    protected function getLoginRoute(): string
    {
        return SecurityController::ROUTE_NAME . 'login';
    }

    protected function getJsonResponse(): array
    {
        $this->hasResponseValidJson();
        return json_decode($this->client->getResponse()->getContent(), true);
    }

    protected function hasResponseValidJson(): static
    {
        $response = $this->client->getResponse();
        $this->assertTrue($response->headers->contains('Content-Type', 'application/json'));
        $this->assertJson($response->getContent());
        return $this;
    }

    abstract protected function getRouteName(string $name): string;
    protected function setProps(): void {}
    protected function create(): void {}
}