<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright Allmega
 * @package   Auth Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Model;

use Allmega\BlogBundle\Entity\MenuPoint;

trait MenuPointsTrait
{
    public function getMenuPointsOptions(): array
    {
        if (!$this->menuPointsParams->getQueryBuilder()) $this->setMenuPointsQueryBuilder();

        return array_merge(
            $this->menuPointsParams->getBaseOptions(MenuPoint::class, 'title'),
            $this->menuPointsParams->getMultipleOptions(),
        );
    }

    private function setMenuPointsQueryBuilder(): void
    {
        $name = $this->menuPointsParams->getEntity();
        $queryBuilder = $this->menuPointRepo->findByEntityName($name);
        $this->menuPointsParams->setQueryBuilder($queryBuilder);
    }
}