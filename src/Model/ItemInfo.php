<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Model;

use Allmega\AuthBundle\Entity\User;
use Allmega\MediaBundle\Model\MediaInterface;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\DBAL\Types\Types;
use DateTimeInterface;
use DateTime;

abstract class ItemInfo implements ItemInfoInterface, MediaInterface
{
    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    protected ?DateTimeInterface $created = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    protected ?DateTimeInterface $updated = null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    protected ?User $creator = null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    protected ?User $editor = null;

    protected string $mediaDir = '';
    protected array $mediatypes = [];

    public function __construct()
    {
        $this->created = new DateTime();
        $this->updated = new DateTime();
    }

    public function getCreated(): ?DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(DateTimeInterface $created): static
    {
        $this->created = $created;
        return $this;
    }

    public function getUpdated(): ?DateTimeInterface
    {
        return $this->updated;
    }

    public function setUpdated(DateTimeInterface $updated = null): static
    {
        $this->updated = $updated ?? new DateTime();
        return $this;
    }

    public function getCreator(): ?User
    {
        return $this->creator;
    }

    public function setCreator(User $creator): static
    {
        $this->creator = $creator;
        return $this;
    }

    public function getEditor(): ?User
    {
        return $this->editor;
    }

    public function setEditor(User $editor): static
    {
        $this->editor = $editor;
        return $this;
    }

    public function getMediatypes(): array
    {
        return $this->mediatypes;
    }

    public function setMediatypes(array $mediatypes): static
    {
        $this->mediatypes = $mediatypes;
        return $this;
    }

    public function getMediaDir(): string
    {
        if (!$this->mediaDir) $this->setMediaDir();
        return $this->mediaDir;
    }

    public function setMediaDir(): void {}
}