<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Model;

use Allmega\AuthBundle\Entity\User;
use Allmega\BlogBundle\Data as BlogData;
use Allmega\AuthBundle\Data as AuthData;
use Allmega\BlogBundle\Utils\Params\VoterParams;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

trait BaseVoterTrait
{
    protected array  $attributes = ['dashboard','delete','search','files', 'state','list','show','edit','add'];
    protected bool   $isSetted = false;
    protected string $dashboard;
    protected string $delete;
    protected string $search;
    protected string $files;
    protected string $state;
    protected string $list;
    protected string $show;
    protected string $edit;
    protected string $add;

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token): bool
    {
        return $this->isGranted($attribute, $subject, $token->getUser());
    }

    protected function isSettedAndSupports(string $attribute, mixed $subject): bool
    {
        return $this->isSetted || $this->supports($attribute, $subject);
    }

    protected function hasAttributeAndValidSubject(VoterParams $params): bool
    {
        $this->setAttributes($params);
        $exists = in_array($params->getAttribute(), $this->attributes, true);
        return $params->getSubject() ? $this->isSubjectValid($params->getSubject()) && $exists : $exists;
    }

    protected function setAttributes(VoterParams $params): void
    {
        if ($this->isSetted) return;

        $attributes = $params->getBase() ? 
            array_merge($this->attributes, $params->getExtraAttributes()) : 
            $params->getExtraAttributes();

        $this->attributes = array_unique($attributes);
        foreach ($this->attributes as $key => $value) {
            $this->$value = $params->getPrefix() . '-' . $value;
            $this->attributes[$key] = $this->$value;
        }
        $this->isSetted = true;
    }

    protected function createVoterParams(string $attribute, mixed $subject, string $prefix, array $extraAttributes = []): VoterParams
    {
        return (new VoterParams())
            ->setExtraAttributes($extraAttributes)
            ->setAttribute($attribute)
            ->setSubject($subject)
            ->setPrefix($prefix);
    }

    protected function hasRole(?User $user, string $role = AuthData::USER_ROLE): bool
    {
        return $this->isUser($user) && $user->hasRole($role);
    }

    protected function isUser(?User $user): bool
    {
        return $user instanceof User;
    }

    protected function isSameUser(?User $user, ?User $author): bool
    {
        return $this->isUser($user) && $this->isUser($author) && $author->getId() == $user->getId();
    }

    protected function isAdmin(?User $user): bool
    {
        return $this->isUser($user) && $user->hasRole(AuthData::ADMIN_ROLE);
    }

    protected function isBlogAuthor(?User $user): bool
    {
        if ($this->isUser($user)) {
            foreach (BlogData::getAuthorRoles() as $role) {
                if ($user->hasRole($role)) return true;
            }
        }
        return false;
    }

    protected function countSubjectGroups(mixed $subject): int
    {
        return $subject ? $subject->getGroups()->count() : 0;
    }

    protected function hasAccessOnSubject(mixed $subject, User $user): bool
    {
        if ($this->countSubjectGroups($subject) === 0) return true;
        foreach ($user->getGroups() as $group) {
            if ($subject->getGroups()->contains($group)) return true;
        }
        return false;
    }
}