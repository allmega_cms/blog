<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Model;

use Allmega\BlogBundle\Utils\FlashBag;
use Symfony\Component\EventDispatcher\GenericEvent;

trait FlashesTrait
{
    public function __construct(private readonly FlashBag $flashbag) {}

    public function addSuccessFlash(GenericEvent $event, string $eventName): void
    {
        $this->addFlash('success', $event, $eventName);
    }

    public function addWarningFlash(GenericEvent $event, string $eventName): void
    {
        $this->addFlash('warning', $event, $eventName);
    }

    public function addDangerFlash(GenericEvent $event, string $eventName): void
    {
        $this->addFlash('danger', $event, $eventName);
    }

    public function addFlash(string $type, GenericEvent $event, string $eventName): void
    {
        $isXmlRequest = $event->getArgument('isXmlRequest');
        $domain = $event->getArgument('domain') ?? 'messages';
        if (!$isXmlRequest) $this->flashbag->add($type, $eventName, $domain);
    }
}