<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Model;

use Allmega\BlogBundle\Entity\MenuPoint;

trait CategoriziableTrait
{
    public function findByMenuPoint(MenuPoint $menuPoint): array
    {
        return $this->createQueryBuilder('a')
            ->join('a.menuPoints', 'm')
            ->where('m.id = :id')
            ->setParameter('id', $menuPoint->getId())
            ->orderBy('a.updated', 'DESC')
            ->getQuery()
            ->getResult();
    }
}