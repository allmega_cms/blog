<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Model;

use Allmega\BlogBundle\Utils\Register\Entries\{EnvEntry, WebpackEntry};
use Allmega\AuthBundle\Entity\{Group, GroupType, Role, User};
use Allmega\BlogBundle\Utils\Loader\Entries\MenupointEntry;
use Allmega\AuthBundle\Utils\PackageAuthBuilder;
use Allmega\BlogBundle\Entity\NotificationType;
use Allmega\AuthBundle\Model\BaseProps;
use Allmega\BlogBundle\Data;

abstract class PackageData
{
	protected ?PackageAuthBuilder $authBuilder = null;
    protected string $routeType;
    protected string $menuType;
	protected array $data = [];
    protected string $package;
	protected bool $register;

	public function __construct(bool $register = true)
	{
        $this->authBuilder = new PackageAuthBuilder($this->getAuthData());
        $this->routeType = Data::MENUPOINT_TYPE_ROUTE;
        $this->menuType = Data::MENUPOINT_TYPE_MENU;
		$this->register = $register;
	}

	public function getData(string $key): array
	{
		if (!$this->data) {
            $this->register ? $this->setRegisterData() : $this->setLoadData();
        }
		return array_key_exists($key, $this->data) ? $this->data[$key] : [];
	}

    protected abstract function setRegisterData(): void;
    protected abstract function setLoadData(): void;
    protected abstract function getAuthData(): array;

    protected function getEnvEntriesData(): array
    {
        return [];
    }

    protected function getNotificationTypesData(): array
    {
        return [];
    }

    /**
     * @return array<int,EnvEntry>
     */
    protected function getEnvEntries(): array
    {
        $entries = [];
        foreach ($this->getEnvEntriesData() as $rows) {
            if ($rows) {
                list($key, $question, $value) = $rows;
                $entries[] = new EnvEntry($key, $question, $value);
            }
        }
        return $entries;
    }

    /**
     * @return array<int,WebpackEntry>
     */
    protected function getWebpackEntries(): array
    {
        return [
            (new WebpackEntry($this->package))->setDefaults('css'),
            (new WebpackEntry($this->package))->setDefaults(),
        ];
    }

    /**
     * @return array<int,NotificationType>
     */
    protected function getNotificationTypes(): array
    {
        $types = [];
        foreach ($this->getNotificationTypesData() as $shortname) {
            $types[] = NotificationType::build($shortname);
        }
        return [$this->package => $types];
    }

    /**
     * @return array<string,GroupType>
     */
    protected function getGroupTypes(): array
    {
        $groupTypes = $this->authBuilder->getGroupTypes();
        return [$this->package => $this->setSysAndSelectable($groupTypes)];
    }

    /**
     * @return array<string,Group>
     */
    protected function getGroups(): array
    {
        $groups = $this->authBuilder->getGroups();
        return [$this->package => $this->setSysAndSelectable($groups)];
    }

    /**
     * @return array<string,Role>
     */
	protected function getRoles(): array
    {
        $roles = $this->authBuilder->getRoles();
        return [$this->package => $this->setSysAndSelectable($roles)];
    }

    /**
     * @return array<int,User>
     */
	protected function getUsers(): array
	{
		$package = $this->package;
		$users = [];

		if (getenv('APP_ENV') === 'test') {
			$groups = $this->buildGroupsForUsers();
			$users = [
				User::build($package, $package, 'Package', ucfirst($package), $package . '@home.lan', $groups, true),
			];
		}
        return [$package => $users];
	}

    /**
     * @return array<int,Group>
     */
    protected function buildGroupsForUsers(): array
    {
        $data = $this->getGroups();
        $groups = [];

        foreach ($data[$this->package] as $group) {
            $groups[] = Group::build(shortname: $group->getShortname());
        }
        return $groups;
    }

	/**
	 * @return array<int,BaseProps>
	 */
    protected function setSysAndSelectable(array $items): array
    {
        foreach ($items as $item) $item->setSelectable(true)->setSys(true);
		return $items;
    }

	/**
	 * @return array<int,MenupointEntry>
	 */
    protected function setSysAndActive(array $items): array
    {
        foreach ($items as $item) $item->setActive(true)->setSys(true);
		return $items;
    }
}