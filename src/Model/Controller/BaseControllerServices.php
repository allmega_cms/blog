<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Model\Controller;

use Allmega\BlogBundle\Utils\{Helper, FlashBag};
use Allmega\MediaBundle\Manager\MediaFileHandler;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Component\HttpFoundation\{Request, RequestStack};
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\String\Slugger\SluggerInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

class BaseControllerServices
{
    protected Request $request;

    public function __construct(
        protected UserPasswordHasherInterface $passwordHasher,
        protected EventDispatcherInterface $eventDispatcher,
        protected CsrfTokenManagerInterface $csrfManager,
        protected UrlGeneratorInterface $urlGenerator,
        protected MediaFileHandler $mediaFileHandler,
        protected TranslatorInterface $translator,
        protected EntityManagerInterface $em,
        protected SluggerInterface $slugger,
        protected MessageBusInterface $bus,
        protected LoggerInterface $logger,
        protected FlashBag $flashBag,
        protected Helper $helper,
        protected string $sender,
        RequestStack $request
    )
    {
        $this->request = $request->getCurrentRequest();
    }

    public function getService(string $name): object
    {
        if (!property_exists($this, $name)) {
            throw new \Exception($name . ' service name was not found! Check the name do you will get!');
        }
        return $this->$name;
    }

    public function getSender(): string
    {
        return $this->sender;
    }
}