<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Model\Controller;

use Allmega\BlogBundle\Data;
use Allmega\BlogBundle\Utils\FlashBag;
use Allmega\MediaBundle\Manager\MediaFileHandler;
use Allmega\BlogBundle\Utils\Form\{Labels,Params};
use Allmega\BlogBundle\Utils\Params\BaseControllerParams;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\HttpFoundation\{Request, Response};
use SMTPValidateEmail\Validator as SmtpEmailValidator;
use Symfony\Component\EventDispatcher\GenericEvent;
use Symfony\Component\Form\FormInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

abstract class BaseController extends AbstractController
{
    protected string $messageboxTemplate = '@AllmegaBlog/inc/_message_box.html.twig';
    protected string $twigFormTemplate = 'forms/_form.html.twig';
    protected string $twigTemplatesPath = '@AllmegaBlog/twig/';

    protected string $changepw = 'changepw';
    protected string $delete = 'delete';
    protected string $index = 'index';
    protected string $store = 'store';
    protected string $state = 'state';
    protected string $show = 'show';
    protected string $edit = 'edit';
    protected string $add = 'add';

    /** Was the validation of the form successful and can the entity be saved? */
    protected bool $done = true;

    /** Should a success (or error if false) message be displayed after data processing? */
    protected bool $success = true;

    /** Is a current Request a XMLHttpRequest? */
    protected bool $isXmlRequest = false;

    protected ?BaseControllerParams $params = null;
    protected ?FormInterface $form = null;

    public  function __construct(protected BaseControllerServices $services)
    {
        $this->isXmlRequest = $this->getRequest()->isXmlHttpRequest();
    }

    /**
     * @param BaseControllerParams $params Init data for proces
     * @param string|null $method Method to be called
     * @return Response
     */
    protected function handle(BaseControllerParams $params, ?string $method = null): Response
    {
        $method = $method ?? $this->store;
        switch ($method) {
            case $this->changepw:
                $method = $this->changepw;
                break;
            case $this->delete:
                $method = 'remove';
                break;
            case $this->state:
                $method = $this->state;
                break;
            case $this->store:
                $method = $this->store;
                break;
            default:
                throw new \BadMethodCallException($method . ' is not valid method name!!!');
        }

        $this->params = $params;
        return $this->$method();
    }

    protected function store(): Response
    {
        $formOptions = $this->params->getFormOptions();
        $formType = $this->params->getFormType();
        $data = $this->params->getEntity();

        $this->form = $this->createForm($formType, $data, $formOptions);
        $this->form->handleRequest($this->getRequest());

        if ($this->form->isSubmitted() && $this->form->isValid()) {
            $response = $this->doSave();
            if ($response) return $response;
        }

        return $this->getForm();
    }

    protected function doSave(): ?Response
    {
        $this->preExecution();
        $this->params->translateErrorMessages($this->services->getService('translator'));

        if (!$this->done) return null;

        if ($this->params->getEntity()) {
            $em = $this->getEntityManager();
            $em->persist($this->params->getEntity());
            $em->flush();
        }

        $this->dispatchEvent()->postExecution();

        if (!$this->isXmlRequest) return $this->setRedirect()->getRedirect();

        $content = $this->renderMessageBoxContent(
            $this->params->getTransId(),
            $this->params->getDomain(),
            $this->params->getReload(),
            $this->success
        );

        $data = ['reload' => $this->params->getReload(), 'content' => $content];
        return $this->json($data);
    }

    protected function renderMessageBoxContent(
        string $transId,
        string $domain,
        bool $reload = true,
        bool $success = true): string
    {
        return $this->renderView($this->messageboxTemplate, [
            'success' => $success, 'transId' => $transId,
            'domain' => $domain, 'reload' => $reload,
        ]);
    }

    protected function state(): Response
    {
        $this->params->setAction($this->state);
        $this->preExecution();

        if ($this->done) {
            $data = $this->params->getEntity();
            $set = 'set' . ucfirst($this->params->getMethod());
            $is = 'is' . ucfirst($this->params->getMethod());

            if (method_exists($data, $is) && method_exists($data, $set)) {
                $state = !$data->$is();
                $data->$set($state);
                $this->getEntityManager()->flush();
            } else {
                $classname = $data::class;
                throw new \BadMethodCallException("Method of $classname with name '$is' or '$set' was not found!");
            }
        }

        return $this->dispatchEvent()->postExecution()->setRedirect()->getRedirect();
    }

    protected function remove(): Response
    {
        $this->params->setAction($this->delete);
        
        if ($this->isCsrfTokenValid('delete' . $this->params->getEntity()->getId(), $this->getRequest()->request->get('_token'))) {
            $this->preExecution();

            if ($this->done) {
                $clonedEntity = clone $this->params->getEntity();
                $em = $this->getEntityManager();
                $em->remove($this->params->getEntity());
                $em->flush();
                $this->params->setEntity($clonedEntity);
            }

            $this->dispatchEvent()->postExecution();
        }
        return $this->setRedirect()->getRedirect();
    }

    protected function dispatchEvent(): static
    {
        if ($this->params->getEventName()) {
            $this->params->addArguments([
                'isXmlRequest' => $this->isXmlRequest,
                'domain' => $this->params->getDomain(),
            ]);
            $event = new GenericEvent($this->params->getEntity(), $this->params->getArguments());
            $this->services->getService('eventDispatcher')->dispatch($event, $this->params->getEventName());
        }
        return $this;
    }

    protected function preExecution(): static
    {
        return $this;
    }

    protected function postExecution(): static
    {
        return $this;
    }

    protected function setRedirect(): static
    {
        return $this;
    }

    protected function cancel(): void
    {
        $this->success = $this->done = false;
    }

    protected function getForm(): Response
    {
        $params = $this->params->getFormPath() ?
            $this->getDefaultFormParams() :
            $this->getFormParams()->init($this->params->getFormParams());

        $this->params->addOptions(['params' => $params]);
        $formPath = $this->params->getFormPath() ?? $this->twigTemplatesPath;
    
        if (!$this->isXmlRequest) {
            return $this->render($formPath . $this->twigFormTemplate, $this->params->getOptions());
        }

        $templateName = $this->params->getFormPath() ? $this->params->getAction() : 'modify';
        $template = $formPath . 'forms/_' . $templateName . '.html.twig';
        
        $this->params->addJsonData('content', $this->renderView($template, $this->params->getOptions()));
        return $this->json($this->params->getJsonData());
    }

    protected function getFormParams(): Params
    {
        $hideLinks = $this->params->getHideLinks() ?: $this->isXmlRequest;

        return new Params(
            $this->services->getService('csrfManager'),
            $this->services->getService('urlGenerator'),
            $this->services->getService('translator'),
            $hideLinks,
            new Labels($this->params->getProp()),
            $this->form->createView(),
            $this->params->getDomain(),
            $this->params->getAction(),
            $this->params->getRouteShort(),
            $this->params->getTemplatesPath());
    }

    protected function getDefaultFormParams(): array
    {
        return [
            'form' => $this->form->createView(),
            'path' => $this->params->getTemplatesPath(),
            'route' => $this->params->getRouteShort(),
            'domain' => $this->params->getDomain(),
            'action' => $this->params->getAction(),
            'item' => $this->params->getEntity(),
        ];
    }

    protected function getRedirect(): Response
    {
        return $this->redirectToRoute($this->params->getRouteName(), $this->params->getRouteParams(), Response::HTTP_SEE_OTHER);
    }

    protected function saveFiles(string $folder, array $files): void
    {
        $mediaFileHandler = $this->getMediaFileHandler()->setDir($folder);
        foreach ($files as $file) $mediaFileHandler->setUploadedFile($file)->move();
    }

    protected function getFormFiles(): array
    {
        return isset($this->form['files']['file']) ? $this->form['files']['file']->getData() : [];
    }

    protected function getRequest(): Request
    {
        return $this->services->getService('request');
    }

    protected function getEntityManager(): EntityManagerInterface
    {
        return $this->services->getService('em');
    }

    protected function getMediaFileHandler(): MediaFileHandler
    {
        return $this->services->getService('mediaFileHandler');
    }

    protected function getUrlGenerator(): UrlGeneratorInterface
    {
        return $this->services->getService('urlGenerator');
    }

    protected function getFlashBag(): FlashBag
    {
        return $this->services->getService('flashBag');
    }

    protected function getLogger(): LoggerInterface
    {
        return $this->services->getService('logger');
    }

    protected function translate(string $phrase, array $params = [], string $domain = null): string
    {
        $domain = $domain ?? Data::DOMAIN;
        return $this->services->getService('translator')->trans($phrase, $params, $domain);
    }

    protected function validateEmails(array $emails): array
    {
        try {
            return (new SmtpEmailValidator($emails, $this->services->getSender()))->validate();
        } catch (\Exception $e) {
            $this->getLogger()->error($e->getMessage());
        }
        return [];
    }

    public function getTemplateParams($controller, string $domain = 'messages', array $optParams = []): array
    {
        $params = [
            'path' => $controller::ROUTE_TEMPLATE_PATH,
            'route' => $controller::ROUTE_NAME,
            'domain' => $domain
        ];
        return array_merge($params, $optParams);
    }
}