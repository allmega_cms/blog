<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Model;

use Allmega\BlogBundle\Utils\IdGenerator;
use Allmega\MediaBundle\Model\MediaInterface;
use Allmega\BlogBundle\Entity\{MenuPoint, Tag};
use Allmega\MediaBundle\Manager\MediaFileHandler;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\{Collection, ArrayCollection};
use Doctrine\ORM\Mapping as ORM;
use Doctrine\DBAL\Types\Types;
use DateTimeInterface;
use DateTime;

abstract class Article implements CategoriziableInterface, SortableItemInterface, MediaInterface
{
    #[ORM\Id]
    #[ORM\Column(length: 191)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: IdGenerator::class)]
    #[Groups(['article:list', 'article:item'])]
    protected ?string $id = null;

    #[ORM\Column(length: 191)]
    #[Groups(['article:list', 'article:item'])]
    protected string $slug = '';

    #[ORM\Column(type: Types::TEXT)]
    #[Assert\NotBlank(message: 'errors.blank')]
    #[Assert\Length(max: 1000, maxMessage: 'errors.max_value')]
    #[Groups(['article:list', 'article:item'])]
    protected ?string $summary = null;

    #[ORM\Column(length: 191)]
    #[Assert\NotBlank(message: 'errors.blank')]
    #[Assert\Length(max: 191, maxMessage: 'errors.max_value')]
    #[Groups(['article:list', 'article:item'])]
    protected ?string $title = null;

    #[ORM\Column(length: 191, nullable: true)]
    #[Assert\Length(max: 191, maxMessage: 'errors.max_value')]
    #[Groups(['article:list', 'article:item'])]
    protected ?string $keywords = null;

    #[ORM\Column(type: Types::TEXT)]
    #[Assert\Length(max: 200000, maxMessage: 'errors.max_value')]
    #[Groups(['article:list', 'article:item'])]
    protected ?string $content = null;

    #[ORM\Column(type: Types::BOOLEAN)]
    #[Groups(['article:list', 'article:item'])]
    protected bool $markdown = false;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    #[Groups(['article:list', 'article:item'])]
    protected DateTimeInterface $published;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    #[Groups(['article:list', 'article:item'])]
    protected DateTimeInterface $updated;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    protected DateTimeInterface $valid;

    #[ORM\Column(type: Types::BOOLEAN)]
    protected bool $start = false;

    #[ORM\Column(type: Types::BOOLEAN)]
    #[Groups(['article:list', 'article:item'])]
    protected bool $active = false;

    #[ORM\Column(length: 191, nullable: true)]
    #[Assert\Length(max: 191, maxMessage: 'errors.max_value')]
    #[Groups(['article:list', 'article:item'])]
    protected ?string $listimage = null;

    #[ORM\Column(length: 191, nullable: true)]
    #[Assert\Length(max: 191, maxMessage: 'errors.max_value')]
    #[Groups(['article:list', 'article:item'])]
    protected ?string $headimage = null;

    #[ORM\Column]
    #[Groups(['article:list', 'article:item'])]
    protected bool $headshow = true;

    #[ORM\Column]
    #[Groups(['article:list', 'article:item'])]
    protected bool $carousel = false;

    protected Collection $menuPoints;
    protected Collection $tags;

    protected ?string $mediaDir = null;
    protected array $mediaTypes = [];
    protected array $images = [];

    public function __construct()
    {
        $this->valid = new DateTime('+10 year');
        $this->published = new DateTime();
        $this->updated = new DateTime();
        $this->tags = new ArrayCollection();
        $this->menuPoints = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): static
    {
        $this->slug = $slug;
        return $this;
    }

    public function getSummary(): ?string
    {
        return $this->summary;
    }

    public function setSummary(string $summary): static
    {
        $this->summary = $summary;
        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): static
    {
        $this->title = $title;
        return $this;
    }

    public function getKeywords(): ?string
    {
        return $this->keywords;
    }

    public function setKeywords(?string $keywords): static
    {
        $this->keywords = $keywords;
        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): static
    {
        $this->content = $content;
        return $this;
    }

    public function isMarkdown(): bool
    {
        return $this->markdown;
    }

    public function setMarkdown(bool $markdown): static
    {
        $this->markdown = $markdown;
        return $this;
    }

    public function getPublished(): DateTimeInterface
    {
        return $this->published;
    }

    public function setPublished(DateTimeInterface $published): static
    {
        $this->published = $published;
        return $this;
    }

    public function getValid(): DateTimeInterface
    {
        return $this->valid;
    }

    public function setValid(DateTimeInterface $valid): static
    {
        $this->valid = $valid;
        return $this;
    }

    public function isStart(): bool
    {
        return $this->start;
    }

    public function setStart(bool $start): static
    {
        $this->start = $start;
        return $this;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): static
    {
        $this->active = $active;
        return $this;
    }

    public function getUpdated(): DateTimeInterface
    {
        return $this->updated;
    }

    public function setUpdated(): static
    {
        $this->updated = new DateTime();
        return $this;
    }

    public function getListimage(): ?string
    {
        return $this->listimage;
    }

    public function setListimage(?string $listimage): static
    {
        $this->listimage = $listimage;
        return $this;
    }

    public function getHeadimage(): ?string
    {
        return $this->headimage;
    }

    public function setHeadimage(?string $headimage): static
    {
        $this->headimage = $headimage;
        return $this;
    }

    public function isHeadshow(): bool
    {
        return $this->headshow;
    }

    public function setHeadshow(bool $headshow): static
    {
        $this->headshow = $headshow;
        return $this;
    }

    public function isCarousel(): bool
    {
        return $this->carousel;
    }

    public function setCarousel(bool $carousel): static
    {
        $this->carousel = $carousel;
        return $this;
    }

    /**
     * @return Collection<int,MenuPoint>
     */
    public function getMenuPoints(): Collection
    {
        return $this->menuPoints;
    }

    public function addMenuPoint(MenuPoint $menuPoint): static
    {
        if (!$this->menuPoints->contains($menuPoint)) {
            $this->menuPoints[] = $menuPoint;
        }
        return $this;
    }

    public function removeMenuPoint(MenuPoint $menuPoint): static
    {
        if ($this->menuPoints->contains($menuPoint)) {
            $this->menuPoints->removeElement($menuPoint);
        }
        return $this;
    }

    /**
     * @return Collection<int,Tag>
     */
    public function getTags(): Collection
    {
        return $this->tags;
    }

    public function addTag(Tag ...$tags): static
    {
        foreach ($tags as $tag) {
            if (!$this->tags->contains($tag)) $this->tags->add($tag);
        }
        return $this;
    }

    public function removeTag(Tag $tag): static
    {
        if ($this->tags->contains($tag)) {
            $this->tags->removeElement($tag);
        }
        return $this;
    }

    public function getFirstTagName(): string
    {
        return $this->tags->count() ? $this->tags->first()->getName() : 'tag.default';
    }

    public function getMediaDir(): string
    {
        if (!$this->mediaDir) $this->setMediaDir();
        return $this->mediaDir;
    }

    abstract function setMediaDir(): static;

    public function getMediaTypes(): array
    {
        return $this->mediaTypes;
    }

    public function setMediaTypes(array $mediaTypes): static
    {
        $this->mediaTypes = $mediaTypes;
        return $this;
    }

    public function getImages(): array
    {
        return $this->images;
    }

    public function setImages(MediaFileHandler $mediaFileHandler): static
    {
        $files = $mediaFileHandler->setDir($this->getMediaDir())->findFiles();
        foreach ($files as $file) {
            if ($file->getMimetype() == 'image/jpeg') $this->images[] = $file;
        }
        return $this;
    }

    public static function getSortableProps(): array
    {
        return ['title', 'published', 'updated', 'valid', 'active'];
    }

    public static function getBundleName(): string
    {
        return 'Blog';
    }
}