<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\MessageHandler;

use Allmega\BlogBundle\Utils\Params\NotificationParams;
use Allmega\BlogBundle\Entity\{Recipient, Notification, NotificationType};
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;
use Symfony\Component\Messenger\Exception\ExceptionInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Mime\Address;

class MailHandler
{
    public const TEMPLATE = '@AllmegaBlog/inc/_mail.html.twig';

    public function __construct(
        private readonly EntityManagerInterface $em,
        private readonly MessageBusInterface $bus,
        private readonly MailerInterface $mailer,
        private readonly string $senderName,
        private readonly string $sender) {}

    /**
     * @throws ExceptionInterface
     */
    #[AsMessageHandler]
    public function handleNotificationParams(NotificationParams $params): void
    {
        $notificationType = $this->em
            ->getRepository(NotificationType::class)
            ->findOneBy(['shortname' => $params->getType(), 'active' => 1]);

        if ($notificationType) {
            $recipients = $this->em->getRepository(Recipient::class)
                ->findRecipientsByNotificationTypeId($notificationType->getId());

            if ($recipients) {
                foreach ($recipients as $recipient) $params->addReceiver($recipient->getAddress());
            }
            
            if ($params->getReceivers()) {
                $subject  = str_replace($params->getSearch(), $params->getReplace(), $notificationType->getSubject());
                $template = str_replace($params->getSearch(), $params->getReplace(), $notificationType->getTemplate());

                $content = ['content' => nl2br($template)];
                $notification = (new Notification())
                    ->setAttachments($params->getAttachments())
                    ->setReceivers($params->getReceivers())
                    ->setSubject($subject)
                    ->setParams($content);

                $this->bus->dispatch($notification);
            }
        }
    }

    #[AsMessageHandler]
    public function handleNotification(Notification $notification): void
    {
        $template = $notification->getTemplate() ?? self::TEMPLATE;
        $email = (new TemplatedEmail())
            ->from(new Address($this->sender, $this->senderName))
            ->to(...$notification->getReceivers())
            ->cc(...$notification->getCc())
            ->bcc(...$notification->getBcc())
            ->subject($notification->getSubject())
            ->htmlTemplate($template)
            ->context($notification->getParams());

        foreach ($notification->getAttachments() as $attachment) $email->attachFromPath($attachment);
        
        try {
            $this->mailer->send($email);
        } catch (TransportExceptionInterface $e) {
            // some error prevented the email sending; display an
            // error message or try to resend the message
        }
    }
}