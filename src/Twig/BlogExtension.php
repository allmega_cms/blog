<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Twig;

use Allmega\BlogBundle\Data;
use Allmega\BlogBundle\Entity\Comment;
use Allmega\BlogBundle\Model\ItemInfoInterface;
use Allmega\BlogBundle\Controller\CommentController;
use Allmega\BlogBundle\Utils\{Helper, Paginator, Priority, SortableItem};
use Allmega\BlogBundle\Utils\Twig\{ActionsList, AuthorInfo, Carousel, Confirmation, Dashboard, DashboardBox, DashboardBoxItem, DeleteForm, Form, Heading, Link, Search, SideBox, Sort, SliderContent, SliderWrapper};
use Twig\{Error\LoaderError, Error\RuntimeError, Error\SyntaxError, TwigFunction, TwigFilter, Environment};
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManagerInterface;
use Twig\Extension\AbstractExtension;

class BlogExtension extends AbstractExtension
{
    private const TEMPLATE_PATH = '@AllmegaBlog/twig/';

    public function __construct(private readonly EntityManagerInterface $em) {}

    public function getFilters(): array
    {
        return [
            new TwigFilter('allmega_sign', [$this, 'styleSign']),
            new TwigFilter('allmega_priority', [$this, 'priority']),
            new TwigFilter('allmega_format_bytes', [$this, 'formatBytes']),
            new TwigFilter('allmega_priority_trans', [$this, 'priorityTrans']),
            new TwigFilter('allmega_is_new', [$this, 'isNew'], ['is_safe' => ['html'], 'needs_environment' => true]),
            new TwigFilter('allmega_print', [$this, 'printText'], ['is_safe' => ['html'], 'needs_environment' => true]),
            new TwigFilter('allmega_item_info', [$this, 'itemInfo'], ['is_safe' => ['html'], 'needs_environment' => true]),
        ];
    }

    public function getFunctions(): array
    {
        return [
            new TwigFunction('allmega_render_form', [$this, 'form'], ['is_safe' => ['html'], 'needs_environment' => true]),
            new TwigFunction('allmega_render_tags', [$this, 'tags'], ['is_safe' => ['html'], 'needs_environment' => true]),
            new TwigFunction('allmega_render_link', [$this, 'link'], ['is_safe' => ['html'], 'needs_environment' => true]),
            new TwigFunction('allmega_render_modal', [$this, 'modal'], ['is_safe' => ['html'], 'needs_environment' => true]),
            new TwigFunction('allmega_render_badge', [$this, 'badge'], ['is_safe' => ['html'], 'needs_environment' => true]),
            new TwigFunction('allmega_render_search', [$this, 'search'], ['is_safe' => ['html'], 'needs_environment' => true]),
            new TwigFunction('allmega_render_loader', [$this, 'loader'], ['is_safe' => ['html'], 'needs_environment' => true]),
            new TwigFunction('allmega_render_pdf_css', [$this, 'pdfCss'], ['is_safe' => ['html'], 'needs_environment' => true]),
            new TwigFunction('allmega_render_pdf_logo', [$this, 'pdfLogo'], ['is_safe' => ['html'], 'needs_environment' => true]),
            new TwigFunction('allmega_render_heading', [$this, 'heading'], ['is_safe' => ['html'], 'needs_environment' => true]),
            new TwigFunction('allmega_render_side_box', [$this, 'sideBox'], ['is_safe' => ['html'], 'needs_environment' => true]),
            new TwigFunction('allmega_render_carousel', [$this, 'carousel'], ['is_safe' => ['html'], 'needs_environment' => true]),
            new TwigFunction('allmega_render_comments', [$this, 'comments'], ['is_safe' => ['html'], 'needs_environment' => true]),
            new TwigFunction('allmega_render_sort_icon', [$this, 'sortIcon'], ['is_safe' => ['html'], 'needs_environment' => true]),
            new TwigFunction('allmega_render_scroll_btn', [$this, 'scrollBtn'], ['is_safe' => ['html'], 'needs_environment' => true]),
            new TwigFunction('allmega_render_pagination', [$this, 'pagination'], ['is_safe' => ['html'], 'needs_environment' => true]),
            new TwigFunction('allmega_render_author_info', [$this, 'authorInfo'], ['is_safe' => ['html'], 'needs_environment' => true]),
            new TwigFunction('allmega_render_prio_legend', [$this, 'prioLegend'], ['is_safe' => ['html'], 'needs_environment' => true]),
            new TwigFunction('allmega_render_delete_form', [$this, 'deleteForm'], ['is_safe' => ['html'], 'needs_environment' => true]),
            new TwigFunction('allmega_render_unavailable', [$this, 'unavailable'], ['is_safe' => ['html'], 'needs_environment' => true]),
            new TwigFunction('allmega_render_page_content', [$this, 'pageContent'], ['is_safe' => ['html'], 'needs_environment' => true]),
            new TwigFunction('allmega_render_confirmation', [$this, 'comfirmation'], ['is_safe' => ['html'], 'needs_environment' => true]),
            new TwigFunction('allmega_render_flash_messages', [$this, 'flashMessages'], ['is_safe' => ['html'], 'needs_environment' => true]),
            new TwigFunction('allmega_render_slider_wrapper', [$this, 'sliderWrapper'], ['is_safe' => ['html'], 'needs_environment' => true]),
            new TwigFunction('allmega_render_slider_content', [$this, 'sliderContent'], ['is_safe' => ['html'], 'needs_environment' => true]),
            new TwigFunction('allmega_render_start_confirmation', [$this, 'startConfirmation'], ['is_safe' => ['html'], 'needs_environment' => true]),
            new TwigFunction('allmega_render_dashboard_box_item', [$this, 'dashboardBoxItem'], ['is_safe' => ['html'], 'needs_environment' => true]),
            new TwigFunction('allmega_render_dashboard_box', [$this, 'dashboardBox'], ['is_safe' => ['html'], 'needs_environment' => true]),
            new TwigFunction('allmega_render_actions_list', [$this, 'actionsList'], ['is_safe' => ['html'], 'needs_environment' => true]),
            new TwigFunction('allmega_render_form_errors', [$this, 'formErrors'], ['is_safe' => ['html'], 'needs_environment' => true]),
            new TwigFunction('allmega_render_dashboard', [$this, 'dashboard'], ['is_safe' => ['html'], 'needs_environment' => true]),
        ];
    }

    public function styleSign(bool $value): string
    {
        return $value ? 'fa-circle-check text-success' : 'fa-ban text-danger';
    }

    /**
     * @throws SyntaxError
     * @throws RuntimeError
     * @throws LoaderError
     */
    public function isNew(Environment $env, ?\DateTimeInterface $date): string
    {
        return $env->render(self::TEMPLATE_PATH . '_is_new.html.twig', ['date' => $date]);
    }

    /**
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws LoaderError
     */
    public function printText(Environment $env, ?string $text): string
    {
        return $env->render(self::TEMPLATE_PATH . '_print.html.twig', ['text' => $text]);
    }

    public function priority(int $prio): string
    {
        return Priority::getPriorityCssClass($prio);
    }

    public function priorityTrans(int $prio): string
    {
        return Priority::getPriorityTranslate($prio);
    }

    public function formatBytes(int $bytes, int $precision = 2): string
    {
        $size = ['B', 'kB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
        $factor = floor((strlen($bytes) - 1) / 3);
        return sprintf("%.{$precision}f", $bytes / pow(1024, $factor)) . ' ' . @$size[$factor];
    }

    /**
     * @throws SyntaxError
     * @throws RuntimeError
     * @throws LoaderError
     */
    public function itemInfo(Environment $env, ItemInfoInterface $item): string
    {
        return $env->render(self::TEMPLATE_PATH . '_item_info.html.twig', ['item' => $item]);
    }

    /**
     * @throws SyntaxError
     * @throws RuntimeError
     * @throws LoaderError
     */
    public function comments(Environment $env, string $entityid, string $entityname, bool $onlyCount = false): string
    {
        $comments = $this->em->getRepository(Comment::class)->findBy(['entityid' => $entityid, 'active' => true], ['posted' => 'DESC']);
        $num = count($comments);

        if ($onlyCount) {
            $params = ['domain' => Data::DOMAIN, 'num' => $num, 'onlyCount' => true];
            return $env->render('@AllmegaBlog/comment/inc/_counter.html.twig', ['params' => $params]);
        }

        $comments = Helper::sort($comments, true);

        $params = [
            'path' => CommentController::ROUTE_TEMPLATE_PATH,
            'route' => CommentController::ROUTE_NAME,
            'domain' => Data::DOMAIN,
            'entityname' => $entityname,
            'entityid' => $entityid,
            'onlyCount' => false,
            'num' => $num
        ];

        return $env->render('@AllmegaBlog/comment/layout.html.twig', [
            'params' => $params, 'comments' => $comments,
        ]);
    }

    public function sideBox(Environment $env, array $params): string
    {
        return (new SideBox($env, $params))->getView();
    }

    public function dashboard(Environment $env, array $params): string
    {
        return (new Dashboard($env, $params))->getView();
    }

    public function dashboardBox(Environment $env, array $params): string
    {
        return (new DashboardBox($env, $params))->getView();
    }

    public function dashboardBoxItem(Environment $env, array $params): string
    {
        return (new DashboardBoxItem($env, $params))->getView();
    }

    public function link(Environment $env, array $params): string
    {
        return (new Link($env, $params))->getView();
    }

    public function comfirmation(Environment $env, array $params): string
    {
        return (new Confirmation($env, $params))->getView();
    }

    public function deleteForm(Environment $env, array $params): string
    {
        return (new DeleteForm($env, $params))->getView();
    }

    public function form(Environment $env, array $params): string
    {
        return (new Form($env, $params))->getView();
    }

    public function sortIcon(Environment $env, SortableItem $item, SlidingPagination $sortables): string
    {
        return (new Sort($env, ['item' => $item, 'sortables' => $sortables]))->getView();
    }

    public function heading(Environment $env, array $params): string
    {
        return (new Heading($env, $params))->getView();
    }

    public function sliderContent(Environment $env, array $params): string
    {
        return (new SliderContent($env, $params))->getView();
    }

    public function sliderWrapper(Environment $env, array $params): string
    {
        return (new SliderWrapper($env, $params))->getView();
    }

    public function carousel(Environment $env, array $params): string
    {
        return (new Carousel($env, $params))->getView();
    }

    public function actionsList(Environment $env, array $params): string
    {
        return (new ActionsList($env, $params))->getView();
    }

    public function authorInfo(Environment $env, array $params): string
    {
        return (new AuthorInfo($env, $params))->getView();
    }

    public function search(Environment $env, array  $params): string
    {
        return (new Search($env, $params))->getView();
    }

    /**
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws LoaderError
     */
    public function pagination(Environment $env, SlidingPagination $items): string
    {
        $count = Paginator::NUM_ITEMS;
        return $env->render(self::TEMPLATE_PATH . '_pagination.html.twig', [
            'items' => $items, 'count' => $count
        ]);
    }

    /**
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws LoaderError
     */
    public function formErrors(Environment $env, array $messages): string
    {
        return $env->render(self::TEMPLATE_PATH . 'form/_errors.html.twig', ['messages' => $messages]);
    }

    /**
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws LoaderError
     */
    public function pageContent(Environment $env, array $params): string
    {
        return $env->render(self::TEMPLATE_PATH . '_page_content.html.twig', ['params' => $params]);
    }

    /**
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws LoaderError
     */
    public function prioLegend(Environment $env): string
    {
        return $env->render(self::TEMPLATE_PATH . '_prio_legend.html.twig', ['priorities' => Priority::$priorities]);
    }

    /**
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws LoaderError
     */
    public function startConfirmation(Environment $env): string
    {
        return $env->render(self::TEMPLATE_PATH . '_start_confirmation.html.twig');
    }

    /**
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws LoaderError
     */
    public function modal(Environment $env): string
    {
        return $env->render(self::TEMPLATE_PATH . '_modal.html.twig');
    }

    /**
     * @throws SyntaxError
     * @throws RuntimeError
     * @throws LoaderError
     */
    public function badge(Environment $env, array $params): string
    {
        return $env->render(self::TEMPLATE_PATH . '_badge.html.twig', ['params' => $params]);
    }

    /**
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws LoaderError
     */
    public function loader(Environment $env): string
    {
        return $env->render(self::TEMPLATE_PATH . '_loader.html.twig');
    }

    /**
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws LoaderError
     */
    public function tags(Environment $env, Collection $tags): string
    {
        return $env->render(self::TEMPLATE_PATH . '_tags.html.twig', ['tags' => $tags]);
    }

    /**
     * @throws SyntaxError
     * @throws RuntimeError
     * @throws LoaderError
     */
    public function pdfCss(Environment $env): string
    {
        return $env->render(self::TEMPLATE_PATH . '_pdf_css.html.twig');
    }

    /**
     * @throws SyntaxError
     * @throws RuntimeError
     * @throws LoaderError
     */
    public function pdfLogo(Environment $env): string
    {
        return $env->render(self::TEMPLATE_PATH . '_pdf_logo.html.twig');
    }

    /**
     * @throws SyntaxError
     * @throws RuntimeError
     * @throws LoaderError
     */
    public function unavailable(Environment $env): string
    {
        return $env->render(self::TEMPLATE_PATH . '_unavailable.html.twig');
    }

    /**
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws LoaderError
     */
    public function flashMessages(Environment $env): string
    {
        return $env->render(self::TEMPLATE_PATH . '_flash_messages.html.twig');
    }

    /**
     * @throws RuntimeError
     * @throws SyntaxError
     * @throws LoaderError
     */
    public function scrollBtn(Environment $env): string
    {
        return $env->render(self::TEMPLATE_PATH . '_scroll_btn.html.twig');
    }
}