<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Security\Voters;

use Allmega\AuthBundle\Entity\User;
use Allmega\BlogBundle\Entity\MenuPoint;
use Allmega\BlogBundle\Model\{AllmegaVoterInterface, BaseVoterTrait};
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class MenuPointVoter extends Voter implements AllmegaVoterInterface
{
    use BaseVoterTrait;

    protected string $build = 'build';

    protected function supports($attribute, $subject): bool
    {
        $voterParams = $this->createVoterParams($attribute, $subject, 'blog-menupoint', [$this->build]);
        return $this->hasAttributeAndValidSubject($voterParams);
    }

    public function isGranted(string $attribute, mixed $subject = null, ?User $user = null): bool
    {
        if (!$this->isSettedAndSupports($attribute, $subject)) return false;

        switch ($attribute) {
            case $this->delete:
            case $this->state:
            case $this->list:
            case $this->edit:
            case $this->add:
                $result = $this->isBlogAuthor($user);
                break;
            case $this->build:
            case $this->show:
                $result = true;
                break;
            default:
                $result = false;
        }
        return $result;
    }

    public function isSubjectValid(mixed $subject): bool
    {
        return $subject instanceof MenuPoint;
    }
}