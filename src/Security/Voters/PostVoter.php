<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Security\Voters;

use Allmega\BlogBundle\Data;
use Allmega\AuthBundle\Entity\User;
use Allmega\BlogBundle\Entity\Post;
use Allmega\BlogBundle\Model\{AllmegaVoterInterface, BaseVoterTrait};
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class PostVoter extends Voter implements AllmegaVoterInterface
{
    use BaseVoterTrait;

    protected string $latest = 'latest';

    protected function supports($attribute, $subject): bool
    {
        $voterParams = $this->createVoterParams($attribute, $subject, 'blog-post', [$this->latest]);
        return $this->hasAttributeAndValidSubject($voterParams);
    }

    public function isGranted(string $attribute, mixed $subject = null, ?User $user = null): bool
    {
        if (!$this->isSettedAndSupports($attribute, $subject)) return false;

        $isAuthor = $this->hasRole($user, Data::POST_AUTHOR_ROLE);

        switch ($attribute) {
            case $this->dashboard:
            case $this->search:
            case $this->list:
            case $this->add:
                $result = $isAuthor;
                break;
            case $this->delete:
            case $this->files:
            case $this->state:
            case $this->edit:
                $result = $isAuthor && $subject && $subject->getAuthors()->contains($user);
                break;
            case $this->latest:
            case $this->show:
                $result = true;
                break;
            default:
                $result = false;
        }
        return $result;
    }

    public function isSubjectValid(mixed $subject): bool
    {
        return $subject instanceof Post;
    }
}