<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Security\Voters;

use Allmega\BlogBundle\Data;
use Allmega\BlogBundle\Entity\Job;
use Allmega\AuthBundle\Entity\User;
use Allmega\BlogBundle\Model\{AllmegaVoterInterface, BaseVoterTrait};
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class JobVoter extends Voter implements AllmegaVoterInterface
{
    use BaseVoterTrait;

    protected string $load = 'load';

    protected function supports($attribute, $subject): bool
    {
        $voterParams = $this->createVoterParams($attribute, $subject, 'blog-job', [$this->load]);
        return $this->hasAttributeAndValidSubject($voterParams);
    }

    public function isGranted(string $attribute, mixed $subject = null, ?User $user = null): bool
    {
        if (!$this->isSettedAndSupports($attribute, $subject)) return false;

        switch ($attribute) {
            case $this->dashboard:
            case $this->delete:
            case $this->search:
            case $this->load:
            case $this->list:
                $result = $this->hasRole($user, Data::JOB_MODERATOR_ROLE);
                break;
            case $this->show:
            case $this->add:
                $result = true;
                break;
            default:
                $result = false;
        }
        return $result;
    }

    public function isSubjectValid(mixed $subject): bool
    {
        return $subject instanceof Job;
    }
}