<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Security\Voters;

use Allmega\BlogBundle\Data;
use Allmega\AuthBundle\Entity\User;
use Allmega\BlogBundle\Entity\Comment;
use Allmega\BlogBundle\Model\{AllmegaVoterInterface, BaseVoterTrait};
use Symfony\Component\Security\Core\Authorization\Voter\Voter;

class CommentVoter extends Voter implements AllmegaVoterInterface
{
    use BaseVoterTrait;

    protected string $settings = 'settings';
    protected string $udelete = 'udelete';

    protected function supports($attribute, $subject): bool
    {
        $voterParams = $this->createVoterParams($attribute, $subject, 'blog-comment', [$this->settings, $this->udelete]);
        return $this->hasAttributeAndValidSubject($voterParams);
    }

    public function isGranted(string $attribute, mixed $subject = null, ?User $user = null): bool
    {
        if (!$this->isSettedAndSupports($attribute, $subject)) return false;

        switch ($attribute) {
            case $this->add:
                $result = $this->hasRole($user);
                break;
            case $this->udelete:
                $result = $this->isSameUser($user, $subject->getAuthor());
                break;
            case $this->settings:
                $result = $this->hasRole($user, Data::BLOG_MANAGER_ROLE);
                break;
            case $this->dashboard:
            case $this->delete:
            case $this->state:
            case $this->list:
            case $this->edit:
                $result = $this->hasRole($user, Data::COMMENT_MODERATOR_ROLE);
                break;
            default:
                $result = false;
        }
        return $result;
    }

    public function isSubjectValid(mixed $subject): bool
    {
        return $subject instanceof Comment;
    }
}