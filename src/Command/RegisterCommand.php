<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Command;

use Allmega\BlogBundle\Utils\Register\Register;
use Symfony\Component\Console\Input\{InputOption, InputArgument, InputInterface};
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;

#[AsCommand(
    name: 'allmega:packages:register',
    description: 'Activate or deactivate the given packages',
)]
class RegisterCommand extends Command
{
    public function __construct(private readonly Register $register)
    {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->setHelp($this->getCommandHelp())
            ->addArgument('packages', InputArgument::IS_ARRAY | InputArgument::REQUIRED, 'Names of the packages. Use "all" option for install all packages')
            ->addOption('deactivate', 'd', InputOption::VALUE_OPTIONAL, 'Deactivate packages', false);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $deactivate = !(false === $input->getOption('deactivate'));
        $packages = $input->getArgument('packages');
        
        $this->register
            ->setHelper($this->getHelper('question'))
            ->setDeactivate($deactivate)
            ->setPackages($packages)
            ->setOutput($output)
            ->setInput($input)
            ->setIo()
            ->process();

        return self::SUCCESS;
    }

    private function getCommandHelp(): string
    {
        return <<<'HELP'
The <info>%command.name%</info> command do activate or deactivate the given packages

By default the command do activate the package(s):
    <info>php %command.full_name%</info> <comment>[package1 package2... ]|all</comment>

For deactivating the package(s) add <comment>-d</comment> option:
    <info>php %command.full_name%</info> <comment>[package1 package2... ]|all</comment> <comment>-d</comment>

HELP;
    }
}