<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Command;

use Allmega\BlogBundle\Utils\Loader\Loader;
use Symfony\Component\Console\Input\{InputOption, InputArgument, InputInterface};
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;

#[AsCommand(
    name: 'allmega:packages:load',
    description: 'Load, update or delete the data for a given packages',
)]
class LoaderCommand extends Command
{
    public function __construct(private readonly Loader $loader)
    {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this->setHelp($this->getCommandHelp())
            ->addArgument('packages', InputArgument::IS_ARRAY | InputArgument::REQUIRED, 'Names of the packages. Use "all" option for all packages')
            ->addOption('menupoints-defaults', 'm', InputOption::VALUE_OPTIONAL, 'Overwrite menupoints with default parents and prio values', false)
            ->addOption('add-users', 's', InputOption::VALUE_OPTIONAL, 'Load default users', false)
            ->addOption('update', 'u', InputOption::VALUE_OPTIONAL, 'Update packages data', false);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $menupointsDefaults = !(false === $input->getOption('menupoints-defaults'));
        $addUsers = !(false === $input->getOption('add-users'));
        $update = !(false === $input->getOption('update'));
        $packages = $input->getArgument('packages');

        $this->loader
            ->setMenupointsDefaults($menupointsDefaults)
            ->setPackages($packages)
            ->setAddUsers($addUsers)
            ->setUpdate($update)
            ->setOutput($output)
            ->setInput($input)
            ->setIo()
            ->process();

        return self::SUCCESS;
    }

    private function getCommandHelp(): string
    {
        return <<<'HELP'
The <info>%command.name%</info> command load or update the data in database for the given packages

By default the command load the packages data to the database:
    <info>php %command.full_name%</info> <comment>[package1, package2, ... ]|all</comment>
    
For load default users to the database add <comment>-s</comment> option:
    <info>php %command.full_name%</info> <comment>[package1, package2, ... ]|all</comment> <comment>-s</comment>
Use with option only if the default data will be installed,not for update

For update the packages data in the database add <comment>-u</comment> option:
    <info>php %command.full_name%</info> <comment>[package1, package2, ... ]|all</comment> <comment>-u</comment>

For overwrite menupoints with the default parents and prio values add <comment>-m</comment> option:
    <info>php %command.full_name%</info> <comment>[package1, package2, ... ]|all</comment> <comment>-m</comment>

HELP;
    }
}