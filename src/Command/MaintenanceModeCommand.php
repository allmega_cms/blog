<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Command;

use Allmega\BlogBundle\Utils\Maintenance;
use Symfony\Component\Console\Input\{InputOption, InputInterface};
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'allmega:maintenance:mode',
    description: 'Activate or deactivate the maintenance mode for the website',
)]
class MaintenanceModeCommand extends Command
{
    public function __construct(private readonly Maintenance $maintenance)
    {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->setHelp($this->getCommandHelp())
            ->addOption('deactivate', 'd', InputOption::VALUE_OPTIONAL, 'Deactivate maintenance mode', false);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $deactivate = !(false === $input->getOption('deactivate'));
        $io = new SymfonyStyle($input, $output);
        $this->maintenance->process($deactivate, $io);
        return self::SUCCESS;
    }

    private function getCommandHelp(): string
    {
        return <<<'HELP'
The <info>%command.name%</info> command do activate or deactivate the maintenance mode for the website

By default the command do activate the maintenance mode:
    <info>php %command.full_name%</info>

For deactivating the maintenance mode add <comment>-d</comment> option:
    <info>php %command.full_name%</info> <comment>-d</comment>

HELP;
    }
}