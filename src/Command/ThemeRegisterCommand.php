<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Command;

use Allmega\BlogBundle\Utils\Register\ThemeRegister;
use Symfony\Component\Console\Input\{InputOption, InputArgument, InputInterface};
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;

#[AsCommand(
    name: 'allmega:theme:register',
    description: 'Activate or deactivate the given theme',
)]
class ThemeRegisterCommand extends Command
{
    public function __construct(private readonly ThemeRegister $register)
    {
        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->setHelp($this->getCommandHelp())
            ->addArgument('theme', InputArgument::REQUIRED, 'Name of the theme, which must be activated or deactivated')
            ->addOption('deactivate', 'd', InputOption::VALUE_OPTIONAL, 'Deactivate the theme', false);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $deactivate = !(false === $input->getOption('deactivate'));
        $theme = $input->getArgument('theme');

        $this->register
            ->setTheme($theme)
            ->setDeactivate($deactivate)
            ->setOutput($output)
            ->setInput($input)
            ->setIo()
            ->process();

        return self::SUCCESS;
    }

    private function getCommandHelp(): string
    {
        return <<<'HELP'
The <info>%command.name%</info> command do activate or deactivate the given theme

By default the command do activate the theme:
    <info>php %command.full_name%</info> <comment>theme</comment>

For deactivating the theme add <comment>-d</comment> option:
    <info>php %command.full_name%</info> <comment>theme</comment> <comment>-d</comment>

HELP;
    }
}