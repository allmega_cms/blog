<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Utils\Search;

use Allmega\BlogBundle\Utils\Paginator;
use Allmega\BlogBundle\Model\SearchableInterface;

class Repo
{
    protected ?SearchableInterface $search = null;
	protected array $options = [];
	protected int $limit = 0;


	public function getSearch(): ?SearchableInterface
    {
		return $this->search;
	}

	public function setSearch(SearchableInterface $search): static
    {
		$this->search = $search;
		return $this;
	}

	public function getOptions(): array {
		return $this->options;
	}

	public function addOptions(array $options): static
	{
		foreach ($options as $key => $val) {
			$this->options[$key] = $val;
		}
		return $this;
	}

	public function getLimit(): int
    {
		return $this->limit ?: Paginator::NUM_ITEMS;
	}

	public function setLimit(int $limit): static
    {
		$this->limit = $limit;
		return $this;
	}
}