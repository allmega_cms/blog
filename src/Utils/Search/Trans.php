<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Utils\Search;

class Trans
{
    
    protected string $domain = 'messages';
    protected array $options = [];
    protected string $label = '';

	public function getDomain(): string
	{
		return $this->domain;
	}

	public function setDomain(string $domain): static
    {
		$this->domain = $domain;
		return $this;
	}

	public function getOptions(): array
    {
		return $this->options;
	}

	public function addOptions(array $options): static
    {
		foreach ($options as $key => $val) {
			$this->options[$key] = $val;
		}
		return $this;
	}

	public function getLabel(): string
    {
		return $this->label;
	}

	public function setLabel(string $label): static
    {
		$this->label = $label;
		return $this;
	}
}