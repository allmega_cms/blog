<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Utils\Search;

class Result implements \JsonSerializable
{
    private string $template = '';
	private string $title = '';
    private string $error = '';
    private array $items = [];
    private array $terms = [];

	public function getTemplate(): string
    {
		return $this->template;
	}

	public function setTemplate(string $template): static
    {
		$this->template = $template;
		return $this;
	}

	public function getTitle(): string
	{
		return $this->title;
	}

	public function setTitle(string $title): static
	{
		$this->title = $title;
		return $this;
	}

	public function getError(): string
    {
		return $this->error;
	}

	public function setError(string $error): static
    {
		$this->error = $error;
		return $this;
	}

	public function getItems(): array
    {
		return $this->items;
	}

	public function setItems(array $items): static
    {
		$this->items = $items;
		return $this;
	}

	public function getTerms(): array
    {
		return $this->terms;
	}

	public function setTerms(array $terms): static
    {
		$this->terms = $terms;
		return $this;
	}

    public function jsonSerialize(): array
    {
        return [
            'template' => $this->template,
			'title' => $this->title,
            'items' => $this->items,
            'terms' => $this->terms,
            'error' => $this->error,
        ];
    }
}