<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Utils\Search;

use Allmega\BlogBundle\Data;
use Allmega\BlogBundle\Utils\Helper;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\{Request, RequestStack};
use Symfony\Contracts\Translation\TranslatorInterface;

class SearchItem extends AbstractController
{
    protected Request $request;
    protected ?Repo $repo = null;
	protected ?Trans $trans = null;
    protected string $template = 'search.html.twig';
	protected string $templatePath = '';
	protected int $searchQueryMax = 190;
	protected int $searchQueryMin = 3;
	protected int $searchTermMax = 20;
	protected int $searchTermMin = 3;
	protected array $routeParams = [];
	protected string $routeProp = 'id';
    protected string $route = '';
    protected array $props = [];
	protected array $items = [];

	public function __construct(
		protected TranslatorInterface $translator,
        protected Helper $helper,
		RequestStack $request)
	{
		$this->repo = new Repo();
		$this->trans = new Trans();
		$this->request = $request->getCurrentRequest();
	}

	public function getSearchQueryMax(): int
	{
		return $this->searchQueryMax;
	}

	public function setSearchQueryMax(int $searchQueryMax): static
	{
		$this->searchQueryMax = $searchQueryMax;
		return $this;
	}

	public function getSearchQueryMin(): int
	{
		return $this->searchQueryMin;
	}

	public function setSearchQueryMin(int $searchQueryMin): static
	{
		$this->searchQueryMin = $searchQueryMin;
		return $this;
	}

	public function getSearchTermMax(): int
	{
		return $this->searchTermMax;
	}

	public function setSearchTermMax(int $searchTermMax): static
	{
		$this->searchTermMax = $searchTermMax;
		return $this;
	}

	public function getSearchTermMin(): int
	{
		return $this->searchTermMin;
	}

	public function setSearchTermMin(int $searchTermMin): static
	{
		$this->searchTermMin = $searchTermMin;
		return $this;
	}

	public function getRepo(): Repo
    {
		return $this->repo;
	}

	public function setRepo(Repo $repo): static
    {
		$this->repo = $repo;
		return $this;
	}

	public function getTrans(): Trans
	{
		return $this->trans;
	}

	public function setTrans(Trans $trans): static
	{
		$this->trans = $trans;
		return $this;
	}

	public function getProps(): array
    {
		return $this->props;
	}

	public function setProps(array $props): static
    {
		$this->props = $props;
		return $this;
	}

	public function getItems(): array
	{
		return $this->items;
	}

	public function setItems(array $items): static
	{
		$this->items = $items;
		return $this;
	}

	public function getRoute(): string
    {
		return $this->route;
	}

	public function setRoute(string $route): static
    {
		$this->route = $route;
		return $this;
	}
 
	public function getRouteParams(): array
	{
		return $this->routeParams;
	}

	public function setRouteParams(array $routeParams): static
	{
		$this->routeParams = $routeParams;
		return $this;
	}

	public function getRouteProp(): string
    {
		return $this->routeProp;
	}

	public function setRouteProp(string $routeProp): static
    {
		$this->routeProp = $routeProp;
		return $this;
	}

	public function getTemplate(): string
    {
		return $this->template;
	}

	public function setTemplate(string $template): static
    {
		$this->template = $template;
		return $this;
	}

	public function getTemplatePath(): string
	{
		return $this->templatePath;
	}

	public function setTemplatePath(string $templatePath): static
	{
		$this->templatePath = $templatePath;
		return $this;
	}

    private function getSearchTerms(): array
    {
        sleep(1);

        $message = 'action.search.error.token';
        $token = $this->request->query->get('_csrf_token');

        if ($this->isCsrfTokenValid('search', $token)) {

            $query = $this->request->query->get('query');
			$queryLength = mb_strlen($query);
			$this->checkSearchLength();

            if ($this->searchQueryMin <= $queryLength && $queryLength <= $this->searchQueryMax) {
                $query = Helper::sanitizeSearchQuery($query);
                return ['terms' => Helper::extractSearchTerms($query, $this->searchTermMin, $this->searchTermMax), 'error' => ''];
            } else {
                $message = 'action.search.error.query';
            }
        }
        
        $message = $this->translator->trans($message, [], Data::DOMAIN);
        return ['error' => $message];
    }

	private function checkSearchLength(): void
	{
		if ($this->searchTermMin < $this->searchQueryMin) {
			$this->searchQueryMin = $this->searchTermMin;
		}
	}

    public function getSearchResult(): Result
    {
        $searchResult = new Result();
        if (!$this->request->isXmlHttpRequest()) return $searchResult->setTitle('How are you?');

        $data = $this->getSearchTerms();
        extract($data);
        //[$error, $terms]

        if ($error) $searchResult->setError($error);
        else {
            $trans = $this->getTrans();
            $title = $this->translator->trans($trans->getLabel(), $trans->getOptions(), $trans->getDomain());

            $repo = $this->getRepo();
            $this->items = $repo->getSearch()->findBySearchQuery($terms, $repo->getOptions(), $repo->getLimit());

            $items = $this->helper->getJsonItems($this);
            $template = $this->renderView($this->getTemplatePath() . $this->getTemplate());
            $searchResult->setTerms($terms)->setItems($items)->setTemplate($template)->setTitle($title);
        }
        return $searchResult;
    }
}