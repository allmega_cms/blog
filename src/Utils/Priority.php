<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Utils;

final class Priority
{
    public const LOW  = 0;
    public const NORM = 1;
    public const HIGH = 2;
    public const VERY_HIGH = 3;
    public static string $prefix = 'label.priority.';
    public static array $priorities = ['low', 'norm', 'high', 'very-high'];

    public static function getPriorityChoices(): array
    {
        $result = [];
        foreach (self::$priorities as $index => $priority) {
            $key = self::getPriorityTranslate($index);
            $result[$key] = $index;
        }
        return $result;
    }

    public static function getPriorityCssClass(int $prio): string
    {
        return self::$priorities[$prio];
    }

    public static function getPriorityTranslate(int $prio): string
    {
        return self::$prefix . self::$priorities[$prio];
    }
}