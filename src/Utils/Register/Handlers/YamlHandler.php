<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Utils\Register\Handlers;

use Allmega\BlogBundle\Utils\Register\Model\AbstractHandler;
use Symfony\Component\Yaml\Yaml;

class YamlHandler extends AbstractHandler
{
    public function handle(): void
    {
        $filepath = $this->getFilepath();

        $originFile = $filepath . '.origin';
        $originFileExists = $this->filesystem->exists($originFile);

        if ($this->deactivate && $originFileExists) {
            $this->filesystem->remove($filepath);
            $this->filesystem->rename($originFile, $filepath);
        } else {
            if ($this->backup && !$originFileExists) {
                $this->filesystem->rename($filepath, $originFile);
            }
            $content = Yaml::dump(input: $this->content, inline: 5, flags: Yaml::DUMP_OBJECT_AS_MAP);
            $this->createFile()->filesystem->dumpFile($filepath, $content);
        }
        $this->reset();
    }

    public function read(): ?\stdClass
    {
        $filepath = $this->getFilepath();
        return $this->filesystem->exists($filepath) ? Yaml::parseFile($filepath, Yaml::PARSE_OBJECT_FOR_MAP) : null;
    }

    public function getFilepath(): string
    {
        $path = $this->path ?: $this->register->getPackagesDir();
        return $path . $this->filename;
    }
}