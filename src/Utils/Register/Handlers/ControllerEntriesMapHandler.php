<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Utils\Register\Handlers;

use Allmega\BlogBundle\Utils\Register\Entries\{ControllerEntry, ControllerEntriesMap};
use Allmega\BlogBundle\Utils\Register\Model\AbstractHandler;
use Symfony\Component\Console\Question\Question;

class ControllerEntriesMapHandler extends AbstractHandler
{
    protected string $question = "Please enter visibility and order information for packages in form 'package:controller_shortname:order:visibility";
    
    public function handle(): void
    {
        $handler = $this->register->getHandlerByShortname('env');
        $this->register->getIo()->section($this->question);

        foreach ($this->entries as $map) {
            $existsEntries = $this->parseExistsControllerEntriesMap($map->getKeyname());
            $i = 1;

            foreach ($map->getControllerEntries() as $entry) {
                $existsEntries ? $this->setControlerEntrySavedValues($existsEntries, $entry) : $entry->setPrio($i++);
                $this->askEntry($entry, $map->getQuestion());
                $handler->setEnvEntryValueByKeyname($map->getKeyname(), $map->build());
            }

            $this->register->getIo()->writeln('');
            $this->writeToFile($map);
        }
    }

    protected function writeToFile(ControllerEntriesMap $map): void
    {
        $content = [];
        foreach ($map->getControllerEntries() as $entry) {
            if ($entry->getEnabled()) $content[$entry->getPrio() - 1] = $entry->getLine();
        }

        if ($content) {
            ksort($content);
            $this
                ->setRegisterPackage($map->getPackage())
                ->setFilename($map->getFilepath())
                ->createFile()->filesystem
                ->dumpFile($this->getFilepath(), implode(PHP_EOL, $content));

            $this->register->getIo()->note("Controller entries from package '{$map->getPackage()}' written to file '{$this->getFilepath()}'");
        }
    }

    protected function parseExistsControllerEntriesMap(string $keyname): array 
    {
        $entries = [];
        $envEntry = $this->register->getHandlerByShortname('env')->getEnvEntryByKeyname($keyname);
        if ($envEntry) $entries = ControllerEntriesMap::parse($keyname, $envEntry->getValue());
        return $entries;
    }

    protected function setControlerEntrySavedValues(array $existsEntries, ControllerEntry &$entry): static
    {
        foreach ($existsEntries as $row) {
            if ($entry->equal($row)) {
                $entry->setPrio($row->getPrio())->setEnabled($row->getEnabled());
                break;
            }
        }
        return $this;
    }

    protected function askEntry(ControllerEntry &$entry, string $question): void
    {
        $search = '{default}';
        if (str_contains($question, $search)) {
            $unit = $entry->build();
            $tmpQuestion = str_replace($search, $unit, $question);

            $question = new Question($tmpQuestion, $unit);
            $answer = $this->register->getHelper()->ask($this->register->getInput(), $this->register->getOutput(), $question);

            $tmpEntry = ControllerEntry::parse($entry->getKeyname(), $answer);
            $entry->setPrio($tmpEntry->getPrio())->setEnabled($tmpEntry->getEnabled());
        }

    }
    
    public function getFilepath(): string
    {
        $path = $this->path ?: $this->register->getPackageTemplatesDir();
        return $path . $this->filename;
    }
}