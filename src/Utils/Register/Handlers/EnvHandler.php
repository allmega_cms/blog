<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Utils\Register\Handlers;

use Allmega\BlogBundle\Utils\Register\Register;
use Allmega\BlogBundle\Utils\Register\Entries\EnvEntry;
use Allmega\BlogBundle\Utils\Register\Model\AbstractHandler;
use Symfony\Component\Console\Question\Question;

class EnvHandler extends AbstractHandler
{
    public function handle(): void
    {
        $envs = [];
        foreach ($this->entries as $entry) $envs[$entry->getEnv()][] = $entry;

        foreach ($envs as $env => $entries) {
            $i = 0;
            $currentEnv = $content = '';
            foreach ($entries as $entry) {
                /** Executed only on last loop */
                if (++$i == count($entries)) {
                    $content .= $entry->getLine();
                    $this->writeToFile($env, $content);
                    break;
                }

                /** Executed only if $env was changed */
                if ($content && $currentEnv != $env) {
                    $this->writeToFile($env, $content);
                    $content = '';
                }

                $content .= $entry->getLine() . PHP_EOL;
                $currentEnv = $env;
            }
        }
        $this->reset();
    }

    protected function writeToFile(string $env, string $content): void
    {
        $this->setFile($env)->createFile()->filesystem->dumpFile($this->getFilepath(), $content);
        $this->register->getIo()->note("Packages env entries written to file '{$this->getFilepath()}'");
    }

    public function getEnvEntryByKeyname(string $keyname): ?EnvEntry
    {
        foreach ($this->entries as $entry) {
            if ($keyname == $entry->getKeyname()) return $entry;
        }
        return null;
    }

    /**
     * If entry exists, set value or create new and set value
     */
    public function setEnvEntryValueByKeyname(string $keyname, string $value): void
    {
        foreach ($this->entries as $entry) {
            if ($keyname == $entry->getKeyname()) {
                $entry->setValue($value);
                return;
            }
        }
        $this->entries[] = new EnvEntry($keyname,'',$value);
    }

    public function modifyEntries(): static
    {
        $entries = [];
        foreach ($this->register->getPackages() as $package) {

            if ($this->deactivate && $this->register->checkRequiredPackage($package)) continue;

            $className = 'Allmega\\' . ucfirst($package) . 'Bundle\Data';
            if (class_exists($className)) {
                $envEntries = (new $className())->getData('envEntries');
                if ($envEntries) $entries = array_merge($entries, $envEntries);
            }
        }

        if ($entries) {
            if ($this->deactivate) $this->removeEnvEntries($entries);
            else {
                $this->register->getIo()->section('Please enter the application informations: ');
                $this->addEntries($entries)->askQuestions();
                $this->register->getIo()->writeln('');
            }
        }
        return $this;
    }

    protected function removeEnvEntries(array $entries): void
    {
        foreach ($entries as $row) {
            foreach ($this->entries as $index => $entry) {
                if ($row->getKeyname() == $entry->getKeyname()) {
                    unset($this->entries[$index]);
                    break;
                }
            }
        }
    }

    protected function addEntries(array $entries): static
    {
        foreach ($entries as $row) {
            $found = false;
            foreach ($this->entries as $entry) {
                if ($row->getKeyname() == $entry->getKeyname()) {
                    $entry->setQuestion($row->getQuestion());
                    $found = true;
                    break;
                }
            }
            if (!$found) $this->entries[] = $row;
        }
        return $this;
    }

    protected function askQuestions(): void
    {
        $helper = $this->register->getHelper();
        $output = $this->register->getOutput();
        $input = $this->register->getInput();
        $search = '{default}';

        foreach ($this->entries as $entry) {
            if ($entry->getQuestion() && str_contains($entry->getQuestion(), $search)) {
                $phrase = str_replace($search, $entry->getValue(), $entry->getQuestion());

                $question = new Question($phrase, $entry->getValue());
                $answer = trim($helper->ask($input, $output, $question));

                $entry->setValue($answer);
                if ($entry->getKeyname() == 'MENU_TYPE') Register::$menuType = $answer;
            }
        }
    }

    public function setExistsEntries(): static
    {
        foreach (EnvEntry::$envs as $env) {
            $this->setFile($env);
            if ($this->filesystem->exists($this->getFilepath())) {
                foreach (file($this->getFilepath(), FILE_SKIP_EMPTY_LINES) as $line) {
                    if (str_contains($line, '=')) {
                        list($keyname, $value) = explode('=', $line);
                        if ($this->getEnvEntryByKeyname($keyname) === null) {
                            $this->entries[] = new EnvEntry(trim($keyname), '', trim($value), $env);
                        }
                    }
                }
            }
        }
        return $this;
    }

    protected function setFile(string $env): static
    {
        $this->filename = $env == 'all' ? '.env.local' : ".env.$env.local";
        return $this;
    }

    public function getFilepath(): string
    {
        $path = $this->path ?: $this->register->getProjectDir();
        return $path . $this->filename;
    }
}