<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Utils\Register\Handlers;

use Allmega\BlogBundle\Utils\Register\Entries\WebpackEntry;
use Allmega\BlogBundle\Utils\Register\Model\AbstractHandler;

class WebpackHandler extends AbstractHandler
{
    private const BUNDLES_BEGIN_MARK = 'ALLMEGA_BEGIN_BUNDLES_ENTRIES';
    private const BUNDLES_END_MARK = 'ALLMEGA_END_BUNDLES_ENTRIES';
    private const THEME_BEGIN_MARK = 'ALLMEGA_BEGIN_THEME_ENTRIES';
    private const THEME_END_MARK = 'ALLMEGA_END_THEME_ENTRIES';
    private const WEBPACKFILE = 'webpack.config.js';

    protected bool  $fileExists = false;
    protected array $contentEntries = [];
    protected array $existsEntries = [];
    protected array $contentBegin = [];
    protected array $contentEnd = [];

    public function handle(): void
    {
        if ($this->fileExists) {
            foreach ($this->entries as $entry) {
                if ($this->checkFileExists($entry)) {
                    $this->contentEntries[] = $entry->getLine();
                }
            }
            $this->writeTofile();
        }
    }

    protected function checkFileExists(WebpackEntry $entry): bool
    {
        $filePath = substr($entry->getPath(), 2) . $entry->getFilename();
        $absoluteFilePath = $this->register->getProjectDir() . $filePath;
        return $this->register->getFilesystem()->exists($absoluteFilePath);
    }

    protected function writeTofile(): void
    {
        $content = array_merge($this->contentBegin, $this->contentEntries, $this->contentEnd);
        $this->filesystem->dumpFile($this->getFilepath(), implode('', $content));
        $this->register->getIo()->note(self::WEBPACKFILE . ' file was modified');
    }

    public function setExistsEntries(): static
    {
        $this->filename = self::WEBPACKFILE;
        $this->fileExists = $this->filesystem->exists($this->getFilepath());

        if ($this->fileExists) {
            $beginMark = $this->theme ? self::THEME_BEGIN_MARK : self::BUNDLES_BEGIN_MARK;
            $endMark = $this->theme ? self::THEME_END_MARK : self::BUNDLES_END_MARK;

            $content = file($this->getFilepath(), FILE_SKIP_EMPTY_LINES);
            $beginFound = $endFound = false;

            foreach ($content as $line) {
                if ($this->checkMark($line, $beginMark)) {
                    $this->contentBegin[] = $line;
                    $beginFound = true;
                } elseif ($this->checkMark($line, $endMark)) $endFound = true;

                if (!$beginFound) $this->contentBegin[] = $line;
                elseif ($endFound) $this->contentEnd[] = $line;
                else $this->existsEntries[] = $line;
            }
        }
        return $this;
    }

    private function checkMark(string $line, string $mark): bool
    {
        return str_contains($line, $mark);
    }
    
    public function getFilepath(): string
    {
        $path = $this->path ?: $this->register->getProjectDir();
        return $path . $this->filename;
    }
}