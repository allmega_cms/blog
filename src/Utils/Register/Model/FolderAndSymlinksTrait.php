<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Utils\Register\Model;

trait FolderAndSymlinksTrait
{
    public function handleFolders(array $folders, bool $delete = false): static
    {
        $this->io->section('Handle folders...');
        foreach ($folders as $dir) {
            $exists = $this->filesystem->exists($dir);
            if ($this->deactivate) {
                if (!$exists) $phrase = 'already deleted';
                else {
                    if ($delete) $this->filesystem->remove($dir);
                    $phrase = 'successfuly deleted';
                }
            } else {
                if ($exists) $phrase = 'already exists';
                else {
                    $this->filesystem->mkdir($dir, 0755);
                    $phrase = 'successfully created';
                }
            }
            $this->io->writeln("Folder '$dir' $phrase");
        }
        $this->io->writeln('');
        return $this;
    }

    public function handleSymlinks(array $symlinks): static
    {
        $this->io->section('Handle symlinks...');
        foreach ($symlinks as $source => $target) {
            $exists = $this->filesystem->exists($target);
            if ($this->deactivate) {
                if (!$exists) $phrase = 'already deleted';
                else {
                    $this->filesystem->remove($target); 
                    $phrase = 'successfully deleted';
                }
            } else {
                if ($exists) $phrase = 'already exists';
                else {
                    if ($this->filesystem->exists($source)) {
                        $this->filesystem->symlink($source, $target);
                        $phrase = 'successfully created';
                    } else {
                        $phrase = 'does not exists';
                    }
                }
            }
            $this->io->writeln("Symlink from '$source' to '$target' $phrase");
        }
        $this->io->writeln('');
        return $this;
    }

    public function handleImages(array $images): static
    {
        $this->io->section('Handle default images...');
        foreach ($images as $source => $target) {
            $exists = $this->filesystem->exists($target);
            if ($this->deactivate) {
                if (!$exists) $phrase = 'already deleted';
                else {
                    $this->filesystem->remove($target); 
                    $phrase = 'successfully deleted';
                }
            } else {
                if ($exists) $phrase = 'already exists';
                else {
                    $this->filesystem->copy($source, $target);
                    $phrase = 'successfully created';
                }
            }
            $this->io->writeln("File copied from '$source' to '$target' $phrase");
        }
        $this->io->writeln('');
        return $this;
    }
}