<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Utils\Register\Model;

use Allmega\BlogBundle\Utils\Register\Handlers\{ControllerEntriesMapHandler, EnvHandler, WebpackHandler, YamlHandler};

trait HandlersTrait
{
    protected array $handlers = [];

    /**
     * @return array<int,AbstractHandler>
     */
    public function getHandlers(): array
    {
        return $this->handlers;
    }

    public function setHandlers(): static
    {
        foreach ($this->getDefaultHandlers() as $shortname => $className) {
            $this->handlers[] = new $className($this, $shortname);
        }
        return $this;
    }

    public function addHandler(string $shortname, string $className): static
    {
        if (!$this->handlers) $this->setHandlers();

        if (!$this->getHandlerByShortname($shortname)) {
            $this->handlers[] = new $className($this, $shortname);
        }
        return $this;
    }

    public function getHandlerByShortname(string $shortname): ?AbstractHandler
    {
        if (!$this->handlers) $this->setHandlers();

        foreach ($this->handlers as $handler) {
            if ($handler->getShortname() === $shortname) return $handler;
        }
        return null;
    }

    public function getDefaultHandlers(): array
    {
        return [
            'controllermap' => ControllerEntriesMapHandler::class, 
            'webpack' => WebpackHandler::class,
            'yaml' => YamlHandler::class,
            'env' => EnvHandler::class,
        ];
    }
}