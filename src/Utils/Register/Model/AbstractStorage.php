<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Utils\Register\Model;

use Allmega\BlogBundle\Utils\Register\Register;

abstract class AbstractStorage
{
    use AbstractStorageTrait;

    public function __construct(protected Register $register, string $package)
    {
        $this->phrase = $register->getDeactivate() ? 'deactivated' : 'registered';
        $this->io = $register->getIo();
        $this->package = $package;
    }

    public abstract function handlePackageData(): void;
    public abstract function addPackageData(): static;
}