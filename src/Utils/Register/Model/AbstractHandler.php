<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Utils\Register\Model;

use Allmega\BlogBundle\Utils\Register\Entries\EnvEntry;
use Allmega\BlogBundle\Utils\Register\Register;
use Symfony\Component\Filesystem\Filesystem;

abstract class AbstractHandler
{
    /** Path to dir with configuration file */
    protected string $path = '';

    /** Name of the configuration file to be modified|created */
    protected string $filename;

    /** Lines to be written to configuration file */
    protected array $entries = [];

    /** Content of configuration file to be written */
    protected mixed $content;

    /** Shortname of handler */
    protected string $shortname;

    /** Entries from theme? */
    protected bool $theme = false;

    protected Filesystem $filesystem;
    protected bool $deactivate = false;
    protected bool $backup = false;

    public function __construct(protected Register $register, string $shortname)
    {
        $this->filesystem = $register->getFilesystem();
        $this->deactivate = $register->getDeactivate();
        $this->shortname = $shortname;
    }

    abstract public function getFilepath(): string;
    abstract public function handle(): void;

    protected function reset(): void
    {
        $this->path = $this->filename = $this->content = '';
        $this->deactivate = $this->backup = false;
        $this->entries = [];
    }

    protected function createFile(): static
    {
        if (!$this->filesystem->exists($this->getFilepath())) {
            $this->filesystem->touch($this->getFilepath());
        }
        return $this;
    }

    public function setRegisterPackage(string $package): static
    {
        $this->register->setPackage($package);
        return $this;
    }

    public function setPath(string $path): static
    {
        $this->path = $path;
        return $this;
    }

    public function setFilename(string $filename): static
    {
        $this->filename = $filename;
        return $this;
    }

    public function getEntries(): array
    {
        return $this->entries;
    }

    public function setEntries(array $entries): static
    {
        $this->entries = $entries;
        return $this;
    }

    public function setEnvEntryValueByKeyname(string $keyname, string $value): void {}
    
    public function getEnvEntryByKeyname(string $keyname): ?EnvEntry
    {
        return null;
    }

    public function setExistsEntries(): static
    {
        return $this;
    }

    public function modifyEntries(): static
    {
        return $this;
    }
    
    public function read(): ?\stdClass
    {
        return null;
    }

    public function setContent(mixed $content): static
    {
        $this->content = $content;
        return $this;
    }

    public function setDeactivate(bool $deactivate): static
    {
        $this->deactivate = $deactivate;
        return $this;
    }

    public function setBackup(bool $backup = true): static
    {
        $this->backup = $backup;
        return $this;
    }

    public function getShortname(): string
    {
        return $this->shortname;
    }
 
    public function setShortname(string $shortname): static
    {
        $this->shortname = $shortname;
        return $this;
    }

    public function setTheme(bool $theme): static
    {
        $this->theme = $theme;
        return $this;
    }
}