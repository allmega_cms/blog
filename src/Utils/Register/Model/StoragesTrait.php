<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Utils\Register\Model;

use Symfony\Component\Console\Helper\HelperInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Finder\Finder;

trait StoragesTrait
{
    public static array $requiredPackages = ['auth', 'blog', 'media'];
    
    protected string $type = 'Register';
    protected array $packages = [];
    protected array $storages = [];

    protected HelperInterface $helper;
    protected OutputInterface $output;
    protected InputInterface $input;
    protected SymfonyStyle $io;

    public function getHelper(): HelperInterface
    {
        return $this->helper;
    }

    public function setHelper(HelperInterface $helper): static
    {
        $this->helper = $helper;
        return $this;
    }

    public function getOutput(): OutputInterface
    {
        return $this->output;
    }

    public function setOutput(OutputInterface $output): static
    {
        $this->output = $output;
        return $this;
    }

    public function getInput(): InputInterface
    {
        return $this->input;
    }

    public function setInput(InputInterface $input): static
    {
        $this->input = $input;
        return $this;
    }

    public function getIo(): SymfonyStyle
    {
        return $this->io;
    }

    public function setIo(): static
    {
        $this->io = new SymfonyStyle($this->input, $this->output);
        return $this;
    }

    public function getPackages(): array
    {
        return $this->packages;
    }

    public function setPackages(array $packages): static
    {
        $this->packages = $packages[0] == 'all' ? $this->readPackages() : $packages;
        return $this;
    }

    protected function readPackages(): array
    {
        $packages = [];
        $packagesDir = $this->getAllmegaDir();

        if ($this->filesystem->exists($packagesDir)) {

            $finder = new Finder();
            $finder->in($packagesDir)->depth('== 0')->directories();

            if ($finder->hasResults()) {
                foreach ($finder as $package) {

                    $tmp = explode('/', $package);
                    $package = ucfirst(array_pop($tmp));
                    $className = "Allmega\\{$package}Bundle\Allmega{$package}Bundle";

                    if (class_exists($className)) $packages[] = strtolower($package);
                }
            }
        }
        sort($packages);
        return $packages;
    }

    /**
     * @return array<int,AbstractStorage>
     */
    public function getStorages(): array
    {
        return $this->storages; 
    }

    public function setStorages(): static
    {
        foreach ($this->packages as $package) {
            $storageClassName = 'Allmega\\' . ucfirst($package) . 'Bundle\Utils\\' . $this->type . '\Storage';
            if (class_exists($storageClassName)) {
                $this->storages[] = new $storageClassName($this, $package);
            }
        }
        return $this;
    }

    public function getStorageByPackage(string $package): ?AbstractStorage
    {
        if (!$this->storages) $this->setStorages();

        foreach ($this->storages as $storage) {
            if ($storage->getPackage() == $package) return $storage;
        }
        return null;
    }

    public function handleStorageData(string $note = '', bool $register = true): void
    {
        $this->setStorages();
        foreach ($this->storages as $storage) {

            if ($register) $this->io->section("$note package '{$storage->getPackage()}'");

            foreach ($this->packages as $package) {
                $dataClassName = 'Allmega\\' . ucfirst($package) . 'Bundle\Data';
                if (class_exists($dataClassName)) {
                    $this->setPackage($package);

                    if ($register) $this->io->note("Load entries from package '$package'");

                    $data = new $dataClassName($register);
                    $storage->setPackageData($data)->addPackageData();
                }
            }
        }

        foreach ($this->storages as $storage) $storage->handlePackageData();
        $this->io->note('DONE!!!');
    }

    public function checkRequiredPackage(string $package): bool
    {
        if (in_array($package, self::$requiredPackages)) {
            $this->io->note("Package '$package' can not be deactivated! Skip...");
            return true;
        }
        return false;
    }
}