<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Utils\Register\Model;

use Allmega\BlogBundle\Model\PackageData;
use Symfony\Component\Console\Style\SymfonyStyle;

trait AbstractStorageTrait
{
    protected SymfonyStyle $io;
    protected PackageData $data;
    protected string $package;
    protected string $phrase;

    public function getPackageData(): PackageData
    {
        return $this->data;
    }

    public function setPackageData(PackageData $data): static
    {
        $this->data = $data;
        return $this;
    }

    public function getPackage(): string
    {
        return $this->package;
    }

    public function setPackage(string $package): static
    {
        $this->package = $package;
        return $this;
    }

    public function getPhrase(): string
    {
        return $this->phrase;
    }

    public function setPhrase(string $phrase): static
    {
        $this->phrase = $phrase;
        return $this;
    }

    public function note(string $message): void
    {
        $this->io->writeln($message);
    }
}