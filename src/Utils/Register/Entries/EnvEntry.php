<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Utils\Register\Entries;

class EnvEntry implements EntryInterface
{
    public static array $envs = ['all', 'test'];
    protected string $line = '';
    
    public function __construct(
        protected string $keyname,
        protected string $question,
        protected string $value,
        protected string $env = 'all') {}

    public function getKeyname(): string
    {
        return $this->keyname;
    }

    public function setKeyname(string $keyname): static
    {
        $this->keyname = $keyname;
        return $this;
    }
 
    public function getQuestion(): string
    {
        return $this->question;
    }

    public function setQuestion($question): static
    {
        $this->question = $question;
        return $this;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    public function setValue(string $value): static
    {
        $this->value = $value;
        return $this;
    }
 
    public function getEnv(): string
    {
        return $this->env;
    }

    public function setEnv(string $env): static
    {
        $this->env = $env;
        return $this;
    }

    public function getLine(): string
    {
        if (!$this->line) $this->setLine();
        return $this->line;
    }

    public function setLine(): static
    {
        $this->line = $this->keyname . '=' . $this->value;
        return $this;
    }
}