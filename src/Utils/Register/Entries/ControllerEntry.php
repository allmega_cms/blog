<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Utils\Register\Entries;

use Symfony\Component\VarExporter\Exception\ClassNotFoundException;
use Symfony\Component\Yaml\Yaml;

class ControllerEntry implements EntryInterface
{
    protected string $line = '';
    
    public function __construct(
        protected string $keyname,
        protected string $package,
        protected string $shortname,
        protected string $controller,
        protected string $method,
        protected array  $params = [],
        protected int $prio = 1,
        protected int $enabled = 1) {}

 
    public function getKeyname(): string
    {
        return $this->keyname;
    }

    public function setKeyname(string $keyname): static
    {
        $this->keyname = $keyname;
        return $this;
    }

    public function getPackage(): string
    {
        return $this->package;
    }

    public function setPackage(string $package): static
    {
        $this->package = $package;
        return $this;
    }
 
    public function getShortname(): string
    {
        return $this->shortname;
    }

    public function setShortname(string $shortname): static
    {
        $this->shortname = $shortname;
        return $this;
    }

    public function getController(): string
    {
        return $this->controller;
    }

    public function setController(string $controller): static
    {
        $this->controller = $controller;
        return $this;
    }

    public function getMethod(): string
    {
            return $this->method;
    }

    public function setMethod(string $method): static
    {
        $this->method = $method;
        return $this;
    }
 
    public function getParams(): array
    {
        return $this->params;
    }

    public function setParams(array $params): static
    {
        $this->params = $params;
        return $this;
    }

    public function getPrio(): int
    {
        return $this->prio;
    }

    public function setPrio(int $prio): static
    {
        $this->prio = $prio;
        return $this;
    }
 
    public function getEnabled(): int
    {
        return $this->enabled;
    }

    public function setEnabled(int $enabled): static
    {
        $this->enabled = $enabled;
        return $this;
    }

    public function getLine(): string
    {
        if (!$this->line) $this->setLine();
        return $this->line;
    }

    public function setLine(): static
    {
        $this->checkEntry();
        
        $controller = str_replace('\\', '\\\\', $this->controller);
        $params = Yaml::dump($this->params);
        $method = $this->method;

        $this->line = "{{ render(controller('$controller::$method',$params)) }}";
        return $this;
    }

    public function checkEntry(): void
    {
        if (!class_exists($this->controller)) {
            throw new ClassNotFoundException($this->controller);
        }
        
        if (!method_exists($this->controller, $this->method)) {
            $message = "Method names '{$this->method}' of class '{$this->controller}' was not found!!!";
            throw new \BadMethodCallException($message);
        }
    }

    public function build(): string
    {
        $this->checkEntry();
        $props = [$this->package, $this->shortname, $this->prio, $this->enabled];
        return implode(':', $props);
    }

    public static function parse(string $keyname, string $entry): static
    {
        if (!strpos($entry, ':')) {
            throw new \ParseError("Parse error by required sign ':'! Check the entry '$entry'...");
        }

        $props = explode(':', $entry);
        if (count($props) != 4) {
            throw new \ParseError("Parse error! Required props count is not equal 4! Check the entry '$entry'...");
        }

        list($package, $shortname, $prio, $enabled) = $props;
        return new static($keyname, $package, $shortname, '', '', [], $prio, $enabled);
    }

    public function equal(ControllerEntry $entry): bool
    {
        return  $entry->getKeyname() === $this->getKeyname() && 
                $entry->getPackage() === $this->getPackage() && 
                $entry->getShortname() === $this->getShortname();
    }
}