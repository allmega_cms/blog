<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Utils\Register\Entries;

use Allmega\BlogBundle\Utils\Register\Register;
use Allmega\BlogBundle\Utils\Helper;

class WebpackEntry implements EntryInterface
{
    protected string $line = '';
    
    public function __construct(
        protected string $package,
        protected string $name = '',
        protected string $path = '',
        protected string $filename = '',
        protected bool $tab = true)
    {
        $this->handleProps();
    }

    protected function handleProps(): void
    {
        foreach (['name', 'filename'] as $name) {
            if (str_contains($this->$name, '{menu_type}')) {
                $this->$name = str_replace('{menu_type}', Register::$menuType, $this->$name);
            }
        }
    }

    public function setDefaults(string $type = 'js'): static
    {
        $this->path = Helper::getRelativePackagesAssetsPath($this->package, $type);
        $this->name = $this->package . '-' . $type;
        $this->filename = 'main.' . $type;
        return $this;
    }

    public function getPackage(): string
    {
        return $this->package;
    }

    public function setPackage(string $package): static
    {
        $this->package = $package;
        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;
        return $this;
    }

    public function getPath(): string
    {
        return $this->path;
    }
 
    public function setPath(string $path): static
    {
        $this->path = $path;
        return $this;
    }

    public function getFilename(): string
    {
            return $this->filename;
    }

    public function setFilename(string $filename): static
    {
        $this->filename = $filename;
        return $this;
    }
        
    public function getTab(): bool
    {
        return $this->tab;
    }

    public function setTab(bool $tab): static
    {
        $this->tab = $tab;
        return $this;
    }
 
    public function getLine(): string
    {
        if (!$this->line) $this->setLine();
        return $this->line;
    }

    public function setLine(): static
    {
        $this->line = $this->tab ? '    ' : '';
        $this->line .= ".addEntry('{$this->name}','{$this->path}{$this->filename}')" . PHP_EOL;
        return $this;
    }
}