<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Utils\Register\Entries;

class ControllerEntriesMap implements EntryInterface
{
    public const DASHBOARD_BOXES = 'DASHBOARD_BOXES';
    public const PROFILE_MENU = 'PROFILE_MENU';
    public const POPUP_BOXES = 'POPUP_BOXES';

    protected array $controllerEntries = [];
    protected string $line = '';

    public function __construct(
        protected string $keyname,
        protected string $question,
        protected string $filepath,
        protected string $package) {}

    public function getKeyname(): string
    {
        return $this->keyname;
    }

    public function setKeyname(string $keyname): static
    {
        $this->keyname = $keyname;
        return $this;
    }

    public function getQuestion(): string
    {
        return $this->question;
    }

    public function setQuestion(string $question): static
    {
        $this->question = $question;
        return $this;
    }
 
    public function getFilepath(): string
    {
        return $this->filepath;
    }
 
    public function setFilepath(string $filepath): static
    {
        $this->filepath = $filepath;
        return $this;
    }

    public function getPackage(): string
    {
        return $this->package;
    }

    public function setPackage(string $package): static
    {
        $this->package = $package;
        return $this;
    }

    /**
     * @return array<int,ControllerEntry>
     */
    public function getControllerEntries(): array
    {
        return $this->controllerEntries;
    }

    public function setControllerEntries(array $entries): static
    {
        $this->controllerEntries = $entries;
        return $this;
    }

    public function addControllerEntry(ControllerEntry $entry): static
    {
        $this->controllerEntries[] = $entry;
        return $this;
    }

    public function getLine(): string
    {
        if (!$this->line) $this->setLine();
        return $this->line;
    }

    public function setLine(): static
    {
        $this->line = $this->keyname . '=' . $this->build();
        return $this;
    }

    public function build(): string
    {
        $lines = [];
        foreach ($this->controllerEntries as $entry) $lines[] = $entry->build();
        return implode('|', $lines);
    }

    /**
     * @return array<int,ControllerEntry>
     */
    public static function parse(string $keyname, string $line): array
    {
        $entries = [];
        if (strpos($line, '|')) {
            foreach (explode('|', $line) as $entry) $entries[] = ControllerEntry::parse($keyname, $entry);
        } else {
            $entries[] = ControllerEntry::parse($keyname, $line);
        }
        return $entries;
    }
}