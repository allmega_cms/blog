<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Utils\Register;

use Allmega\BlogBundle\Utils\Register\Model\{FolderAndSymlinksTrait, HandlersTrait, StoragesTrait};
use Symfony\Component\Filesystem\Exception\FileNotFoundException;

class Register extends Config
{
    use FolderAndSymlinksTrait, HandlersTrait, StoragesTrait;

    public static string $menuType = '';
    protected bool $deactivate;

    public function getDeactivate(): bool
    {
        return $this->deactivate;
    }

    public function setDeactivate(bool $deactivate): static
    {
        $this->deactivate = $deactivate;
        return $this;
    }

    public function process(): void
    {
        $this
            ->writeBundlesConfig()
            ->writeRoutesConfig()
            ->handleCMSFoldersSymlinksImages();

        $env = $this->getHandlerByShortname('env');
        /** 
         * Read existing env entries from .env.{prod|dev|test}.local
         * Ask user the questions to application config or 
         * remove entries from config file
         */
        $env->setExistsEntries()->modifyEntries();

        $note = $this->deactivate ? 'Deactivate' : 'Activate';
        $this->handleStorageData($note);
        $env->handle();
    }

    /** Register bundles in the "config/bundles.php" */
    public function writeBundlesConfig(): static
    {
        $filename = $this->getConfigDir() . 'bundles.php';
        if (!$this->filesystem->exists($filename)) {
            throw new FileNotFoundException("$filename was not found! Check this first...");
        }
        
        $this->io->section('Register packages in config/bundles.php file');
        $content = file($filename, FILE_SKIP_EMPTY_LINES);
        
        foreach ($this->packages as $package) {

            if ($this->deactivate) {
                if ($this->checkRequiredPackage($package)) continue;
            }

            $row = "Allmega\\" . ucfirst($package) . "Bundle\Allmega" . ucfirst($package) . "Bundle::class => ['all' => true],";
            $phrase = 'already deleted!';

            foreach ($content as $index => $line) {
                if (preg_match('/\],/', $line)) {
                    if (str_contains($line, $row)) {
                        if (!$this->deactivate) $phrase = 'already registered';
                        else {
                            unset($content[$index]);
                            $phrase = 'successfully unregistered';
                        } 
                        break;
                    }
                } else if (preg_match('/\];/', $line) && !$this->deactivate) {
                    $content[$index] = '    ' . $row . "\n];\n";
                    $phrase = 'successfully registered';
                    break;
                }
            }
            $this->io->writeln("Package '$package' $phrase");
        }
        $this->filesystem->dumpFile($filename, implode('', $content));
        $this->io->writeln('');
        return $this;
    }

    /** Add bundles routes to the "config/routes.yaml" */
    public function writeRoutesConfig(): static
    {
        $filename = 'routes.yaml';
        $yaml = $this->getHandlerByShortname('yaml');
        $config = $yaml->setPath($this->getConfigDir())->setFilename($filename)->read();

        if ($config) {
            $this->io->section('Add packages routes to config/routes.yaml');
            foreach ($this->packages as $package) {

                if ($this->deactivate) {
                    if ($this->checkRequiredPackage($package)) continue;
                }

                $keyname = 'allmega_' . $package;
                $propExists = property_exists($config, $keyname);

                if ($this->deactivate) {
                    if (!$propExists) $phrase = 'already deleted';
                    else {
                        unset($config->$keyname);
                        $phrase = 'successfully deleted';
                    }
                } else {
                    if ($propExists) $phrase = 'already exists';
                    else {
                        $config->$keyname = new \stdClass();
                        $config->$keyname->resource = '@Allmega' . ucfirst($package) . 'Bundle/config/routing.yaml';
                        $phrase = 'successfully created';
                    }
                }
                $this->io->writeln("Package '$package' routes entries with keyname '$keyname' $phrase");
            }
            $yaml->setContent($config)->handle();
            $this->io->writeln('');
        }
        return $this;
    }

    /**
     * Make symlink from {current_package}/assets folder
     * to assets/packages/{current_package} folder
     */
    public function handleAssetsSymlinks(): static
    {
        $target = substr($this->getPackageAssetsSymlink(), 0, -1);
        $source = substr($this->getPackageAssetsDir(), 0, -1);
        
        return $this->handleSymlinks([$source => $target]);
    }

    protected function handleCMSFoldersSymlinksImages(): void
    {
        if (!$this->deactivate && in_array('blog', $this->packages)) {
            $this->setPackage('blog');

            /**
             * Handle CMS dirs
             * - cms/galleris-public
             */
            $folders = $this->getCMSDirs();
            
            /**
             * Create CMS symlinks 
             * - blog/templates/bundles to template/bundles
             * - cms/galleries-public to public/media
             */
            $symlinks = $this->getCMSSymlinks();

            /**
             * Copy CMS default images for logo 
             * and default image for post
             * - blog/assets/images/logo.png to cms/galleris-public/Web/logo.png
             * - blog/assets/images/post.jpg to cms/galleris-public/Web/post.jpg
             */
            $images = $this->getImagesToCopy();

            $this
                ->handleFolders($folders)
                ->handleSymlinks($symlinks)
                ->handleImages($images);
        }
    }
}