<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Utils\Register;

use Allmega\BlogBundle\Utils\Register\Entries\WebpackEntry;
use Symfony\Component\Finder\Finder;

class ThemeRegister extends Register
{
    protected string $packageDir;
    protected string $theme;

    public function setTheme(string $theme): static
    {
        $this->theme = $theme;
        return $this;
    }

    public function process(): void
    {
        $this->packageDir = $this->setPackage($this->theme)->getPackageDir();
        if (!$this->checkPackageDir()) return;

        /** Handle the Theme symlinks and webpack entries */
        $this
            ->removeTemplatesBundlesDir()
            ->handleSymlinks($this->getSymlinks())
            ->handleWebpackEntries()
            ->reset();
        
        $this->io->note('DONE!!!');
    }

    protected function checkPackageDir(): bool
    {
        if ($this->filesystem->exists($this->packageDir)) return true;

        $this->io->note("Package folder '{$this->packageDir}' does not exists! Check this first...");
        return false;
    }

    protected function removeTemplatesBundlesDir(): static
    {
        /** Symlink templates/bundles must be first deleted */
        $templatesBundlesDir = $this->getTemplatesBundlesDir();
        if ($this->filesystem->exists($templatesBundlesDir)) {
            $this->filesystem->remove($templatesBundlesDir);
        }
        return $this;
    }

    protected function getSymlinks(): array
    {
        $themeBundlesDir = $this->getPackageBundlesDir();
        $themeAssetsDir = $this->getPackageAssetsDir();

        $symlinks = [
            $themeAssetsDir => $this->getPackagesAssetsDir() . $this->theme,
            $themeBundlesDir => $this->getTemplatesBundlesDir(),
        ];

        return array_merge($symlinks, $this->getTranslationsSymlinks());
    }

    protected function getTranslationsSymlinks(): array
    {
        $themeTranslationsDir = $this->getPackageTranslationsDir();
        $symlinks = [];

        if ($this->filesystem->exists($themeTranslationsDir)) {

            $finder = new Finder();
            $finder
                ->depth('== 0')
                ->name('*.yaml')
                ->files()
                ->in($themeTranslationsDir);

            if ($finder->hasResults()) {
                foreach ($finder as $file) {
                    $target = $this->getTranslationsDir() . $file->getFilename();
                    $source = $themeTranslationsDir . DIRECTORY_SEPARATOR . $file->getFilename();
                    $symlinks[$source] = $target;
                }
            }
        }
        return $symlinks;
    }

    /**
     * If theme will be deactivated, activate Blog default theme 
     */
    protected function reset(): void
    {
        if ($this->deactivate) {
            $this->deactivate = false;
            $packageBundlesDir = $this->setPackage('blog')->getPackageBundlesDir();
            $symlinks = [
                $packageBundlesDir  => $this->getTemplatesBundlesDir()
            ];
            $this->handleSymlinks($symlinks);
        }
    }

    protected function handleWebpackEntries(): static
    {
        $this
            ->getHandlerByShortname('webpack')
            ->setTheme(true)
            ->setEntries($this->getEntries())
            ->setExistsEntries()
            ->handle();

        return $this;
    }

    private function getEntries(): array
    {
        if ($this->deactivate) return [];
        
        return [
            (new WebpackEntry($this->theme))->setDefaults('css'),
            (new WebpackEntry($this->theme))->setDefaults(),
        ];
    }
}