<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Utils\Register;

use Allmega\MediaBundle\Data as MediaData;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\Filesystem\Filesystem;
use Doctrine\ORM\EntityManagerInterface;

class Config
{
    protected string $projectDir;
    protected string $package;

    public function __construct(
        protected EntityManagerInterface $em,
        protected Filesystem $filesystem,
        ContainerBagInterface $params)
    {
        $this->projectDir = $params->get('kernel.project_dir') . DIRECTORY_SEPARATOR;
    }

    public function getPackage(): string
    {
        return $this->package;
    }

    public function setPackage(string $package): static
    {
        $this->package = $package;
        return $this;
    }

    public function getProjectDir(): string
    {
        return $this->projectDir;
    }

    public function getFilesystem(): Filesystem
    {
        return $this->filesystem;
    }

    /**
     * Symfony public directory - public
     */
    public function getPublicDir(): string
    {
        return $this->projectDir . 'public' . DIRECTORY_SEPARATOR;
    }

    /**
     * Symfony assets directory - assets
     */
    public function getAssetsDir(): string
    {
        return $this->projectDir . 'assets' . DIRECTORY_SEPARATOR;
    }

    /**
     * Symfony config directory - config
     */
    public function getConfigDir(): string
    {
        return $this->projectDir . 'config' . DIRECTORY_SEPARATOR;
    }

    /**
     * Symfony packages config directory - config/packages
     */
    public function getPackagesDir(): string
    {
        return $this->getConfigDir() . 'packages' . DIRECTORY_SEPARATOR;
    }

    /**
     * Symfony templates directory - template
     */
    public function getTemplatesDir(): string
    {
        return $this->projectDir . 'templates' . DIRECTORY_SEPARATOR;
    }

    /**
     * Symfony translations directory - translations
     */
    public function getTranslationsDir(): string
    {
        return $this->projectDir . 'translations' . DIRECTORY_SEPARATOR;
    }

    /**
     * Symfony templates directory - templates/bundles
     */
    public function getTemplatesBundlesDir(): string
    {
        return $this->getTemplatesDir() . 'bundles';
    }

    /**
     * Directory for store the uploaded files - cms/media|public|galleries etc
     */
    public function getCMSDir(): string
    {
        return $this->projectDir . 'cms' . DIRECTORY_SEPARATOR;
    }

    public function getUploadsDir(): string
    {
        return $this->getCMSDir() . 'uploads' . DIRECTORY_SEPARATOR;
    }

    public function getDocumentsDir(): string
    {
        return $this->getUploadsDir() . 'documents' . DIRECTORY_SEPARATOR;
    }

    /**
     * Web gallery folder - cms/galleries-public/Web/
     */
    public function getWebGalleryDir(): string
    {
        return $this->getPublicGalleriesDir() . MediaData::MEDIA_GALLERY . DIRECTORY_SEPARATOR;
    }

    /**
     * Assets directory for the symlinks to the package public directory
     * - vendor/package/assets -> assets/packages/package
     */
    public function getPackagesAssetsDir(): string
    {
        return $this->projectDir . 'assets' . DIRECTORY_SEPARATOR . 'packages' . DIRECTORY_SEPARATOR;
    }

    /**
     * Galleries directory - here be stored the galeries files - cms/galleries
     */
    public function getGalleriesDir(): string
    {
        return $this->getCMSDir() . 'galleries' . DIRECTORY_SEPARATOR;
    }

    /**
     * Web galleries directory - here be stored the web galeries files - cms/galleries-public
     */
    public function getPublicGalleriesDir(): string
    {
        return $this->getCMSDir() . 'galleries-public' . DIRECTORY_SEPARATOR;
    }

    /**
     * Symlink to CMS media public dir - public/media
     */
    public function getPublicMediaDir(): string
    {
        return $this->getPublicDir() . 'media' . DIRECTORY_SEPARATOR;
    }

    /**
     * Directory with all allmega packages, folders and files
     */
    public function getAllmegaDir(): string
    {
        return $this->projectDir . implode(DIRECTORY_SEPARATOR, ['vendor', 'allmega', '']);
    }

    /**
     * Directory with all files for the setted package
     */
    public function getPackageDir(): string
    {
        return $this->getAllmegaDir() . $this->package . DIRECTORY_SEPARATOR;
    }

    /**
     * This is a symlink to the setted package assets directory
     * assets/packages/symlink_to_the_package_assets_directory
     */
    public function getPackageAssetsSymlink(): string
    {
        return $this->getPackagesAssetsDir() . $this->package . DIRECTORY_SEPARATOR;
    }
    /**
     * Public directory for the setted package - package/assets
     */
    public function getPackageAssetsDir(): string
    {
        return $this->getPackageDir() . 'assets' . DIRECTORY_SEPARATOR;
    }

    /**
     * Views directory for the setted package - package/templates
     */
    public function getPackageTemplatesDir(): string
    {
        return $this->getPackageDir() . 'templates' . DIRECTORY_SEPARATOR;
    }

    /**
     * Translations directory for the setted package - package/translations
     */
    public function getPackageTranslationsDir(): string
    {
        return $this->getPackageDir() . 'translations' . DIRECTORY_SEPARATOR;
    }

    /**
     * Images directory for the setted package - package/assets/images
     */
    public function getPackageImagesDir(): string
    {
        return $this->getPackageAssetsDir() . 'images' . DIRECTORY_SEPARATOR;
    }

    /**
     * Other bundles directory for the setted package - package/templates/bundles
     */
    public function getPackageBundlesDir(): string
    {
        return $this->getPackageTemplatesDir() . 'bundles';
    }

    /**
     * Directories to be created|deleted by instal|uninstall packages
     */
    public function getCMSDirs(): array
    {
        return [
            $this->getPublicGalleriesDir(),
            $this->getDocumentsDir(),
        ];
    }

    /**
     * Symlinks to be created|deleted by instal|uninstall packages
     */
    public function getCMSSymlinks(): array
    {
        $packageBundlesDir = $this->getPackageBundlesDir();
        $galleriesPublicDir = substr($this->getPublicGalleriesDir(), 0, -1);
        $publicMediaDir = substr($this->getPublicMediaDir(), 0, -1);

        return [
            $packageBundlesDir  => $this->getTemplatesBundlesDir(),
            $galleriesPublicDir => $publicMediaDir,
        ];
    }

    public function getImagesToCopy(): array
    {
        $logo = 'logo.png';
        $post = 'post.jpg';

        $sourceLogo = $this->getPackageImagesDir() . $logo;
        $sourcePost = $this->getPackageImagesDir() . $post;

        $targetLogo = $this->getWebGalleryDir() . $logo;
        $targetPost = $this->getWebGalleryDir() . $post;

        return [$sourceLogo => $targetLogo, $sourcePost => $targetPost]; 
    }
}