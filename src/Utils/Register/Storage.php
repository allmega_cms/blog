<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Utils\Register;

use Allmega\BlogBundle\Utils\Register\Entries\{ControllerEntry, ControllerEntriesMap, WebpackEntry};
use Allmega\BlogBundle\Utils\Register\Model\AbstractStorage;

class Storage extends AbstractStorage
{
    protected array $controllerEntriesMap = [];
    protected array $controllerEntries = [];
    protected array $webpackEntries = [];

    public function handlePackageData(): void
    {
        $webpackEntries = $this->getWebpackEntries();
        if ($webpackEntries) { 
            $this->register
                ->getHandlerByShortname('webpack')
                ->setEntries($webpackEntries)
                ->setExistsEntries()
                ->handle();
        }

        $controllerEntriesMap = $this->mapControllerEntries()->getControllerEntriesMap();
        if ($controllerEntriesMap) {
            $this->register
                ->getHandlerByShortname('controllermap')
                ->setEntries($controllerEntriesMap)
                ->handle();
        }
    }
    
    public function getWebpackEntries(): array
    {
        return $this->webpackEntries;
    }

    public function getWebpackEntryByName(string $name): ?WebpackEntry
    {
        foreach ($this->webpackEntries as $entry) {
            if ($entry->getName() == $name) return $entry;
        }
        return null;
    }

    public function getControllerEntries(): array
    {
        return $this->controllerEntries;
    }

    public function getEqualControllerEntry(ControllerEntry $row): ?ControllerEntry
    {
        foreach ($this->controllerEntries as $entry) {
            if ($entry->equal($row)) return $entry;
        }
        return null;
    }

    public function getControllerEntriesMap(): array
    {
        return $this->controllerEntriesMap;
    }

    public function getControllerEntryMapByKeyname(string $keyname): ?ControllerEntriesMap
    {
        foreach ($this->controllerEntriesMap as $entry) {
            if ($entry->getKeyname() == $keyname) return $entry;
        }
        return null;
    }

    public function mapControllerEntries(): static
    {
        foreach ($this->controllerEntriesMap as $map) {
            foreach ($this->controllerEntries as $entry) {
                if ($map->getKeyname() == $entry->getKeyname()) $map->addControllerEntry($entry);
            }
        }
        return $this;
    }

    public function addPackageData(): static
    {
        return $this
            ->addWebpackEntries()
            ->addControllerEntries()
            ->addControllerEntriesMap();
    }

    protected function addWebpackEntries(): static
    {
        $entries = $this->data->getData('webpackEntries');
        if (!$entries) return $this;
        
        $this->register->handleAssetsSymlinks();
        foreach ($entries as $row) {
            $entry = $this->getWebpackEntryByName($row->getName());
            if (!$entry) {
                $this->webpackEntries[] = $row;
                $this->note("Package webpack entry with mark '{$row->getName()}' successfully {$this->phrase}");
            }
        }
        $this->io->writeln('');
        return $this;
    }

    protected function addControllerEntries(): static
    {
        $entries = $this->data->getData('controllerEntries');
        if (!$entries) return $this;

        $types = [];
        foreach ($entries as $row) {
            $entry = $this->getEqualControllerEntry($row);
            if (!$entry) {
                $this->controllerEntries[] = $row;
                $types[] = $row->getKeyname();
            }
        }

        foreach (array_unique($types) as $type) {
            $this->note("Package controller entries from type '$type' successfully {$this->phrase}");
        }
        $this->io->writeln('');
        return $this;
    }

    protected function addControllerEntriesMap(): static
    {
        $entries = $this->data->getData('controllerEntriesMap');
        if (!$entries) return $this;
        
        foreach ($entries as $row) {
            $entry = $this->getControllerEntryMapByKeyname($row->getKeyname());
            if (!$entry) {
                $this->controllerEntriesMap[] = $row;
                $this->note("Package controller entries map from type '{$row->getKeyname()}' successfully {$this->phrase}");
            }
        }
        $this->io->writeln('');
        return $this;
    }
}