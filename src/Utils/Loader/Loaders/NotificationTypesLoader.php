<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Utils\Loader\Loaders;

use Allmega\BlogBundle\Utils\Loader\Model\AbstractLoader;
use Allmega\BlogBundle\Entity\NotificationType;

class NotificationTypesLoader extends AbstractLoader
{
    protected array $props = ['title', 'description', 'subject', 'message'];

    public function init(): static
    {
        $this->transId = 'loader.notificationtypes.message';
        $this->title = 'loader.notificationtypes.phrase';
        $this->class = NotificationType::class;
        return $this;
    }

    public function prototype(object &$item, ?object $existsItem): void
    {
        $package = $this->trans('notifications.package', $this->currentPackage);
        $id = 'notifications.' . $item->getShortname() . '.';
        $translations = [];

        foreach ($this->props as $prop) {
            $translations[$prop] = $this->trans($id . $prop, $this->currentPackage);
        }
        extract($translations);

        if ($existsItem) $item = $existsItem;
        $item
            ->setDescription($description)
            ->setTemplate($message)
            ->setSubject($subject)
            ->setPackage($package)
            ->setName($title);
    }
}