<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Utils\Loader\Loaders;

use Allmega\BlogBundle\Utils\Loader\Converters\MenupointsConverter;
use Allmega\BlogBundle\Utils\Loader\Model\AbstractLoader;
use Allmega\BlogBundle\Entity\MenuPoint;

class MenupointsLoader extends AbstractLoader
{
    protected bool $flush = true;
    
    public function init(): static
    {
        $this->transId = 'loader.menupoints.message';
        $this->title = 'loader.menupoints.phrase';
        $this->class = MenuPoint::class;
        return $this;
    }

    public function preLoad(): bool
    {
        $items['blog'] = (new MenupointsConverter($this->urlGenerator, $this->translator, $this->em))
            ->setIo($this->io)
            ->setItems($this->items)
            ->getItems();

        $this->items = $items;
        return true;
    }

    public function prototype(object &$item, ?object $existsItem): void
    {
        if ($this->menupointsDefaults || !$existsItem) $this->handleParent($item);
        if ($existsItem) {
            $existsItem
                ->setDescription($item->getDescription())
                ->setActive($item->isActive())
                ->setRoute($item->getRoute())
                ->setName($item->getName())
                ->setType($item->getType())
                ->setSlug($item->getSlug())
                ->setSys($item->isSys());

            $existsItem->getGroups()->clear();
            foreach ($item->getGroups() as $group) $existsItem->addGroup($group);

            if ($this->menupointsDefaults) {
                $existsItem
                    ->setParent($item->getParent())
                    ->setPrio($item->getPrio());
            }
            $item = $existsItem;
        }
    }

    protected function handleParent(MenuPoint $item): void
    {
        $parent = $item->getParent();
        if ($parent) {
            $parent = $this->em->getRepository(MenuPoint::class)->findOneBy(['shortname' => $parent->getShortname()]);
            if ($parent) $item->setParent($parent);
        }
    }
}