<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Utils\Loader\Loaders;

use Allmega\BlogBundle\Utils\Loader\Model\AbstractLoader;
use Allmega\BlogBundle\Entity\Company;

class CompanyLoader extends AbstractLoader
{
    public function init(): static
    {
        $this->transId = 'loader.company.message';
        $this->title = 'loader.company.phrase';
        $this->class = Company::class;
        $this->updateable = false;
        $this->prop = 'name';
        return $this;
    }

    public function prototype(object &$item, ?object $existsItem): void {}
}