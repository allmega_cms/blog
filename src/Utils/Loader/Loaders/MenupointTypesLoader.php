<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Utils\Loader\Loaders;

use Allmega\BlogBundle\Utils\Loader\Model\AbstractLoader;
use Allmega\BlogBundle\Entity\MenuPointType;

class MenupointTypesLoader extends AbstractLoader
{
    public function init(): static
    {
        $this->transId = 'loader.menupointtypes.message';
        $this->title = 'loader.menupointtypes.phrase';
        $this->class = MenuPointType::class;
        return $this;
    }

    public function prototype(object &$item, ?object $existsItem): void
    {
        if ($existsItem) $item = $existsItem;
        $name = $this->trans('storage.menupointtypes.' . $item->getShortname(), $this->currentPackage);
        $item->setName($name);
    }
}