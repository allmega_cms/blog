<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Utils\Loader\Loaders;

use Allmega\BlogBundle\Utils\Loader\Model\AbstractLoader;
use Allmega\BlogBundle\Entity\Item;

class ItemsLoader extends AbstractLoader
{
    public function init(): static
    {
        $this->transId = 'loader.items.message';
        $this->title = 'loader.items.phrase';
        $this->class = Item::class;
        $this->prop = 'entityname';
        return $this;
    }

    public function prototype(object &$item, ?object $existsItem): void
    {
        if ($existsItem) {
            $item = $existsItem->setRoute($item->getRoute())->setProp($item->getProp());
        }
        $description = $this->trans($item->getDescription(), $this->currentPackage);
        $label = $this->trans($item->getLabel(), $this->currentPackage);
        $item->setLabel($label)->setDescription($description);
    }
}