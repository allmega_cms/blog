<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Utils\Loader;

use Allmega\BlogBundle\Utils\Loader\Loaders\{CategoriesTypesLoader, CompanyLoader, ItemsLoader, MenupointsLoader, MenupointTypesLoader, NotificationTypesLoader};
use Allmega\BlogBundle\Utils\Loader\Model\AbstractStorage;

class Storage extends AbstractStorage
{
    protected array $props = ['categoriestypes', 'companies', 'items', 'menupoints', 'menupointtypes', 'notificationtypes'];
    
    protected array $notificationtypes = [];
    protected array $categoriestypes = [];
    protected array $menupointtypes = [];
    protected array $menupoints = [];
    protected array $companies = [];
    protected array $items = [];


    public function getLoaders(): array
    {
        return [
            NotificationTypesLoader::class => $this->notificationtypes,
            CategoriesTypesLoader::class => $this->categoriestypes,
            MenupointTypesLoader::class => $this->menupointtypes,
            MenupointsLoader::class => $this->menupoints,
            CompanyLoader::class => $this->companies,
            ItemsLoader::class => $this->items,
       ];
    }
}