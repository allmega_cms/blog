<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Utils\Loader\Model;

use Allmega\BlogBundle\Utils\Helper;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Doctrine\ORM\EntityManagerInterface;

abstract class AbstractLoader
{
    protected bool $addUsers;
    protected bool $menupointsDefaults;
    protected string $message = '';
    protected string $title = 'items';
    protected string $transId = 'item';
    protected string $prop = 'shortname';
    
    protected UserPasswordHasherInterface $passwordHasher;
    protected UrlGeneratorInterface $urlGenerator;
    protected TranslatorInterface $translator;
    protected EntityManagerInterface $em;
    protected bool $updateable = true;
    protected string $currentPackage;
    protected bool $flush = false;
    protected SymfonyStyle $io;
    protected string $package;
    protected string $phrase;
    protected string $class;
    protected bool $update;
    protected array $items;

    public function __construct(AbstractStorage $storage, array $items)
    {
        $loader = $storage->getLoader();

        $this->menupointsDefaults = $loader->getMenupointsDefaults();
        $this->passwordHasher = $loader->getPasswordHasher();
        $this->urlGenerator = $loader->getUrlGenerator();
        $this->translator = $loader->getTranslator();
        $this->addUsers = $loader->getAddUsers();
        $this->em = $loader->getEntityManager();
        $this->package = $storage->getPackage();
        $this->phrase = $storage->getPhrase();
        $this->update = $loader->getUpdate();
        $this->io = $loader->getIo();
        $this->items = $items;
    }

    public function load(): void
    {
        if (!$this->items) return;

        $this->io->section($this->trans($this->title, $this->package));

        if ($this->preLoad()) {

            $repo = $this->em->getRepository($this->class);
            foreach ($this->items as $package => $items) {

                $this->currentPackage = $package;
                $message = $this->trans('loader.package', 'blog');
                $this->io->note($message . " '$package'");

                foreach ($items as $item) {

                    $this->setMessage($item);
                    $existsItem = $repo->findOneBy($this->getCriteria($item));

                    if ($this->checkItemExists($existsItem)) continue;

                    $this->io->writeln('-> ' . $this->message);
                    $this->prototype($item, $existsItem);

                    $this->em->persist($item);
                    $this->handle($item);
                    if ($this->flush) $this->em->flush();
                }
            }
            if (!$this->flush) $this->em->flush();
        }
    }

    public function preLoad(): bool
    {
        return true;
    }
    
    public function trans(string $id, string $package, array $params = []): string
    {
        $domain = 'Allmega' . ucfirst($package) . 'Bundle';
        return $this->translator->trans($id, $params, $domain);
    }

    public function checkItemExists(?object $item): bool
    {
        if (!$this->update && $item) {
            $transId = 'loader.item.exists';
            $message = $this->trans($transId, 'blog', ['%message%' => $this->message]);
            $this->io->writeln($message);
            return true;
        } elseif (!$this->updateable) {
            $item = $this->em->getRepository($this->class)->findOneBy([]);
            if ($item) {
                $params = ['%entity%' => Helper::getClassname($this->class)];
                $this->io->note($this->trans('loader.updateable', 'blog', $params));
                return true;
            }
        }
        return false;
    }

    public function getCriteria(object $item): array
    {
        return [$this->prop => $this->getPropValue($item)];
    }

    public function setMessage(object $item): void
    {
        $prop = $this->getPropValue($item);
        $this->message = $this->trans($this->transId, $this->package, ['%prop%' => $prop]);
    }

    protected function getPropValue(object $item): string|int
    {
        $method = 'get' . ucfirst($this->prop);
        if (!method_exists($item, $method)) {
            $classname = Helper::getClassname($item);
            throw new \Exception("Method with name '$method' of class '$classname' was not found!");
        }
        return $item->$method();
    }

    public function handle(object $item): void {}
    public abstract function prototype(object &$item, ?object $existsItem): void;
}