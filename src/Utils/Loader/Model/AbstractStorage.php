<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Utils\Loader\Model;

use Allmega\BlogBundle\Utils\Register\Model\AbstractStorageTrait;
use Allmega\BlogBundle\Utils\Loader\Loader;

abstract class AbstractStorage
{
    use AbstractStorageTrait;
    
    protected array $props = [];
    protected bool $menupointsDefaults;
    protected bool $addUsers;

    public function __construct(protected Loader $loader, string $package)
    {
        $this->phrase = $loader->getUpdate() ? 'reload' : 'load';
        $this->menupointsDefaults = $loader->getMenupointsDefaults();
        $this->addUsers = $loader->getAddUsers();
        $this->io = $loader->getIo();
        $this->package = $package;
    }

    public abstract function getLoaders(): array;

    public function getLoader(): Loader
    {
        return $this->loader;
    }

    public function handlePackageData(): void
    {
        foreach ($this->getLoaders() as $loaderClass => $data) {
            (new $loaderClass($this, $data))->init()->load();
        }
    }

    public function addPackageData(): static
    {
        foreach ($this->props as $prop) {
            $items = $this->data->getData($prop);
            if ($items) {
                $this->$prop = array_merge($this->$prop, $items);
            }
        }
        
        return $this;
    }
}