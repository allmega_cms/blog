<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Utils\Loader\Model;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\String\Slugger\AsciiSlugger;
use Symfony\Component\Console\Style\SymfonyStyle;
use Doctrine\ORM\EntityManagerInterface;

abstract class AbstractConverter
{
    protected AsciiSlugger $slugger;
    protected SymfonyStyle $io;
    protected string $package;
    
    public function __construct(
        protected UrlGeneratorInterface $urlGenerator,
        protected TranslatorInterface $translator,
        protected EntityManagerInterface $em)
    {
        $this->slugger = new AsciiSlugger();
    }
 
    public function setIo(SymfonyStyle $io): static
    {
        $this->io = $io;
        return $this;
    }
 
    public function setPackage(string $package): static
    {
        $this->package = $package;
        return $this;
    }
    
    public function trans(string $id, string $package, array $params = []): string
    {
        $domain = 'Allmega' . ucfirst($package) . 'Bundle';
        return $this->translator->trans($id, $params, $domain);
    }
}