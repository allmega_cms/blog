<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Utils\Loader\Converters;

use Allmega\AuthBundle\Entity\Group;
use Allmega\BlogBundle\Data;
use Allmega\BlogBundle\Entity\{MenuPoint, MenuPointType};
use Allmega\BlogBundle\Utils\Loader\Entries\MenupointEntry;
use Allmega\BlogBundle\Utils\Loader\Model\AbstractConverter;
use Doctrine\ORM\EntityNotFoundException;

class MenupointsConverter extends AbstractConverter
{
    protected array $quellItems;
    protected array $items = [];
    protected int $level = 0;

    /**
     * @return array<int,MenuPoint>
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @param array<int,MenupointEntry> $entries
     */
    public function setItems(array $entries): static
    {
        $this->quellItems = $entries;

        /** Set package, for translate the shortname of menupoint later */
        foreach ($entries as $package => $items) {
            foreach ($items as $item) $item->setPackage($package);
        }

        /** Convert array of menupoints entries to menupoints array */
        foreach ($entries as $package => $items) {
            $this->package = $package;
            foreach ($items as $item) $this->convert($item);
        }

        return $this->sort();
    }

    protected function convert(MenupointEntry $item): MenuPoint
    {
        $route = $item->getRoutename() ? $this->urlGenerator->generate($item->getRoutename(), $item->getRouteparams()) : '#';
        $groups = $item->getGroups() ? $this->em->getRepository(Group::class)->findByShortNames($item->getGroups()) : [];

        $title = $this->trans('menupoints.' . $item->getShortname(), $item->getPackage());
        $menuPointType = $this->getMenupointType($item);
        $slug = $this->slugger->slug($title);
        $parent = null;

        if ($item->getParent()) {
            $parent = $this->findInQuellItems($item->getParent());
            if ($parent) $parent = $this->convert($parent);
        }

        $item = (new MenuPoint())
            ->setSlug($slug)
            ->setName($title)
            ->setTitle($title)
            ->setParent($parent)
            ->setShortname($item->getShortname())
            ->setActive($item->getActive())
            ->setPrio($item->getPrio())
            ->setType($menuPointType)
            ->setSys($item->getSys())
            ->setRoute($route);

        foreach ($groups as $group) $item->addGroup($group);
        $this->addToItems($item);
        return $item;
    }

    protected function getMenupointType(MenupointEntry $item): MenuPointType
    {
        $menuPointType = $this->em->getRepository(MenuPointType::class)->findOneBy(['shortname' => $item->getRoutetype()]);
        if (!$menuPointType) {
            $message = $this->trans('loader.entityNotFound', 'blog', [
                '%shortname%' => $item->getShortname(), 
                '%type%' => $item->getRoutetype(),
                '%package%' => $this->package
            ]);
            throw new EntityNotFoundException($message);
        }
        return $menuPointType;
    }

    protected function findInQuellItems(string $shortname): ?MenupointEntry
    {
        foreach ($this->quellItems as $items) {
            foreach ($items as $item) if ($item->getShortname() == $shortname) return $item;
        }
        return null;
    }

    protected function addToItems(MenuPoint $menupoint): void
    {
        $found = false;
        foreach ($this->items as $item) {
            if ($item->equal($menupoint)) {
                $found = true;
                break;
            }
        }
        if (!$found) $this->items[] = $menupoint;
    }

    protected function sort(): static
    {
        $this->setLevels();
        usort($this->items, fn($a, $b) => $a->getLevel() - $b->getLevel());

        $items = [];
        foreach ($this->items as $item) $items[$item->getLevel()][] = $item;

        $this->items = [];
        foreach ($items as $rows) $this->items = array_merge($this->items, $rows);
        
		return $this->orderGroups();
    }

    protected function test(): void
    {
        foreach ($this->items as $menupoint) {
			if ($menupoint->getType()->getShortname() == Data::MENUPOINT_TYPE_MENU && $menupoint->getGroups()) {
				$tmp = [];
				foreach ($menupoint->getGroups() as $group) $tmp[] = $group->getName();
				$this->io->writeln($menupoint->getShortname() . ' => ' . implode('|', $tmp));
			}
		}
        exit;
    }

    protected function setLevels(): void
    {
         /** Set menupoints levels */
        foreach ($this->items as $item) {
            $this->level = 0;
            $this->countLevel($item);
            $item->setLevel($this->level);
        }
    }

    protected function countLevel(MenuPoint $item): void
    {
        $parent = $item->getParent();
        if ($parent) {
            $this->level++;
            $this->countLevel($parent);
        }
    }

    protected function orderGroups(): static
    {
        $items = array_reverse($this->items);
        foreach ($items as $item) $this->synchronize($item);
        $this->items = array_reverse($items);
        return $this;
    }

    protected function synchronize(MenuPoint $menupoint): static
    {
        $parent = $menupoint->getParent();
        if ($parent) {
            foreach ($this->items as &$item) {
                if ($item->equal($parent)) {
                    foreach ($menupoint->getGroups() as $group) {
                        if (!$item->getGroups()->contains($group)) $item->addGroup($group);
                    }
                }
            }
        }
        return $this;
    }
}