<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Utils\Loader\Entries;

class MenupointEntry
{
    protected string $package;

    public function __construct(
        protected string $shortname,
        protected string $routename,
        protected array $routeparams,
        protected string $routetype,
        protected int $prio = 1, 
        protected array $groups = [],
        protected string $parent = '',
        protected bool $active = false,
        protected bool $sys = false) {}

    public function getShortname(): string
    {
        return $this->shortname;
    }

    public function getRoutename(): string
    {
        return $this->routename;
    }

    public function getRouteparams(): array
    {
        return $this->routeparams;
    }

    public function getRoutetype(): string
    {
        return $this->routetype;
    }

    public function getPrio(): int
    {
        return $this->prio;
    }

    public function getGroups(): array
    {
        return $this->groups;
    }

    public function getParent(): string
    {
        return $this->parent;
    }

    public function getActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): static
    {
        $this->active = $active;
        return $this;
    }

    public function getSys(): bool
    {
        return $this->sys;
    }

    public function setSys(bool $sys): static
    {
        $this->sys = $sys;
        return $this;
    }

    public function getPackage(): string
    {
        return $this->package;
    }

    public function setPackage(string $package): static
    {
        $this->package = $package;
        return $this;
    }
}