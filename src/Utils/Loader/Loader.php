<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Utils\Loader;

use Allmega\BlogBundle\Utils\Register\Register;
use Allmega\BlogBundle\Utils\Register\Model\StoragesTrait;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Filesystem\Filesystem;
use Doctrine\ORM\EntityManagerInterface;

class Loader
{
    use StoragesTrait;

    protected string $package;
    protected string $allmegaDir;
    protected Filesystem $filesystem;
    protected bool $menupointsDefaults;
    protected bool $addUsers;
    protected bool $update;

    public function __construct(
        protected UserPasswordHasherInterface $passwordHasher,
        protected UrlGeneratorInterface $urlGenerator,
        protected TranslatorInterface $translator,
        protected EntityManagerInterface $em,
        Register $register)
    {
        $this->allmegaDir = $register->getAllmegaDir();
        $this->filesystem = $register->getFilesystem();
        $this->type = 'Loader';
    }

    public function getAllmegaDir(): string
    {
        return $this->allmegaDir;
    }

    public function getPasswordHasher(): UserPasswordHasherInterface
    {
        return $this->passwordHasher;
    }
    
    public function getUrlGenerator(): UrlGeneratorInterface
    {
        return $this->urlGenerator;
    }

    public function getTranslator(): TranslatorInterface
    {
        return $this->translator;
    }

    public function getEntityManager(): EntityManagerInterface
    {
        return $this->em;
    }

    public function getPackage(): string
    {
        return $this->package;
    }

    public function setPackage(string $package): static
    {
        $this->package = $package;
        return $this;
    }

    public function getMenupointsDefaults(): bool
    {
        return $this->menupointsDefaults;
    }

    public function setMenupointsDefaults(bool $menupointsDefaults): static
    {
        $this->menupointsDefaults = $menupointsDefaults;
        return $this;
    }

    public function getAddUsers(): bool
    {
        return $this->addUsers;
    }

    public function setAddUsers(bool $addUsers): static
    {
        $this->addUsers = $addUsers;
        return $this;
    }

    public function getUpdate(): bool
    {
        return $this->update;
    }

    public function setUpdate(string $update): static
    {
        $this->update = $update;
        return $this;
    }

    public function process(): void
    {
        $this->handleStorageData(register: false);
    }
}