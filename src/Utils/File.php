<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

 namespace Allmega\BlogBundle\Utils;

 use Symfony\Component\Finder\SplFileInfo;
 class File
 {
     public function __construct(private readonly SplFileInfo $splFile) {}
 
     public function getSplFile(): SplFileInfo
     {
         return $this->splFile;
     }
 
     public function getSize(): int
     {
         return $this->splFile->getSize();
     }
 
     public function getPath(): string
     {
         return $this->splFile->getPath();
     }
 
     public function getFilename(): string
     {
         return $this->splFile->getFilename();
     }
 
     public function getMimetype(): string
     {
         return mime_content_type($this->getFilepath()) ?: '';
     }
 
     private function getFilepath(): string
     {
         return $this->getPath() . DIRECTORY_SEPARATOR . $this->getFilename();
     }
 
     public function getBase64EncodedContent(): string
     {
         return Helper::getBase64Content($this->getFilepath());
     }
 }