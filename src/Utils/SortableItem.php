<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Utils;

use Allmega\BlogBundle\Model\SortableItemInterface;

class SortableItem
{
    public function __construct(private array $props, private string $label, private string $bundle) {}

    public function getProps(): array
    {
        return $this->props;
    }

    public function setProps(array $props): static
    {
        $this->props = $props;
        return $this;
    }

    public function addProp(string $prop): static
    {
        $this->props[] = $prop;
        return $this;
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function setLabel(string $label): static
    {
        $this->label = $label;
        return $this;
    }

    public function getBundle(): string
    {
        return $this->bundle;
    }

    public function setBundle(string $bundle): static
    {
        $this->bundle = $bundle;
        return $this;
    }

    public static function getInstance(SortableItemInterface $entity, string $label = ''): static
    {
        if (!$label) {
            $classname = Helper::getClassname($entity);
            $label = strtolower($classname);
        }
        $label .= '.label.';

        return new self($entity::getSortableProps(), $label, $entity::getBundleName());
    }
}