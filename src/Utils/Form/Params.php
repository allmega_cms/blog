<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Utils\Form;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Csrf\CsrfTokenManagerInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Form\FormView;

class Params
{
    private array $blockNames = ['bottom' => ['form', 'modify']];
    private string $bundle = 'AllmegaBlog';
    private array $heading = [];
    private array $delete = [];
    private array $blocks = [];
    private mixed $item = null;
    private array $forms = [];
    private bool $load = true;

    public function __construct(
        protected CsrfTokenManagerInterface    $csrfManager,
        private readonly UrlGeneratorInterface $urlGenerator,
        private readonly TranslatorInterface $translator,
        private readonly bool $hideLinks,
        private readonly Labels $labels,
        private readonly FormView $form,
        private readonly string $domain,
        private readonly string $action,
        private readonly string $route,
        private readonly string $path
    ) {}

    public function getBundle(): string
    {
        return strpos($this->domain, 'Bundle') ? str_replace('Bundle', '', $this->domain) : $this->bundle;
    }

    public function getDelete(): array
    {
        return $this->delete;
    }

    public function getHeading(): array
    {
        return $this->heading;
    }

    public function getForms(): array
    {
        return $this->forms;
    }

    public function getBlocks(): array
    {
        return $this->blocks;
    }

    public function getItem(): mixed
    {
        return $this->item;
    }

    public function getDomain(): string
    {
        return $this->domain;
    }

    public function getAction(): string
    {
        return $this->action;
    }

    public function getRoute(): string
    {
        return $this->route;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function init(array $params): static
    {
        $heading = $delete = $blocks = $link = $title = $form = $formParams = [];
        $showDelete = $load = true;
        $item = null;

        extract($params);
        // [$blocks, $form, $formParams, $delete, $heading, $item, $link, $title, $load, $showDelete]

        $this->load = $load;
        $this->item = $item;

        if ($this->item && $this->item->getId()) {
            $formParams = $formParams ?: ['id' => $this->item->getId()];
            if (method_exists($this->item, 'isSys')) $showDelete = !$this->item->isSys();

            if ($this->action == 'edit' && $showDelete) {
                $this->delete = $delete ?: $this->getDefaultDeleteData();
            }
        }

        $this->heading = $heading ?: $this->getDefaultHeadingData($link, $title);
        $this->forms = $form ? $this->getFormData($form) : $this->getDefaultFormData($formParams);
        $this->blocks = $this->checkBlocks($blocks);

        return $this;
    }

    private function getDefaultHeadingData(array $link, array $title): array
    {
        $actionsList = $this->hideLinks ? [] : ['links' => [$this->getDefaultLinkData($link)]];
        return [
            'title' => $this->getDefaultTitleData($title),
            'actionsList' => $actionsList
        ];
    }

    private function getFormData(array $form): array
    {
        $form['load'] = array_key_exists('load', $form) ? $form['load'] : true;
        $form['content'] = array_key_exists('content', $form) ? $form['content'] : $this->form;
        $form['path'] = $this->urlGenerator->generate($form['path'], $form['params']);
        return $form;
    }

    private function getDefaultFormData(array $params): array
    {
        $url = $this->urlGenerator->generate($this->route . $this->action, $params);
        return ['load' => $this->load, 'content' => $this->form, 'path' => $url];
    }

    private function getDefaultDeleteData(): array
    {
        $message = $this->translator->trans($this->labels->getConfirm(), [], $this->domain);
        $path = $this->urlGenerator->generate($this->route . 'delete', ['id' => $this->item->getId()]);
        $title = $this->translator->trans($this->labels->getDelete(), [], $this->domain);
        $csrf = $this->csrfManager->getToken('delete' . $this->item->getId())->getValue();

        return [
            'confirmation' => ['message' => $message, 'action' => 'delete'],
            'form' => ['path' => $path, 'title' => $title, 'csrf' => $csrf]
        ];
    }

    private function checkBlocks(array $blocks): array
    {
        foreach ($this->blockNames as $area => $names) {
            foreach ($names as $name) {
                if (!isset($blocks[$name])) $blocks[$name] = [$area => ''];
            }
        }
        return $blocks;
    }

    private function getDefaultLinkData(array $link): array
    {
        $title = $this->labels->getList();
        $route = $this->route . 'index';
        $icon = 'backward';
        $params = [];

        extract($link);
        // [$title, $icon, $route, $params]

        return [
            'title' => $this->translator->trans($title, [], $this->domain),
            'path'  => $this->urlGenerator->generate($route, $params),
            'icon'  => $icon
        ];
    }

    private function getDefaultTitleData(array $title): array
    {
        $add = $this->action == 'add';
        $content = $add ? $this->labels->getCreate() : $this->labels->getEdit();
        $icon = $add ? 'plus-circle' : 'edit';

        extract($title);
        // [$icon, $content]

        return ['icon' => $icon, 'content' => $this->translator->trans($content, [], $this->domain)];
    }
}
