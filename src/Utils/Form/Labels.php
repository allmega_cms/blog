<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

 namespace Allmega\BlogBundle\Utils\Form;

class Labels
{
    public function __construct(private readonly string $prop = 'label') {}

    public function getCreate(): string
    {
        return $this->prop . '.create';
    }

    public function getEdit(): string
    {
        return $this->prop . '.edit';
    }

    public function getList(): string
    {
        return $this->prop . '.list';
    }

    public function getDelete(): string
    {
        return $this->prop . '.delete';
    }

    public function getConfirm(): string
    {
        return $this->prop . '.delete_confirm';
    }
}