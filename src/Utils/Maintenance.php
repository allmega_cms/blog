<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Utils;

use Allmega\BlogBundle\Utils\Register\Config;
use Symfony\Component\Console\Style\SymfonyStyle;

class Maintenance extends Config
{
    public function process(bool $deactivate, SymfonyStyle $io): void
    {
        $envFile = $this->projectDir . '.env.local';
        $message = "Error! The file '$envFile' was not found! Check first the file exists!";
        $phrase = 'Maintenance mode successfully ';
        $key = 'MAINTENANCE_MODE=';

        if ($this->filesystem->exists($envFile)) {
            $content = file_get_contents($envFile);
            $content = $deactivate ?
                str_replace($key . '1', $key . '0', $content) :
                str_replace($key . '0', $key . '1', $content);
            file_put_contents($envFile, $content);
            $message = $deactivate ? $phrase . 'deactivated!' : $phrase . 'activated!';
        }
        $io->writeln($message);
    }
}