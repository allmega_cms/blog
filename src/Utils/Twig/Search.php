<?php

/**
 * @package   Allmega
 * @copyright Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Utils\Twig;

use Twig\Environment;

class Search
{
    private string $placeholder;
    private string $help;
    private string $path;
    private string $size;
    private string $css;

    public function __construct(private Environment $env, array $params)
    {
        $css = $size = $path = $help = $placeholder = '';
        extract($params);
        $this->placeholder = $placeholder;
        $this->size = $size ?: 'lg';
        $this->path = $path;
        $this->help = $help;
        $this->css = $css;
    }

    public function getView(): string
    {
        return $this->env->render('@AllmegaBlog/twig/_search.html.twig', ['search' => $this]);
    }

    public function getPlaceholder(): string
    {
        return $this->placeholder;
    }

    public function setPlaceholder(string $placeholder): static
    {
        $this->placeholder = $placeholder;
        return $this;
    }

	public function getHelp(): string
    {
		return $this->help;
	}

	public function setHelp(string $help): static
    {
		$this->help = $help;
		return $this;
	}

    public function getPath(): string
    {
        return $this->path;
    }

    public function setPath(string $path): static
    {
        $this->path = $path;
        return $this;
    }

    public function getSize(): string
    {
        return $this->size;
    }

    public function setSize(string $size): static
    {
        $this->size = $size;
        return $this;
    }

    public function getCss(): string
    {
        return $this->css;
    }

    public function setCss(string $css): static
    {
        $this->css = $css;
        return $this;
    }
}