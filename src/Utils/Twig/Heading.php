<?php

/**
 * @package   Allmega
 * @copyright Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Utils\Twig;

use Allmega\BlogBundle\Utils\Twig\{ActionsList, Title};
use Twig\Environment;

class Heading
{
    private bool $show;
    private string $css;
    private ?Title $title;
    private ?ActionsList $actionsList;

    public function __construct(private Environment $env, array $params)
    {
        $css = '';
        $show = true;
        $title = $actionsList = [];

        extract($params);

        $this->css = $css;
        $this->show = $show;
        $this->title = $title ? new Title($env, $title) : null;
        $this->actionsList = $actionsList ? new ActionsList($env, $actionsList) : null;

    }

    public function getView(): string
    {
        return $this->env->render('@AllmegaBlog/twig/heading/_layout.html.twig', ['heading' => $this]);
    }

    public function getCss(): string
    {
        return $this->css;
    }

    public function setCss(string $css): static
    {
        $this->css = $css;
        return $this;
    }

    public function getShow(): bool
    {
        return $this->show;
    }

    public function setShow(bool $show): static
    {
        $this->show = $show;
        return $this;
    }

    public function getTitle(): ?Title
    {
        return $this->title;
    }

    public function setTitle(?Title $title): static
    {
        $this->title = $title;
        return $this;
    }

    public function getActionsList(): ?ActionsList
    {
        return $this->actionsList;
    }

    public function setActionsList(?ActionsList $actionsList): static
    {
        $this->actionsList = $actionsList;
        return $this;
    }
}