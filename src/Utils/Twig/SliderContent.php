<?php

/**
 * @package   Allmega
 * @copyright Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Utils\Twig;

use Twig\Environment;

class SliderContent
{
    private ?int $state = null;
    private array $templates;
    private string $title;
    private array $params;
    private string $size;
    private string $icon;
    private string $css;
    private bool $card;

    public function __construct(private Environment $env, array $slider)
    {
        $css = $icon = $title = '';
        $params = $templates = [];
        $size = 'lg-6';
        $card = false;

        extract($slider);

        if (array_key_exists('card', $params)) $card = $params['card'];
        if (array_key_exists('size', $params)) $size = $params['size'];

        if (isset($state)) $this->state = $state;

        $this->templates = $templates;
        $this->params = $params;
        $this->title = $title;
        $this->size = $size;
        $this->icon = $icon;
        $this->card = $card;
        $this->css = $css;
    }

    public function getView(): string
    {
        return $this->env->render('@AllmegaBlog/twig/slider/_content.html.twig', ['slider' => $this]);
    }

    public function getSize(): string
    {
        return $this->size;
    }

    public function setSize(string $size): static
    {
        $this->size = $size;
        return $this;
    }

    public function getCss(): string
    {
        return $this->css;
    }

    public function setCss(string $css): static
    {
        $this->css = $css;
        return $this;
    }

    public function getIcon(): string
    {
        return $this->icon;
    }

    public function setIcon(string $icon): static
    {
        $this->icon = $icon;
        return $this;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): static
    {
        $this->title = $title;
        return $this;
    }

	public function getState(): ?int
    {
		return $this->state;
	}
	
	public function setState(int $state): static
    {
		$this->state = $state;
		return $this;
	}

    public function getParams(): array
    {
        return $this->params;
    }

    public function setParams(array $params): static
    {
        $this->params = $params;
        return $this;
    }

    public function getTemplates(): array
    {
        return $this->templates;
    }

    public function setTemplates(array $templates): static
    {
        $this->templates = $templates;
        return $this;
    }

    public function isCard(): bool
    {
        return $this->card;
    }

    public function setCard(bool $card): static
    {
        $this->card = $card;
        return $this;
    }
}