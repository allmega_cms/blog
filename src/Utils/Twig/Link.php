<?php

/**
 * @package   Allmega
 * @copyright Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Utils\Twig;

use Twig\Environment;

class Link
{
    private ?int $state = null;
    private string $type;
    private string $icon;
    private string $title;
    private string $path;
    private string $id;
    private string $css;
    private string $size;
    private string $label;
    private bool $confirm;
    private bool $load;

    public function __construct(private Environment $env, array $params)
    {
        $type = $icon = $title = $path = $id = $css = $size = $label = '';
        $confirm = $load = false;

        extract($params);

        if (isset($state)) $this->state = $state;

        $this->type = $type;
        $this->icon = $icon;
        $this->title = $title;
        $this->path = $path;
        $this->id = $id;
        $this->css = $css;
        $this->size = $size;
        $this->label = $label;
        $this->load = $load;
        $this->confirm = $confirm;
    }

    public function getView(): string
    {
        return $this->env->render('@AllmegaBlog/twig/_link.html.twig', ['link' => $this]);
    }

    public function getCls(): string
    {
        $css = 'fa-solid fa-';
        if ($this->icon) return $css . $this->icon;
        elseif ($this->state !== null ) {
            $color = $this->state ? 'success' : 'danger';
            $type = $this->state ? 'circle-check' : 'ban';
            return $css . $type . ' text-' . $color;
        }
        return '';
    }

    public function getState(): ?int
    {
        return $this->state;
    }

    public function setState(int $state): static
    {
        $this->state = $state;
        return $this;
    }

    public function getIcon(): string
    {
        return $this->icon;
    }

    public function setIcon(string $icon): static
    {
        $this->icon = $icon;
        return $this;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): static
    {
        $this->title = $title;
        return $this;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function setPath(string $path): static
    {
        $this->path = $path;
        return $this;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): static
    {
        $this->id = $id;
        return $this;
    }

    public function getCss(): string
    {
        return $this->css;
    }

    public function setCss(string $css): static
    {
        $this->css = $css;
        return $this;
    }

    public function getSize(): string
    {
        return $this->size;
    }

    public function setSize(string $size): static
    {
        $this->size = $size;
        return $this;
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function setLabel(string $label): static
    {
        $this->label = $label;
        return $this;
    }

    public function getConfirm(): bool
    {
        return $this->confirm;
    }

    public function setConfirm(bool $confirm): static
    {
        $this->confirm = $confirm;
        return $this;
    }

    public function getLoad(): bool
    {
        return $this->load;
    }

    public function setLoad(bool $load): static
    {
        $this->load = $load;
        return $this;
    }

	public function getType(): string
    {
		return $this->type;
	}

	public function setType(string $type): static
    {
		$this->type = $type;
		return $this;
	}
}