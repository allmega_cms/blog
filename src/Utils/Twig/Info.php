<?php

/**
 * @package   Allmega
 * @copyright Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Utils\Twig;

use Twig\Environment;

class Info
{
    private bool $show;
    private string $title;
    private ?string $content;

    public function __construct(private Environment $env, array $params)
    {
        $show = false;
        $title = 'label.show.infos';
        $content = '';

        extract($params);

        $this->show = $show;
        $this->title = $title;
        $this->content = $content;
    }

    public function view(): string
    {
        return $this->env->render('@AllmegaBlog/twig/_info.html.twig', ['info' => $this]);
    }

    public function getShow(): bool
    {
        return $this->show;
    }

    public function setShow(bool $show): static
    {
        $this->show = $show;
        return $this;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): static
    {
        $this->title = $title;
        return $this;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function setContent(string $content): static
    {
        $this->content = $content;
        return $this;
    }
}