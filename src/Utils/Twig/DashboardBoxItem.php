<?php

/**
 * @package   Allmega
 * @copyright Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Utils\Twig;

use Twig\Environment;

class DashboardBoxItem extends DashboardBox
{
    private string $badge;
    private string $bg;

    public function __construct(protected Environment $env, array $params)
	{
        parent::__construct($env, $params);

        $badge = $bg = null;
        extract($params);
        // [$badge, $bg]

        $this->badge = $badge ?? '';
        $this->bg = $bg ?? 'light';
    }
    
	public function getView(): string
    {
        return $this->env->render('@AllmegaBlog/twig/dashboard/inc/_box_item.html.twig', ['item' => $this]);
    }

	public function getBadge(): ?string
    {
		return $this->badge;
	}

	public function setBadge(string $badge): static
    {
		$this->badge = $badge;
		return $this;
	}

	public function getBg(): ?string
    {
		return $this->bg;
	}

	public function setBg(string $bg): static
    {
		$this->bg = $bg;
		return $this;
	}
}