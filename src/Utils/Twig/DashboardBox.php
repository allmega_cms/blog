<?php

/**
 * @package   Allmega
 * @copyright Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Utils\Twig;

use Allmega\BlogBundle\Utils\Twig\{ActionsList, Info, Title};
use Twig\Environment;

class DashboardBox
{
	protected ?Info $info;
	protected ?Title $title;
	protected string $content;
	protected ?ActionsList $actionsList;

	public function __construct(protected Environment $env, array $params)
	{
		$title = $info = $actionsList = [];
		$content = '';

		extract($params);

		$this->actionsList = $actionsList ? new ActionsList($env, $actionsList) : null;
		$this->title = $title ? new Title($env, $title) : null;
		$this->info = $info ? new Info($env, $info) : null;
		$this->content = $content;
	}

    public function getView(): string
    {
        return $this->env->render('@AllmegaBlog/twig/dashboard/inc/_box.html.twig', ['box' => $this]);
    }

	public function getInfo(): ?Info
	{
		return $this->info;
	}

	public function setInfo(?Info $info): static
	{
		$this->info = $info;
		return $this;
	}

	public function getTitle(): ?Title
	{
		return $this->title;
	}

	public function setTitle(?Title $title): static
	{
		$this->title = $title;
		return $this;
	}

	public function getContent(): string
	{
		return $this->content;
	}

	public function setContent(string $content): static
	{
		$this->content = $content;
		return $this;
	}

	public function getActionsList(): ?ActionsList
	{
		return $this->actionsList;
	}

	public function setActionsList(?ActionsList $actionsList): static
	{
		$this->actionsList = $actionsList;
		return $this;
	}
}