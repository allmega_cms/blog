<?php

/**
 * @package   Allmega
 * @copyright Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Utils\Twig;

use Allmega\BlogBundle\Utils\Twig\DashboardBox;
use Twig\Environment;

class Dashboard
{
    private array $boxes = [];
    private ?Heading $heading;

    public function   __construct(private Environment $env, array $params)
    {
        $heading = $boxes = [];
        extract($params);

        $this->heading = $heading ? new Heading($env, $heading) : null;
        foreach ($boxes as $box) {
            $this->addBox(new DashboardBox($env, $box));
        }
    }

    public function getView(): string
    {
        return $this->env->render('@AllmegaBlog/twig/dashboard/_layout.html.twig', ['dashboard' => $this]);
    }

    public function getHeading(): ?Heading
    {
        return $this->heading;
    }

    public function setHeading(?Heading $heading): static
    {
        $this->heading = $heading;
        return $this;
    }

    public function getBoxes(): array
    {
        return $this->boxes;
    }

    public function addBox(DashboardBox $box): static
    {
        $this->boxes[] = $box;
        return $this;
    }
}