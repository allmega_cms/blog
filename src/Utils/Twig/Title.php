<?php

/**
 * @package   Allmega
 * @copyright Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Utils\Twig;

use Twig\Environment;

class Title
{
    private int $size;
    private int $count;
    private bool $html;
    private string $css;
    private string $icon;
    private string $content;

    public function __construct(private Environment $env, array $params)
    {
        $html = false;
        $size = $count = 0;
        $css = $icon = $content = '';

        extract($params);

        $this->size = $size ?: 1;
        $this->content = $content;
        $this->count = $count;
        $this->icon = '';//$icon;
        $this->html = $html;
        $this->css = $css;
    }

    public function getView(): string
    {
        return $this->env->render('@AllmegaBlog/twig/heading/_title.html.twig', ['title' => $this]);
    }

    public function getSize(): int
    {
        return $this->size;
    }

    public function setSize(int $size): static
    {
        $this->size = $size;
        return $this;
    }

    public function getCount(): int
    {
        return $this->count;
    }

    public function setCount(int $count): static
    {
        $this->count = $count;
        return $this;
    }

    public function getCss(): string
    {
        return $this->css;
    }

    public function setCss(string $css): static
    {
        $this->css = $css;
        return $this;
    }

    public function getIcon(): string
    {
        return $this->icon;
    }

    public function setIcon(string $icon): static
    {
        $this->icon = $icon;
        return $this;
    }

    public function isHtml(): bool
    {
        return $this->html;
    }

    public function setHtml(bool $html): static
    {
        $this->html = $html;
        return $this;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function setContent(string $content): static
    {
        $this->content = $content;
        return $this;
    }
}