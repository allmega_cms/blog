<?php

/**
 * @package   Allmega
 * @copyright Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Utils\Twig;

use Allmega\BlogBundle\Utils\Twig\{DeleteForm, Heading};
use Symfony\Component\Form\FormView;
use Twig\Environment;

class Form
{
    private bool $load;
    private string $id;
    private string $css;
    private string $icon;
    private string $path;
    private string $label;
    private ?FormView $content;
    private array $errorMessages;
    private ?DeleteForm $delete;
    private ?Heading $heading;

    public function __construct(private Environment $env, array $params)
    {
        $load = false;
        $content = null;
        $id = $css = $icon = $path = $label = '';
        $form = $errorMessages = $heading = $delete = [];

        extract($params);
        // [$heading, $form]

        extract($form);

        $this->id = $id;
        $this->css = $css;
        $this->load = $load;
        $this->icon = $icon;
        $this->path = $path;
        $this->label = $label;
        $this->content = $content;
        $this->errorMessages = $errorMessages;
        $this->heading = $heading ? new Heading($env, $heading) : null;
        $this->delete = $delete ? new DeleteForm($env, $delete) : null;
    }

    public function getView(): string
    {
        return $this->env->render('@AllmegaBlog/twig/form/_layout.html.twig', ['form' => $this]);
    }

    public function getLoad(): bool
    {
        return $this->load;
    }

    public function setLoad(bool $load): static
    {
        $this->load = $load;
        return $this;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): static
    {
        $this->id = $id;
        return $this;
    }

    public function getCss(): string
    {
        return $this->css;
    }

    public function setCss(string $css): static
    {
        $this->css = $css;
        return $this;
    }

    public function getIcon(): string
    {
        return $this->icon;
    }

    public function setIcon(string $icon): static
    {
        $this->icon = $icon;
        return $this;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function setPath(string $path): static
    {
        $this->path = $path;
        return $this;
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function setLabel(string $label): static
    {
        $this->label = $label;
        return $this;
    }

    public function getContent(): ?FormView
    {
        return $this->content;
    }

    public function setContent(FormView $content): static
    {
        $this->content = $content;
        return $this;
    }

    public function getErrorMessages(): array
    {
        return $this->errorMessages;
    }

    public function setErrorMessages(array $errorMessages): static
    {
        $this->errorMessages = $errorMessages;
        return $this;
    }

    public function getHeading(): ?Heading
    {
        return $this->heading;
    }

    public function setHeading(?Heading $heading): static
    {
        $this->heading = $heading;
        return $this;
    }

    public function getDelete(): ?DeleteForm
    {
        return $this->delete;
    }

    public function setDelete(?DeleteForm $delete): static
    {
        $this->delete = $delete;
        return $this;
    }
}