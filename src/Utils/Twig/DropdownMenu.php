<?php

/**
 * @package   Allmega
 * @copyright Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Utils\Twig;

use Allmega\BlogBundle\Utils\Twig\Link;
use Twig\Environment;

class DropdownMenu
{
    private string $id;
    private array $links;

    public function __construct(private Environment $env, array $params)
    {
        $id = '';
        $links = [];
        extract($params);
        $this->id = $id;
        foreach ($links as $link) $this->addLink(new Link($env, $link));
    }

    public function getView(): string
    {
        return $this->env->render('@AllmegaBlog/twig/_dropdownmenu.html.twig', ['menu' => $this]);
    }

	public function getId(): string
    {
		return $this->id;
	}

	public function setId(string $id): static
    {
		$this->id = $id;
		return $this;
	}

    public function getLinks(): array
    {
        return $this->links;
    }

    public function addLink(Link $link): static
    {
        $this->links[] = $link;
        return $this;
    }
}