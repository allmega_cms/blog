<?php

/**
 * @package   Allmega
 * @copyright Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Utils\Twig;

use Twig\Environment;

class SideBox
{
    private array $content;
    private string $label;
    private string $side;
    private string $id;
    private int $top;

    public function __construct(private Environment $env, array $params)
    {
        $label = $id = '';
        $side = 'start';
        $content = [];
        $top = 50;

        extract($params);

        $this->content = $content;
        $this->label = $label;
        $this->side = $side;
        $this->top = $top;
        $this->id = $id;

    }

    public function getView(): string
    {
        return $this->env->render('@AllmegaBlog/twig/sidebox/_main.html.twig', ['box' => $this]);
    }

    public function getContent(): array
    {
        return $this->content;
    }

    public function setContent(array $content): static
    {
        $this->content = $content;
        return $this;
    }

    public function getLabel(): string
    {
        return $this->label;
    }

    public function setLabel(string $label): static
    {
        $this->label = $label;
        return $this;
    }

    public function getSide(): string
    {
        return $this->side;
    }

    public function setSide(string $side): static
    {
        $this->side = $side;
        return $this;
    }
 
    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): static
    {
        $this->id = $id;
        return $this;
    }

    public function getTop(): int
    {
        return $this->top;
    }

    public function setTop(int $top): static
    {
        $this->top = $top;
        return $this;
    }

    public function getDirection(): string
    {
        return $this->side == 'start' ? 'end' : 'start';
    }
}