<?php

/**
 * @package   Allmega
 * @copyright Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Utils\Twig;

use Allmega\AuthBundle\Entity\User;
use Twig\Error\{LoaderError, RuntimeError, SyntaxError};
use Twig\Environment;
use DateTimeInterface;

class AuthorInfo
{
    private ?DateTimeInterface $created;
    private ?DateTimeInterface $updated;
    private ?string $labelCreated;
    private ?string $labelUpdated;
    private ?User $author;
    private ?User $editor;

    public function __construct(private readonly Environment $env, array $params)
    {
        $author = $editor = $created = $updated = $labelCreated = $labelUpdated = null;

        extract($params);
        //[$author, $editor, $created, $updated, $labelCreated, $labelUpdated]

        $this->labelCreated = $labelCreated;
        $this->labelUpdated = $labelUpdated;
        $this->created = $created;
        $this->updated = $updated;
        $this->author = $author;
        $this->editor = $editor;
    }

    /**
     * @throws SyntaxError
     * @throws RuntimeError
     * @throws LoaderError
     */
    public function getView(): string
    {
        return $this->env->render('@AllmegaBlog/twig/_author_info.html.twig', ['item' => $this]);
    }

    public function getAuthor(): ?User
    {
        return $this->author;
    }

    public function setAuthor(User $author): static
    {
        $this->author = $author;
        return $this;
    }

    public function getEditor(): ?User
    {
        return $this->editor;
    }

    public function setEditor(User $editor): static
    {
        $this->editor = $editor;
        return $this;
    }

    public function getCreated(): ?DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(DateTimeInterface $created): static
    {
        $this->created = $created;
        return $this;
    }

    public function getUpdated(): ?DateTimeInterface
    {
        return $this->updated;
    }

    public function setUpdated(DateTimeInterface $updated): static
    {
        $this->updated = $updated;
        return $this;
    }

    public function getLabelCreated(): ?string
    {
        return $this->labelCreated;
    }

    public function setLabelCreated(string $labelCreated): static
    {
        $this->labelCreated = $labelCreated;
        return $this;
    }

    public function getLabelUpdated(): ?string
    {
        return $this->labelUpdated;
    }

    public function setLabelUpdated(string $labelUpdated): static
    {
        $this->labelUpdated = $labelUpdated;
        return $this;
    }
}