<?php

/**
 * @package   Allmega
 * @copyright Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Utils\Twig;

use Twig\Environment;

class Carousel
{
    private bool $indicators;
    private array $images;
    private string $size;

    public function __construct(private Environment $env, array $params)
    {
        $indicators = false;
        $images = [];
        $size = '';

        extract($params);

        $this->indicators = $indicators;
        $this->images = $images;
        $this->size = $size;

    }

    public function getView(): string
    {
        return $this->images ? $this->env->render('@AllmegaBlog/twig/_carousel.html.twig', ['carousel' => $this]) : '';
    }

	public function getIndicators(): bool
    {
		return $this->indicators;
	}

	public function setIndicators(bool $indicators): static
    {
		$this->indicators = $indicators;
		return $this;
	}

    public function getImages(): array
    {
        return $this->images;
    }

    public function setImages(array $images): static
    {
        $this->images = $images;
        return $this;
    }

    public function getSize(): string
    {
        return $this->size;
    }

    public function setSize(string $size): static
    {
        $this->size = $size;
        return $this;
    }
}