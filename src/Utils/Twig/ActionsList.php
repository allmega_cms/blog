<?php

/**
 * @package   Allmega
 * @copyright Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Utils\Twig;

use Twig\Error\{LoaderError, RuntimeError, SyntaxError};
use Twig\Environment;

class ActionsList
{
    private ?DropdownMenu $dropdownmenu;
    private array $tags = [];
    private array $links = [];
    private ?Search $search;
    private ?Sort $sort;
    private ?Info $info;

    public function __construct(private readonly Environment $env, array $params)
    {
        $tags = $links = [];
        $dropdownmenu = $search = $sort = $info = null;

        extract($params);
        // [$dropdownmenu, $info, $links, $search, $sort, $tags]

        $this->setTags($tags);
        foreach ($links as $link) {
            $this->addLink(new Link($env, $link));
        }

        $this->dropdownmenu = $dropdownmenu ? new DropdownMenu($env, $dropdownmenu) : null;
        $this->search = $search ? new Search($env, $search) : null;
        $this->sort = $sort ? new Sort($env, $sort) : null;
        $this->info = $info ? new Info($env, $info) : null;
    }

    /**
     * @throws SyntaxError
     * @throws RuntimeError
     * @throws LoaderError
     */
    public function getView(): string
    {
        return $this->env->render('@AllmegaBlog/twig/_actions_list.html.twig', ['actionsList' => $this]);
    }

    public function hasContent(): bool
    {
        return $this->tags || $this->links || $this->search || $this->sort || $this->info;
    }

    public function getTags(): array
    {
        return $this->tags;
    }

    public function setTags(array $tags): static
    {
        $this->tags = $tags;
        return $this;
    }

    public function getLinks(): array
    {
        return $this->links;
    }

    public function addLink(Link $link): static
    {
        $this->links[] = $link;
        return $this;
    }

	public function getDropdownmenu(): ?DropdownMenu
    {
		return $this->dropdownmenu;
	}

	public function setDropdownmenu(?DropdownMenu $dropdownmenu): static
    {
		$this->dropdownmenu = $dropdownmenu;
		return $this;
	}

	public function getSearch(): ?Search
    {
		return $this->search;
	}

	public function setSearch(?Search $search): static
    {
		$this->search = $search;
		return $this;
	}

    public function getSort(): ?Sort
    {
        return $this->sort;
    }

    public function setSort(?Sort $sort): static
    {
        $this->sort = $sort;
        return $this;
    }

    public function getInfo(): ?Info
    {
        return $this->info;
    }

    public function setInfo(?Info $info): static
    {
        $this->info = $info;
        return $this;
    }
}