<?php

/**
 * @package   Allmega
 * @copyright Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Utils\Twig;

use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use Doctrine\Common\Collections\Collection;
use Twig\Environment;

class SliderWrapper
{
    private Collection|SlidingPagination|array $items;
    private string $template;
    private string $listCss;
    private string $empty;
    private array $params;

    public function __construct(private Environment $env, array $param)
    {
        $empty = $template = $listCss = '';
        $params = $items = [];
        $card = false;

        extract($param);

        $this->params = array_merge($params, ['card' => $card]);
        $this->template = $template;
        $this->listCss = $listCss;
        $this->empty = $empty;
        $this->items = $items;
    }

    public function getView(): string
    {
        return $this->env->render('@AllmegaBlog/twig/slider/_wrapper.html.twig', ['sliderWrapper' => $this]);
    }

    public function getEmpty(): string
    {
        return $this->empty;
    }

    public function setEmpty(string $empty): static
    {
        $this->empty = $empty;
        return $this;
    }

    public function getTemplate(): string
    {
        return $this->template;
    }

    public function setTemplate(string $template): static
    {
        $this->template = $template;
        return $this;
    }

    public function getParams(): array
    {
        return $this->params;
    }

    public function setParams(array $params): static
    {
        $this->params = $params;
        return $this;
    }

    public function getItems(): Collection|SlidingPagination|array
    {
        return $this->items;
    }

    public function setItems(Collection|SlidingPagination|array $items): static
    {
        $this->items = $items;
        return $this;
    }

    public function getListCss(): string
    {
        return $this->listCss;
    }

    public function setListCss(string $listCss): static
    {
        $this->listCss = $listCss;
        return $this;
    }
}