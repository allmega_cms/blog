<?php

/**
 * @package   Allmega
 * @copyright Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Utils\Twig;

use Allmega\BlogBundle\Utils\Twig\Confirmation;
use Twig\Environment;

class DeleteForm
{
    private string $title;
    private string $csrf;
    private string $path;
    private ?Confirmation $confirmation;

    public function __construct(private Environment $env, array $params)
    {
        $title = $csrf = $path = '';
        $confirmation = $form = [];

        extract($params);
        // [$form, $confirmation]

        extract($form);
        // [$title, $csrf, $path]

        if ($confirmation) $confirmation['type'] = 'form';
        
        $this->confirmation = $confirmation ? new Confirmation($env, $confirmation) : null;
        $this->title = $title;
        $this->csrf = $csrf;
        $this->path = $path;
    }

    public function getView(): string
    {
        return $this->env->render('@AllmegaBlog/twig/form/_delete.html.twig', ['form' => $this]);
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): static
    {
        $this->title = $title;
        return $this;
    }

    public function getCsrf(): string
    {
        return $this->csrf;
    }

    public function setCsrf(string $csrf): static
    {
        $this->csrf = $csrf;
        return $this;
    }

    public function getPath(): string
    {
        return $this->path;
    }

    public function setPath(string $path): static
    {
        $this->path = $path;
        return $this;
    }

    public function getConfirmation(): ?Confirmation
    {
        return $this->confirmation;
    }

    public function setConfirmation(?Confirmation $confirmation): static
    {
        $this->confirmation = $confirmation;
        return $this;
    }
}