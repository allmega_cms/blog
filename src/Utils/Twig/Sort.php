<?php

/**
 * @package   Allmega
 * @copyright Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Utils\Twig;

use Allmega\BlogBundle\Utils\SortableItem;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use Twig\Environment;

class Sort
{
    private SortableItem $item;
    private SlidingPagination $sortables;

    public function __construct(private Environment $env, array $params)
    {
        $item = $sortables = null;
        extract($params);
        $this->sortables = $sortables;
        $this->item = $item;
    }

    public function getView(): string
    {
        return $this->env->render('@AllmegaBlog/twig/_sort_icon.html.twig', ['item' => $this->item, 'sortables' => $this->sortables]);
    }

    public function getItem(): SortableItem
    {
        return $this->item;
    }

    public function setItem(SortableItem $item): static
    {
        $this->item = $item;
        return $this;
    }

    public function getSortables(): SlidingPagination
    {
        return $this->sortables;
    }

    public function setSortables(SlidingPagination $sortables): static
    {
        $this->sortables = $sortables;
        return $this;
    }
}