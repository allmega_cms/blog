<?php

/**
 * @package   Allmega
 * @copyright Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Utils\Twig;

use Twig\Environment;

class Confirmation
{
    private string $type;
    private string $action;
    private string $message;

    public function __construct(private Environment $env, array $params)
    {
        $type = $action = $message = '';
        extract($params);
        // [$type, $action, $message]
        
        $this->type = $type ?: 'link';
        $this->action = $action ?: 'state';
        $this->message = $message;
    }

    public function getView(): string
    {
        return $this->env->render('@AllmegaBlog/twig/_confirmation.html.twig', ['confirmation' => $this]);
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): static
    {
        $this->type = $type;
        return $this;
    }

    public function getAction(): string
    {
        return $this->action;
    }

    public function setAction(string $action): static
    {
        $this->action = $action;
        return $this;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function setMessage(string $message): static
    {
        $this->message = $message;
        return $this;
    }
}