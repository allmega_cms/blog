<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Utils;

use Allmega\BlogBundle\Data;
use Allmega\BlogBundle\Utils\Search\SearchItem;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Validator\Constraints\{IsTrue, Length, NotBlank};

readonly class Helper
{
    public function __construct(private UrlGeneratorInterface $urlGenerator) {}

    public static function getTermsInputOptions(string $url): array
    {
        return [
            'mapped' => false,
            'constraints' => [
                new IsTrue(['message' => 'terms.not_accepted']),
            ],
            'label_translation_parameters' => ['%url%' => $url],
            'help_translation_parameters' => ['%url%' => $url],
            'translation_domain' => Data::DOMAIN,
            'label' => 'label.terms.acccept',
            'help' => 'label.terms.help',
            'label_html' => true,
            'help_html' => true
        ];
    }

    public static function getRepeatPasswordInputOptions(): array
    {
        return [
            'type' => PasswordType::class,
            'first_options' => [
                'attr' => ['placeholder' => 'user.label.password'],
                'label' => 'user.label.password',
            ],
            'second_options' => [
                'attr' => ['placeholder' => 'user.label.password_confirmation'],
                'label' => 'user.label.password_confirmation',
            ],
            'invalid_message' => 'errors.password_mismatch',
            'constraints' => [
                new NotBlank([
                    'message' => 'errors.blank',
                ]),
                new Length([
                    'min' => 8,
                    'minMessage' => 'errors.min_value',
                    'max' => 4096,
                ]),
            ],
        ];
    }

	public static function getRelativePackagesAssetsPath(string $package, string $dir = 'js'): string
	{
		return implode(DIRECTORY_SEPARATOR, ['.', 'assets', 'packages', $package, $dir, '']);
	}

    public static function isSameDay(\DateTimeInterface $begin, \DateTimeInterface $finish): bool
    {
        return $begin->format('Y-m-d') === $finish->format('Y-m-d');
    }

    public static function getDaysInYear(int $year): int
    {
        $date = new \DateTime($year . '-01-01');
        return intval($date->format('L')) ? 366 : 365;
    }

	/**
	 * Rounding up or down according to the following logic
	 * 2 - 2.25 = 2
	 * 2,26 - 2,75 = 2,5
	 * 2,76 - 2.99 = 3
	 */
	public static function roundDays(float $daysInYear): float
	{
		$floored = floor($daysInYear);
        $diff = $daysInYear - $floored;

		if ($diff <= 0.25) {
			$daysInYear = $floored;
		} else if ($diff > 0.25 && $diff <= 0.75) {
			$daysInYear = $floored + 0.5;
		} else {
			$daysInYear = $floored + 1;
        }
		return $daysInYear;
	}

    public function getJsonItems(SearchItem $searchItem): array
    {
        $routeProp = $searchItem->getRouteProp();
        $methodName = 'get' . ucfirst($routeProp);
        $items = [];

        foreach ($searchItem->getItems() as $item) {
            $routeParams = [$routeProp => $item->$methodName()];
            $routeParams = array_merge($searchItem->getRouteParams(), $routeParams);
            $items[] = $this->buildJsonItem($searchItem, $item, $routeParams);
        }
        return $items;
    }

    private function buildJsonItem(SearchItem $searchItem, object $item, array $routeParams): array
    {
        $row = [];
        $row['url'] = $this->urlGenerator->generate($searchItem->getRoute(), $routeParams);

        foreach ($searchItem->getProps() as $prop) {
            $methodName = 'get' . ucfirst($prop);
            $row[$prop] = $item->$methodName();
        }
        return $row;
    }

    public static function removeTags(string $text): string
    {
        return preg_replace('/<[^>]*>/', '', $text);
    }

    public static function getClassname(string|object $object): string
    {
        $class = is_object($object) ? get_class($object) : $object;
        return strtolower(substr(strrchr($class, "\\"), 1));
    }

    public static function sort(array $rows, bool $active = false): array
    {
        $dataset = [];
        foreach ($rows as $row) {

            if ($active && !$row->isActive()) continue;

            $id  = $row->getId();
            $pid = gettype($row->getParent()) != 'NULL' ? $row->getParent()->getId() : 0;

            $dataset[$id] = [
                'id' => $id,
                'pid' => $pid,
                'row' => $row,
                'childs' => [],
            ];
        }

        $tree = [];
        foreach ($dataset as $id => &$node) {
            if (!$node['pid']) $tree[$id] = &$node;
            else $dataset[$node['pid']]['childs'][$id] = &$node;
        }
        return $tree;
    }

    /**
     * Removes all non-alphanumeric characters except whitespaces
     */
    public static function sanitizeSearchQuery(string $query): string
    {
        return trim(preg_replace('/[[:space:]]+/', ' ', $query));
    }

    /**
     * Splits the search query into terms and removes the ones which are irrelevant
     */
    public static function extractSearchTerms(string $searchQuery, int $searchMin = 3, int $searchMax = 70): array
    {
        $terms = strpos($searchQuery, ':') ? array_unique(explode(':', $searchQuery)) : [$searchQuery];
        foreach ($terms as $i => $term) $terms[$i] = trim($term);

        return array_filter($terms, function ($term) use ($searchMin, $searchMax) {
            $length = mb_strlen($term);
            return $searchMin <= $length && $length <= $searchMax;
        });
    }

    public static function generateRandomString(int $length = 15): string
    {
        $chars = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        return mb_substr(str_shuffle(str_repeat($chars, ceil($length / mb_strlen($chars)))), 1, $length);
    }

    public static function getBase64Content(string $file): string
    {
        $content = base64_encode(file_get_contents($file));
        return 'data: ' . mime_content_type($file) . ';base64,' . $content;
    }

    public static function replace_signs(string $text): string
    {
        $search = [' ', 'Ä', 'Ö', 'Ü', 'ä', 'ö', 'ü', 'ß', '?', '!', '.', ',', '(', ')'];
        $replace = ['_', 'Ae', 'Oe', 'Ue', 'ae', 'oe', 'ue', 'ss', '', '', '', '', '_', '_'];
        return str_replace($search, $replace, $text);
    }

    public static function getLoremIpsum(): string
    {
        return <<<TEXT
Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor 
invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam 
et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea takimata sanctus est 
Lorem ipsum dolor sit amet. Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam 
nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua. 
At vero eos et accusam et justo duo dolores et ea rebum. Stet clita kasd gubergren, no sea 
takimata sanctus est Lorem ipsum dolor sit amet.
TEXT;
    }
}