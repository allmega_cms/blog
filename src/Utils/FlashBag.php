<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Utils;

use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\HttpFoundation\RequestStack;

readonly class FlashBag
{
    public function __construct(
        private TranslatorInterface $translator,
        private RequestStack $requestStack) {}

	public function add(string $type, string $sentence, string $bundle, array $params = []): void
	{
		$message = $this->translator->trans($sentence, $params, $bundle);
        $this->requestStack->getSession()->getFlashBag()->add($type, $message);
	}
}