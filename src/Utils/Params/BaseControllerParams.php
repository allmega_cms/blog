<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Utils\Params;

use Allmega\BlogBundle\Utils\Helper;
use Symfony\Contracts\Translation\TranslatorInterface;

class BaseControllerParams
{
    /** Entity to be modificated */
    protected ?object $entity = null;

    /** Should the web page be reloaded after the data has been updated? */
    protected bool $reload = true;

    /** Additional variables for the JSON response */
    protected array $jsonData = [];

    /** Path to the templates that are rendered */
    protected ?string $templatesPath = null;

    /** A shortname of the route, for example 'allmega_post_' */
    protected ?string $routeShort = null;

    /** Parameters for the setted route (see routeName) */
    protected array $routeParams = [];

    /** Current setted route name, for redirect */
    protected ?string $routeName = null;

    /** Name of the event that is triggered after data processing */
    protected ?string $eventName = null;

    /** What form of data(entity) should be rendered? */
    protected ?string $formType = null;

    /**
     * The parameters to be passed into the template specifically
     * for the data form. Example for heading, form, delete
     */
    protected array $formParams = [];

    /**
     * The path to the data form if the custom data
     * form is to be used for the form generation
     */
    protected ?string $formPath = null;

    /** Template parameters to be passed into the data form */
    protected array $formOptions = [];

    /**
     * The event parameters that are to be transferred
     * to the processing with the event to be triggered
     */
    protected array $arguments = [];

    /** The parameters to be passed into the template */
    protected array $options = [];

    /** What action do you want to take right now? Example add, edit, state, delete */
    protected ?string $action = null;

    /** Translate domain */
    protected ?string $domain = null;

    /**
     * The name of the class being updated
     * Relevant to the translation phrases in the template
     */
    protected ?string $prop = null;

    /** Notification text that will be displayed to the visitor after data processing */
    protected ?string $transId = null;

    /** Method name for change state prop active|enabled|state */
    protected ?string $method = null;

    /** Hide links in forms for add or edit entity */
    protected bool $hideLinks = false;

    /** Error messages key name */
    protected string $errorMessagesKey = 'errorMessages';

    public function init(
        array  $arguments = [],
        array  $formOptions = [],
        array  $formParams = [],
        array  $options = [],
        array  $routeParams = [],
        bool   $reload = true,
        bool $hideLinks = false,
        object $entity = null,
        string $action = null,
        string $domain = null,
        string $eventName = null,
        string $formPath = null,
        string $formType = null,
        string $method = 'active',
        string $prop = null,
        string $routeName = null,
        string $templatesPath = null,
        string $transId = null,
    ): static {
        return $this
            ->setEntity($entity)
            ->setTemplatesPath($templatesPath)
            ->addFormOptions($formOptions)
            ->addRouteParams($routeParams)
            ->addFormParams($formParams)
            ->setHideLinks($hideLinks)
            ->setRouteName($routeName)
            ->setEventName($eventName)
            ->addArguments($arguments)
            ->setFormType($formType)
            ->setFormPath($formPath)
            ->addOptions($options)
            ->setTransId($transId)
            ->setMethod($method)
            ->setDomain($domain)
            ->setAction($action)
            ->setReload($reload)
            ->setProp($prop)
            ->initProps();
    }

    private function initProps(): static
    {
        $id = $this->entity?->getId();
        if (!$this->action) $this->action = $id ? 'edit' : 'add';
        
        $phrase = $id ? 'updated' : 'created';
        $this->transId = $this->transId ?? $this->prop . '.' . $phrase;

        $this->formOptions['action'] = $this->action;
        $this->options[$this->errorMessagesKey] = [];

        return $this;
    }

    public function getEntity(): ?object
    {
        return $this->entity;
    }

    public function setEntity(?object $entity): static
    {
        $this->entity = $entity;
        return $this;
    }

    public function getFormParams(): array
    {
        return $this->formParams;
    }

    public function addFormParams(array $formParams): static
    {
        foreach ($formParams as $key => $value) {
            $this->formParams[$key] = $value;
        }
        return $this;
    }

    public function getFormType(): ?string
    {
        return $this->formType;
    }

    public function setFormType(?string $formType): static
    {
        $this->formType = $formType;
        return $this;
    }

    public function getFormPath(): ?string
    {
        return $this->formPath;
    }

    public function setFormPath(?string $formPath): static
    {
        $this->formPath = $formPath;
        return $this;
    }

    public function getEventName(): ?string
    {
        return $this->eventName;
    }

    public function setEventName(?string $eventName): static
    {
        $this->eventName = $eventName;
        return $this;
    }

    public function getArguments(): array
    {
        return $this->arguments;
    }

    public function addArguments(array $arguments): static
    {
        foreach ($arguments as $key => $value) {
            $this->arguments[$key] = $value;
        }
        return $this;
    }

    public function getOptions(): array
    {
        return $this->options;
    }

    public function getOption(string $key): mixed
    {
        if (array_key_exists($key, $this->options)) {
            return $this->options[$key];
        }
        return null;
    }

    public function addOptions(array $options): static
    {
        foreach ($options as $key => $value) {
            $this->options[$key] = $value;
        }
        return $this;
    }

    public function getFormOptions(): array
    {
        return $this->formOptions;
    }

    public function addFormOptions(array $options): static
    {
        foreach ($options as $key => $value) {
            $this->formOptions[$key] = $value;
        }
        return $this;
    }

    public function getDomain(): ?string
    {
        return $this->domain;
    }

    public function setDomain(?string $domain): static
    {
        $this->domain = $domain;
        return $this;
    }

    public function getAction(): ?string
    {
        return $this->action;
    }

    public function setAction(?string $action): static
    {
        $this->action = $action;
        return $this;
    }

    public function getReload(): bool
    {
        return $this->reload;
    }

    public function setReload(bool $reload): static
    {
        $this->reload = $reload;
        return $this;
    }

    public function getProp(): ?string
    {
        return $this->prop;
    }

    public function setProp(?string $prop = null): static
    {
        $this->prop = $prop ?? strtolower(Helper::getClassname($this->entity));
        return $this;
    }

    public function getTemplatesPath(): ?string
    {
        return $this->templatesPath;
    }

    public function setTemplatesPath(?string $templatesPath): static
    {
        $this->templatesPath = $templatesPath;
        return $this;
    }

    public function getRouteName(): ?string
    {
        return $this->routeName;
    }

    public function setRouteName(?string $routeName, string $prefix = 'index'): static
    {
        $this->routeName = $routeName . $prefix;
        $this->routeShort = $routeName;
        return $this;
    }

    public function getRouteShort(): ?string
    {
        return $this->routeShort;
    }

    public function setRouteShort(?string $routeShort): static
    {
        $this->routeShort = $routeShort;
        return $this;
    }

    public function getRouteParams(): array
    {
        return $this->routeParams;
    }

    public function addRouteParams(array $routeParams): static
    {
        foreach ($routeParams as $key => $val) $this->routeParams[$key] = $val;
        return $this;
    }

    public function getJsonData(): array
    {
        return $this->jsonData;
    }

    public function addJsonData(string $key, mixed $value): static
    {
        $this->jsonData[$key] = $value;
        return $this;
    }

    public function getTransId(): ?string
    {
        return $this->transId;
    }

    public function setTransId(?string $transId): static
    {
        $this->transId = $transId;
        return $this;
    }

    public function getMethod(): ?string
    {
        return $this->method;
    }

    public function setMethod(?string $method): static
    {
        $this->method = $method;
        return $this;
    }

    public function getHideLinks(): bool
    {
        return $this->hideLinks;
    }

    public function setHideLinks(bool $hideLinks): static
    {
        $this->hideLinks = $hideLinks;
        return $this;
    }

    public function addErrorMessage(string $message): static
    {
        $this->options[$this->errorMessagesKey][] = $message;
        return $this;
    }

    public function translateErrorMessages(TranslatorInterface $translator): static
    {
        $key = $this->errorMessagesKey;
        foreach ($this->options[$key] as $index => $message) {
            $this->options[$key][$index] = $translator->trans($message, [], $this->domain);
        }
        return $this;
    }
}