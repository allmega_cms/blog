<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @package   Allmega
 * @copyright Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Utils\Params;

use Allmega\AuthBundle\Entity\User;

class RepoParams
{
    public function __construct(
        private User $user,
        private string $type = UTypes::MEMBER,
        private bool $count = false,
        private int $limit = 0) {}

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): static
    {
        $this->user = $user;
        return $this;
    }
    
    public function getType(): string
    {
        return $this->type;
    }
    
    public function setType(string $type): static
    {
        $this->type = $type;
        return $this;
    }

    public function getCount(): bool
    {
        return $this->count;
    }
    
    public function setCount(bool $count): static
    {
        $this->count = $count;
        return $this;
    }

    public function getLimit(): int
    {
        return $this->limit;
    }

    public function setLimit(int $limit): static
    {
        $this->limit = $limit;
        return $this;
    }
}