<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @package   Allmega
 * @copyright Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Utils\Params;

class TemplateParams
{
    private string $empty;
    private string $title;
    private string $info;

    public function __construct(
        private readonly string $prop,
        private readonly string $type = UTypes::MEMBER,
        private bool $show = false)
    {
        $this->empty = $prop . '.empty.' . $type;
        $this->title = $prop . '.list.' . $type;
        $this->info = $prop . '.info.' . $type;
    }

    public function getProp(): string
    {
        return $this->prop;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getEmpty(): string
    {
        return $this->empty;
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function getInfo(): string
    {
        return $this->info;
    }

    public function getShow(): bool
    {
        return $this->show;
    }

    public function setShow(bool $show): static
    {
        $this->show = $show;
        return $this;
    }
}