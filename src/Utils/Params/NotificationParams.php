<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Utils\Params;

use Symfony\Component\Messenger\Attribute\AsMessage;

#[AsMessage]
class NotificationParams
{
    public function __construct(
        private string $type,
        private array $receivers,
        private array $search = [],
        private array $replace = [],
        private array $attachments = []) {}

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): static
    {
        $this->type = $type;
        return $this;
    }

    public function getReceivers(): array
    {
        return $this->receivers;
    }

    public function addReceiver(string $receiver): static
    {
        $this->receivers[] = $receiver;
        return $this;
    }

    public function setReceivers(array $receivers): static
    {
        $this->receivers = $receivers;
        return $this;
    }

    public function getSearch(): array
    {
        return $this->search;
    }

    public function addSearch(string $phrase): static
    {
        $this->search[] = $phrase;
        return $this;
    }

    public function setSearch(array $search): static
    {
        $this->search = $search;
        return $this;
    }

    public function getReplace(): array
    {
        return $this->replace;
    }

    public function addReplace(string $phrase): static
    {
        $this->replace[] = $phrase;
        return $this;
    }

    public function setReplace(array $replace): static
    {
        $this->replace = $replace;
        return $this;
    }

    public function getAttachments(): array
    {
        return $this->attachments;
    }

    public function addAttachment(mixed $attachment): static
    {
        $this->attachments[] = $attachment;
        return $this;
    }

    public function setAttachments(array $attachments): static
    {
        $this->attachments = $attachments;
        return $this;
    }
}