<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Utils\Params;

class VoterParams
{
    private array $extraAttributes = [];
    private bool  $base = true;
    private string $attribute;
    private mixed  $subject;
    private string $prefix;
 
    public function getBase(): bool
    {
        return $this->base;
    }
 
    public function setBase(bool $base): static
    {
        $this->base = $base;
        return $this;
    }

    public function getExtraAttributes(): array
    {
        return $this->extraAttributes;
    }

    public function setExtraAttributes(array $extraAttributes): static
    {
        $this->extraAttributes = $extraAttributes;
        return $this;
    }

    public function getAttribute(): string
    {
        return $this->attribute;
    }

    public function setAttribute(string $attribute): static
    {
        $this->attribute = $attribute;
        return $this;
    }

    public function getSubject(): mixed
    {
        return $this->subject;
    }

    public function setSubject(mixed $subject): static
    {
        $this->subject = $subject;
        return $this;
    }

    public function getPrefix(): string
    {
        return $this->prefix;
    }

    public function setPrefix(string $prefix): static
    {
        $this->prefix = $prefix;
        return $this;
    }
}