<?php

/**
 * This file is part of the Allmega Announcement Bundle package.
 *
 * @copyright Allmega 
 * @package   Announcement Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Utils\Params;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\RouterInterface;

class WebTestParams
{
    protected ?object $entity = null;
    
    protected ?string $fnRedirectParams;
    protected ?string $redirectRoute;
    protected array $redirectParams;
    protected ?string $redirectName;
    protected ?string $redirectUrl;
    protected int $redirectCode;

    protected ?string $fnRouteParams;
    protected array $routeParams;
    protected ?string $routeName;
    protected ?string $routeUrl;
    
    protected bool $testRedirect;
    protected array $optParams;
    protected string $method;
    protected bool $xmlHttp;
    
    protected ?string $role;
    protected array $files;
    protected bool $create;
    protected bool $login;

    public function __construct(private readonly RouterInterface $router) {}

    /**
     * @param string  $fnRedirectParams Function to get redirect route parameter
     * @param array   $redirectParams   Redirect route parameter
     * @param string  $redirectRoute    Full route name for redirect
     * @param string  $redirectName     Route name for redirect
     * @param int     $redirectCode     Redirect code (302, 303)
     * @param bool    $testRedirect     Test redirect to login form for anonymous user?
     * @param string  $redirectUrl      Redirect URL (/path/to/url)
     * @param array   $fnRouteParams    Function to get parameter for route name
     * @param array   $routeParams      Route parameter to generate the url from route name
     * @param string  $routeName        Route name for generation
     * @param string  $routeUrl         Route direct URL (/path/to/url)
     * @param array   $optParams        Optional parameters for the client request (GET. POST query|form parameter)
     * @param bool    $xmlHttp          Should be an XmlHttpRequest?
     * @param string  $method           Method for HTPP Request (GET|POST)
     * @param bool    $create           Is an entity to be generated and stored?
     * @param bool    $login            Should login take place? If yes, you need define the $role property
     * @param array   $files            Files for client request (Form FILES parameter, files for upload)
     * @param string  $role             You can enter a role here. The user is auto logged in with this role
     */
    public function init(
        string $fnRedirectParams = null,
        string $redirectRoute = null,
        string $redirectName = null,
        array $redirectParams = [],
        string $redirectUrl = null,
        bool $testRedirect = true,
        string $fnRouteParams = null,
        string $routeName = null,
        array $routeParams = [],
        string $routeUrl = null,
        string $method = 'GET',
        array $optParams = [],
        bool $xmlHttp = false,
        int $redirectCode = 0,
        bool $create = false,
        string $role = null,
        bool $login = true,
        array $files = []): static
    {
        $this->fnRedirectParams = $fnRedirectParams;
        $this->redirectParams = $redirectParams;
        $this->redirectRoute = $redirectRoute;
        $this->redirectName = $redirectName;
        $this->redirectCode = $redirectCode;
        $this->redirectUrl = $redirectUrl;
        $this->testRedirect = $testRedirect;
        $this->fnRouteParams = $fnRouteParams;
        $this->routeParams = $routeParams;
        $this->routeName = $routeName;
        $this->optParams = $optParams;
        $this->routeUrl = $routeUrl;
        $this->xmlHttp = $xmlHttp;
        $this->method = $method;
        $this->create = $create;
        $this->files = $files;
        $this->login = $login;
        $this->role = $role;

        if ($this->redirectCode === 0) $this->redirectCode = Response::HTTP_SEE_OTHER;
        return $this;
    }

    public function reset(): static
    {
        return $this->init();
    }

    public function getEntity(): ?object
    {
        return $this->entity;
    }

    public function setEntity(?object $entity)
    {
        $this->entity = $entity;
        return $this;
    }

    public function getFnRedirectParams(): ?string
    {
        return $this->fnRedirectParams;
    }

    public function setFnRedirectParams(?string $fnRedirectParams)
    {
        $this->fnRedirectParams = $fnRedirectParams;
        return $this;
    }
    
    public function getRedirectParams(): array
    {
        return $this->redirectParams;
    }

    public function addRedirectParams(array $redirectParams): static
    {
        foreach ($redirectParams as $key => $param) {
            $this->redirectParams[$key] = $param;
        }
        return $this;
    }

    public function getRedirectRoute(): ?string
    {
        return $this->redirectRoute;
    }

    public function setRedirectRoute(?string $redirectRoute): static
    {
        $this->redirectRoute = $redirectRoute;
        return $this;
    }

    public function getRedirectName(): ?string
    {
        return $this->redirectName;
    }

    public function setRedirectName(?string $redirectName): static
    {
        $this->redirectName = $redirectName;
        return $this;
    }

    public function getRedirectUrl(): ?string
    {
        return $this->redirectUrl;
    }

    public function setRedirectUrl(bool $reset = false): static
    {
        $this->redirectUrl = $reset ? null : $this->handleUrl($this->redirectUrl, $this->redirectName, $this->redirectParams);
        return $this;
    }

    public function getRedirectCode(): int
    {
        return $this->redirectCode;
    }

    public function setRedirectCode(int $redirectCode): static
    {
        $this->redirectCode = $redirectCode;
        return $this;
    }
 
    public function getTestRedirect(): bool
    {
        return $this->testRedirect;
    }
 
    public function setTestRedirect(bool $testRedirect)
    {
        $this->testRedirect = $testRedirect;
        return $this;
    }

    public function getMethod(): string
    {
        return $this->method;
    }

    public function setMethod(string $method): static
    {
        $this->method = $method;
        return $this;
    }
 
    public function getCreate(): bool
    {
        return $this->create;
    }

    public function setCreate(bool $create): static
    {
        $this->create = $create;
        return $this;
    }

    public function getRouteName(): ?string
    {
        return $this->routeName;
    }

    public function setRouteName(string $routeName): static
    {
        $this->routeName = $routeName;
        return $this;
    }

    public function getFnRouteParams(): ?string
    {
        return $this->fnRouteParams;
    }

    public function setFnRouteParams(?string $fnRouteParams): static
    {
        $this->fnRouteParams = $fnRouteParams;
        return $this;
    }

    public function getRouteParams(): array
    {
        return $this->routeParams;
    }

    public function setRouteParams(array $routeParams): static
    {
        $this->routeParams = $routeParams;
        return $this;
    }

    public function addRouteParams(array $routeParams): static
    {
        foreach ($routeParams as $key => $value) {
            $this->routeParams[$key] = $value;
        }
        return $this;
    }

    public function getOptParams(): array
    {
        return $this->optParams;
    }

    public function addOptParams(array $optParams): static
    {
        foreach ($optParams as $key => $value) {
            $this->optParams[$key] = $value;
        }
        return $this;
    }

    public function getFiles(): array
    {
        return $this->files;
    }

    public function addFiles(array $files): static
    {
        foreach ($files as $key => $value) {
            $this->files[$key] = $value;
        }
        return $this;
    }

    public function isXmlHttp(): bool
    {
        return $this->xmlHttp;
    }

    public function setXmlHttp(bool $xmlHttp): static
    {
        $this->xmlHttp = $xmlHttp;
        return $this;
    }

    public function getRole(): ?string
    {
        return $this->role;
    }

    public function setRole(?string $role): static
    {
        $this->role = $role;
        return $this;
    }

    public function getLogin(): bool
    {
        return $this->login;
    }

    public function setLogin(bool $login): static
    {
        $this->login = $login;
        return $this;
    }

    public function getRouteUrl(): ?string
    {
        return $this->routeUrl;
    }

    public function setRouteUrl(bool $reset = false): static
    {
        $this->routeUrl = $reset ? null : $this->handleUrl($this->routeUrl, $this->routeName, $this->routeParams);
        return $this;
    }

    private function handleUrl(?string $url, ?string $route, array $params): ?string
    {
        if ($url || $route || $this->redirectRoute) {
            $route = $route ?? $this->redirectRoute;
            $url = $url ?? $this->router->generate($route, $params);
        }
        return $url;
    }
}