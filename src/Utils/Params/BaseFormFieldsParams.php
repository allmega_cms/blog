<?php

/**
 * This file is part of the Allmega Auth Bundle package.
 *
 * @copyright Allmega
 * @package   Auth Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Utils\Params;

use Allmega\BlogBundle\Data;
use Symfony\Contracts\Translation\TranslatorInterface;
use Doctrine\ORM\QueryBuilder;

abstract class BaseFormFieldsParams
{
    protected string $multiplemark;

    public function __construct(
        TranslatorInterface $translator,
        protected ?QueryBuilder $queryBuilder = null,
        protected bool $reference = true,
        protected bool $required = false,
        protected bool $multiple = false,
        protected ?string $label = null,
        protected ?string $help = null,
        protected bool $mapped = true)
    {
        $this->multiplemark = $translator->trans('action.multiplemark', [], Data::DOMAIN);
    }

    public function getMultiplemark(): string
    {
        return $this->multiplemark;
    }

    public function isReference(): bool
    {
        return $this->reference;
    }

    public function setReference(bool $reference): static
    {
        $this->reference = $reference;
        return $this;
    }

    public function isRequired(): bool
    {
        return $this->required;
    }

    public function setRequired(bool $required): static
    {
        $this->required = $required;
        return $this;
    }

    public function isMultiple(): bool
    {
        return $this->multiple;
    }

    public function setMultiple(bool $multiple): static
    {
        $this->multiple = $multiple;
        return $this;
    }

    public function isMapped(): bool
    {
        return $this->mapped;
    }

    public function setMapped(bool $mapped): static
    {
        $this->mapped = $mapped;
        return $this;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(string $label): static
    {
        $this->label = $label;
        return $this;
    }

    public function getHelp(): ?string
    {
        return $this->help;
    }

    public function setHelp(string $help): static
    {
        $this->help = $help;
        return $this;
    }

    public function getQueryBuilder(): ?QueryBuilder
    {
        return $this->queryBuilder;
    }

    public function setQueryBuilder(?QueryBuilder $queryBuilder): static
    {
        $this->queryBuilder = $queryBuilder;
        return $this;
    }

    public function getBaseOptions(string $className, string $prop): array
    {
        return [
            'class' => $className,
            'choice_label' => $prop,
            'query_builder' => fn () => $this->queryBuilder,
            'by_reference' => $this->reference,
            'required' => $this->required,
            'mapped' => $this->mapped,
            'label' => $this->label,
            'help' => $this->help,
        ];
    }

    public function getMultipleOptions(): array
    {
        if ($this->multiple) {
            return [
                'help_translation_parameters' => [
                    '%multiplemark%' => $this->multiplemark,
                ],
                'help_html' => true,
                'multiple' => true,
            ];
        }
        return [];
    }
}