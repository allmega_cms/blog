<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @package   Allmega
 * @copyright Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Utils\Params;

class UTypes
{
    public const TYPES = 'member|author|manager|archive';
    public const ARCHIVE = 'archive';
    public const MANAGER = 'manager';
    public const AUTHOR = 'author';
    public const MEMBER = 'member';
}