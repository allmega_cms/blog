<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Utils\Params;

class MenuPointsTraitParams extends BaseFormFieldsParams
{
    private ?string $entity = null;

    public function getEntity(): ?string
    {
        return $this->entity;
    }

    public function setEntity(string $entity): static
    {
        $this->entity = $entity;
        return $this;
    }
}