<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Utils;

use Doctrine\ORM\Query;
use Knp\Component\Pager\PaginatorInterface;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Symfony\Component\HttpFoundation\{Request, RequestStack};

class Paginator
{
	public const NUM_ITEMS = 20;
	private Request $request;

    public function __construct(private readonly PaginatorInterface $paginator, RequestStack $requestStack)
    {
    	$this->request = $requestStack->getCurrentRequest();
    }

    public function getPagination(Query $query): PaginationInterface
    {
    	return $this->paginator->paginate($query, $this->request->query->getInt('page', 1), self::NUM_ITEMS);
    }
}