<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Form;

use Allmega\BlogBundle\Data;
use Allmega\BlogBundle\Entity\Company;
use Symfony\Component\Form\{AbstractType, FormBuilderInterface};
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CompanyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', null, [
                'label' => 'company.label.name',
                'attr' => ['autofocus' => true, 'placeholder' => 'company.label.name']
            ])
            ->add('description', null, [
                'label' => 'company.label.description',
                'attr' => ['placeholder' => 'company.label.description']
            ])
            ->add('address', null, [
                'label' => 'company.label.address',
                'attr' => ['placeholder' => 'company.label.address'],
                'help' => 'company.help.address'
            ])
            ->add('phone', null, [
                'label' => 'company.label.phone',
                'attr' => ['placeholder' => 'company.label.phone']
            ])
            ->add('fax', null, [
                'label' => 'company.label.fax',
                'attr' => ['placeholder' => 'company.label.fax']
            ])
            ->add('mobile', null, [
                'label' => 'company.label.mobile',
                'attr' => ['placeholder' => 'company.label.mobile']
            ])
            ->add('mail', null, [
                'label' => 'company.label.mail',
                'attr' => ['placeholder' => 'company.label.mail']
            ])
            ->add('manager', null, [
                'label' => 'company.label.manager',
                'attr' => ['placeholder' => 'company.label.manager']
            ])
            ->add('director', null, [
                'label' => 'company.label.director',
                'attr' => ['placeholder' => 'company.label.director']
            ])
            ->add('register', null, [
                'label' => 'company.label.register',
                'attr' => ['placeholder' => 'company.label.register']
            ])
            ->add('bank', null, [
                'label' => 'company.label.bank',
                'attr' => ['placeholder' => 'company.label.bank']
            ])
            ->add('iban', null, [
                'label' => 'company.label.iban',
                'attr' => ['placeholder' => 'company.label.iban']
            ])
            ->add('bic', null, [
                'label' => 'company.label.bic',
                'attr' => ['placeholder' => 'company.label.bic']
            ])
            ->add('webauthors', null, [
                'label' => 'company.label.webauthors',
                'attr' => ['placeholder' => 'company.label.webauthors'],
                'help' => 'company.help.webauthors'
            ])
            ->add('content', TextareaType::class, [
                'attr' => ['rows' => 20, 'placeholder' => 'company.label.content'],
                'label' => 'company.label.content'
            ])
            ->add('markdown', null, ['label' => 'company.label.markdown']);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Company::class,
            'translation_domain' => Data::DOMAIN
        ]);
    }
}