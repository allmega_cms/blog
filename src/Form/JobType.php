<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Form;

use Allmega\BlogBundle\Data;
use Allmega\BlogBundle\Entity\Job;
use Allmega\BlogBundle\Utils\Helper;
use Symfony\Component\Form\Extension\Core\Type\{CheckboxType, EmailType, TextareaType};
use Symfony\Component\Form\{AbstractType, FormBuilderInterface};
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\Routing\RouterInterface;

class JobType extends AbstractType
{
    public function __construct(
        private readonly RouterInterface $router,
        private readonly string $termsPageSlug) {}

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $url = $this->router->generate('allmega_blog_page_show', ['slug' => $this->termsPageSlug]);

        $builder
            ->add('sender', null, [
                'label' => 'label.sender',
                'attr' => ['autofocus' => true, 'placeholder' => 'job.label.sender']
            ])
            ->add('email', EmailType::class, [
                'label' => 'label.email',
                'attr' => ['placeholder' => 'job.label.email']
            ])
            ->add('phone', null, [
                'label' => 'label.phone',
                'attr' => ['placeholder' => 'job.label.phone']
            ])
            ->add('contact', null, [
                'attr' => ['placeholder' => 'job.label.placeholder'],
                'label' => 'job.label.contact',
                'help' => 'job.help.contact'
            ])
            ->add('message', TextareaType::class, [
                'label' => 'job.label.message',
                //'help' => 'job.help.message',
                'attr' => ['rows' => 15, 'placeholder' => 'job.label.message']
            ])
            ->add('documents', FileType::class, [
                'label' => 'job.label.documents',
                'help' => 'job.help.documents',
                'help_html' => true,
                'mapped' => false,
                'required' => false,
                'constraints' => [
                    new File([
                        'maxSize' => '10m',
                        'mimeTypes' => [
                            'application/pdf',
                            'application/x-pdf',
                        ],
                        'mimeTypesMessage' => 'Please upload a valid PDF document',
                    ])
                ],
            ])
            ->add('agreeTerms', CheckboxType::class, Helper::getTermsInputOptions($url));
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Job::class,
            'translation_domain' => Data::DOMAIN
        ]);
    }
}