<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Form;

use Allmega\BlogBundle\Data;
use Allmega\BlogBundle\Entity\NotificationType;
use Symfony\Component\Form\{AbstractType, FormBuilderInterface};
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\OptionsResolver\OptionsResolver;

class NotificationTypeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('name', null, [
                'attr' => ['autofocus' => true, 'placeholder' => 'notificationtype.label.name'],
                'label' => 'notificationtype.label.name'
            ])
            ->add('description', null, [
                'attr' => ['placeholder' => 'notificationtype.label.description'],
                'label' => 'notificationtype.label.description'
            ])
            ->add('subject', null, [
                'attr' => ['placeholder' => 'notificationtype.label.subject'],
                'label' => 'notificationtype.label.subject'
            ])
            ->add('template', TextareaType::class, [
                'attr' => ['rows' => 5, 'placeholder' => 'notificationtype.label.template'],
                'label' => 'notificationtype.label.template',
                'help' => 'notificationtype.help.template'
            ])
            ->add('active', null, [
                'label' => 'notificationtype.label.active',
                'help' => 'notificationtype.help.active'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => NotificationType::class,
            'translation_domain' => Data::DOMAIN
        ]);
    }
}