<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Form;

use Allmega\BlogBundle\Data;
use Allmega\AuthBundle\Entity\User;
use Allmega\BlogBundle\Entity\EmailAlias;
use Symfony\Component\Form\{AbstractType, FormBuilderInterface};
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;

class EmailAliasType extends AbstractType
{
    public function __construct(private readonly TranslatorInterface $translator) {}
    
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('address', EmailType::class, [
                'attr' => ['autofocus' => true, 'placeholder' => 'emailalias.label.address'],
                'label' => 'emailalias.label.address'
            ])
            ->add('description', null, [
                'attr' => ['rows' => 5, 'placeholder' => 'emailalias.label.description'],
                'label' => 'emailalias.label.description'
            ])
            ->add('users', EntityType::class, [
                'label' => 'emailalias.label.users',
                'help' => 'emailalias.help.users',
                'help_translation_parameters' => [
                    '%multiplemark%' => $this->translator->trans('action.multiplemark', [], Data::DOMAIN)
				],
                'help_html' => true,
                'multiple' => true,
                'required' => false,
                'class' => User::class,
                'choice_label' => 'fullname',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->where('u.enabled=1')
						->andWhere('u.selectable=1')
                        ->orderBy('u.lastname', 'ASC');
                }
            ])
            ->add('active', null, [
                'label' => 'emailalias.label.active',
                'help' => 'emailalias.help.active'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => EmailAlias::class,
            'translation_domain' => Data::DOMAIN
        ]);
    }
}