<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Form;

use Allmega\BlogBundle\Data;
use Allmega\BlogBundle\Entity\Post;
use Allmega\AuthBundle\Model\UsersTrait;
use Allmega\BlogBundle\Model\MenuPointsTrait;
use Allmega\AuthBundle\Repository\UserRepository;
use Allmega\AuthBundle\Utils\Params\UsersTraitParams;
use Allmega\BlogBundle\Repository\MenuPointRepository;
use Allmega\BlogBundle\Utils\Params\MenuPointsTraitParams;
use Symfony\Component\Form\{AbstractType, FormBuilderInterface};
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class PostType extends AbstractType
{
    use MenuPointsTrait;
    use UsersTrait;

    public function __construct(
        private readonly MenuPointsTraitParams $menuPointsParams,
        private readonly MenuPointRepository $menuPointRepo,
        private readonly UsersTraitParams $usersParams,
        private readonly UserRepository $userRepo) {}

	public function buildForm(FormBuilderInterface $builder, array $options): void
	{
        extract($this->createOptions());
		$builder->add('menuPoints', EntityType::class, $mpOptions);
        ArticleType::addFields($builder);
        $builder->add('authors', EntityType::class, $usersOptions);
	}

	public function configureOptions(OptionsResolver $resolver): void
	{
		$resolver->setDefaults([
			'data_class' => Post::class,
			'translation_domain' => Data::DOMAIN
		]);
	}

    private function createOptions(): array
    {
        $this->menuPointsParams
            ->setLabel('article.label.menupoints')
            ->setHelp('article.help.menupoints')
            ->setEntity(Post::class)
            ->setReference(false)
            ->setMultiple(true);

        $mpOptions = $this->getMenuPointsOptions();

        $this->usersParams
            ->setRoles([Data::POST_AUTHOR_ROLE])
            ->setLabel('post.label.users')
            ->setHelp('post.help.users')
            ->setMultiple(true);

        $usersOptions = $this->getUsersOptions();

        return [
            'usersOptions' => $usersOptions,
            'mpOptions' => $mpOptions,
        ];
    }
}