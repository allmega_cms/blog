<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Form;

use Allmega\BlogBundle\Data;
use Allmega\BlogBundle\Entity\Page;
use Allmega\BlogBundle\Model\MenuPointsTrait;
use Allmega\BlogBundle\Repository\MenuPointRepository;
use Allmega\BlogBundle\Utils\Params\MenuPointsTraitParams;
use Allmega\MediaBundle\Form\FileLoadType;
use Allmega\BlogBundle\Form\Type\{DatePickerType, TagsInputType};
use Symfony\Component\Form\{AbstractType, FormBuilderInterface};
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class PageType extends AbstractType
{
    use MenuPointsTrait;

	public function __construct(
        private readonly MenuPointsTraitParams $menuPointsParams,
        private readonly MenuPointRepository $menuPointRepo) {}

	public function buildForm(FormBuilderInterface $builder, array $options): void
	{
        extract($this->createOptions());
		$builder->add('menuPoints', EntityType::class, $mpOptions);
		ArticleType::addFields($builder);
	}

	public function configureOptions(OptionsResolver $resolver): void
	{
		$resolver->setDefaults([
			'data_class' => Page::class,
			'translation_domain' => Data::DOMAIN
		]);
	}

    private function createOptions(): array
    {
        $this->menuPointsParams
            ->setLabel('article.label.menupoints')
            ->setHelp('article.help.menupoints')
            ->setEntity(Page::class)
            ->setReference(false)
            ->setMultiple(true);

        $mpOptions = $this->getMenuPointsOptions();
        return ['mpOptions' => $mpOptions];
    }
}