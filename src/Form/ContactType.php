<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Form;

use Allmega\BlogBundle\Data;
use Allmega\BlogBundle\Utils\Helper;
use Allmega\BlogBundle\Entity\Contact;
use Symfony\Component\Form\Extension\Core\Type\{CheckboxType, EmailType, TextareaType};
use Symfony\Component\Form\{AbstractType, FormBuilderInterface};
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Routing\RouterInterface;

class ContactType extends AbstractType
{
    public function __construct(
        private readonly RouterInterface $router,
        private readonly string $termsPageSlug) {}

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $url = $this->router->generate('allmega_blog_page_show', ['slug' => $this->termsPageSlug]);

        $builder
            ->add('sender', null, [
                'label' => 'label.sender',
                'attr' => ['placeholder' => 'label.sender']
            ])
            ->add('email', EmailType::class, [
                'label' => 'label.email',
                'attr' => ['placeholder' => 'label.email']
            ])
            ->add('phone', null, [
                'label' => 'label.phone',
                'attr' => ['placeholder' => 'label.phone']
            ])
            ->add('subject', null, [
                'label' => 'label.subject',
                'attr' => ['placeholder' => 'label.subject']
            ])
            ->add('message', TextareaType::class, [
                'attr' => ['rows' => 15, 'placeholder' => 'label.message'],
                'label' => 'label.message'
            ])
            ->add('agreeTerms', CheckboxType::class, Helper::getTermsInputOptions($url));
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Contact::class,
            'translation_domain' => Data::DOMAIN
        ]);
    }
}