<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Form;

use Allmega\MediaBundle\Form\FileLoadType;
use Allmega\BlogBundle\Form\Type\{TagsInputType, DatePickerType};
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;

class ArticleType
{
    public static function addFields(FormBuilderInterface &$builder): void
    {
        $builder
            ->add('title', null, [
                'attr' => ['autofocus' => true, 'placeholder' => 'article.label.title'],
                'label' => 'article.label.title'
            ])
            ->add('keywords', null, [
                'attr' => ['placeholder' => 'article.label.keywords'],
                'label' => 'article.label.keywords'
            ])
            ->add('summary', TextareaType::class, [
                'attr' => ['rows' => 5, 'placeholder' => 'article.label.summary'],
                'label' => 'article.label.summary'
            ])
            ->add('listimage', null, [
                'attr' => ['placeholder' => 'article.label.listimage'],
                'label' => 'article.label.listimage',
                'help' => 'article.help.listimage',
                'required' => false
            ])
            ->add('headimage', null, [
                'attr' => ['placeholder' => 'article.label.headimage'],
                'label' => 'article.label.headimage',
                'help' => 'article.help.headimage',
                'required' => false
            ])
            ->add('headshow', null, [
                'label' => 'article.label.headshow',
                'help' => 'article.help.headshow'
            ])
            ->add('carousel', null, [
                'label' => 'article.label.carousel',
                'help' => 'article.help.carousel'
            ])
            ->add('content', TextareaType::class, [
                'attr' => ['rows' => 20, 'placeholder' => 'article.label.content'],
                'label' => 'article.label.content'
            ])
            ->add('markdown', null, ['label' => 'label.markdown'])
            ->add('published', DatePickerType::class, [
                'label' => 'article.label.published',
                'help' => 'article.help.published'
            ])
            ->add('valid', DatePickerType::class, [
                'label' => 'article.label.valid',
                'help' => 'article.help.valid'
            ])
            ->add('tags', TagsInputType::class, [
                'label' => 'article.label.tags',
                'help' => 'article.help.tags',
                'required' => false
            ])
            ->add('files', FileLoadType::class, [
                'attr' => ['multiple' => true],
                'required' => false,
                'mapped' => false,
                'label' => false
            ])
            ->add('active', null, [
                'label' => 'article.label.active',
                'help' => 'article.help.active'
            ]);
    }
}