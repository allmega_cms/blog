<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Form;

use Allmega\BlogBundle\Data;
use Allmega\AuthBundle\Entity\Group;
use Allmega\AuthBundle\Data as AuthData;
use Allmega\BlogBundle\Entity\{CategoryType, MenuPoint, MenuPointType as PointType};
use Symfony\Component\Form\{AbstractType, FormBuilderInterface};
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\SecurityBundle\Security;
use Doctrine\ORM\EntityRepository;

class MenuPointType extends AbstractType
{
    public function __construct(
        private readonly TranslatorInterface $translator,
        private readonly Security $security) {}

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $multiplemark = $this->translator->trans('action.multiplemark', [], Data::DOMAIN);
        $menuPoint = $builder->getData();

        $categoriesTypesOptions = $this->getCategoriesTypesOptions($multiplemark);
        $groupsOptions = $this->getGroupsOptions($multiplemark);
        $parentOptions = $this->getParentOptions($menuPoint);
        $activeOptions = $this->getCustomOptions();
        $routeOptions = $this->getRouteOptions();
        $typeOptions = $this->getTypeOptions();

        if ($menuPoint->isSys()) {
            $categoriesTypesOptions['disabled'] = true;
            $groupsOptions['disabled'] = true;
            //$parentOptions['disabled'] = true;
            $routeOptions['disabled'] = true;
            $typeOptions['disabled'] = true;
            $activeOptions['disabled']= true;
        } elseif ($menuPoint->getType() !== null) {
            $typeOptions['disabled'] = true;
        }

        $builder
            ->add('groups', EntityType::class, $groupsOptions)
            ->add('parent', EntityType::class, $parentOptions)
            ->add('type', EntityType::class, $typeOptions)
            ->add('categoriesTypes', EntityType::class, $categoriesTypesOptions)
            ->add('route', null, $routeOptions)
            ->add('name', null, [
                'attr' => ['placeholder' => 'menupoint.label.name'],
                'label' => 'menupoint.label.name',
                'help' => 'menupoint.help.name'
            ])
            ->add('title', null, [
                'attr' => ['placeholder' => 'menupoint.label.title'],
                'label' => 'menupoint.label.title',
                'help' => 'menupoint.help.title'
            ])
            ->add('description', null, [
                'attr' => ['placeholder' => 'menupoint.label.description'],
                'label' => 'menupoint.label.description',
                'help' => 'menupoint.help.description'
            ])
            ->add('css', null, [
                'attr' => ['placeholder' => 'menupoint.label.css'],
                'label' => 'menupoint.label.css',
                'help' => 'menupoint.help.css'
            ])
            ->add('prio', null, [
                'attr' => ['placeholder' => 'menupoint.label.prio'],
                'label' => 'menupoint.label.prio',
                'help' => 'menupoint.help.prio'
            ])
            ->add('active', null, $activeOptions);

        if ($this->security->isGranted(AuthData::ADMIN_ROLE)) {
            $sysOptions = $this->getCustomOptions('sysmenu');
            if ($menuPoint->isSys()) $sysOptions['disabled'] = true;
            $builder->add('sys', null, $sysOptions);
        }
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => MenuPoint::class,
            'translation_domain' => Data::DOMAIN
        ]);
    }

    private function getGroupsOptions(string $multiplemark): array
    {
        return [
            'label' => 'menupoint.label.group',
            'help' => 'menupoint.help.groups',
            'help_translation_parameters' => ['%multiplemark%' => $multiplemark],
            'help_html' => true,
            'multiple' => true,
            'required' => false,
            'class' => Group::class,
            'choice_label' => 'name',
            'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('g')
                    ->where('g.selectable = 1')
                    ->orderBy('g.name', 'ASC');
            },
        ];
    }

    private function getParentOptions(MenuPoint $menuPoint): array
    {
        $security = $this->security;
        return [
            'label' => 'menupoint.label.parent',
            'help' => 'menupoint.help.parent',
            'required' => false,
            'class' => MenuPoint::class,
            'choice_label' => 'title',
            'query_builder' => function (EntityRepository $er) use ($menuPoint, $security) {
                $query = $er->createQueryBuilder('m')
                    ->join('m.type', 'p')
                    ->where('m.active = 1')
                    ->andWhere("p.shortname = :shortname")
                    ->setParameter('shortname', Data::MENUPOINT_TYPE_MENU)
                    ->orderBy('m.prio', 'ASC')
                    ->addOrderBy('m.title', 'ASC');

                $mid = $menuPoint->getId();
                if ($mid) {
                    $query
                        ->andWhere('m.id != :id')
                        ->setParameter('id', $mid)
                        ->andWhere('m.sys = :sys')
                        ->setParameter('sys', $menuPoint->isSys());
                }

                if (!$security->isGranted(AuthData::ADMIN_ROLE) || !$mid) $query->andWhere('m.sys = 0');
                return $query;
            },
        ];
    }

    private function getCategoriesTypesOptions(string $multiplemark): array
    {
        return [
            'help_translation_parameters' => ['%multiplemark%' => $multiplemark],
            'label' => 'menupoint.label.category-types',
            'help'  => 'menupoint.help.category-types',
            'help_html' => true,
            'required' => false,
            'multiple' => true,
            'class' => CategoryType::class,
            'choice_label' => 'name',
            'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('c')->orderBy('c.name', 'ASC');
            },
        ];
    }

    private function getRouteOptions(): array
    {
        return [
            'attr' => ['placeholder' => 'menupoint.label.route'],
            'label' => 'menupoint.label.route',
            'help' => 'menupoint.help.route'
        ];
    }

    private function getTypeOptions(): array
    {
        return [
            'label' => 'menupoint.label.type',
            'help' => 'menupoint.help.type',
            'class' => PointType::class,
            'choice_label' => 'name',
            'query_builder' => function (EntityRepository $er) {
                return $er->createQueryBuilder('p')->orderBy('p.name', 'ASC');
            },
        ];
    }

    private function getCustomOptions(string $option = 'active'):array
    {
        return [
            'label' => "menupoint.label.$option",
            'help' => "menupoint.help.$option",
        ];
    }
}