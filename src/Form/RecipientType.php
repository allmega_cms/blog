<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Form;

use Allmega\BlogBundle\Data;
use Allmega\BlogBundle\Entity\{Recipient, NotificationType};
use Symfony\Component\Form\{AbstractType, FormBuilderInterface};
use Symfony\Component\Form\Extension\Core\Type\{TextareaType, EmailType};
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Doctrine\ORM\EntityRepository;

class RecipientType extends AbstractType
{
	public function __construct(private readonly TranslatorInterface $translator) {}

    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $multiplemark = $this->translator->trans('action.multiplemark', [], Data::DOMAIN);
        $builder
            ->add('address', EmailType::class, [
                'attr' => ['autofocus' => true, 'placeholder' => 'recipient.label.address'],
                'label' => 'recipient.label.address'
            ])
            ->add('description', TextareaType::class, [
                'attr' => ['rows' => 5, 'placeholder' => 'recipient.label.description'],
                'label' => 'recipient.label.description'
            ])
            ->add('notificationTypes', EntityType::class, [
                'label' => 'recipient.label.notificationtypes',
                'help' => 'recipient.help.notificationtypes',
                'help_translation_parameters' => [
                    '%multiplemark%' => $multiplemark
				],
                'help_html' => true,
                'multiple' => true,
                'required' => false,
                'class' => NotificationType::class,
                'choice_label' => 'name',
                'query_builder' => function (EntityRepository $er) {
                    return $er->createQueryBuilder('n')
                        ->where('n.active = 1')
                        ->orderBy('n.name', 'ASC');
                }
            ])
            ->add('active', null, [
                'label' => 'recipient.label.active',
                'help' => 'recipient.help.active'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => Recipient::class,
            'translation_domain' => Data::DOMAIN
        ]);
    }
}