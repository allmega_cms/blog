<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle;

use Allmega\BlogBundle\Controller\{CompanyController, ContactController, CommentController, DashboardController, EmailAliasController, FaqController, JobController, MenuPointController, NotificationTypeController, PageController, PostController, RecipientController, TagController};
use Allmega\BlogBundle\Utils\Register\Entries\{ControllerEntry, ControllerEntriesMap, WebpackEntry};
use Allmega\BlogBundle\Entity\{CategoryType, Company, Faq, Item, MenuPointType, Page, Post};
use Allmega\BlogBundle\Utils\Loader\Entries\MenupointEntry;
use Allmega\BlogBundle\Model\PackageData;
use Allmega\BlogBundle\Utils\Helper;

class Data extends PackageData
{
    public const DOMAIN = 'AllmegaBlogBundle';
    public const PACKAGE = 'blog';

    public const NOTIFICATION_JOB = 'blog_job';
    public const NOTIFICATION_CONTACT = 'blog_contact';
    public const NOTIFICATION_COMMENT_NEW = 'blog_comment_new';
    public const NOTIFICATION_COMMENT_ENABLED = 'blog_comment_enabled';

    public const MENUPOINT_BLOG_MODERATE = 'blog.moderate.main';
    public const MENUPOINT_BLOG_MANAGE = 'blog.manage';
    public const MENUPOINT_BLOG_MAIN = 'blog.main';

    public const MENUPOINT_TYPE_CATEGORY = 'blog.category';
    public const MENUPOINT_TYPE_ROUTE = 'blog.route';
    public const MENUPOINT_TYPE_MENU = 'blog.menu';

    public const GROUP_TYPE_BLOG = 'blog.main';

    public const CONTACT_MODERATOR_GROUP = 'blog.contact';
    public const COMMENT_MODERATOR_GROUP = 'blog.comment';
    public const BLOG_MANAGER_GROUP = 'blog.manager';
    public const JOB_MODERATOR_GROUP = 'blog.job';
    public const AUTHOR_GROUP = 'blog.author';

    public const CONTACT_MODERATOR_ROLE = 'ROLE_CONTACT_MODERATOR';
    public const COMMENT_MODERATOR_ROLE = 'ROLE_COMMENT_MODERATOR';
    public const JOB_MODERATOR_ROLE = 'ROLE_JOB_MODERATOR';
    public const BLOG_MANAGER_ROLE = 'ROLE_BLOG_MANAGER';
    public const PAGE_AUTHOR_ROLE = 'ROLE_PAGE_AUTHOR';
    public const POST_AUTHOR_ROLE = 'ROLE_POST_AUTHOR';
    public const FAQ_AUTHOR_ROLE = 'ROLE_FAQ_AUTHOR';

    public const CAN_BE_DEACTIVATED = ['blog.impressum', 'blog.contact', 'blog.jobs', 'blog.last_posts'];

	protected function setRegisterData(): void
    {
        $this->package = self::PACKAGE;
        $this->data = [
            'controllerEntriesMap' => $this->getControllerEntriesMap(),
            'controllerEntries' => $this->getControllerEntries(),
            'webpackEntries' => $this->getWebpackEntries(),
            'envEntries' => $this->getEnvEntries(),
        ];
    }

    protected function setLoadData(): void
    {
        $this->package = self::PACKAGE;
        $this->data = [
            'notificationtypes' => $this->getNotificationTypes(),
            'categoriestypes' => $this->getCategoriesTypes(),
            'menupointtypes' => $this->getMenuPointTypes(),
            'menupoints' => $this->getMenuPoints(),
            'grouptypes' => $this->getGroupTypes(),
            'companies' => $this->getCompanies(),
            'groups' => $this->getGroups(),
            'roles' => $this->getRoles(),
            'users' => $this->getUsers(),
            'items' => $this->getItems(),
        ];
    }

    /**
     * @return array<int,ControllerEntriesMap>
     */
    protected function getControllerEntriesMap(): array
    {
        return [
            new ControllerEntriesMap(
                ControllerEntriesMap::DASHBOARD_BOXES,
                'Dashboard box [{default}]',
                'dashboard/inc/_content.html.twig',
                self::PACKAGE),
            new ControllerEntriesMap(
                ControllerEntriesMap::POPUP_BOXES,
                'Side popup boxes [{default}]',
                'inc/_side_popup_boxes.html.twig',
                self::PACKAGE),
            new ControllerEntriesMap(
                ControllerEntriesMap::PROFILE_MENU,
                'Profile menu [{default}]',
                'inc/_icons_menu_points.html.twig',
                self::PACKAGE),
        ];
    }

    /**
     * @return array<int,ControllerEntry>
     */
    protected function getControllerEntries(): array
    {
        return [
            new ControllerEntry(
                ControllerEntriesMap::DASHBOARD_BOXES,
                self::PACKAGE,
                'contact',
                ContactController::class,
                'getDashboardWidget'),
            new ControllerEntry(
                ControllerEntriesMap::DASHBOARD_BOXES,
                self::PACKAGE,
                'comment',
                CommentController::class,
                'getDashboardWidget'),
            new ControllerEntry(
                ControllerEntriesMap::DASHBOARD_BOXES,
                self::PACKAGE,
                'job',
                JobController::class,
                'getDashboardWidget'),
            new ControllerEntry(
                ControllerEntriesMap::DASHBOARD_BOXES,
                self::PACKAGE,
                'post',
                PostController::class,
                'getDashboardWidget'),
            new ControllerEntry(
                ControllerEntriesMap::PROFILE_MENU,
                self::PACKAGE,
                'dashboard',
                DashboardController::class,
                'getMenuIcon'),
        ];
    }

    /**
     * @return array<int,WebpackEntry>
     */
    protected function getWebpackEntries(): array
    {
        $cssPath = Helper::getRelativePackagesAssetsPath(self::PACKAGE, 'css');
        $jsPath = Helper::getRelativePackagesAssetsPath(self::PACKAGE);

        return [
            new WebpackEntry(self::PACKAGE, '{menu_type}-menu-css', $cssPath, 'menu/{menu_type}-menu.css'),
            new WebpackEntry(self::PACKAGE, '{menu_type}-menu-js', $jsPath, 'menu/{menu_type}-menu.js'),
            (new WebpackEntry(self::PACKAGE))->setDefaults('css'),
            (new WebpackEntry(self::PACKAGE))->setDefaults(),
        ];
    }

    protected function getEnvEntriesData(): array
    {
        return [
            ['VALIDATE_MAIL', 'Should the sender email be checked for validity?[{default}]: ', 0],
            ['MAINTENANCE_MODE', 'Please enter the current maintenance mode [{default}]: ', 0],
            ['APP_URL', 'Please enter the app url [{default}]: ', 'https://hostname'],
            ['FILE_MAX_SIZE', 'Please enter the max upload size for mediafiles [{default}]: ', '20m'],
            ['MENU_TYPE', 'Please enter the main menu type (bootstrap5|slide) [{default}]: ', 'bootstrap5'],
            ['TERMS_PAGE_SLUG', 'Please enter slug to terms and conditions page [{default}]: ', 'terms-and-conditions'],
            ['MODAL_TITLE_SIZE', 'Please enter the title size for bootstrap modals [{default}]: ', 3],
            ['MAILER_NOREPLY', 'Please enter the no reply email [{default}]: ', 'noreply@hostname'],
            ['MAILER_SENDER', 'Please enter the sender name for the no reply address [{default}]: ', 'sender_name'],
            ['MAILER_DSN', 'Please enter the mailer dsn [{default}]: ', 'smtp://localhost?verify_peer=0'],
            ['DATABASE_URL', 'Please enter the database dsn [{default}]: ', 'mysql://db_user:db_password@127.0.0.1:3306/db_name'],
        ];
    }

    /**
     * @return array<int,Company>
     */
    protected function getCompanies(): array
    {
        $companies =[
            Company::build('Musterfirma', 'Musterstrasse 23, 12345 Musterstadt', '+40 211 294 96 83'),
        ];
        return [self::PACKAGE => $companies];
    }

    /**
     * @return array<int,Item>
     */
    protected function getItems(): array
    {
        $items = [
            Item::build(
                Post::class,
                PostController::ROUTE_NAME . 'show',
                'post.item.description',
                'post.item.label',
                'slug'),
        ];
        return [self::PACKAGE => $items];
    }

    /**
     * @return array<int,CategoryType>
     */
    protected function getCategoriesTypes(): array
    {
        $categories = [
            CategoryType::build(
                shortname: 'blog.post',
                package: self::PACKAGE,
                template: 'post/inc/_posts.html.twig',
                entityname: Post::class,
                controller: PostController::class),
            CategoryType::build(
                shortname: 'blog.page',
                package: self::PACKAGE,
                template: 'page/inc/_pages.html.twig',
                entityname: Page::class,
                controller: PageController::class),
            CategoryType::build(
                shortname: 'blog.faq',
                package: self::PACKAGE,
                template: 'faq/inc/_faqs.html.twig',
                entityname: Faq::class,
                controller: FaqController::class),
        ];
        return [self::PACKAGE => $categories];
    }

    /**
     * @return array<int,MenupointType>
     */
    protected function getMenuPointTypes(): array
    {
        $shortnames = [
            self::MENUPOINT_TYPE_CATEGORY,
            self::MENUPOINT_TYPE_ROUTE,
            self::MENUPOINT_TYPE_MENU,
        ];

        $types = [];
        foreach ($shortnames as $shortname) $types[] = MenuPointType::build($shortname);
        return [self::PACKAGE => $types];
    }

    /**
     * @return array<int,MenupointEntry>
     */
    protected function getMenuPoints(): array
    {
        $managers = [self::BLOG_MANAGER_GROUP];
        $kGroup = [self::COMMENT_MODERATOR_GROUP];
        $cGroup = [self::CONTACT_MODERATOR_GROUP];
        $jGroup = [self::JOB_MODERATOR_GROUP];
        $authors = [self::AUTHOR_GROUP];

        $moderate = self::MENUPOINT_BLOG_MODERATE;
        $manage = self::MENUPOINT_BLOG_MANAGE;
        $main = self::MENUPOINT_BLOG_MAIN;

        $menuPoints = [
            new MenupointEntry($manage, '', [], $this->menuType, 20, []),
            new MenupointEntry($main, '', [], $this->menuType, 1, [], $manage),
            new MenupointEntry($moderate, '', [], $this->menuType, 2, [], $manage),

            new MenupointEntry('blog.impressum', CompanyController::ROUTE_NAME . 'show', [], $this->routeType, 1,),
            new MenupointEntry('blog.contact', ContactController::ROUTE_NAME . 'add', [], $this->routeType, 2),
            new MenupointEntry('blog.last_posts', PostController::ROUTE_NAME . 'latest', [], $this->routeType, 3),
            new MenupointEntry('blog.jobs', JobController::ROUTE_NAME . 'show', [], $this->routeType, 4),

            new MenupointEntry('blog.impress', CompanyController::ROUTE_NAME . 'edit', [], $this->routeType, 1, $managers, $main),
            new MenupointEntry('blog.points', MenuPointController::ROUTE_NAME . 'index', [], $this->routeType, 2, $authors, $main),
            new MenupointEntry('blog.tags', TagController::ROUTE_NAME . 'index', [], $this->routeType, 3, $authors, $main),
            new MenupointEntry('blog.websites', PageController::ROUTE_NAME . 'index', [], $this->routeType, 4, $authors, $main),
            new MenupointEntry('blog.posts', PostController::ROUTE_NAME . 'index', [], $this->routeType, 5, $authors, $main),
            new MenupointEntry('blog.faqs', FaqController::ROUTE_NAME . 'index', [], $this->routeType, 6, $authors, $main),

            new MenupointEntry('blog.mails', RecipientController::ROUTE_NAME . 'index', [], $this->routeType, 7, $managers, $main),
            new MenupointEntry('blog.mail_types', NotificationTypeController::ROUTE_NAME . 'index', [], $this->routeType, 8, $managers, $main),
            new MenupointEntry('blog.mail_aliases', EmailAliasController::ROUTE_NAME . 'index', [], $this->routeType, 9, $managers, $main),

            new MenupointEntry('blog.moderate.comments', CommentController::ROUTE_NAME . 'index', [], $this->routeType, 1, $kGroup, $moderate),
            new MenupointEntry('blog.moderate.contacts', ContactController::ROUTE_NAME . 'index', [], $this->routeType, 2, $cGroup, $moderate),
            new MenupointEntry('blog.moderate.jobs', JobController::ROUTE_NAME . 'index', [], $this->routeType, 3, $jGroup, $moderate),
        ];
        return [self::PACKAGE => $this->setSysAndActive($menuPoints)];
    }

    protected function getAuthData(): array
    {
        return [
            self::GROUP_TYPE_BLOG => [
                self::BLOG_MANAGER_GROUP => $this->getManagerRoles(),
                self::CONTACT_MODERATOR_GROUP => [
                    self::CONTACT_MODERATOR_ROLE => 'blog.contact',
                ],
                self::COMMENT_MODERATOR_GROUP => [
                    self::COMMENT_MODERATOR_ROLE => 'blog.comment'
                ],
                self::JOB_MODERATOR_GROUP => [
                    self::JOB_MODERATOR_ROLE => 'blog.job'
                ],
                self::AUTHOR_GROUP => [
                    self::PAGE_AUTHOR_ROLE => 'blog.author.page',
                    self::POST_AUTHOR_ROLE => 'blog.author.post',
                    self::FAQ_AUTHOR_ROLE => 'blog.author.faq'
                ],
            ]
        ];
    }

    protected function getNotificationTypesData(): array
    {
        return [
            self::NOTIFICATION_JOB,
            self::NOTIFICATION_CONTACT,
            self::NOTIFICATION_COMMENT_NEW,
            self::NOTIFICATION_COMMENT_ENABLED,
        ];
    }

    protected function getManagerRoles(): array
    {
        return [
            self::BLOG_MANAGER_ROLE => 'blog.manager',
            self::COMMENT_MODERATOR_ROLE => 'blog.comment',
            self::CONTACT_MODERATOR_ROLE => 'blog.contact',
            self::JOB_MODERATOR_ROLE => 'blog.job',
            self::PAGE_AUTHOR_ROLE => 'blog.author.page',
            self::POST_AUTHOR_ROLE => 'blog.author.post',
            self::FAQ_AUTHOR_ROLE => 'blog.author.faq',
        ];
    }

    public static function getAuthorRoles(): array
    {
        return [self::PAGE_AUTHOR_ROLE, self::POST_AUTHOR_ROLE, self::FAQ_AUTHOR_ROLE];
    }
}