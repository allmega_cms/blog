<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Entity;

use Allmega\BlogBundle\Data;
use Allmega\BlogBundle\Controller\PostController;
use Allmega\BlogBundle\Utils\{Helper, IdGenerator};
use Allmega\BlogBundle\Repository\CategoryTypeRepository;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\{ArrayCollection, Collection};
use Doctrine\ORM\Mapping as ORM;

#[UniqueEntity('shortname', message: 'errors.exists')]
#[ORM\Entity(repositoryClass: CategoryTypeRepository::class)]
#[ORM\Table(name: '`allmega_blog__category_type`')]
class CategoryType
{
    #[ORM\Id]
    #[ORM\Column(length: 191)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: IdGenerator::class)]
    private ?string $id = null;

    #[ORM\Column(length: 191)]
    private ?string $name = null;

    #[ORM\Column(length: 191, unique: true)]
    private ?string $shortname = null;

    #[ORM\Column(length: 191)]
    private ?string $description = null;

    #[ORM\Column(length: 191)]
    private ?string $package = null;

    #[ORM\Column(length: 191)]
    private ?string $entityname = null;

    #[ORM\Column(length: 191)]
    private ?string $controller = null;

    #[ORM\Column(length: 191)]
    private ?string $template = null;

    #[ORM\ManyToMany(targetEntity: MenuPoint::class, mappedBy: 'categoriesTypes')]
    #[ORM\OrderBy(['slug' => 'ASC'])]
    private Collection $menuPoints;

    /**
     * Create a new CategoryType entity with predetermined data,  
     * if no data is provided, it will be generated:
     *  - $description, $shortname as dummy text
     *  - $entityname may be Post::class name
     */
    public static function build(
        string $name = null,
        string $shortname = null,
        string $description = null,
        string $package = null,
        string $template = null,
        string $entityname = null,
        string $controller = null): static
    {
        $name = $name ?? Helper::generateRandomString();
        $shortname = $shortname ?? Helper::generateRandomString();
        $description = $description ?? Helper::generateRandomString(30);
        $controller = $controller ?? PostController::class;
        $template = $template ?? 'list/_posts.html.twig';
        $entityname = $entityname ?? Post::class;
        $package = $package ?? Data::PACKAGE;

        return (new static())
            ->setDescription($description)
            ->setEntityname($entityname)
            ->setController($controller)
            ->setShortname($shortname)
            ->setTemplate($template)
            ->setPackage($package)
            ->setName($name);
    }

    public function __construct()
    {
        $this->menuPoints = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;
        return $this;
    }

    public function getShortname(): ?string
    {
        return $this->shortname;
    }

    public function setShortname(string $shortname): static
    {
        $this->shortname = $shortname;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): static
    {
        $this->description = $description;
        return $this;
    }

    public function getPackage(): ?string
    {
        return $this->package;
    }

    public function setPackage(string $package): static
    {
        $this->package = $package;
        return $this;
    }

    public function getEntityname(): ?string
    {
        return $this->entityname;
    }

    public function setEntityname(string $entityname): static
    {
        $this->entityname = $entityname;
        return $this;
    }

    public function getController(): ?string
    {
        return $this->controller;
    }

    public function setController(string $controller): static
    {
        $this->controller = $controller;
        return $this;
    }

    public function getTemplate(): ?string
    {
        return $this->template;
    }

    public function setTemplate(string $template): static
    {
        $this->template = $template;
        return $this;
    }

    /**
     * @return Collection<int,MenuPoint>
     */
    public function getMenuPoints(): Collection
    {
        return $this->menuPoints;
    }

    public function addMenuPoint(MenuPoint $menuPoint): static
    {
        if (!$this->menuPoints->contains($menuPoint)) {
            $this->menuPoints[] = $menuPoint;
            $menuPoint->addCategoryType($this);
        }
        return $this;
    }

    public function removeMenuPoint(MenuPoint $menuPoint): static
    {
        if ($this->menuPoints->contains($menuPoint)) {
            $this->menuPoints->removeElement($menuPoint);
            $menuPoint->removeCategoryType($this);
        }
        return $this;
    }
}