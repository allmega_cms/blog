<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Entity;

use Allmega\BlogBundle\Utils\Helper;
use Allmega\BlogBundle\Model\Article;
use Allmega\BlogBundle\Repository\PageRepository;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\String\Slugger\AsciiSlugger;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PageRepository::class)]
#[ORM\Table(name: '`allmega_blog__page`')]
class Page extends Article
{
    #[ORM\ManyToMany(targetEntity: MenuPoint::class)]
    #[ORM\JoinTable(name: '`allmega_blog__menupoint_page`')]
    #[ORM\OrderBy(['slug' => 'ASC'])]
    protected Collection $menuPoints;

    #[ORM\ManyToMany(targetEntity: Tag::class, cascade: ['persist'])]
    #[ORM\JoinTable(name: '`allmega_blog__page_tag`')]
    #[ORM\OrderBy(['name' => 'ASC'])]
    #[Assert\Count(max: 4, maxMessage: 'page.too_many_tags')]
    protected Collection $tags;
  
    /**
     * Create a new Page entity with predetermined data,  
     * if no data is provided, it will be generated:
     *  - $summary, title, $content as dummy text
     */
    public static function build(
        string $summary = null,
        string $title = null,
        string $content = null): static
    {
        $content = $content ?? Helper::getLoremIpsum();
        $title = $title ?? Helper::generateRandomString();
        $summary = $summary ?? substr($content, 0, 300);
        $slug = (new AsciiSlugger())->slug($title);

        return (new static())
            ->setSummary($summary)
            ->setContent($content)
            ->setTitle($title)
            ->setSlug($slug);
    }

    public function setMediaDir(): static
    {
        $this->mediaDir = 'pages_' . $this->id;
        return $this;
    }
}