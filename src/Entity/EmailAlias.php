<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Entity;

use Allmega\AuthBundle\Entity\User;
use Allmega\BlogBundle\Utils\{Helper, IdGenerator};
use Allmega\BlogBundle\Repository\EmailAliasRepository;
use Allmega\BlogBundle\Model\{ItemInfo, SortableItemInterface};
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\{ArrayCollection, Collection};
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\DBAL\Types\Types;

#[UniqueEntity('address', message: 'errors.exists')]
#[ORM\Entity(repositoryClass: EmailAliasRepository::class)]
#[ORM\Table(name: '`allmega_blog__email_alias`')]
class EmailAlias extends ItemInfo implements SortableItemInterface
{
    #[ORM\Id]
    #[ORM\Column(length: 191)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: IdGenerator::class)]
    private ?string $id = null;

    #[ORM\Column(length: 191, unique: true)]
    #[Assert\NotBlank(message: 'errors.blank')]
    #[Assert\Length(max: 191, maxMessage: 'errors.max_value')]
    private ?string $address = null;

    #[ORM\Column(length: 191, nullable: true)]
    #[Assert\Length(max: 191, maxMessage: 'errors.max_value')]
    private ?string $description = null;

    #[ORM\ManyToMany(targetEntity: User::class)]
    #[ORM\JoinTable(name: '`allmega_blog__email_alias_user`')]
    #[ORM\OrderBy(['lastname' => 'ASC'])]
    private Collection $users;

    #[ORM\Column(type: Types::BOOLEAN)]
    private bool $active = false;
  
    /**
     * Create a new EmailAlias entity with predetermined data,  
     * if no data is provided, it will be generated:
     *  - $address, $description as dummy text
     *  - $user will be created
     */
    public static function build(
        string $address = null,
        string $description = null,
        ?User $user = null,
        array $users = []): static
    {
        $description = $description ?? Helper::generateRandomString();
        $address = $address ?? Helper::generateRandomString();
        $user = $user ?? User::build();

        $emailalias = (new static())
            ->setDescription($description)
            ->setAddress($address)
            ->setCreator($user)
            ->setEditor($user);

        foreach ($users as $user) $emailalias->addUser($user);
        return $emailalias;
    }

    public function __construct()
    {
        parent::__construct();
        $this->users = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): static
    {
        $this->address = $address;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): static
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return Collection<int,User>
     */
    public function getUsers(): Collection
    {
        return $this->users;
    }

    public function addUser(UserInterface $user): static
    {
        if (!$this->users->contains($user)) {
            $this->users[] = $user;
        }
        return $this;
    }

    public function removeUser(UserInterface $user): static
    {
        if ($this->users->contains($user)) {
            $this->users->removeElement($user);
        }
        return $this;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): static
    {
        $this->active = $active;
        return $this;
    }

    public function setMediaDir(): void {}

    public static function getSortableProps(): array
    {
        return ['address', 'created', 'active'];
    }

    public static function getBundleName(): string
    {
        return 'Blog';
    }
}