<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Entity;

use Allmega\BlogBundle\Controller\PostController;
use Allmega\BlogBundle\Repository\ItemRepository;
use Allmega\BlogBundle\Utils\{Helper, IdGenerator};
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\DBAL\Types\Types;

#[UniqueEntity('entityname', message: 'errors.exists')]
#[ORM\Entity(repositoryClass: ItemRepository::class)]
#[ORM\Table(name: '`allmega_blog__comment_item`')]
class Item
{
    #[ORM\Id]
    #[ORM\Column(length: 191)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: IdGenerator::class)]
    private ?string $id = null;

    #[ORM\Column(length: 191, unique: true)]
    private ?string $entityname = null;

    #[ORM\Column(length: 191)]
    private ?string $route = null;

    #[ORM\Column(length: 191)]
    private ?string $prop = null;

    #[ORM\Column(length: 191, nullable: true)]
    private ?string $description = null;

    #[ORM\Column(length: 191)]
    private ?string $label = null;

    #[ORM\Column(type: Types::BOOLEAN)]
    private bool $examine = false;
    
    /**
     * Create a new Item entity with predetermined data,  
     * if no data is provided, it will be generated:
     *  - $entityname may be a Post::class name
     *  - $description, $label as dummy text
     */
    public static function build(
        string $entityname = null,
        string $route = null,
        string $description = null,
        string $label = null,
        string $prop = 'id',
        bool $examine = false): static
    {
        $description = $description ?? Helper::generateRandomString(30);
        $route = $route ?? PostController::ROUTE_NAME . 'show';
        $label = $label ?? Helper::generateRandomString();
        $entityname = $entityname ?? Post::class;

        return (new static())
            ->setDescription($description)
            ->setEntityname($entityname)
            ->setExamine($examine)
            ->setRoute($route)
            ->setLabel($label)
            ->setProp($prop);
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getEntityname(): ?string
    {
        return $this->entityname;
    }

    public function setEntityname(?string $entityname): static
    {
        $this->entityname = $entityname;
        return $this;
    }

    public function getRoute(): ?string
    {
        return $this->route;
    }

    public function setRoute(?string $route): static
    {
        $this->route = $route;
        return $this;
    }

    public function getProp(): ?string
    {
        return $this->prop;
    }

    public function setProp(?string $prop): static
    {
        $this->prop = $prop;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): static
    {
        $this->description = $description;
        return $this;
    }

    public function getLabel(): ?string
    {
        return $this->label;
    }

    public function setLabel(?string $label): static
    {
        $this->label = $label;
        return $this;
    }

    public function getExamine(): bool
    {
        return $this->examine;
    }

    public function setExamine(bool $examine): static
    {
        $this->examine = $examine;
        return $this;
    }
}