<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Entity;

use Allmega\AuthBundle\Entity\User;
use Allmega\BlogBundle\Utils\{Helper, IdGenerator};
use Allmega\BlogBundle\Repository\CommentRepository;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\DBAL\Types\Types;
use DateTimeInterface;
use DateTime;

#[ORM\Entity(repositoryClass: CommentRepository::class)]
#[ORM\Table(name: '`allmega_blog__comment`')]
class Comment
{
    #[ORM\Id]
    #[ORM\Column(length: 191)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: IdGenerator::class)]
    private ?string $id = null;

    #[ORM\ManyToOne]
    private ?Comment $parent;

    #[ORM\Column(type: Types::TEXT)]
    #[Assert\NotBlank(message: 'errors.blank')]
    #[Assert\Length(min: 10, max: 1000, minMessage: 'errors.min_value', maxMessage: 'errors.max_value')]
    private ?string $content = null;

    #[ORM\Column(length: 191)]
    private ?string $entityid = null;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    private ?User $author;

    #[ORM\ManyToOne]
    #[ORM\JoinColumn(nullable: false)]
    private ?Item $item;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private DateTimeInterface $posted;

    #[ORM\Column(type: Types::BOOLEAN)]
    private bool $active = false;
  
    /**
     * Create a new Comment entity with predetermined data,  
     * if no data is provided, it will be generated:
     *  - $content as dummy text
     *  - $entityid must be selected from DB bevor
     *  - $author, $item (with Post::class name) will be created
     */
    public static function build(
        string $entityid = null,
        string $content = null,
        User $author = null,
        Item $item = null): static
    {
        $content = $content ?? Helper::generateRandomString(50);
        $author = $author ?? User::build();
        $item = $item ?? Item::build();

        return (new static())
            ->setEntityid($entityid)
            ->setContent($content)
            ->setAuthor($author)
            ->setItem($item);
    }

    public function __construct()
    {
        $this->posted = new DateTime();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getParent(): ?self
    {
        return $this->parent;
    }

    public function setParent(?Comment $parent): static
    {
        $this->parent = $parent;
        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): static
    {
        $this->content = $content;
        return $this;
    }

    public function getPosted(): DateTimeInterface
    {
        return $this->posted;
    }

    public function setPosted(DateTimeInterface $posted): static
    {
        $this->posted = $posted;
        return $this;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): static
    {
        $this->active = $active;
        return $this;
    }

    public function getAuthor(): ?User
    {
        return $this->author;
    }

    public function setAuthor(?User $author): static
    {
        $this->author = $author;
        return $this;
    }

    public function getEntityid(): ?string
    {
        return $this->entityid;
    }

    public function setEntityid(string $entityid): static
    {
        $this->entityid = $entityid;
        return $this;
    }

    public function getItem(): ?Item
    {
        return $this->item;
    }

    public function setItem(?Item $item): static
    {
        $this->item = $item;
        return $this;
    }
}