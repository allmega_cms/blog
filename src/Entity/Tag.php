<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Entity;

use Allmega\BlogBundle\Repository\TagRepository;
use Allmega\BlogBundle\Utils\{Helper, IdGenerator};
use Allmega\BlogBundle\Model\SortableItemInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;

#[UniqueEntity('name', message: 'errors.exists')]
#[ORM\Entity(repositoryClass: TagRepository::class)]
#[ORM\Table(name: '`allmega_blog__tag`')]
class Tag implements SortableItemInterface, \JsonSerializable
{
    #[ORM\Id]
    #[ORM\Column(length: 191)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: IdGenerator::class)]
    private ?string $id = null;

    #[ORM\Column(length: 191, unique: true)]
    #[Assert\Length(max: 191, maxMessage: 'errors.max_value')]
    #[Groups(['article:list', 'article:item'])]
    private ?string $name = null;
  
    /**
     * Create a new Tag entity with predetermined data,  
     * if no data is provided, it will be generated:
     *  - $name as dummy text
     */
    public static function build(string $name = null): static
    {
        $name = $name ?? Helper::generateRandomString(5);
        return (new static())->setName($name);
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;
        return $this;
    }

    public static function getBundleName(): string
    {
        return 'Blog';
    }

    public static function getSortableProps(): array
    {
        return ['name'];
    }

    public function jsonSerialize(): array
    {
        return ['name' => $this->name];
    }

    public function __toString(): string
    {
        return $this->name;
    }
}