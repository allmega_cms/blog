<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Entity;

use Allmega\AuthBundle\Entity\User;
use Allmega\BlogBundle\Utils\Helper;
use Allmega\BlogBundle\Model\Article;
use Allmega\BlogBundle\Repository\PostRepository;
use Symfony\Component\String\Slugger\AsciiSlugger;
use Symfony\Component\Serializer\Attribute\Groups;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\{Collection, ArrayCollection};
use ApiPlatform\Metadata\{ApiResource, Get, GetCollection};
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: PostRepository::class)]
#[ORM\Table(name: '`allmega_blog__post`')]
#[ApiResource(
    operations: [
        new Get(normalizationContext: ['groups' => 'article:item']),
        new GetCollection(normalizationContext: ['groups' => 'article:list'])
    ],
    order: ['published' => 'DESC', 'slug' => 'ASC'],
    paginationEnabled: false,
)]
class Post extends Article
{
    #[ORM\ManyToMany(targetEntity: User::class)]
    #[ORM\JoinTable(name: '`allmega_blog__post_user`')]
    #[ORM\OrderBy(['lastname' => 'ASC'])]
    protected Collection $authors;

    #[ORM\ManyToMany(targetEntity: MenuPoint::class)]
    #[ORM\JoinTable(name: '`allmega_blog__menupoint_post`')]
    #[ORM\OrderBy(['slug' => 'ASC'])]
    protected Collection $menuPoints;

    #[ORM\ManyToMany(targetEntity: Tag::class, cascade: ['persist'])]
    #[ORM\JoinTable(name: '`allmega_blog__post_tag`')]
    #[ORM\OrderBy(['name' => 'ASC'])]
    #[Assert\Count(max: 4, maxMessage: 'post.too_many_tags')]
    #[Groups(['article:list', 'article:item'])]
    protected Collection $tags;

    /**
     * Create a new Page entity with predetermined data,  
     * if no data is provided, it will be generated:
     *  - $summary, title, $content as dummy text
     *  - $authors, array with User entities
     */
    public static function build(
        string $summary = null,
        string $title = null,
        string $content = null,
        array $authors = []): static
    {
        $content = $content ?? Helper::getLoremIpsum();
        $summary = $summary ?? substr($content, 0, 300);
        $title = $title ?? Helper::generateRandomString();
        $slug = (new AsciiSlugger())->slug($title);
        if (!$authors) $authors = [User::build()];

        $post = (new static())
            ->setSummary($summary)
            ->setContent($content)
            ->setTitle($title)
            ->setSlug($slug);

        foreach ($authors as $author) $post->addAuthor($author);
        return $post;
    }

    public function __construct()
    {
        parent::__construct();
        $this->authors = new ArrayCollection();
    }

    /**
     * @return Collection<int,User>
     */
    public function getAuthors(): Collection
    {
        return $this->authors;
    }

    public function addAuthor(User $user): static
    {
        if (!$this->authors->contains($user)) $this->authors[] = $user;
        return $this;
    }

    public function removeAuthor(User $user): static
    {
        if ($this->authors->contains($user)) $this->authors->removeElement($user);
        return $this;
    }

    public function setMediaDir(): static
    {
        $this->mediaDir = 'posts_' . $this->id;
        return $this;
    }
}