<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Entity;

use Allmega\BlogBundle\Utils\{Helper, IdGenerator};
use Allmega\BlogBundle\Repository\MenuPointTypeRepository;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\{ArrayCollection, Collection};
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: MenuPointTypeRepository::class)]
#[ORM\Table(name: '`allmega_blog__menupoint_type`')]
class MenuPointType
{
    #[ORM\Id]
    #[ORM\Column(length: 191)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: IdGenerator::class)]
    private ?string $id = null;

    #[ORM\Column(length: 191)]
    #[Assert\Length(max: 191, maxMessage: 'errors.max_value')]
    private ?string $name = null;

    #[ORM\Column(length: 191)]
    #[Assert\Length(max: 191, maxMessage: 'errors.max_value')]
    private ?string $shortname = null;

    #[ORM\OneToMany(targetEntity: MenuPoint::class, mappedBy: 'type', orphanRemoval: true)]
    #[ORM\OrderBy(['title' => 'ASC'])]
    private Collection $menuPoints;
  
    /**
     * Create a new MenuPointType entity with predetermined data,  
     * if no data is provided, it will be generated:
     *  - $name, $shortname as dummy text
     */
    public static function build(string $shortname = null, string $name = null): static
    {
        $shortname = $shortname ?? Helper::generateRandomString();
        $name = $name ?? Helper::generateRandomString();
        return (new static())->setShortname($shortname)->setName($name);
    }

    public function __construct()
    {
        $this->menuPoints = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;
        return $this;
    }

    public function getShortname(): ?string
    {
        return $this->shortname;
    }

    public function setShortname(string $shortname): static
    {
        $this->shortname = $shortname;
        return $this;
    }

    /**
     * @return Collection<<int,MenuPoint>
     */
    public function getMenuPoints(): Collection
    {
        return $this->menuPoints;
    }

    public function addMenuPoint(MenuPoint $menuPoint): static
    {
        if (!$this->menuPoints->contains($menuPoint)) {
            $this->menuPoints[] = $menuPoint;
            $menuPoint->setType($this);
        }
        return $this;
    }

    public function removeMenuPoint(MenuPoint $menuPoint): static
    {
        if ($this->menuPoints->contains($menuPoint)) {
            $this->menuPoints->removeElement($menuPoint);
            if ($menuPoint->getType() === $this) {
                $menuPoint->setType(null);
            }
        }
        return $this;
    }
}