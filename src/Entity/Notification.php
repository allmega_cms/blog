<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

 namespace Allmega\BlogBundle\Entity;

 use Allmega\BlogBundle\Utils\IdGenerator;
 use Allmega\BlogBundle\Model\SortableItemInterface;
 use Allmega\BlogBundle\Repository\NotificationRepository;
 use Symfony\Component\Validator\Constraints as Assert;
 use Symfony\Component\Messenger\Attribute\AsMessage;
 use Doctrine\ORM\Mapping as ORM;
 use Doctrine\DBAL\Types\Types;
 use DateTimeInterface;
 use DateTime;

#[AsMessage]
#[ORM\Entity(repositoryClass: NotificationRepository::class)]
#[ORM\Table(name: '`allmega_blog__notification`')]
class Notification implements SortableItemInterface
{
    #[ORM\Id]
    #[ORM\Column(length: 191)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: IdGenerator::class)]
    private ?string $id = null;

    #[ORM\Column(length: 191)]
    #[Assert\Length(max: 191, maxMessage: 'errors.max_value')]
    private ?string $sender = null;

    #[ORM\Column(type: Types::JSON)]
    private array $receivers = [];

    #[ORM\Column(type: Types::JSON)]
    private array $cc = [];

    #[ORM\Column(type: Types::JSON)]
    private array $bcc = [];

    #[ORM\Column(length: 191)]
    #[Assert\Length(max: 191, maxMessage: 'errors.max_value')]
    private ?string $subject = null;

    #[ORM\Column(length: 191)]
    #[Assert\Length(max: 191, maxMessage: 'errors.max_value')]
    private ?string $template = null;

    #[ORM\Column(type: Types::JSON)]
    private array $params = [];

    #[ORM\Column(type: Types::JSON)]
    private array $attachments = [];

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private DateTimeInterface $created;

    public function __construct()
    {
        $this->created = new DateTime();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getSender(): ?string
    {
        return $this->sender;
    }

    public function setSender(string $sender): static
    {
        $this->sender = $sender;
        return $this;
    }

    public function getReceivers(): array
    {
        return $this->receivers;
    }

    public function setReceivers(array $receivers): static
    {
        $this->receivers = $receivers;
        return $this;
    }

	public function getCc(): array
    {
		return $this->cc;
	}

	public function setCc(array $cc): static
    {
		$this->cc = $cc;
		return $this;
	}

	public function getBcc(): array
    {
		return $this->bcc;
	}

	public function setBcc(array $bcc): static
    {
		$this->bcc = $bcc;
		return $this;
	}

    public function getSubject(): ?string
    {
        return $this->subject;
    }

    public function setSubject(string $subject): static
    {
        $this->subject = $subject;
        return $this;
    }

	public function getTemplate(): ?string
    {
		return $this->template;
	}

	public function setTemplate(string $template): static
    {
		$this->template = $template;
		return $this;
	}

    public function getParams(): array
    {
        return $this->params;
    }

    public function setParams(array $params): static
    {
        $this->params = $params;
        return $this;
    }

	public function getAttachments(): array
    {
		return $this->attachments;
	}

	public function setAttachments(array $attachments): static
    {
		$this->attachments = $attachments;
		return $this;
	}

    public function getCreated(): DateTimeInterface
    {
        return $this->created;
    }

    public function setCreated(DateTimeInterface $created): static
    {
        $this->created = $created;
        return $this;
    }

    public static function getBundleName(): string
    {
        return 'Blog';
    }

    public static function getSortableProps(): array
    {
        return ['created', 'subject'];
    }
}