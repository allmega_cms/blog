<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Entity;

use Allmega\BlogBundle\Utils\Helper;
use Allmega\BlogBundle\Utils\IdGenerator;
use Allmega\BlogBundle\Model\SortableItemInterface;
use Allmega\BlogBundle\Repository\NotificationTypeRepository;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\DBAL\Types\Types;

#[UniqueEntity('shortname', message: 'errors.exists')]
#[ORM\Entity(repositoryClass: NotificationTypeRepository::class)]
#[ORM\Table(name: '`allmega_blog__notificationtype`')]
class NotificationType implements SortableItemInterface
{
    #[ORM\Id]
    #[ORM\Column(length: 191)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: IdGenerator::class)]
    private ?string $id = null;

    #[ORM\Column(length: 191)]
    #[Assert\NotBlank(message: 'errors.blank')]
    #[Assert\Length(max: 191, maxMessage:'errors.max_value')]
    private ?string $package = null;

    #[ORM\Column(length: 191)]
    #[Assert\NotBlank(message: 'errors.blank')]
    #[Assert\Length(max: 191, maxMessage:'errors.max_value')]
    private ?string $name = null;

    #[ORM\Column(length: 191, unique: true)]
    #[Assert\NotBlank(message: 'errors.blank')]
    #[Assert\Length(max: 191, maxMessage:'errors.max_value')]
    private ?string $shortname = null;

    #[ORM\Column(length: 191)]
    #[Assert\NotBlank(message: 'errors.blank')]
    #[Assert\Length(max: 191, maxMessage:'errors.max_value')]
    private ?string $description = null;

    #[ORM\Column(length: 191)]
    #[Assert\NotBlank(message: 'errors.blank')]
    #[Assert\Length(max: 191, maxMessage:'errors.max_value')]
    private ?string $subject = null;

    #[ORM\Column(type: Types::TEXT)]
    #[Assert\NotBlank(message: 'errors.blank')]
    #[Assert\Length(max: 2000, maxMessage:'errors.max_value')]
    private ?string $template = null;

    #[ORM\Column(type: Types::BOOLEAN)]
    private bool $active = false;
    
    /**
     * Create a new NotificationType entity with predetermined data,  
     * if no data is provided, it will be generated:
     *  - $shortname, $description, $template, $subject, $name as dummy text
     */
    public static function build(
        string $shortname = null,
        string $description = null,
        string $template = null,
        string $subject = null,
        string $package = null,
        bool $active = false,
        string $name = null): static
    {
        $description = $description ?? Helper::generateRandomString();
        $shortname = $shortname ?? Helper::generateRandomString();
        $subject = $subject ?? Helper::generateRandomString();
        $package = $package ?? Helper::generateRandomString();
        $template = $template ?? Helper::getLoremIpsum();
        $name = $name ?? Helper::generateRandomString();

        return (new static())
            ->setDescription($description)
            ->setShortname($shortname)
            ->setTemplate($template)
            ->setSubject($subject)
            ->setPackage($package)
            ->setActive($active)
            ->setName($name);
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getPackage(): ?string
    {
        return $this->package;
    }

    public function setPackage(?string $package): static
    {
        $this->package = $package;
        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;
        return $this;
    }

    public function getShortname(): ?string
    {
        return $this->shortname;
    }

    public function setShortname(string $shortname): static
    {
        $this->shortname = $shortname;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): static
    {
        $this->description = $description;
        return $this;
    }

    public function getSubject(): ?string
    {
        return $this->subject;
    }

    public function setSubject(string $subject): static
    {
        $this->subject = $subject;
        return $this;
    }

    public function getTemplate(): ?string
    {
        return $this->template;
    }

    public function setTemplate(string $template): static
    {
        $this->template = $template;
        return $this;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): static
    {
        $this->active = $active;
        return $this;
    }

    public static function getBundleName(): string
    {
        return 'Blog';
    }

    public static function getSortableProps(): array
    {
        return ['package', 'shortname'];
    }
}