<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Entity;

use Allmega\AuthBundle\Entity\User;
use Allmega\BlogBundle\Model\ItemInfo;
use Allmega\BlogBundle\Repository\FaqRepository;
use Allmega\BlogBundle\Utils\{Helper, IdGenerator};
use Allmega\BlogBundle\Model\SortableItemInterface;
use Allmega\BlogBundle\Model\CategoriziableInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\Common\Collections\{ArrayCollection, Collection};
use Doctrine\ORM\Mapping as ORM;
use Doctrine\DBAL\Types\Types;

#[ORM\Entity(repositoryClass: FaqRepository::class)]
#[ORM\Table(name: '`allmega_blog__faq`')]
class Faq extends ItemInfo implements CategoriziableInterface, SortableItemInterface
{
    #[ORM\Id]
    #[ORM\Column(length: 191)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: IdGenerator::class)]
    private ?string $id = null;

    #[ORM\Column(type: Types::TEXT)]
    #[Assert\NotBlank(message: 'errors.blank')]
    #[Assert\Length(max: 1000, maxMessage: 'errors.max_value')]
    private ?string $question = null;

    #[ORM\Column(type: Types::TEXT)]
    #[Assert\NotBlank(message: 'errors.blank')]
    #[Assert\Length(min: 10, minMessage: 'errors.min_value')]
    private ?string $answer = null;

    #[ORM\Column(type: Types::BOOLEAN)]
    private bool $markdown = false;

    #[ORM\Column(type: Types::BOOLEAN)]
    private bool $active = false;

    #[ORM\ManyToMany(targetEntity: MenuPoint::class)]
    #[ORM\JoinTable(name: '`allmega_blog__menupoint_faq`')]
    #[ORM\OrderBy(['slug' => 'ASC'])]
    private Collection $menuPoints;
  
    /**
     * Create a new Faq entity with predetermined data,  
     * if no data is provided, it will be generated:
     *  - $question, $answer as dummy text
     *  - $users will be created
     */
    public static function build(
        string $question = null,
        string $answer = null,
        User $user = null): static
    {
        $question = $question ?? Helper::generateRandomString(30);
        $answer = $answer ?? Helper::generateRandomString(100);
        $user = $user ?? User::build();

        return (new static())
            ->setQuestion($question)
            ->setAnswer($answer)
            ->setCreator($user)
            ->setEditor($user);
    }

    public function __construct()
    {
        parent::__construct();
        $this->menuPoints = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getQuestion(): ?string
    {
        return $this->question;
    }

    public function setQuestion(string $question): static
    {
        $this->question = $question;
        return $this;
    }

    public function getAnswer(): ?string
    {
        return $this->answer;
    }

    public function setAnswer(string $answer): static
    {
        $this->answer = $answer;
        return $this;
    }

    public function isMarkdown(): bool
    {
        return $this->markdown;
    }

    public function setMarkdown(bool $markdown): static
    {
        $this->markdown = $markdown;
        return $this;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): static
    {
        $this->active = $active;
        return $this;
    }

    /**
     * @return Collection<int,MenuPoint>
     */
    public function getMenuPoints(): Collection
    {
        return $this->menuPoints;
    }

    public function addMenuPoint(MenuPoint $menuPoint): static
    {
        if (!$this->menuPoints->contains($menuPoint)) {
            $this->menuPoints[] = $menuPoint;
        }
        return $this;
    }

    public function removeMenuPoint(MenuPoint $menuPoint): static
    {
        if ($this->menuPoints->contains($menuPoint)) {
            $this->menuPoints->removeElement($menuPoint);
        }
        return $this;
    }

    public function setMediaDir(): void {}

    public static function getSortableProps(): array
    {
        return ['question', 'updated', 'active'];
    }

    public static function getBundleName(): string
    {
        return 'Blog';
    }
}