<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Entity;

use Allmega\BlogBundle\Data;
use Allmega\AuthBundle\Entity\Group;
use Allmega\AuthBundle\Model\GroupInterface;
use Allmega\BlogBundle\Utils\{Helper, IdGenerator};
use Allmega\BlogBundle\Repository\MenuPointRepository;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\{Collection, ArrayCollection};
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\String\Slugger\AsciiSlugger;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\DBAL\Types\Types;

#[UniqueEntity('title', message: 'errors.exists')]
#[ORM\Entity(repositoryClass: MenuPointRepository::class)]
#[ORM\Table(name: '`allmega_blog__menupoint`')]
class MenuPoint
{
    #[ORM\Id]
    #[ORM\Column(length: 191)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: IdGenerator::class)]
    private ?string $id = null;

    #[ORM\ManyToOne]
    private ?MenuPoint $parent = null;

    #[ORM\Column(length: 191, nullable: true)]
    #[Assert\Length(max: 191, maxMessage: 'errors.max_value')]
    private ?string $shortname = null;

    #[ORM\Column(length: 191)]
    #[Assert\NotBlank(message: 'errors.blank')]
    #[Assert\Length(max: 191, maxMessage: 'errors.max_value')]
    private ?string $name = null;

    #[ORM\Column(length: 191, unique: true)]
    #[Assert\NotBlank(message: 'errors.blank')]
    #[Assert\Length(max: 191, maxMessage: 'errors.max_value')]
    private ?string $title = null;

    #[ORM\Column(length: 191)]
    private string $slug = '';

    #[ORM\Column(length: 191, nullable: true)]
    #[Assert\Length(max: 191, maxMessage: 'errors.max_value')]
    private ?string $description = null;

    #[ORM\Column(length: 191, nullable: true)]
    private ?string $route = null;

    #[ORM\Column(length: 191, nullable: true)]
    #[Assert\Length(max: 191, maxMessage: 'errors.max_value')]
    private ?string $css = null;

    #[ORM\Column(type: Types::BOOLEAN)]
    private bool $sys = false;

    #[ORM\Column(type: Types::INTEGER)]
    private int $prio = 1;

    #[ORM\Column(type: Types::BOOLEAN)]
    private bool $active = false;
    private bool $selected = false;

    #[ORM\Column(type: Types::BOOLEAN)]
    private bool $start = false;

    #[ORM\ManyToOne(inversedBy: 'menuPoints')]
    #[ORM\JoinColumn(nullable: false)]
    private ?MenuPointType $type = null;

    #[ORM\ManyToMany(targetEntity: Group::class)]
    #[ORM\JoinTable(name: '`allmega_blog__menupoint_group`')]
    #[ORM\OrderBy(['name' => 'ASC'])]
    private Collection $groups;

    #[ORM\ManyToMany(targetEntity: CategoryType::class, inversedBy: 'menuPoints')]
    #[ORM\JoinTable(name: '`allmega_blog__menupoint_category_type`')]
    #[ORM\OrderBy(['name' => 'ASC'])]
    private Collection $categoriesTypes;

    protected int $level = 0;
    
    /**
     * Create a new MenuPoint entity with predetermined data,  
     * if no data is provided, it will be generated:
     *  - $shortname, $title, $name, $route, $description as dummy text
     *  - $type will be created with "route" shortname
     */
    public static function build(
        string $shortname = null,
        string $title = null,
        string $name = null,
        string $route = null,
        string $description = null,
        MenuPoint $parent = null,
        MenuPointType $type = null): static
    {
        $type = $type ?? MenuPointType::build(Data::MENUPOINT_TYPE_ROUTE);
        $description = $description ?? Helper::generateRandomString(50);
        $shortname = $shortname ?? Helper::generateRandomString();
        $title = $title ?? Helper::generateRandomString();
        $route = $route ?? Helper::generateRandomString();
        $name = $name ?? Helper::generateRandomString();
        $slug = (new AsciiSlugger())->slug($title);

        return (new static())
            ->setDescription($description)
            ->setShortname($shortname)
            ->setParent($parent)
            ->setRoute($route)
            ->setTitle($title)
            ->setName($name)
            ->setType($type)
            ->setSlug($slug);
    }

    public function __construct()
    {
        $this->categoriesTypes = new ArrayCollection();
        $this->groups = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getParent(): ?self
    {
        return $this->parent;
    }

    public function setParent(?MenuPoint $parent): static
    {
        $this->parent = $parent;
        return $this;
    }

    public function getShortname(): ?string
    {
        return $this->shortname;
    }

    public function setShortname(?string $shortname): static
    {
        $this->shortname = $shortname;
        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): static
    {
        $this->name = $name;
        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): static
    {
        $this->title = $title;
        return $this;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function setSlug(string $slug): static
    {
        $this->slug = $slug;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): static
    {
        $this->description = $description;
        return $this;
    }

    public function getRoute(): ?string
    {
        return $this->route;
    }

    public function setRoute(?string $route): static
    {
        $this->route = $route;
        return $this;
    }

    public function getCss(): ?string
    {
        return $this->css;
    }

    public function setCss(?string $css): static
    {
        $this->css = $css;
        return $this;
    }

    public function isSys(): bool
    {
        return $this->sys;
    }

    public function setSys(bool $sys): static
    {
        $this->sys = $sys;
        return $this;
    }

    public function getPrio(): int
    {
        return $this->prio;
    }

    public function setPrio(int $prio): static
    {
        $this->prio = $prio;
        return $this;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): static
    {
        $this->active = $active;
        return $this;
    }

    public function getSelected(): bool
    {
        return $this->selected;
    }

    public function setSelected(bool $selected): static
    {
        $this->selected = $selected;
        return $this;
    }

    public function isStart(): bool
    {
        return $this->start;
    }

    public function setStart(bool $start): static
    {
        $this->start = $start;
        return $this;
    }

    /**
     * @return Collection<int,Group>
     */
    public function getGroups(): Collection
    {
        return $this->groups;
    }

    public function addGroup(GroupInterface $group): static
    {
        if (!$this->groups->contains($group)) $this->groups[] = $group;
        return $this;
    }

    public function removeGroup(GroupInterface $group): static
    {
        if ($this->groups->contains($group)) $this->groups->removeElement($group);
        return $this;
    }

    public function getType(): ?MenuPointType
    {
        return $this->type;
    }

    public function setType(?MenuPointType $type): static
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return Collection<int,CategoryType>
     */
    public function getCategoriesTypes(): Collection
    {
        return $this->categoriesTypes;
    }

    public function addCategoryType(CategoryType $categoryType): static
    {
        if (!$this->categoriesTypes->contains($categoryType)) {
            $this->categoriesTypes[] = $categoryType;
        }
        return $this;
    }

    public function removeCategoryType(CategoryType $categoryType): static
    {
        if ($this->categoriesTypes->contains($categoryType)) {
            $this->categoriesTypes->removeElement($categoryType);
        }
        return $this;
    }

    public function getLevel(): int
    {
        return $this->level;
    }

    public function setLevel(int $level): static
    {
        $this->level = $level;
        return $this;
    }

    public function equal(MenuPoint $menuPoint): bool
    {
        return $this->getShortname() === $menuPoint->getShortname();
    }

    public function canBeDeactivated(): bool
    {
        return in_array($this->getShortname(), Data::CAN_BE_DEACTIVATED);
    }
}