<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Entity;

use Allmega\BlogBundle\Utils\{Helper, IdGenerator};
use Allmega\BlogBundle\Repository\RecipientRepository;
use Allmega\BlogBundle\Model\SortableItemInterface;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\Common\Collections\{Collection, ArrayCollection};
use Doctrine\ORM\Mapping as ORM;
use Doctrine\DBAL\Types\Types;

#[UniqueEntity('address', message: 'errors.exists')]
#[ORM\Entity(repositoryClass: RecipientRepository::class)]
#[ORM\Table(name: '`allmega_blog__recipient`')]
class Recipient implements SortableItemInterface
{
    #[ORM\Id]
    #[ORM\Column(length: 191)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: IdGenerator::class)]
    private ?string $id = null;

    #[ORM\Column(length: 191, unique: true)]
    #[Assert\NotBlank(message: 'errors.blank')]
    #[Assert\Length(max: 191, maxMessage: 'errors.max_value')]
    private ?string $address = null;

    #[ORM\Column(type: Types::TEXT)]
    #[Assert\NotBlank(message: 'errors.blank')]
    #[Assert\Length(max: 700, maxMessage: 'errors.max_value')]
    private ?string $description = null;

    #[ORM\ManyToMany(targetEntity: NotificationType::class)]
    #[ORM\JoinTable(name: '`allmega_blog__recipient_notificationtype`')]
    #[ORM\OrderBy(['name' => 'ASC'])]
    private Collection $notificationTypes;

    #[ORM\Column(type: Types::BOOLEAN)]
    private bool $active = false;
  
    /**
     * Create a new Recipient entity with predetermined data,  
     * if no data is provided, it will be generated:
     *  - $description, $address as dummy text
     */
    public static function build(string $address = null, string $description = null): static
    {
        $description = $description ?? Helper::generateRandomString(30);
        $address = $address ?? Helper::generateRandomString(5) . '@home.lan';
        return (new static())->setDescription($description)->setAddress($address);
    }

    public function __construct()
    {
        $this->notificationTypes = new ArrayCollection();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): static
    {
        $this->address = $address;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): static
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return Collection<int,NotificationType>
     */
    public function getNotificationTypes(): Collection
    {
        return $this->notificationTypes;
    }

    public function addNotificationType(NotificationType $notificationType): static
    {
        if (!$this->notificationTypes->contains($notificationType)) {
            $this->notificationTypes[] = $notificationType;
        }
        return $this;
    }

    public function removeNotificationType(NotificationType $notificationType): static
    {
        if ($this->notificationTypes->contains($notificationType)) {
            $this->notificationTypes->removeElement($notificationType);
        }
        return $this;
    }

    public function isActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): static
    {
        $this->active = $active;
        return $this;
    }

    public static function getBundleName(): string
    {
        return 'Blog';
    }

    public static function getSortableProps(): array
    {
        return ['address', 'active'];
    }
}