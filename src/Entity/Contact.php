<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Entity;

use Allmega\BlogBundle\Utils\{Helper, IdGenerator};
use Allmega\BlogBundle\Model\SortableItemInterface;
use Allmega\BlogBundle\Repository\ContactRepository;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\DBAL\Types\Types;
use DateTimeInterface;
use DateTime;

#[ORM\Entity(repositoryClass: ContactRepository::class)]
#[ORM\Table(name: '`allmega_blog__contact`')]
class Contact implements SortableItemInterface
{
    #[ORM\Id]
    #[ORM\Column(length: 191)]
    #[ORM\GeneratedValue(strategy: 'CUSTOM')]
    #[ORM\CustomIdGenerator(class: IdGenerator::class)]
    private ?string $id = null;

    #[ORM\Column(length: 191)]
    #[Assert\NotBlank(message: 'errors.blank')]
    #[Assert\Length(max: 191, maxMessage: 'errors.max_value')]
    private ?string $sender = null;

    #[ORM\Column(length: 191)]
    #[Assert\NotBlank(message: 'errors.blank')]
    #[Assert\Length(max: 191, maxMessage: 'errors.max_value')]
    private ?string $email = null;

    #[ORM\Column(length: 191, nullable: true)]
    #[Assert\Length(max: 191, maxMessage: 'errors.max_value')]
    private ?string $phone = null;

    #[ORM\Column(length: 191)]
    #[Assert\NotBlank(message: 'errors.blank')]
    #[Assert\Length(max: 191, maxMessage: 'errors.max_value')]
    private ?string $subject = null;

    #[ORM\Column(type: Types::TEXT)]
    #[Assert\NotBlank(message: 'errors.blank')]
    #[Assert\Length(min: 10, max: 2000, minMessage: 'errors.min_value', maxMessage: 'errors.max_value')]
    private ?string $message = null;

    #[ORM\Column(type: Types::DATETIME_MUTABLE)]
    private DateTimeInterface $posted;

    #[ORM\Column(type: Types::DATETIME_MUTABLE, nullable: true)]
    private ?DateTimeInterface $readedAt = null;

    #[ORM\Column(type: Types::BOOLEAN)]
    private bool $answered = false;
  
    /**
     * Create a new Contact entity with predetermined data,  
     * if no data is provided, it will be generated:
     *  - $sender, $email, $subject, $message as dummy text
     */
    public static function build(
        string $sender = null,
        string $email = null,
        string $subject = null,
        string $message = null): static
    {
        $email = $email ?? Helper::generateRandomString(5) . '@home.lan';
        $message = $message ?? Helper::generateRandomString(50);
        $subject = $subject ?? Helper::generateRandomString();
        $sender = $sender ?? Helper::generateRandomString();

        return (new static())
            ->setSubject($subject)
            ->setMessage($message)
            ->setSender($sender)
            ->setEmail($email);
    }

    public function __construct()
    {
        $this->posted = new DateTime();
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getSender(): ?string
    {
        return $this->sender;
    }

    public function setSender(string $sender): static
    {
        $this->sender = $sender;
        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): static
    {
        $this->email = $email;
        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(?string $phone): static
    {
        $this->phone = $phone;
        return $this;
    }

    public function getSubject(): ?string
    {
        return $this->subject;
    }

    public function setSubject(string $subject): static
    {
        $this->subject = $subject;
        return $this;
    }

    public function getMessage(): ?string
    {
        return $this->message;
    }

    public function setMessage(string $message): static
    {
        $this->message = $message;
        return $this;
    }

    public function getPosted(): DateTimeInterface
    {
        return $this->posted;
    }

    public function setPosted(DateTimeInterface $posted): static
    {
        $this->posted = $posted;
        return $this;
    }

    public function isAnswered(): bool
    {
        return $this->answered;
    }

    public function setAnswered(bool $answered): static
    {
        $this->answered = $answered;
        return $this;
    }

    public function getReadedAt(): ?DateTimeInterface
    {
        return $this->readedAt;
    }

    public function setReadedAt(?DateTimeInterface $readedAt): static
    {
        $this->readedAt = $readedAt;
        return $this;
    }

    public static function getBundleName(): string
    {
        return 'Blog';
    }

    public static function getSortableProps(): array
    {
        return ['sender', 'email', 'subject', 'answered', 'posted', 'readed'];
    }
}