<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Repository;

use Allmega\BlogBundle\Entity\CategoryType;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

class CategoryTypeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CategoryType::class);
    }
}