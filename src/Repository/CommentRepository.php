<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Repository;

use Allmega\AuthBundle\Entity\User;
use Allmega\BlogBundle\Entity\Comment;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\DBAL\Exception;

class CommentRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Comment::class);
    }

    public function changeChildParentId(Comment $comment): void
    {
        $parentId = $comment->getParent()?->getId();
        $this->createQueryBuilder('c')
            ->update()
            ->set('c.parent', ':parent')
            ->setParameter('parent', $parentId)
            ->where('c.parent = :id')
            ->setParameter('id', $comment->getId())
            ->getQuery()
            ->execute();
    }

    public function findForModeration(bool $count = false, int $limit = 0): int|array
    {
        $query = $this->createQueryBuilder('c');

        if ($count) $query->select('COUNT(c.id)');
        elseif ($limit) $query->setMaxResults($limit);

        $query = $query
            ->where('c.active = 0')
            ->orderBy('c.posted', 'ASC')
            ->getQuery();

        return $count ? $query->getSingleScalarResult() : $query->getResult();
    }

    /**
     * @throws Exception
     */
    public function deleteByIds(array $ids)
    {
        $sql = 'SET FOREIGN_KEY_CHECKS = 0;';
        $this->getEntityManager()->getConnection()->prepare($sql)->executeQuery();
        $this->createQueryBuilder('c')
            ->delete()
            ->where('c.entityid IN (:ids)')
            ->setParameter('ids', $ids)
            ->getQuery()
            ->execute();
    }

    public function countLatest(User $user): int
    {
        return $this->createQueryBuilder('c')
            ->select('COUNT(c.id)')
            ->where('c.active = 0')
            ->andWhere('c.posted BETWEEN :last AND :now')
            ->setParameter('last', $user->getLastSeen()->format('Y-m-d H:i:S'))
            ->setParameter('now', (new \DateTime())->format('Y-m-d H:i:s'))
            ->getQuery()
            ->getSingleScalarResult();
    }
}