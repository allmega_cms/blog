<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Repository;

use Allmega\BlogBundle\Entity\Tag;
use Symfony\Component\EventDispatcher\GenericEvent;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Query\ResultSetMapping;
use Doctrine\ORM\Query;

class TagRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Tag::class);
    }

    public function findAllQuery(): Query
    {
        return $this->createQueryBuilder('a')->orderBy('a.name', 'ASC')->getQuery();
    }

    public function deleteTagRelations(string $table, GenericEvent $event): void
    {
        if ($event->hasArgument('id')) {
            $id = $event->getArgument('id');
            
            $em = $this->getEntityManager();
            $rsm = new ResultSetMapping();
    
            $em->createNativeQuery('SET FOREIGN_KEY_CHECKS = 0', $rsm)->execute();
    
            $sql = "DELETE FROM `$table` WHERE tag_id = :id";
            $em->createNativeQuery($sql, $rsm)->setParameter('id', $id)->execute();
        }
    }
}