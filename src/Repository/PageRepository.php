<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Repository;

use Allmega\BlogBundle\Entity\Page;
use Allmega\BlogBundle\Data as BlogData;
use Allmega\BlogBundle\Model\{CategoriziableTrait, SearchableInterface};
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;

class PageRepository extends ServiceEntityRepository implements SearchableInterface
{
    use CategoriziableTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Page::class);
    }

    public function resetDefaultSite(): void
    {
        $this->createQueryBuilder('p')
            ->update()
            ->set('p.start', 0)
            ->getQuery()
            ->execute();
    }

    public function setExpiredInactiv(): void
    {
        $this->createQueryBuilder('p')
            ->update()
            ->set('p.active', 0)
            ->where('p.valid < :now')
            ->setParameter('now', new \DateTime())
            ->getQuery()
            ->execute();
    }

    public function findAllQuery(): Query
    {
        return $this->createQueryBuilder('a')->orderBy('a.published', 'DESC')->getQuery();
    }

    public function findBySearchQuery(array $terms, array $options, int $limit): array
    {
        if (0 === count($terms)) return [];

        extract($options);
        // [$user]

        $query = $this->createQueryBuilder('p');
        foreach ($terms as $key => $term) {
            $query
                ->orWhere('p.title LIKE :t_'.$key)->setParameter('t_'.$key, '%'.$term.'%')
                ->orWhere('p.summary LIKE :s_'.$key)->setParameter('s_'.$key, '%'.$term.'%')
                ->orWhere('p.content LIKE :c_'.$key)->setParameter('c_'.$key, '%'.$term.'%');
        }

        if (!$user->hasGroup(BlogData::AUTHOR_GROUP)) {
            $query
                ->andWhere('p.published <= :now')
                ->andWhere('p.valid >= :now')
                ->andWhere('p.active = 1')
                ->setParameter('now', new \DateTime());
        }

        return $query
            ->orderBy('p.title', 'ASC')
            ->addOrderBy('p.published', 'DESC')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }
}