<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Repository;

use Allmega\BlogBundle\Data;
use Allmega\AuthBundle\Entity\User;
use Allmega\BlogBundle\Entity\MenuPoint;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\QueryBuilder;
use Doctrine\DBAL\Exception;

class MenuPointRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MenuPoint::class);
    }

    public function resetDefaultSite(): void
    {
        $this->createQueryBuilder('m')
            ->update()
            ->set('m.start', 0)
            ->getQuery()
            ->execute();
    }

    public function setAsSysMenuPoint(array $slugs): void
    {
        $this->createQueryBuilder('m')
            ->update()
            ->set('m.sys', 1)
            ->where('m.slug IN (:slugs)')
            ->setParameter('slugs', $slugs)
            ->getQuery()
            ->execute();
    }

    public function findChilds(string $id): array
    {
        return $this->createQueryBuilder('m')
            ->where('m.parent = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult();
    }

    public function setParentToNull(string $id): void
    {
        $this->createQueryBuilder('m')
            ->update()
            ->set('m.parent', 'NULL')
            ->where('m.parent = :id')
            ->setParameter('id', $id)
            ->getQuery()
            ->execute();
    }

    public function findByUserGroups(User $user): array
    {
        $ids = [];
        foreach ($user->getGroups() as $group) $ids[] = $group->getId();
        return $this->createQueryBuilder('m')
            ->join('m.groups', 'g')
            ->where('g.id IN (:ids)')
            ->setParameter('ids', $ids)
            ->andWhere('m.active=1')
            ->orderBy('m.prio, m.title', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @throws Exception
     */
    public function findWithoutGroups(): array
    {
        $sql = 'SELECT DISTINCT menu_point_id FROM `allmega_blog__menupoint_group`';
        $stmt = $this->getEntityManager()->getConnection()->prepare($sql);
        $rows = $stmt->executeQuery()->fetchAllAssociative();

        $ids = [];
        foreach ($rows as $row) $ids[] = $row['menu_point_id'];

        return $this->createQueryBuilder('m')
            ->where('m.id NOT IN (:ids)')
            ->andWhere('m.active=1')
            ->orderBy('m.prio, m.title', 'ASC')
            ->setParameter('ids', $ids)
            ->getQuery()
            ->getResult();
    }

    public function findByEntityName(string $name): QueryBuilder
    {
        return $this->createQueryBuilder('m')
            ->join('m.type', 't')
            ->join('m.categoriesTypes', 'c')
            ->where('t.shortname = :shortname')
            ->setParameter('shortname', Data::MENUPOINT_TYPE_CATEGORY)
            ->andWhere('c.entityname = :name')
            ->setParameter('name', $name)
            ->andWhere('m.active = 1')
            ->andWhere('m.sys = 0')
            ->orderBy('m.title', 'ASC');
    }
}