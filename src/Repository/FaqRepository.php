<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Repository;

use Allmega\BlogBundle\Entity\Faq;
use Allmega\BlogBundle\Model\{CategoriziableTrait, SearchableInterface};
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;

class FaqRepository extends ServiceEntityRepository implements SearchableInterface
{
    use CategoriziableTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Faq::class);
    }

    public function findAllQuery(): Query
    {
        return $this->createQueryBuilder('a')->orderBy('a.question', 'ASC')->getQuery();
    }

    public function findBySearchQuery(array $terms, array $options, int $limit): array
    {
        if (0 === count($terms)) return [];

        $query = $this->createQueryBuilder('f');

        foreach ($terms as $key => $term) {
            $query
                ->orWhere('f.question LIKE :q_'.$key)->setParameter('q_'.$key, '%'.$term.'%')
                ->orWhere('f.answer LIKE :a_'.$key)->setParameter('a_'.$key, '%'.$term.'%');
        }

        return $query
            ->orderBy('f.created', 'DESC')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }
}