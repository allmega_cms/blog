<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Repository;

use Allmega\AuthBundle\Entity\User;
use Allmega\BlogBundle\Entity\Post;
use Allmega\BlogBundle\Model\{CategoriziableTrait, SearchableInterface};
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;
use DateTime;

class PostRepository extends ServiceEntityRepository implements SearchableInterface
{
    use CategoriziableTrait;

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Post::class);
    }

    public function resetDefaultSite(): void
    {
        $this->createQueryBuilder('p')
                ->update()
                ->set('p.start', 0)
                ->getQuery()
                ->execute();
    }

    public function setExpiredInactiv(): void
    {
        $this->createQueryBuilder('p')
                ->update()
                ->set('p.active', 0)
                ->where('p.valid < :now')
                ->setParameter('now', new DateTime())
                ->getQuery()
                ->execute();
    }

    public function findByAuthorIdsQuery(array $uids, bool $count = false, int $limit = 0): Query
    {
        $query = $this->createQueryBuilder('a');

        if ($count) $query->select('COUNT(a.id)');
        elseif ($limit > 0) $query->setMaxResults($limit);

        return $query
            ->join('a.authors', 'u')
            ->where('u.id IN (:uids)')
            ->orderBy('a.published', 'DESC')
            ->setParameter('uids', $uids)
            ->getQuery();
    }

    public function findByAuthorIds(array $uids, bool $count = false, int $limit = 0): int|array
    {
        $query = $this->findByAuthorIdsQuery($uids, $count, $limit);
        return $count ? $query->getSingleScalarResult() : $query->getResult();
    }

    public function findLatest(bool $result = false): Query|array
    {
        $query = $this->createQueryBuilder('p')
            ->where('p.published <= :now')
            ->andWhere('p.valid >= :now ')
            ->andWhere('p.active = 1')
            ->orderBy('p.published', 'DESC')
            ->setParameter('now', new DateTime())
            ->getQuery();

        return $result ? $query->getResult() : $query;
    }

    public function countLatest(User $user): int
    {
        return $this->createQueryBuilder('p')
            ->select('COUNT(p.id)')
            ->join('p.authors', 'u')
            ->where('u.id = :uid')
            ->andWhere('p.published BETWEEN :last AND :now')
            ->setParameter('now', (new DateTime())->format('Y-m-d H:i:s'))
            ->setParameter('last', $user->getLastSeen()->format('Y-m-d H:i:S'))
            ->setParameter('uid', $user->getId())
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function findBySearchQuery(array $terms, array $options, int $limit): array
    {
        if (0 === count($terms)) return [];

        extract($options);
        // [$user, $admin]

        $query = $this->createQueryBuilder('p');
        foreach ($terms as $key => $term) {
            $query
                ->orWhere('p.title LIKE :t_'.$key)->setParameter('t_'.$key, '%'.$term.'%')
                ->orWhere('p.summary LIKE :s_'.$key)->setParameter('s_'.$key, '%'.$term.'%')
                ->orWhere('p.content LIKE :c_'.$key)->setParameter('c_'.$key, '%'.$term.'%');
        }

        if (!$admin) {
            $query
                ->andWhere('p.published <= :now')
                ->andWhere('p.valid >= :now')
                ->andWhere('p.active = 1')
                ->setParameter('now', new DateTime());
        }

        return $query
            ->orderBy('p.title', 'ASC')
            ->addOrderBy('p.published', 'DESC')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }
}