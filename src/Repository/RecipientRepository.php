<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Repository;

use Allmega\BlogBundle\Entity\Recipient;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

class RecipientRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Recipient::class);
    }

    public function findAllQuery(): Query
    {
        return $this->createQueryBuilder('a')->orderBy('a.address', 'ASC')->getQuery();
    }

    public function findRecipientsByNotificationTypeId(string $id): array
    {
        return $this->createQueryBuilder('r')
            ->join('r.notificationTypes', 'n')
            ->where('n.id = :id')
            ->andWhere('r.active = 1')
            ->orderBy('r.address', 'ASC')
            ->setParameter('id', $id)
            ->getQuery()
            ->getResult();
    }
}