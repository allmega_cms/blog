<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Repository;

use Allmega\AuthBundle\Entity\User;
use Allmega\BlogBundle\Entity\Contact;
use Allmega\BlogBundle\Model\SearchableInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;

class ContactRepository extends ServiceEntityRepository implements SearchableInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Contact::class);
    }

    public function findAllQuery(): Query
    {
        return $this->createQueryBuilder('a')->orderBy('a.posted', 'DESC')->getQuery();
    }

    public function findForModeration(bool $count = false, int $limit = 0): int|array
    {
        $query =$this->createQueryBuilder('c');

        if ($count) $query->select('COUNT(c.id)');
        elseif ($limit > 0) $query->setMaxResults($limit);
        
        $query = $query->orderBy('c.posted', 'DESC')->getQuery();
        return $count ? $query->getSingleScalarResult() : $query->getResult();
    }

    public function countLatest(User $user): int
    {
        return $this->createQueryBuilder('c')
            ->select('COUNT(c.id)')
            ->where('c.readedAt IS NULL')
            ->andWhere('c.posted BETWEEN :last AND :now')
            ->setParameter('last', $user->getLastSeen()->format('Y-m-d H:i:S'))
            ->setParameter('now', (new \DateTime())->format('Y-m-d H:i:s'))
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function findBySearchQuery(array $terms, array $options, int $limit): array
    {
        if (0 === count($terms)) return [];

        $query = $this->createQueryBuilder('c');

        foreach ($terms as $key => $term) {
            $query
                ->orWhere('c.sender LIKE :s_'.$key)->setParameter('s_'.$key, '%'.$term.'%')
                ->orWhere('c.email LIKE :e_'.$key)->setParameter('e_'.$key, '%'.$term.'%')
                ->orWhere('c.phone LIKE :p_'.$key)->setParameter('p_'.$key, '%'.$term.'%')
                ->orWhere('c.subject LIKE :b_'.$key)->setParameter('b_'.$key, '%'.$term.'%')
                ->orWhere('c.message LIKE :m_'.$key)->setParameter('m_'.$key, '%'.$term.'%');
        }

        return $query
            ->orderBy('c.posted', 'DESC')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }
}