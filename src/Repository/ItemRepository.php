<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Repository;

use Allmega\BlogBundle\Entity\Item;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class ItemRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Item::class);
    }

    public function setAllInactive(): void
    {
        $this->createQueryBuilder('i')
                ->update()
                ->set('i.examine', 0)
                ->getQuery()
                ->execute();
    }

    public function setExamine(array $names): void
    {
    	$this->setAllInactive();
        $this->createQueryBuilder('i')
                ->update()
                ->set('i.examine', 1)
                ->where('i.entityname IN (:names)')
                ->setParameter(':names', $names)
                ->getQuery()
                ->execute();
    }
}