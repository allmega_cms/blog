<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Repository;

use Allmega\BlogBundle\Entity\Job;
use Allmega\AuthBundle\Entity\User;
use Allmega\BlogBundle\Model\SearchableInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;

class JobRepository extends ServiceEntityRepository implements SearchableInterface
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Job::class);
    }

    public function findAllQuery(): Query
    {
        return $this->createQueryBuilder('a')->orderBy('a.posted', 'DESC')->getQuery();
    }
    
    public function findForModeration(bool $count = false, int $limit = 0): int|array
    {
        $query =$this->createQueryBuilder('j');

        if ($count) $query->select('COUNT(j.id)');
        elseif ($limit > 0) $query->setMaxResults($limit);
        
        $query = $query->orderBy('j.posted', 'DESC')->getQuery();
        return $count ? $query->getSingleScalarResult() : $query->getResult();
    }

    public function countLatest(User $user): int
    {
        return $this->createQueryBuilder('j')
            ->select('COUNT(j.id)')
            ->where('j.posted BETWEEN :last AND :now')
            ->setParameter('last', $user->getLastSeen()->format('Y-m-d H:i:S'))
            ->setParameter('now', (new \DateTime())->format('Y-m-d H:i:s'))
            ->getQuery()
            ->getSingleScalarResult();
    }

    public function findBySearchQuery(array $terms, array $options, int $limit): array
    {
        if (0 === count($terms)) return [];

        $query = $this->createQueryBuilder('j');

        foreach ($terms as $key => $term) {
            $query
                ->orWhere('j.sender LIKE :s_'.$key)->setParameter('s_'.$key, '%'.$term.'%')
                ->orWhere('j.email LIKE :e_'.$key)->setParameter('e_'.$key, '%'.$term.'%')
                ->orWhere('j.phone LIKE :p_'.$key)->setParameter('p_'.$key, '%'.$term.'%')
                ->orWhere('j.message LIKE :m_'.$key)->setParameter('m_'.$key, '%'.$term.'%');
        }

        return $query
            ->orderBy('j.posted', 'DESC')
            ->setMaxResults($limit)
            ->getQuery()
            ->getResult();
    }
}