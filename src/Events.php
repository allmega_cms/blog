<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle;

final class Events
{
    public const STARTPAGE_CHANGED = 'startpage.changed';
    public const COMPANY_UPDATED = 'company.updated';
    public const TAG_DELETED = 'tag.deleted';
    
    public const JOB_INQUIRY = 'job.created';
    public const JOB_DELETED = 'job.deleted';

    public const CONTACT_INQUIRY = 'contact.sended';
    public const CONTACT_ANSWERED = 'contact.answered';
    public const CONTACT_DELETED = 'contact.deleted';
    public const CONTACT_STATE_CHANGED = 'contact.state_changed';

    public const PAGE_CREATED = 'page.created';
    public const PAGE_UPDATED = 'page.updated';
    public const PAGE_DELETED = 'page.deleted';
    public const PAGE_STATE_CHANGED = 'page.state_changed';

    public const POST_CREATED = 'post.created';
    public const POST_UPDATED = 'post.updated';
    public const POST_DELETED = 'post.deleted';
    public const POST_STATE_CHANGED = 'post.state_changed';

    public const NOTIFICATIONTYPE_CREATED = 'notificationtype.created';
    public const NOTIFICATIONTYPE_UPDATED = 'notificationtype.updated';
    public const NOTIFICATIONTYPE_DELETED = 'notificationtype.deleted';
    public const NOTIFICATIONTYPE_STATE_CHANGED = 'notificationtype.state_changed';

    public const RECIPIENT_CREATED = 'recipient.created';
    public const RECIPIENT_UPDATED = 'recipient.updated';
    public const RECIPIENT_DELETED = 'recipient.deleted';
    public const RECIPIENT_STATE_CHANGED = 'recipient.state_changed';

    public const COMMENT_ENABLED = 'comment.enabled';
    public const COMMENT_CREATED = 'comment.created';
    public const COMMENT_UPDATED = 'comment.updated';
    public const COMMENT_DELETED = 'comment.deleted';
    public const DELETE_COMMENTS = 'comment.all_deleted';
    
    public const MENUPOINT_CREATED = 'menupoint.created';
    public const MENUPOINT_UPDATED = 'menupoint.updated';
    public const MENUPOINT_DELETED = 'menupoint.deleted';
    public const MENUPOINT_DELETE_FAILED = 'menupoint.delete_failed';
    public const MENUPOINT_STATE_CHANGED = 'menupoint.state_changed';
    public const MENUPOINT_STATE_CHANGE_FAILED = 'label.sysmenu_state_error';

    public const FAQ_CREATED = 'faq.created';
    public const FAQ_UPDATED = 'faq.updated';
    public const FAQ_DELETED = 'faq.deleted';
    public const FAQ_STATE_CHANGED = 'faq.state_changed';

    public const EMAILALIAS_CREATED = 'emailalias.created';
    public const EMAILALIAS_UPDATED = 'emailalias.updated';
    public const EMAILALIAS_DELETED = 'emailalias.deleted';
    public const EMAILALIAS_STATE_CHANGED = 'emailalias.state_changed';
}