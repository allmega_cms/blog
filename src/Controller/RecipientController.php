<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Controller;

use Allmega\BlogBundle\{Data, Events};
use Allmega\BlogBundle\Entity\Recipient;
use Allmega\BlogBundle\Repository\RecipientRepository;
use Allmega\BlogBundle\Utils\{Paginator, SortableItem};
use Allmega\BlogBundle\Manager\RecipientControllerTrait;
use Allmega\BlogBundle\Utils\Params\BaseControllerParams;
use Allmega\BlogBundle\Model\Controller\{BaseController, BaseControllerServices};
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\HttpFoundation\Response;

#[Route('/recipient', name: 'allmega_blog_recipient_')]
class RecipientController extends BaseController
{
    use RecipientControllerTrait;

    public const ROUTE_TEMPLATE_PATH = '@AllmegaBlog/recipient/';
    public const ROUTE_NAME = 'allmega_blog_recipient_';
    public const PROP = 'recipient';

    public function __construct(BaseControllerServices $services)
    {
        parent::__construct($services);
    }

    #[Route('/list', name: 'index', methods: 'GET')]
    #[IsGranted('blog-recipient-list')]
    public function index(RecipientRepository $recipientRepo, Paginator $paginator): Response
    {
        $query = $recipientRepo->findAllQuery();
        return $this->render(self::ROUTE_TEMPLATE_PATH . 'index.html.twig', [
            'params' => $this->getTemplateParams($this, Data::DOMAIN),
            'item' => SortableItem::getInstance(new Recipient()),
            'recipients' => $paginator->getPagination($query)
        ]);
    }

    #[Route('/add', name: 'add', methods: ['GET', 'POST'])]
    #[IsGranted('blog-recipient-add')]
    public function add(): Response
    {
        return $this->save();
    }

    #[Route('/edit/{id}', name: 'edit', methods: ['GET', 'POST'])]
    #[IsGranted('blog-recipient-edit', subject: self::PROP)]
    public function edit(Recipient $recipient): Response
    {
        return $this->save($recipient);
    }

    #[Route('/{id}', name: 'delete', methods: 'DELETE')]
    #[IsGranted('blog-recipient-delete', subject: self::PROP)]
    public function delete(Recipient $recipient): Response
    {
        $params = (new BaseControllerParams())->init(
            entity: $recipient,
            domain: Data::DOMAIN,
            eventName: Events::RECIPIENT_DELETED,
            routeName: self::ROUTE_NAME
        );
        return $this->handle($params, $this->delete);
    }

    #[Route('/state/{id}', name: 'state', methods: 'GET')]
    #[IsGranted('blog-recipient-state', subject: self::PROP)]
    public function changeState(Recipient $recipient): Response
    {
        $params = (new BaseControllerParams())->init(
            entity: $recipient,
            domain: Data::DOMAIN,
            eventName: Events::RECIPIENT_STATE_CHANGED,
            routeName: self::ROUTE_NAME
        );
        return $this->handle($params, $this->state);
    }
}