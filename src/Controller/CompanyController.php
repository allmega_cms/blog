<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Controller;

use Allmega\BlogBundle\Data;
use Allmega\BlogBundle\Repository\CompanyRepository;
use Allmega\BlogBundle\Manager\CompanyControllerTrait;
use Allmega\BlogBundle\Model\Controller\{BaseController, BaseControllerServices};
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\HttpFoundation\Response;

#[Route('/impressum', name: 'allmega_blog_company_')]
class CompanyController extends BaseController
{
    use CompanyControllerTrait;

    public const ROUTE_TEMPLATE_PATH = '@AllmegaBlog/company/';
    public const ROUTE_NAME = 'allmega_blog_company_';

    public function __construct(
        private readonly CompanyRepository $companyRepo,
        BaseControllerServices $services)
    {
        parent::__construct($services);
    }

    #[Route('', name: 'show', methods: 'GET')]
    #[IsGranted('blog-company-show')]
    public function show(): Response
    {
        return $this->render(self::ROUTE_TEMPLATE_PATH . 'show.html.twig', [
            'params' => $this->getTemplateParams($this, Data::DOMAIN),
            'company' => $this->companyRepo->findFirst()
        ]);
    }

    #[Route('/edit', name: 'edit', methods: ['GET', 'POST'])]
    #[IsGranted('blog-company-edit')]
    public function edit(): Response
    {
        return $this->save();
    }
}