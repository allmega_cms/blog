<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Controller;

use Allmega\BlogBundle\Entity\Post;
use Allmega\BlogBundle\{Data, Events};
use Allmega\BlogBundle\Utils\Search\SearchItem;
use Allmega\BlogBundle\Security\Voters\PostVoter;
use Allmega\BlogBundle\Repository\PostRepository;
use Allmega\BlogBundle\Manager\PostControllerTrait;
use Allmega\BlogBundle\Utils\{Paginator, SortableItem};
use Allmega\BlogBundle\Model\Controller\{BaseController, BaseControllerServices};
use Allmega\BlogBundle\Utils\Params\BaseControllerParams;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\HttpFoundation\Response;

#[Route('/post', name: 'allmega_blog_post_')]
class PostController extends BaseController
{
    use PostControllerTrait;

    public const ROUTE_TEMPLATE_PATH = '@AllmegaBlog/post/';
    public const ROUTE_NAME = 'allmega_blog_post_';
    public const PROP = 'post';

    public function __construct(
        private readonly PostRepository $postRepo,
        BaseControllerServices          $services)
    {
        parent::__construct($services);
    }

    #[Route('/search', name: 'search', methods: 'GET')]
    #[IsGranted('blog-post-search')]
    public function search(PostRepository $postRepo, SearchItem $item): Response
    {
        $item
            ->setTemplatePath(self::ROUTE_TEMPLATE_PATH)
            ->setRoute(self::ROUTE_NAME . 'show')
            ->setRouteProp('slug')
            ->setProps(['title'])
            ->getRepo()
            ->setSearch($postRepo)
            ->addOptions(['user' => $this->getUser()]);

        $item->getTrans()->setLabel('page.search.title')->setDomain(Data::DOMAIN);       
        return $this->json(['data' => $item->getSearchResult()]);
    }

    #[Route('/list', name: 'index', methods: 'GET')]
    #[IsGranted('blog-post-list')]
    public function index(Paginator $paginator): Response
    {
        $this->postRepo->setExpiredInactiv();
        $query = $this->postRepo->findByAuthorIdsQuery([$this->getUser()->getId()]);

        return $this->render(self::ROUTE_TEMPLATE_PATH . 'list.html.twig', [
            'params' => $this->getTemplateParams($this, Data::DOMAIN, $this->getStartParams()),
            'item' => SortableItem::getInstance(new Post(), 'article'),
            'posts' => $paginator->getPagination($query)
        ]);
    }

    #[Route('/latest', name: 'latest', defaults: ['_format' => 'html'], methods: 'GET')]
    #[Route('/rss.xml', name: 'rss', defaults: ['_format' => 'xml'], methods: 'GET')]
    #[IsGranted('blog-post-latest')]
    public function latest(string $_format, Paginator $paginator, PostRepository $postRepo): Response
    {
        if ($_format != 'html') {
            $posts = $postRepo->findLatest(true);
        } else {
            $query = $postRepo->findLatest();
            $posts = $paginator->getPagination($query);
        }

        return $this->render(self::ROUTE_TEMPLATE_PATH . 'index.'.$_format.'.twig', [
            'params' => $this->getTemplateParams($this, Data::DOMAIN),
            'posts' => $posts
        ]);
    }

    #[Route('/show/{slug}', name: 'show', methods: 'GET')]
    #[IsGranted('blog-post-show', subject: self::PROP)]
    public function show(PostVoter $postVoter, Post $post): Response
    {
        $optParams = [
            'entityname' => Post::class,
            'postVoter' => $postVoter,
        ];
        $optParams = array_merge($this->getStartParams(), $optParams);

        $params = [
            'params' => $this->getTemplateParams($this, Data::DOMAIN, $optParams),
            'post' => $post->setImages($this->getMediaFileHandler()),
        ];

        if ($this->isXmlRequest) {
            $view = $this->renderView(self::ROUTE_TEMPLATE_PATH . 'show/_show.html.twig', $params);
            return $this->json(['content' => $view]);
        }

        return $this->render(self::ROUTE_TEMPLATE_PATH . 'show.html.twig', $params);
    }

    #[Route('/add', name: 'add', methods: ['GET', 'POST'])]
    #[IsGranted('blog-post-add')]
    public function add(): Response
    {
        return $this->save();
    }

    #[Route('/edit/{id}', name: 'edit', methods: ['GET', 'POST'])]
    #[IsGranted('blog-post-edit', subject: self::PROP)]
    public function edit(Post $post): Response
    {
        return $this->save($post);
    }

    #[Route('/{id}', name: 'delete', methods: 'DELETE')]
    #[IsGranted('blog-post-delete', subject: self::PROP)]
    public function delete(Post $post): Response
    {
        $params = (new BaseControllerParams())->init(
            entity: $post,
            domain: Data::DOMAIN,
            eventName: Events::POST_DELETED,
            routeName: self::ROUTE_NAME
        );
        return $this->handle($params, $this->delete);
    }

    #[Route('/state/{id}', name: 'state', methods: 'GET')]
    #[IsGranted('blog-post-state', subject: self::PROP)]
    public function changeState(Post $post): Response
    {
        $params = (new BaseControllerParams())->init(
            entity: $post,
            domain: Data::DOMAIN,
            eventName: Events::POST_STATE_CHANGED,
            routeName: self::ROUTE_NAME
        );
        return $this->handle($params, $this->state);
    }

    #[Route('/dashboard', name: 'dashboard', methods: 'GET')]
    public function getDashboardWidget(): Response
    {
        if (!$this->isGranted('blog-post-dashboard')) return new Response();

        $user = $this->getUser();
        $latestNum = $this->postRepo->countLatest($user);
        
        $uid = $user->getId();
        $posts = $this->postRepo->findByAuthorIds([$uid], false, 3);
        $num = $this->postRepo->findByAuthorIds([$uid], true);

        return $this->render(self::ROUTE_TEMPLATE_PATH . 'dashboard.html.twig', [
            'params' => $this->getTemplateParams($this, Data::DOMAIN),
            'posts' => $posts, 'num' => $num, 'latestNum' => $latestNum,
        ]);
    }
}