<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Controller;

use Allmega\BlogBundle\Entity\Job;
use Allmega\BlogBundle\{Data,Events};
use Allmega\BlogBundle\Utils\Search\SearchItem;
use Allmega\BlogBundle\Repository\JobRepository;
use Allmega\BlogBundle\Manager\JobControllerTrait;
use Allmega\BlogBundle\Utils\{Paginator, Register\Config, SortableItem};
use Allmega\BlogBundle\Utils\Params\BaseControllerParams;
use Allmega\BlogBundle\Model\Controller\{BaseController, BaseControllerServices};
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Filesystem\Filesystem;

#[Route('/job', name: 'allmega_blog_job_')]
class JobController extends BaseController
{
    use JobControllerTrait;

    public const ROUTE_TEMPLATE_PATH = '@AllmegaBlog/job/';
    public const ROUTE_NAME = 'allmega_blog_job_';

    public function __construct(
        private readonly Filesystem $filesystem,
        private readonly JobRepository $jobRepo,
        private readonly string $validateMail,
        private readonly Config $config,
        BaseControllerServices $services)
    {
        parent::__construct($services);
    }

    #[Route('/search', name: 'search', methods: 'GET')]
    #[IsGranted('blog-job-search')]
    public function search(SearchItem $item): Response
    {
        $item
            ->setTemplatePath(self::ROUTE_TEMPLATE_PATH)
            ->setRoute(self::ROUTE_NAME . 'show')
            ->setProps(['sender'])
            ->getRepo()
            ->setSearch($this->jobRepo)
            ->addOptions(['user' => $this->getUser()]);

        $item->getTrans()->setLabel('job.search.title')->setDomain(Data::DOMAIN);       
        return $this->json(['data' => $item->getSearchResult()]);
    }

    #[Route('/list', name: 'index', methods: 'GET')]
    #[IsGranted('blog-job-list')]
    public function index(Paginator $paginator): Response
    {
        $query = $this->jobRepo->findAllQuery();
        return $this->render(self::ROUTE_TEMPLATE_PATH . 'index.html.twig', [
            'params' => $this->getTemplateParams($this, Data::DOMAIN),
            'item' => SortableItem::getInstance(new Job()),
            'jobs' => $paginator->getPagination($query)
        ]);
    }

    #[Route('', name: 'show', methods: 'GET')]
    #[IsGranted('blog-job-show')]
    public function show(): Response
    {
        return $this->render(self::ROUTE_TEMPLATE_PATH . 'show.html.twig', [
            'params' => $this->getTemplateParams($this, Data::DOMAIN)
        ]);
    }

    #[Route('/add', name: 'add', methods: ['GET', 'POST'])]
    #[IsGranted('blog-job-add')]
    public function add(): Response
    {
        return $this->save();
    }

    #[Route('/{id}', name: 'delete', methods: 'DELETE')]
    #[IsGranted('blog-job-delete', subject: 'job')]
    public function delete(Job $job): Response
    {
        $params = (new BaseControllerParams())->init(
            entity: $job,
            domain: Data::DOMAIN,
            eventName: Events::JOB_DELETED,
            routeName: self::ROUTE_NAME
        );
        return $this->handle($params, $this->delete);
    }

    #[Route('/load/{filename}', name: 'load', methods: 'GET')]
    #[IsGranted('blog-job-load')]
    public function load(string $filename): Response
    {
        $filename = $this->config->getDocumentsDir() . $filename;
        return $this->filesystem->exists($filename) ? $this->file($filename) : $this->redirect('/');
    }

    #[Route('/dashboard', name: 'dashboard', methods: 'GET')]
    public function getDashboardWidget(): Response
    {
        if (!$this->isGranted('blog-job-dashboard')) return new Response();

        $latestNum = $this->jobRepo->countLatest($this->getUser());
        $jobs = $this->jobRepo->findForModeration(false, 3);
        $num = $this->jobRepo->findForModeration(true);

        return $this->render(self::ROUTE_TEMPLATE_PATH . 'dashboard.html.twig', [
            'params' => $this->getTemplateParams($this, Data::DOMAIN),
            'jobs' => $jobs, 'latestNum' => $latestNum, 'num' => $num,
        ]);
    }
}