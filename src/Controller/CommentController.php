<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Controller;

use Allmega\BlogBundle\{Data, Events};
use Allmega\BlogBundle\Entity\Comment;
use Allmega\BlogBundle\Security\Voters\CommentVoter;
use Allmega\BlogBundle\Manager\CommentControllerTrait;
use Allmega\BlogBundle\Repository\{CommentRepository, ItemRepository};
use Allmega\BlogBundle\Model\Controller\{BaseController, BaseControllerServices};
use Allmega\BlogBundle\Utils\Params\BaseControllerParams;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\HttpFoundation\Response;

#[Route('/comment', name: 'allmega_blog_comment_')]
class CommentController extends BaseController
{
    use CommentControllerTrait;

    public const ROUTE_TEMPLATE_PATH = '@AllmegaBlog/comment/';
    public const ROUTE_NAME = 'allmega_blog_comment_';
    public const PROP = 'comment';

    public function __construct(
        private readonly CommentRepository $commentRepo,
        private readonly ItemRepository $itemRepo,
        BaseControllerServices $services)
    {
        parent::__construct($services);
    }

    #[Route('/list', name: 'index', methods: 'GET')]
    #[IsGranted('blog-comment-list')]
    public function index(CommentVoter $commentVoter): Response
    {
        $optParams = ['commentVoter' => $commentVoter];
        return $this->render(self::ROUTE_TEMPLATE_PATH . 'index.html.twig', [
            'params' => $this->getTemplateParams($this, Data::DOMAIN, $optParams),
            'comments' => $this->commentRepo->findForModeration(),
            'items' => $this->itemRepo->findAll()
        ]);
    }
    
    #[Route('/add', name: 'add', methods: 'POST')]
    #[IsGranted('blog-comment-add')]
    public function add(): Response
    {
        return $this->addComment();
    }

    #[Route('/edit/{id}/{place}', name: 'edit', requirements: ['place' => 'manage|use'], methods: ['GET', 'POST'])]
    #[IsGranted('blog-comment-edit', subject: self::PROP)]
    public function edit(Comment $comment, string $place = 'use'): Response
    {
        return $this->save($comment, $place);
    }

    #[Route('/delete/{id}', name: 'udelete', methods: ['GET'])]
    #[IsGranted('blog-comment-udelete', subject: self::PROP)]
    public function udelete(Comment $comment): Response
    {
        return $this->deleteComment($comment);
    }

    #[Route('/{id}', name: 'delete', methods: 'DELETE')]
    #[IsGranted('blog-comment-delete', subject: self::PROP)]
    public function delete(Comment $comment): Response
    {
        $params = (new BaseControllerParams())->init(
            entity: $comment,
            domain: Data::DOMAIN,
            eventName: Events::COMMENT_DELETED,
            routeName: self::ROUTE_NAME
        );
        return $this->handle($params, $this->delete);
    }

    #[Route('/enable/{id}', name: 'enable', methods: 'GET')]
    #[IsGranted('blog-comment-state', subject: self::PROP)]
    public function setEnabled(Comment $comment): Response
    {
        $params = (new BaseControllerParams())->init(
            entity: $comment,
            domain: Data::DOMAIN,
            eventName: Events::COMMENT_ENABLED,
            routeName: self::ROUTE_NAME
        );
        return $this->handle($params, $this->state);
    }

    #[Route('/settings', name: 'settings', methods: 'POST')]
    #[IsGranted('blog-comment-settings')]
    public function settings(): Response
    {
        $entityname = [];
        extract($this->getRequest()->request->all());
        // [$entityname]

        if ($this->isCsrfTokenValid('blog-comment-settings', $_token)) {
            $entityname ? 
                $this->itemRepo->setExamine(array_keys($entityname)) :
                $this->itemRepo->setAllInactive();
        }
        return $this->redirectToRoute(self::ROUTE_NAME . 'index');
    }

    #[Route('/dashboard', name: 'dashboard', methods: 'GET')]
    public function getDashboardWidget(): Response
    {
        if (!$this->isGranted('blog-comment-dashboard')) return new Response();
        
        $latestNum = $this->commentRepo->countLatest($this->getUser());
        $comments = $this->commentRepo->findForModeration(false, 3);
        $num = $this->commentRepo->findForModeration(true);

        return $this->render(self::ROUTE_TEMPLATE_PATH . 'dashboard.html.twig', [
            'comments' => $comments, 'latestNum' => $latestNum, 'num' => $num,
            'params' => $this->getTemplateParams($this, Data::DOMAIN),
        ]);
    }
}