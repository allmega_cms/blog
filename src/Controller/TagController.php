<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Controller;

use Allmega\BlogBundle\Entity\Tag;
use Allmega\BlogBundle\{Data,Events};
use Allmega\BlogBundle\Repository\TagRepository;
use Allmega\BlogBundle\Utils\{Paginator, SortableItem};
use Allmega\BlogBundle\Utils\Params\BaseControllerParams;
use Allmega\BlogBundle\Model\Controller\{BaseController, BaseControllerServices};
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\HttpFoundation\Response;

#[Route('/tag', name: 'allmega_blog_tag_')]
class TagController extends BaseController
{
    public const ROUTE_TEMPLATE_PATH = '@AllmegaBlog/tag/';
    public const ROUTE_NAME = 'allmega_blog_tag_';

    public function __construct(BaseControllerServices $services)
    {
        parent::__construct($services);
    }

    #[Route('/list', name: 'index', methods: 'GET')]
    #[IsGranted('blog-tag-list')]
    public function index(TagRepository $tagRepo, Paginator $paginator): Response
    {
        $query = $tagRepo->findAllQuery();
        return $this->render(self::ROUTE_TEMPLATE_PATH . 'index.html.twig', [
            'params' => $this->getTemplateParams($this, Data::DOMAIN),
            'item' => SortableItem::getInstance(new Tag()),
            'tags' => $paginator->getPagination($query)
        ]);
    }

    #[Route('/{id}', name: 'delete', methods: 'DELETE')]
    #[IsGranted('blog-tag-delete', subject: 'tag')]
    public function delete(Tag $tag): Response
    {
        $params = (new BaseControllerParams())->init(
            arguments: ['id' => $tag->getId()],
            entity: $tag,
            domain: Data::DOMAIN,
            eventName: Events::TAG_DELETED,
            routeName: self::ROUTE_NAME
        );
        return $this->handle($params, $this->delete);
    }
}