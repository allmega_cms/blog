<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Controller;

use Allmega\BlogBundle\Utils\Helper;
use Allmega\BlogBundle\{Data, Events};
use Allmega\AuthBundle\Data as AuthData;
use Allmega\BlogBundle\Entity\MenuPoint;
use Allmega\BlogBundle\Utils\Params\BaseControllerParams;
use Allmega\BlogBundle\Manager\{MenuPointControllerTrait, MenupointHandler};
use Allmega\BlogBundle\Model\Controller\{BaseController, BaseControllerServices};
use Allmega\BlogBundle\Repository\{MenuPointRepository, MenuPointTypeRepository};
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\HttpFoundation\Response;

#[Route('/menu', name: 'allmega_blog_menupoint_')]
class MenuPointController extends BaseController
{
    use MenuPointControllerTrait;

    public const ROUTE_TEMPLATE_PATH = '@AllmegaBlog/menupoint/';
    public const ROUTE_NAME = 'allmega_blog_menupoint_';
    public const PROP = 'menupoint';

    public function __construct(
        private readonly MenuPointTypeRepository $menupointTypeRepo,
        private readonly MenuPointRepository $menupointRepo,
        private readonly MenupointHandler $menupointHandler,
        BaseControllerServices $services)
    {
        parent::__construct($services);
    }

    #[Route('/list', name: 'index', methods: 'GET')]
    #[IsGranted('blog-menupoint-list')]
    public function index(): Response
    {
        $criteria = $this->isGranted(AuthData::ADMIN_ROLE) ? [] : ['sys' => 0];
        $menupoints = $this->menupointRepo->findBy($criteria, ['prio' => 'ASC', 'title' => 'ASC']);
        $optParams = ['start' => ['route' => DefaultController::ROUTE_NAME]];
        
        return $this->render(self::ROUTE_TEMPLATE_PATH . 'index.html.twig', [
            'params' => $this->getTemplateParams($this, Data::DOMAIN, $optParams),
            'menupoints' => Helper::sort($menupoints)
        ]);
    }

	#[Route('/build', name: 'build', methods: 'GET')]
    #[IsGranted('blog-menupoint-build')]
	public function build(string $menuType): Response
	{
		return $this->render(self::ROUTE_TEMPLATE_PATH . 'designs/' . $menuType . '/menu.html.twig', [
			'menupoints' => Helper::sort($this->buildMenuPoints())
		]);
	}

	#[Route('/category/{slug}', name: 'show', methods: 'GET')]
    #[IsGranted('blog-menupoint-show')]
	public function show(MenuPoint $menupoint): Response
	{
        return $this->render(self::ROUTE_TEMPLATE_PATH . 'show.html.twig', [
            'params' => $this->getTemplateParams($this, Data::DOMAIN),
            'content' => $this->renderCategoriesTypes($menupoint),
            'menupoint' => $menupoint,
		]);
	}

    #[Route('/add', name: 'add', methods: ['GET', 'POST'])]
    #[IsGranted('blog-menupoint-add')]
    public function add(): Response
    {
        return $this->save();
    }

    #[Route('/edit/{id}', name: 'edit', methods: ['GET', 'POST'])]
    #[IsGranted('blog-menupoint-edit', subject: self::PROP)]
    public function edit(MenuPoint $menupoint): Response
    {
        $groups = $menupoint->getGroups();
        if ($groups->count()) {
            if ($menupoint->isSys() && !$this->isGranted(AuthData::ADMIN_ROLE)) {
                return $this->redirectToRoute(self::ROUTE_NAME.'index');
            }
        }

        return $this->save($menupoint);
    }

    #[Route('/{id}', name: 'delete', methods: 'DELETE')]
    #[IsGranted('blog-menupoint-delete', subject: self::PROP)]
    public function delete(MenuPoint $menupoint): Response
    {
        $params = (new BaseControllerParams())->init(
            entity: $menupoint,
            domain: Data::DOMAIN,
            eventName: Events::MENUPOINT_DELETED,
            routeName: self::ROUTE_NAME
        );
        return $this->handle($params, $this->delete);
    }

    #[Route('/state/{id}', name: 'state', methods: 'GET')]
    #[IsGranted('blog-menupoint-state', subject: self::PROP)]
    public function changeState(MenuPoint $menupoint): Response
    {
        $params = (new BaseControllerParams())->init(
            entity: $menupoint,
            domain: Data::DOMAIN,
            eventName: Events::MENUPOINT_STATE_CHANGED,
            routeName: self::ROUTE_NAME
        );
        return $this->handle($params, $this->state);
    }
}