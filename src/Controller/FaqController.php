<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Controller;

use Allmega\BlogBundle\Entity\Faq;
use Allmega\BlogBundle\{Data, Events};
use Allmega\BlogBundle\Utils\Search\SearchItem;
use Allmega\BlogBundle\Repository\FaqRepository;
use Allmega\BlogBundle\Manager\FaqControllerTrait;
use Allmega\BlogBundle\Utils\{Paginator, SortableItem};
use Allmega\BlogBundle\Model\Controller\{BaseController, BaseControllerServices};
use Allmega\BlogBundle\Utils\Params\BaseControllerParams;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\HttpFoundation\Response;

#[Route('/faq', name: 'allmega_blog_faq_')]
class FaqController extends BaseController
{
    use FaqControllerTrait;

    public const ROUTE_TEMPLATE_PATH = '@AllmegaBlog/faq/';
    public const ROUTE_NAME = 'allmega_blog_faq_';
    public const PROP = 'faq';

    public function __construct(BaseControllerServices $services)
    {
        parent::__construct($services);
    }

    #[Route('/search', name: 'search', methods: 'GET')]
    #[IsGranted('blog-faq-search')]
    public function search(FaqRepository $faqRepo, SearchItem $item): Response
    {
        $item
            ->setTemplatePath(self::ROUTE_TEMPLATE_PATH)
            ->setRoute(self::ROUTE_NAME . 'show')
            ->setProps(['question'])
            ->getRepo()
            ->setSearch($faqRepo)
            ->addOptions(['user' => $this->getUser()]);

        $item->getTrans()->setLabel('faq.search.title')->setDomain(Data::DOMAIN);       
        return $this->json(['data' => $item->getSearchResult()]);
    }

    #[Route('/list', name: 'index', methods: 'GET')]
    #[IsGranted('blog-faq-list')]
    public function index(FaqRepository $faqRepo, Paginator $paginator): Response
    {
        $query = $faqRepo->findAllQuery();
        return $this->render(self::ROUTE_TEMPLATE_PATH . 'index.html.twig', [
            'params' => $this->getTemplateParams($this, Data::DOMAIN),
            'item' => SortableItem::getInstance(new Faq()),
            'faqs' => $paginator->getPagination($query)
        ]);
    }

    #[Route('/show/{id}', name: 'show', methods: 'GET')]
    #[IsGranted('blog-faq-show', subject: self::PROP)]
    public function show(Faq $faq): Response
    {
        $params = ['params' => $this->getTemplateParams($this, Data::DOMAIN, ['faq' => $faq])];
        if ($this->isXmlRequest) {
            $data = [
                'content' =>  $this->renderView(self::ROUTE_TEMPLATE_PATH . 'show/_content.html.twig', $params)
            ];
            return $this->json($data);
        }
        return $this->render(self::ROUTE_TEMPLATE_PATH . 'show.html.twig', $params);
    }

    #[Route('/add', name: 'add', methods: ['GET', 'POST'])]
    #[IsGranted('blog-faq-add')]
    public function add(): Response
    {
        return $this->save();
    }

    #[Route('/edit/{id}', name: 'edit', methods: ['GET', 'POST'])]
    #[IsGranted('blog-faq-edit', subject: self::PROP)]
    public function edit(Faq $faq): Response
    {
        return $this->save($faq);
    }

    #[Route('/{id}', name: 'delete', methods: 'DELETE')]
    #[IsGranted('blog-faq-delete', subject: self::PROP)]
    public function delete(Faq $faq): Response
    {
        $params = (new BaseControllerParams())->init(
            entity: $faq,
            domain: Data::DOMAIN,
            eventName: Events::FAQ_DELETED,
            routeName: self::ROUTE_NAME
        );
        return $this->handle($params, $this->delete);
    }

    #[Route('/state/{id}', name: 'state', methods: 'GET')]
    #[IsGranted('blog-faq-state', subject: self::PROP)]
    public function changeState(Faq $faq): Response
    {
        $params = (new BaseControllerParams())->init(
            entity: $faq,
            domain: Data::DOMAIN,
            eventName: Events::FAQ_STATE_CHANGED,
            routeName: self::ROUTE_NAME
        );
        return $this->handle($params, $this->state);
    }
}