<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Controller;

use Allmega\BlogBundle\{Data, Events};
use Allmega\BlogBundle\Entity\NotificationType;
use Allmega\BlogBundle\Utils\{Paginator, SortableItem};
use Allmega\BlogBundle\Utils\Params\BaseControllerParams;
use Allmega\BlogBundle\Repository\NotificationTypeRepository;
use Allmega\BlogBundle\Manager\NotificationTypeControllerTrait;
use Allmega\BlogBundle\Model\Controller\{BaseController, BaseControllerServices};
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\HttpFoundation\Response;

#[Route('/notificationtype', name: 'allmega_blog_notificationtype_')]
class NotificationTypeController extends BaseController
{
    use NotificationTypeControllerTrait;

    public const ROUTE_TEMPLATE_PATH = '@AllmegaBlog/notificationtype/';
    public const ROUTE_NAME = 'allmega_blog_notificationtype_';
    public const PROP = 'notificationtype';

    public function __construct(BaseControllerServices $services)
    {
        parent::__construct($services);
    }

    #[Route('/list', name: 'index', methods: 'GET')]
    #[IsGranted('blog-notificationtype-list')]
    public function index(NotificationTypeRepository $notificationtypeRepo, Paginator $paginator): Response
    {
        $query = $notificationtypeRepo->findAllQuery();
        return $this->render(self::ROUTE_TEMPLATE_PATH . 'index.html.twig', [
            'params' => $this->getTemplateParams($this, Data::DOMAIN),
            'item' => SortableItem::getInstance(new NotificationType()),
            'notificationtypes' => $paginator->getPagination($query)
        ]);
    }

    #[Route('/add', name: 'add', methods: ['GET', 'POST'])]
    #[IsGranted('blog-notificationtype-add')]
    public function add(): Response
    {
        return $this->save();
    }

    #[Route('/edit/{id}', name: 'edit', methods: ['GET', 'POST'])]
    #[IsGranted('blog-notificationtype-edit', subject: self::PROP)]
    public function edit(NotificationType $notificationtype): Response
    {
        return $this->save($notificationtype);
    }

    #[Route('/{id}', name: 'delete', methods: 'DELETE')]
    #[IsGranted('blog-notificationtype-delete', subject: self::PROP)]
    public function delete(NotificationType $notificationtype): Response
    {
        $params = (new BaseControllerParams())->init(
            entity: $notificationtype,
            domain: Data::DOMAIN,
            eventName: Events::NOTIFICATIONTYPE_DELETED,
            routeName: self::ROUTE_NAME
        );
        return $this->handle($params, $this->delete);
    }

    #[Route('/state/{id}', name: 'state', methods: 'GET')]
    #[IsGranted('blog-notificationtype-state', subject: self::PROP)]
    public function changeState(NotificationType $notificationtype): Response
    {
        $params = (new BaseControllerParams())->init(
            entity: $notificationtype,
            domain: Data::DOMAIN,
            eventName: Events::NOTIFICATIONTYPE_STATE_CHANGED,
            routeName: self::ROUTE_NAME
        );
        return $this->handle($params, $this->state);
    }
}