<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Controller;

use Allmega\BlogBundle\{Data, Events};
use Allmega\BlogBundle\Entity\Contact;
use Allmega\BlogBundle\Utils\Search\SearchItem;
use Allmega\BlogBundle\Repository\ContactRepository;
use Allmega\BlogBundle\Manager\ContactControllerTrait;
use Allmega\BlogBundle\Utils\{Paginator, SortableItem};
use Allmega\BlogBundle\Utils\Params\BaseControllerParams;
use Allmega\BlogBundle\Model\Controller\{BaseController, BaseControllerServices};
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\HttpFoundation\Response;

#[Route('/contact', name: 'allmega_blog_contact_')]
class ContactController extends BaseController
{
    use ContactControllerTrait;

    public const ROUTE_TEMPLATE_PATH = '@AllmegaBlog/contact/';
    public const ROUTE_NAME = 'allmega_blog_contact_';
    public const PROP = 'contact';

    public function __construct(
        private readonly ContactRepository $contactRepo,
        private readonly string $validateMail,
        BaseControllerServices $services)
    {
        parent::__construct($services);
    }

    #[Route('/search', name: 'search', methods: 'GET')]
    #[IsGranted('blog-contact-search')]
    public function search(ContactRepository $contactRepo, SearchItem $item): Response
    {
        $item
            ->setTemplatePath(self::ROUTE_TEMPLATE_PATH)
            ->setRoute(self::ROUTE_NAME . 'edit')
            ->setProps(['sender'])
            ->getRepo()
            ->setSearch($contactRepo)
            ->addOptions(['user' => $this->getUser()]);

        $item->getTrans()->setLabel('contact.search.title')->setDomain(Data::DOMAIN);       
        return $this->json(['data' => $item->getSearchResult()]);
    }

    #[Route('/list', name: 'index', methods: 'GET')]
    #[IsGranted('blog-contact-list')]
    public function index(Paginator $paginator): Response
    {
        $query = $this->contactRepo->findAllQuery();
        return $this->render(self::ROUTE_TEMPLATE_PATH . 'index.html.twig', [
            'params' => $this->getTemplateParams($this, Data::DOMAIN),
            'item' => SortableItem::getInstance(new Contact()),
            'contacts' => $paginator->getPagination($query)
        ]);
    }

    #[Route('', name: 'add', methods: ['GET', 'POST'])]
    #[IsGranted('blog-contact-add')]
    public function add(): Response
    {
        return $this->save();
    }

    #[Route('/edit/{id}', name: 'edit', methods: ['GET', 'POST'])]
    #[IsGranted('blog-contact-edit', subject: self::PROP)]
    public function edit(Contact $contact): Response
    {
        return $this->save($contact);
    }

    #[Route('/{id}', name: 'delete', methods: 'DELETE')]
    #[IsGranted('blog-contact-delete', subject: self::PROP)]
    public function delete(Contact $contact): Response
    {
        $params = (new BaseControllerParams())->init(
            entity: $contact,
            domain: Data::DOMAIN,
            eventName: Events::CONTACT_DELETED,
            routeName: self::ROUTE_NAME
        );
        return $this->handle($params, $this->delete);
    }

    #[Route('/state/{id}', name: 'state', methods: 'GET')]
    #[IsGranted('blog-contact-state', subject: self::PROP)]
    public function changeState(Contact $contact): Response
    {
        $params = (new BaseControllerParams())->init(
            entity: $contact,
            domain: Data::DOMAIN,
            eventName: Events::CONTACT_STATE_CHANGED,
            method: 'answered',
            routeName: self::ROUTE_NAME
        );
        return $this->handle($params, $this->state);
    }

    #[Route('/dashboard', name: 'dashboard', methods: 'GET')]
    public function getDashboardWidget(): Response
    {
        if (!$this->isGranted('blog-contact-dashboard')) return new Response();

        $latestNum = $this->contactRepo->countLatest($this->getUser());
        
        $contacts = $this->contactRepo->findForModeration(false, 3);
        $num = $this->contactRepo->findForModeration(true);

        return $this->render(self::ROUTE_TEMPLATE_PATH . 'dashboard.html.twig', [
            'contacts' => $contacts, 'latestNum' => $latestNum, 'num' => $num,
            'params' => $this->getTemplateParams($this, Data::DOMAIN),
        ]);
    }
}