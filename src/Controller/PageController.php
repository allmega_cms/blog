<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Controller;

use Allmega\BlogBundle\Entity\Page;
use Allmega\BlogBundle\{Data, Events, Security\Voters\PageVoter};
use Allmega\BlogBundle\Utils\Search\SearchItem;
use Allmega\BlogBundle\Repository\PageRepository;
use Allmega\BlogBundle\Manager\PageControllerTrait;
use Allmega\BlogBundle\Utils\{Paginator, SortableItem};
use Allmega\BlogBundle\Model\Controller\{BaseController, BaseControllerServices};
use Allmega\BlogBundle\Utils\Params\BaseControllerParams;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\HttpFoundation\Response;

#[Route('/page', name: 'allmega_blog_page_')]
class PageController extends BaseController
{
    use PageControllerTrait;

    public const ROUTE_TEMPLATE_PATH = '@AllmegaBlog/page/';
    public const ROUTE_NAME = 'allmega_blog_page_';
    public const PROP = 'page';

    public function __construct(BaseControllerServices $services)
    {
        parent::__construct($services);
    }

    #[Route('/search', name: 'search', methods: 'GET')]
    #[IsGranted('blog-page-search')]
    public function search(PageRepository $pageRepo, SearchItem $item): Response
    {
        $item
            ->setTemplatePath(self::ROUTE_TEMPLATE_PATH)
            ->setRoute(self::ROUTE_NAME . 'show')
            ->setRouteProp('slug')
            ->setProps(['title'])
            ->getRepo()
            ->setSearch($pageRepo)
            ->addOptions(['user' => $this->getUser()]);

        $item->getTrans()->setLabel('page.search.title')->setDomain(Data::DOMAIN);       
        return $this->json(['data' => $item->getSearchResult()]);
    }

    #[Route('/list', name: 'index', methods: 'GET')]
    #[IsGranted('blog-page-list')]
    public function index(PageRepository $pageRepo, Paginator $paginator): Response
    {
        $pageRepo->setExpiredInactiv();
        $query = $pageRepo->findAllQuery();

        return $this->render(self::ROUTE_TEMPLATE_PATH . 'index.html.twig', [
            'params' => $this->getTemplateParams($this, Data::DOMAIN, $this->getStartParams()),
            'item' => SortableItem::getInstance(new Page(), 'article'),
            'pages' => $paginator->getPagination($query)
        ]);
    }

    #[Route('/show/{slug}', name: 'show', methods: 'GET')]
    #[IsGranted('blog-page-show', subject: self::PROP)]
    public function show(PageVoter $pageVoter, Page $page): Response
    {
        $optParams = array_merge($this->getStartParams(), ['pageVoter' => $pageVoter]);
        $params = [
            'params' => $this->getTemplateParams($this, Data::DOMAIN, $optParams),
            'page' => $page->setImages($this->getMediaFileHandler()),
        ];

        if ($this->isXmlRequest) {
            $data = [
                'content' =>  $this->renderView(self::ROUTE_TEMPLATE_PATH . 'show/_layout.html.twig', $params)
            ];
            return $this->json($data);
        }
        return $this->render(self::ROUTE_TEMPLATE_PATH . 'show.html.twig', $params);
    }

    #[Route('/add', name: 'add', methods: ['GET', 'POST'])]
    #[IsGranted('blog-page-add')]
    public function add(): Response
    {
        return $this->save();
    }

    #[Route('/edit/{id}', name: 'edit', methods: ['GET', 'POST'])]
    #[IsGranted('blog-page-edit', subject: self::PROP)]
    public function edit(Page $page): Response
    {
        return $this->save($page);
    }

    #[Route('/{id}', name: 'delete', methods: 'DELETE')]
    #[IsGranted('blog-page-delete', subject: self::PROP)]
    public function delete(Page $page): Response
    {
        $params = (new BaseControllerParams())->init(
            entity: $page,
            domain: Data::DOMAIN,
            eventName: Events::PAGE_DELETED,
            routeName: self::ROUTE_NAME
        );
        return $this->handle($params, $this->delete);
    }

    #[Route('/state/{id}', name: 'state', methods: 'GET')]
    #[IsGranted('blog-page-state', subject: self::PROP)]
    public function changeState(Page $page): Response
    {
        $params = (new BaseControllerParams())->init(
            entity: $page,
            domain: Data::DOMAIN,
            eventName: Events::PAGE_STATE_CHANGED,
            routeName: self::ROUTE_NAME
        );
        return $this->handle($params, $this->state);
    }
}