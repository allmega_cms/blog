<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Controller;

use Allmega\BlogBundle\Data;
use Allmega\BlogBundle\Events;
use Allmega\BlogBundle\Entity\{Page, Post, MenuPoint};
use Symfony\Component\EventDispatcher\{EventDispatcherInterface, GenericEvent};
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\HttpFoundation\Response;
use Doctrine\ORM\EntityManagerInterface;

#[Route(name: 'allmega_blog_')]
class DefaultController extends AbstractController
{
    private array $classes = [Page::class, Post::class, MenuPoint::class];
    public const ROUTE_TEMPLATE_PATH = '@AllmegaBlog/default/';
    public const ROUTE_NAME = 'allmega_blog_';
    
    public function __construct(
        private readonly EventDispatcherInterface $eventDispatcher,
        private readonly EntityManagerInterface   $em) {}

    /**
     * Here redirect because of locale, so no route here
     */
    public function index(): Response
    {
        foreach ($this->classes as $class) {
            $item = $this->em->getRepository($class)->findOneBy(['start' => 1]);
            if ($item) {
                $name = strtolower(substr(strrchr(get_class($item), "\\"), 1));
                return $this->redirectToRoute('allmega_blog_' . $name . '_show', ['slug' => $item->getSlug()]);
            }
        }
        return $this->redirectToRoute(DashboardController::ROUTE_NAME . 'show');
    }

    #[Route('/maintenance', name: 'maintenance', methods: 'GET')]
    public function maintenance(): Response
    {
        return $this->render('@AllmegaBlog/inc/_maintenance.html.twig');
    }

    #[Route('/start/{type}/{id}', name: 'start', methods: 'GET')]
    #[IsGranted('ROLE_BLOG_MANAGER')]
    public function setAsStartSite(string $type = '', string $id = ''): Response
    {
        if (!$type) {
            foreach ($this->classes as $class) $this->em->getRepository($class)->resetDefaultSite();
            return $this->redirectToRoute(DashboardController::ROUTE_NAME . 'show');
        }

        switch ($type) {
            case 'page':
                $route = PageController::ROUTE_NAME . 'index';
                $class = Page::class;
                break;
            case 'post':
                $route = PostController::ROUTE_NAME . 'index';
                $class = Post::class;
                break;
            default:
                $route = MenuPointController::ROUTE_NAME . 'index';
                $class = MenuPoint::class;
                break;
        }

        $defaultSite = $this->em->getRepository($class)->find($id);
        if ($defaultSite) {
            if (!$defaultSite->isStart()) {

                foreach ($this->classes as $class) $this->em->getRepository($class)->resetDefaultSite();

                $defaultSite->setStart(true)->setActive(true);

                $this->em->persist($defaultSite);
                $this->em->flush();

                $event = new GenericEvent($defaultSite, [
                    'isXmlRequest' => false,
                    'domain' => Data::DOMAIN,
                ]);
                $this->eventDispatcher->dispatch($event, Events::STARTPAGE_CHANGED);
            }

            return $this->redirectToRoute($route);
        } else {
            return $this->redirectToRoute(DashboardController::ROUTE_NAME . 'show');
        }
    }
}