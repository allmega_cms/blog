<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Controller;

use Allmega\BlogBundle\Data;
use Allmega\BlogBundle\Repository\{MenuPointRepository, PostRepository};
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\HttpFoundation\Response;

#[Route(name: 'allmega_blog_')]
class SitemapController extends AbstractController
{
    public const ROUTE_TEMPLATE_PATH = '@AllmegaBlog/sitemap/';
    public const ROUTE_NAME = 'allmega_blog_';

    public function __construct(
        private readonly MenuPointRepository   $menupointRepo,
        private readonly UrlGeneratorInterface $router,
        private readonly PostRepository        $postRepo) {}

    #[Route('/sitemap.xml', name: 'sitemap', defaults: ['_format' => 'xml'], methods: 'GET')]
    public function index(): Response
    {
        $items = array_merge($this->getMenupointsUrls(), $this->getPostsUrls());
        return $this->render(self::ROUTE_TEMPLATE_PATH . 'index.xml.twig', ['items' => $items]);
    }

    private function getMenupointsUrls(): array
    {
        $menupoints = $this->menupointRepo->findWithoutGroups();
        $items = [];

        foreach ($menupoints as $menupoint) {
            $type = $menupoint->getType()->getShortname();
            switch ($type) {
                case Data::MENUPOINT_TYPE_ROUTE:
                    $items[] = $menupoint->getRoute();
                    break;
                case Data::MENUPOINT_TYPE_CATEGORY:
                    $slug = $menupoint->getSlug();
                    $route = MenuPointController::ROUTE_NAME . 'show';
                    $items[] = $this->router->generate($route, ['slug' => $slug]);
                    break;
                default:
            } 
        }
        return $items;
    }

    private function getPostsUrls(): array
    {
        $posts = $this->postRepo->findLatest(true);
        $items = [];

        foreach ($posts as $post) {
            $slug = $post->getSlug();
            $route = PostController::ROUTE_NAME . 'show';
            $items[] = $this->router->generate($route, ['slug' => $slug]);
        }
        return $items;
    }
}