<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Controller;

use Allmega\BlogBundle\{Data, Events};
use Allmega\BlogBundle\Entity\EmailAlias;
use Allmega\BlogBundle\Repository\EmailAliasRepository;
use Allmega\BlogBundle\Utils\{Paginator, SortableItem};
use Allmega\BlogBundle\Utils\Params\BaseControllerParams;
use Allmega\BlogBundle\Manager\EmailAliasControllerTrait;
use Allmega\BlogBundle\Model\Controller\{BaseController, BaseControllerServices};
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\HttpFoundation\Response;

#[Route('/emailalias', name: 'allmega_blog_emailalias_')]
class EmailAliasController extends BaseController
{
    use EmailAliasControllerTrait;

    public const ROUTE_TEMPLATE_PATH = '@AllmegaBlog/emailalias/';
    public const ROUTE_NAME = 'allmega_blog_emailalias_';
    public const PROP = 'emailalias';

    public function __construct(BaseControllerServices $services)
    {
        parent::__construct($services);
    }

    #[Route('/list', name: 'index', methods: 'GET')]
    #[IsGranted('blog-emailalias-list')]
    public function index(EmailAliasRepository $emailaliasRepo, Paginator $paginator): Response
    {
        $query = $emailaliasRepo->findAllQuery();
        return $this->render(self::ROUTE_TEMPLATE_PATH . 'index.html.twig', [
            'params' => $this->getTemplateParams($this, Data::DOMAIN),
            'item' => SortableItem::getInstance(new EmailAlias()),
            'emailaliases' => $paginator->getPagination($query)
        ]);
    }

    #[Route('/add', name: 'add', methods: ['GET', 'POST'])]
    #[IsGranted('blog-emailalias-add')]
    public function add(): Response
    {
        return $this->save();
    }

    #[Route('/edit/{id}', name: 'edit', methods: ['GET', 'POST'])]
    #[IsGranted('blog-emailalias-edit', subject: self::PROP)]
    public function edit(EmailAlias $emailalias): Response
    {
        return $this->save($emailalias);
    }

    #[Route('/{id}', name: 'delete', methods: 'DELETE')]
    #[IsGranted('blog-emailalias-delete', subject: self::PROP)]
    public function delete(EmailAlias $emailalias): Response
    {
        $params = (new BaseControllerParams())->init(
            entity: $emailalias,
            domain: Data::DOMAIN,
            eventName: Events::EMAILALIAS_DELETED,
            routeName: self::ROUTE_NAME
        );
        return $this->handle($params, $this->delete);
    }

    #[Route('/state/{id}', name: 'state', methods: 'GET')]
    #[IsGranted('blog-emailalias-state', subject: self::PROP)]
    public function changeState(EmailAlias $emailalias): Response
    {
        $params = (new BaseControllerParams())->init(
            entity: $emailalias,
            domain: Data::DOMAIN,
            eventName: Events::EMAILALIAS_STATE_CHANGED,
            routeName: self::ROUTE_NAME
        );
        return $this->handle($params, $this->state);
    }
}