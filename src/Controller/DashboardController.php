<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Controller;

use Allmega\BlogBundle\Data;
use Allmega\BlogBundle\Model\Controller\{BaseController, BaseControllerServices};
use Symfony\Component\Security\Http\Attribute\IsGranted;
use Symfony\Component\Routing\Attribute\Route;
use Symfony\Component\HttpFoundation\Response;

#[Route('/dashboard', name: 'allmega_blog_dashboard_')]
class DashboardController extends BaseController
{
    public const ROUTE_TEMPLATE_PATH = '@AllmegaBlog/dashboard/';
    public const ROUTE_NAME = 'allmega_blog_dashboard_';

    public function __construct(BaseControllerServices $services)
    {
        parent::__construct($services);
    }

    #[Route('/show', name: 'show')]
    #[IsGranted('blog-dashboard-show')]
    public function show(): Response
    {
        return $this->render(self::ROUTE_TEMPLATE_PATH . 'show.html.twig', [
            'params' => $this->getTemplateParams($this, Data::DOMAIN)
        ]);
    }

    #[Route('/icon', name: 'icon')]
    public function getMenuIcon(): Response
    {
        if (!$this->isGranted('blog-dashboard-icon')) new Response();

        return $this->render(self::ROUTE_TEMPLATE_PATH . 'inc/_icon.html.twig', [
            'params' => $this->getTemplateParams($this, Data::DOMAIN)
        ]);
    }
}