Installation
============

Make sure
[Symfony](https://symfony.com/download),
[Nodejs](https://nodejs.org/en/download/package-manager/),
[Composer](https://getcomposer.org/doc/00-intro.md)
is installed globally, as explained in the installation chapters of their documentations

Create your project directory
-----------------------------

Open a command console, enter your webroot directory and execute:

```shell
$ symfony new YOUR_PROJECT_NAME --webapp --version=lts
```

Install all required dependencies
---------------------------------

Open a command console, enter your project directory and execute:

```shell
$ composer require api symfony/webpack-encore-bundle symfony/finder twig/markdown-extra twig/intl-extra symfonycasts/verify-email-bundle symfonycasts/reset-password-bundle symfony/rate-limiter league/commonmark nelmio/api-doc-bundle nelmio/security-bundle gesdinet/jwt-refresh-token-bundle nucleos/dompdf-bundle knplabs/knp-snappy-bundle knplabs/knp-paginator-bundle zytzagoo/smtp-validate-email
```

Install Dama bundle for phpunit testing
---------------------------------------

```shell
$ composer require dama/doctrine-test-bundle --dev
```

Generate the SSL keys for JWT Lexik Bundle
------------------------------------------

```shell
$ ./bin/console lexik:jwt:generate-keypair
```

Your keys will land in config/jwt/private.pem and config/jwt/public.pem
Add to file `config/packages/security.yaml` the following empty key `security.firewalls.api`

Applications that use Symfony Flex
----------------------------------

Open a command console, enter your project directory and execute:

```shell
$ composer require allmega/blog
```

Applications that don't use Symfony Flex
----------------------------------------

Step 1: Download the Bundle
---------------------------

Open a command console, enter your project directory and execute the
following command to download the latest stable version of this bundle:

```shell
$ composer require allmega/blog
```

Step 2: Enable the Bundle
-------------------------

Then, enable the bundle by adding it to the list of registered bundles
in the `config/bundles.php` file of your project:

```php
return [
    // ...
    Allmega\BlogBundle\AllmegaBlogBundle::class => ['all' => true],
];
```

Modify Webpack config
------------------------

- Uncomment in file `webpack.config.js` the line `.autoProvidejQuery()`
- Add after this line `.autoProvideVariables({ $: 'jquery', jQuery: 'jquery', 'window.jQuery': 'jquery' })`
- Add section for bundles and themes assets entries in `ENTRY CONFIG` area
  
```js
/** ALLMEGA_BEGIN_BUNDLES_ENTRIES */
/** ALLMEGA_END_BUNDLES_ENTRIES */

/** ALLMEGA_BEGIN_THEME_ENTRIES */
/** ALLMEGA_END_THEME_ENTRIES */
```

Register all bundles, that your need
------------------------------------

Open a command console, enter your project directory and execute:

```shell
$ ./bin/console allmega:packages:register all
```

Add config options for tests, if needed
---------------------------------------

Create, if not exists, the file `.env.test.local` and copy-paste content
from file `.env.local` to the created file, save and close this file

Install libraries for webpack-encore
---------------------------------------

Open a command console, enter your project directory and execute:

```shell
$ npm install @popperjs/core @fortawesome/fontawesome-free jquery bootstrap flatpickr hc-offcanvas-nav
$ npm run build
```

Import packages config files
----------------------------

Add following lines to `config/services.yaml`

```yaml
imports:
    - { resource: '@AllmegaAuthBundle/config/packages/' }
    - { resource: '@AllmegaBlogBundle/config/packages/' }
```

Create database tables for CMS
----------------------------------------

Open a command console, enter your project directory and execute:

```shell
$ ./bin/console doctrine:schema:update -f
```

Load the data for registered bundles
------------------------------------

Open a command console, enter your project directory and execute:

```shell
$ ./bin/console allmega:packages:load all -s
```

Edit the "auto-scripts" section in "composer.json"
-------------------------------------------------

```json
"auto-scripts": {
    "cache:clear": "symfony-cmd",
    "importmap:install": "symfony-cmd",
    "assets:install --symlink --relative %PUBLIC_DIR%": "symfony-cmd",
    "doctrine:schema:update -f --complete": "symfony-cmd",
    "allmega:packages:register all --no-interaction": "symfony-cmd",
    "allmega:packages:load all": "symfony-cmd"
},
```

Testing
-------

Open the `phpunit.xml.dist` and add to `php` tag:

```xml
<env name="SYMFONY_DEPRECATIONS_HELPER" value="disabled" />
```

Open a command console, enter your project directory
and execute the following command:

```shell
$ ./bin/phpunit --stop-on-failure
```
