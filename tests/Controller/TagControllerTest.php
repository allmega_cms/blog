<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Tests\Controller;

use Symfony\Component\VarExporter\Exception\ClassNotFoundException;
use Allmega\BlogBundle\Controller\TagController;
use Allmega\BlogBundle\Model\AllmegaWebTest;
use Allmega\BlogBundle\Entity\Tag;
use Allmega\BlogBundle\Data;

class TagControllerTest extends AllmegaWebTest
{
    /**
     * @throws ClassNotFoundException
     */
    protected function testIndex(): void
    {
        $this->runTests($this->index, Data::POST_AUTHOR_ROLE);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testDelete(): void
    {
        $this->runModifyTests(role: Data::POST_AUTHOR_ROLE, delete: true);
    }

    protected function create(): void
    {
        $entity = Tag::build();
        $this->em->persist($entity);
        $this->em->flush();
        $this->params->setEntity($entity);
    }

    protected function getRouteName(string $name): string 
    {
        return TagController::ROUTE_NAME . $name;
    }
}