<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Tests\Controller;

use Symfony\Component\VarExporter\Exception\ClassNotFoundException;
use Allmega\BlogBundle\Manager\PostControllerTestTrait;
use Allmega\BlogBundle\Controller\CommentController;
use Allmega\BlogBundle\Entity\{Comment, Post};
use Allmega\BlogBundle\Model\AllmegaWebTest;
use Allmega\BlogBundle\Data as BlogData;
use Allmega\AuthBundle\Data as AuthData;
use Allmega\BlogBundle\Utils\Helper;

class CommentControllerTest extends AllmegaWebTest
{
    use PostControllerTestTrait;

    /**
     * @throws ClassNotFoundException
     */
    public function testDashboardWidget(): void
    {
        $this->runTests($this->dashboard, BlogData::COMMENT_MODERATOR_ROLE, false, false);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testIndex(): void
    {
        $this->runTests($this->index, BlogData::COMMENT_MODERATOR_ROLE);
    }

    public function testSettings(): void
    {
        /*$this->params->init(routeName: 'settings', method: 'POST');
        $this->runBaseTests(true);

        $this->params
            ->reset()
            ->init(routeName: 'settings', method: 'POST', role: BlogData::BLOG_MANAGER_ROLE);
        $this->runBaseTests();*/
        $this->assertTrue(true);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testAdd(): void
    {
        $post = $this->createPost();
        $optParams = [
            'message' => Helper::generateRandomString(),
            'entityId' => $post->getId(),
            'entityName' => Post::class,
            'parentId' => '',
        ];

        $this->runTests(
            routeName: $this->add,
            role: AuthData::USER_ROLE,
            optParams: $optParams,
            xmlHttp: true,
            method: 'POST');

        $response = $this->getJsonResponse();
        $this->assertArrayHasKey('success', $response);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testEdit(): void
    {
        $this->runTests($this->edit, BlogData::COMMENT_MODERATOR_ROLE, true);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testEnabled(): void
    {
        $this->runModifyTests(
            routeName: 'enable',
            role: BlogData::COMMENT_MODERATOR_ROLE,
            redirectName: $this->index);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testDelete(): void
    {
        $this->runModifyTests(role: BlogData::COMMENT_MODERATOR_ROLE, delete: true);
    }

    /**
     * @throws ClassNotFoundException
     */
    protected function create(): void
    {
        $user = $this->findUserByRole(BlogData::POST_AUTHOR_ROLE);
        $item = $this->findItemByName(Post::class);
        
        $post = Post::build(authors: [$user]);
        $this->em->persist($post);
        
        $entity = Comment::build(entityid: $post->getId(), author: $user, item: $item);
        $this->em->persist($entity);
        $this->em->flush();
        $this->params->setEntity($entity);
    }

    protected function getRouteName(string $name): string 
    {
        return CommentController::ROUTE_NAME . $name;
    }
}