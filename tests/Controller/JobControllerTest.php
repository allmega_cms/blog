<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Tests\Controller;

use Symfony\Component\VarExporter\Exception\ClassNotFoundException;
use Allmega\BlogBundle\Controller\JobController;
use Allmega\BlogBundle\Model\AllmegaWebTest;
use Allmega\BlogBundle\Entity\Job;
use Allmega\BlogBundle\Data;

class JobControllerTest extends AllmegaWebTest
{
    /**
     * @throws ClassNotFoundException
     */
    public function testDashboardWidget(): void
    {
        $this->runTests($this->dashboard, Data::JOB_MODERATOR_ROLE, false, false);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testSearch(): void
    {
        $this->runTests($this->search, Data::JOB_MODERATOR_ROLE);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testIndex(): void
    {
        $this->runTests($this->index, Data::JOB_MODERATOR_ROLE);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testShow(): void
    {
        $this->runPublicTest($this->show);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testAdd(): void
    {
        $this->runPublicTest($this->add);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testDelete(): void
    {
        $this->runModifyTests(role: Data::JOB_MODERATOR_ROLE, delete: true);
    }

    protected function create(): void
    {
        $entity = Job::build();
        $this->em->persist($entity);
        $this->em->flush();
        $this->params->setEntity($entity);
    }

    protected function getRouteName(string $name): string 
    {
        return JobController::ROUTE_NAME . $name;
    }
}