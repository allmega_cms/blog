<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Tests\Controller;

use Symfony\Component\VarExporter\Exception\ClassNotFoundException;
use Allmega\BlogBundle\Controller\PageController;
use Allmega\BlogBundle\Model\AllmegaWebTest;
use Allmega\BlogBundle\Entity\Page;
use Allmega\BlogBundle\Data;

class PageControllerTest extends AllmegaWebTest
{
    /**
     * @throws ClassNotFoundException
     */
    public function testSearch(): void
    {
        $this->runTests($this->search, Data::PAGE_AUTHOR_ROLE);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testIndex(): void
    {
        $this->runTests($this->index, Data::PAGE_AUTHOR_ROLE);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testShow(): void
    {
        $this->runTests(
            routeName: $this->show,
            role: Data::PAGE_AUTHOR_ROLE,
            create: true,
            testRedirect: false,
            fnRouteParams: 'getSlugAsRouteParams');
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testAdd(): void
    {
        $this->runTests($this->add, Data::PAGE_AUTHOR_ROLE);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testEdit(): void
    {
        $this->runTests($this->edit, Data::PAGE_AUTHOR_ROLE, true);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testChangeState(): void
    {
        $this->runModifyTests(
            role: Data::PAGE_AUTHOR_ROLE,
            fnRedirectParams: 'getSlugAsRouteParams');
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testDelete(): void
    {
        $this->runModifyTests(role: Data::PAGE_AUTHOR_ROLE, delete: true);
    }

    protected function create(): void
    {
        $entity = Page::build();
        $this->em->persist($entity);
        $this->em->flush();
        $this->params->setEntity($entity);
    }

    protected function getRouteName(string $name): string
    {
        return PageController::ROUTE_NAME . $name;
    }
}