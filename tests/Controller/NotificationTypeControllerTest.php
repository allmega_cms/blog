<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Tests\Controller;

use Symfony\Component\VarExporter\Exception\ClassNotFoundException;
use Allmega\BlogBundle\Controller\NotificationTypeController;
use Allmega\BlogBundle\Entity\NotificationType;
use Allmega\BlogBundle\Model\AllmegaWebTest;
use Allmega\BlogBundle\Data;

class NotificationTypeControllerTest extends AllmegaWebTest
{
    /**
     * @throws ClassNotFoundException
     */
    public function testIndex(): void
    {
        $this->runTests($this->index, Data::BLOG_MANAGER_ROLE);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testEdit(): void
    {
        $this->runTests($this->edit, Data::BLOG_MANAGER_ROLE, true);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testChangeState(): void
    {
        $this->runModifyTests(role: Data::BLOG_MANAGER_ROLE, redirectName: $this->index);
    }

    protected function create(): void
    {
        $entity = NotificationType::build();
        $this->em->persist($entity);
        $this->em->flush();
        $this->params->setEntity($entity);
    }

    protected function getRouteName(string $name): string 
    {
        return NotificationTypeController::ROUTE_NAME . $name;
    }
}