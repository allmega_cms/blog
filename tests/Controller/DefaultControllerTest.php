<?php

/**
 * This file is part of the Allmega BlogBundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Tests\Controller;

use Symfony\Component\VarExporter\Exception\ClassNotFoundException;
use Allmega\BlogBundle\Controller\{DashboardController, DefaultController};
use Allmega\BlogBundle\Model\AllmegaWebTest;
use Allmega\AuthBundle\Data as AuthData;
use Allmega\BlogBundle\Data as BlogData;

class DefaultControllerTest extends AllmegaWebTest
{
    /**
     * @throws ClassNotFoundException
     */
    public function testIndex(): void
    {
        $this->params
            ->init(
                redirectRoute: DashboardController::ROUTE_NAME . $this->show,
                testRedirect: false,
                routeUrl: '/',
                redirectCode: 302,
                role: AuthData::USER_ROLE
            );
        $this->handleParams()->runWebTest();
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testSetAsStartSite(): void
    {
        $this->params->init(routeName: 'start');
        $this->handleParams()->runWebTest();

        $this->params
            ->reset()
            ->init(
                redirectRoute: DashboardController::ROUTE_NAME . $this->show,
                testRedirect: false,
                routeName: 'start',
                redirectCode: 302,
                role: BlogData::BLOG_MANAGER_ROLE
            );
        $this->handleParams()->runWebTest();
    }

    protected function getRouteName(string $name): string 
    {
        return DefaultController::ROUTE_NAME . $name;
    }
}