<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Tests\Controller;

use Symfony\Component\VarExporter\Exception\ClassNotFoundException;
use Allmega\BlogBundle\Controller\RecipientController;
use Allmega\BlogBundle\Model\AllmegaWebTest;
use Allmega\BlogBundle\Entity\Recipient;
use Allmega\BlogBundle\Data;

class RecipientControllerTest extends AllmegaWebTest
{
    /**
     * @throws ClassNotFoundException
     */
    public function testIndex(): void
    {
        $this->runTests($this->index, Data::BLOG_MANAGER_ROLE);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testAdd(): void
    {
        $this->runTests($this->add, Data::BLOG_MANAGER_ROLE);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testEdit(): void
    {
        $this->runTests($this->edit, Data::BLOG_MANAGER_ROLE, true);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testChangeState(): void
    {
        $this->runModifyTests(role: Data::BLOG_MANAGER_ROLE, redirectName: $this->index);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testDelete(): void
    {
        $this->runModifyTests(role: Data::BLOG_MANAGER_ROLE, delete: true);
    }

    protected function create(): void
    {
        $entity = Recipient::build();
        $this->em->persist($entity);
        $this->em->flush();
        $this->params->setEntity($entity);
    }

    protected function getRouteName(string $name): string 
    {
        return RecipientController::ROUTE_NAME . $name;
    }
}