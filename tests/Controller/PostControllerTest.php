<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Tests\Controller;

use Symfony\Component\VarExporter\Exception\ClassNotFoundException;
use Allmega\BlogBundle\Manager\PostControllerTestTrait;
use Allmega\BlogBundle\Controller\PostController;
use Allmega\BlogBundle\Model\AllmegaWebTest;
use Allmega\BlogBundle\Data;

class PostControllerTest extends AllmegaWebTest
{
    use PostControllerTestTrait;

    /**
     * @throws ClassNotFoundException
     */
    public function testSearch(): void
    {
        $this->runTests($this->search, Data::POST_AUTHOR_ROLE);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testIndex(): void
    {
        $this->runTests($this->index, Data::POST_AUTHOR_ROLE);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testLatest(): void
    {
        $this->runPublicTest('latest');
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testShow(): void
    {
        $this->runTests(
            routeName: $this->show,
            role: Data::POST_AUTHOR_ROLE,
            create: true,
            testRedirect: false,
            fnRouteParams: 'getSlugAsRouteParams');
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testAdd(): void
    {
        $this->runTests($this->add, Data::POST_AUTHOR_ROLE);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testEdit(): void
    {
        $this->runTests($this->edit, Data::POST_AUTHOR_ROLE, true);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testChangeState(): void
    {
        $this->runModifyTests(
            role: Data::POST_AUTHOR_ROLE,
            fnRedirectParams: 'getSlugAsRouteParams');
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testDelete(): void
    {
        $this->runModifyTests(role: Data::POST_AUTHOR_ROLE, delete: true);
    }

    protected function create(): void
    {
        $this->params->setEntity($this->createPost());
    }

    protected function getRouteName(string $name): string 
    {
        return PostController::ROUTE_NAME . $name;
    }
}