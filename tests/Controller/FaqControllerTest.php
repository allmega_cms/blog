<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Tests\Controller;

use Symfony\Component\VarExporter\Exception\ClassNotFoundException;
use Allmega\BlogBundle\Controller\FaqController;
use Allmega\BlogBundle\Model\AllmegaWebTest;
use Allmega\BlogBundle\Entity\Faq;
use Allmega\BlogBundle\Data;

class FaqControllerTest extends AllmegaWebTest
{
    /**
     * @throws ClassNotFoundException
     */
    public function testSearch(): void
    {
        $this->runTests($this->search, Data::FAQ_AUTHOR_ROLE);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testIndex(): void
    {
        $this->runTests($this->index, Data::FAQ_AUTHOR_ROLE);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testShow(): void
    {
        $this->runTests(
            routeName: $this->show,
            role: Data::FAQ_AUTHOR_ROLE,
            create: true,
            testRedirect: false,
            fnRouteParams: 'getIdAsRouteParams');
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testAdd(): void
    {
        $this->runTests($this->add, Data::FAQ_AUTHOR_ROLE);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testEdit(): void
    {
        $this->runTests($this->edit, Data::FAQ_AUTHOR_ROLE, true);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testChangeState(): void
    {
        $this->runModifyTests(role: Data::FAQ_AUTHOR_ROLE, redirectName: $this->index);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testDelete(): void
    {
        $this->runModifyTests(role: Data::FAQ_AUTHOR_ROLE, delete: true);
    }

    /**
     * @throws ClassNotFoundException
     */
    protected function create(): void
    {
        $user = $this->findUserByRole(Data::FAQ_AUTHOR_ROLE);
        $entity = Faq::build(user: $user);
        $this->em->persist($entity);
        $this->em->flush();
        $this->params->setEntity($entity);
    }

    protected function getRouteName(string $name): string 
    {
        return FaqController::ROUTE_NAME . $name;
    }
}