<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Tests\Controller;

use Symfony\Component\VarExporter\Exception\ClassNotFoundException;
use Allmega\BlogBundle\Controller\SitemapController;
use Allmega\BlogBundle\Model\AllmegaWebTest;

class SitemapControllerTest extends AllmegaWebTest
{
    /**
     * @throws ClassNotFoundException
     */
    public function testShow(): void
    {
        $this->runPublicTest('sitemap');
    }

    protected function getRouteName(string $name): string 
    {
        return SitemapController::ROUTE_NAME . $name;
    }
}