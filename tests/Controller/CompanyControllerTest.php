<?php

/**
 * This file is part of the Allmega BlogBundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Tests\Controller;

use Symfony\Component\VarExporter\Exception\ClassNotFoundException;
use Allmega\BlogBundle\Repository\CompanyRepository;
use Allmega\BlogBundle\Controller\CompanyController;
use Allmega\BlogBundle\Model\AllmegaWebTest;
use Allmega\BlogBundle\Data;

class CompanyControllerTest extends AllmegaWebTest
{
    /**
     * @throws ClassNotFoundException
     */
    public function testShow(): void
    {
        $this->runPublicTest($this->show);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testEdit(): void
    {
        $this->runTests($this->edit, Data::BLOG_MANAGER_ROLE, true);
    }

    /**
     * @throws ClassNotFoundException
     */
    protected function create(): void
    {
        $entity = $this->getService(CompanyRepository::class)->findOneBy([]);
        $this->params->setEntity($entity);
    }

    protected function getRouteName(string $name): string 
    {
        return CompanyController::ROUTE_NAME . $name;
    }
}
