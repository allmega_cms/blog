<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Tests\Controller;

use Symfony\Component\VarExporter\Exception\ClassNotFoundException;
use Allmega\BlogBundle\Controller\DashboardController;
use Allmega\BlogBundle\Model\AllmegaWebTest;
use Allmega\AuthBundle\Data as AuthData;

class DashboardControllerTest extends AllmegaWebTest
{
    /**
     * @throws ClassNotFoundException
     */
    public function testShow(): void
    {
        $this->runTests($this->show, AuthData::USER_ROLE);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testMenuIcon(): void
    {
        $this->runTests($this->icon, AuthData::USER_ROLE, false, false);
    }

    protected function getRouteName(string $name): string 
    {
        return DashboardController::ROUTE_NAME . $name;
    }
}