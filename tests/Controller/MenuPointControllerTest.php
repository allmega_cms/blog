<?php

/**
 * This file is part of the Allmega Blog Bundle package.
 *
 * @copyright Allmega 
 * @package   Blog Bundle
 * @author    Eduard Jung <eddie@allmega.de>
 * @license   https://opensource.org/licenses/gpl-license.php GNU Public License
 */

namespace Allmega\BlogBundle\Tests\Controller;

use Symfony\Component\VarExporter\Exception\ClassNotFoundException;
use Allmega\BlogBundle\Repository\MenuPointTypeRepository;
use Allmega\BlogBundle\Controller\MenuPointController;
use Allmega\BlogBundle\Model\AllmegaWebTest;
use Allmega\BlogBundle\Entity\MenuPoint;
use Allmega\BlogBundle\Data;

class MenuPointControllerTest extends AllmegaWebTest
{
    /**
     * @throws ClassNotFoundException
     */
    public function testIndex(): void
    {
        $this->runTests($this->index, Data::POST_AUTHOR_ROLE);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testShow(): void
    {
        $this->runTests(
            routeName: $this->show,
            role: Data::POST_AUTHOR_ROLE,
            create: true,
            testRedirect: false,
            fnRouteParams: 'getSlugAsRouteParams');
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testBuildMenu(): void
    {
        $this->runTests('build', Data::POST_AUTHOR_ROLE, false, false);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testAdd(): void
    {
        $this->runTests($this->add, Data::POST_AUTHOR_ROLE);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testEdit(): void
    {
        $this->runTests($this->edit, Data::POST_AUTHOR_ROLE, true);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testChangeState(): void
    {
        $this->runModifyTests(role: Data::POST_AUTHOR_ROLE, redirectName: $this->index);
    }

    /**
     * @throws ClassNotFoundException
     */
    public function testDelete(): void
    {
        $this->runModifyTests(role: Data::POST_AUTHOR_ROLE, delete: true);
    }

    /**
     * @throws ClassNotFoundException
     */
    protected function create(): void
    {
        $type = $this
            ->getService(MenuPointTypeRepository::class)
            ->findOneBy(['shortname' => Data::MENUPOINT_TYPE_ROUTE]);

        $entity = MenuPoint::build(type: $type);
        $this->em->persist($entity);
        $this->em->flush();
        $this->params->setEntity($entity);
    }

    protected function getRouteName(string $name): string 
    {
        return MenuPointController::ROUTE_NAME . $name;
    }
}